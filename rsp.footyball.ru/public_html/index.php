<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Добро пожаловать в мир Footyball</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="assets/css/style.css">
    <meta name="theme-color" content="#00aeef">
    <meta name="msapplication-navbutton-color" content="#00aeef">
    <meta name="apple-mobile-web-app-status-bar-style" content="#00aeef">

    <link rel="apple-touch-icon" sizes="180x180" href="assets/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/ico/favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="assets/ico/safari-pinned-tab.svg" color="#5bbad5">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script>window.html5 || document.write('<script src="assets/js/html5shiv.js"><\/script>')</script>
    <![endif]-->

    <style>
        .bodyAddClass  {
            position: fixed !important;
            width: 100% !important;
        }
         .bodyAddClass .modal.open {
             position: absolute !important;
             top: .2% !important;
             overflow: scroll !important;
        }
    </style>
</head>
<body>

<div id="header" class="headerblock">
    <div class="row">
        <div class="col m2 s12 headerblock__rightblock">
	        <a href="tel:88007078899" class="headerblock__phone">
		        8-800-707-88-99
	        </a>
        </div>
        <div class="col s6 m8 headerblock__leftblock">
            <a class="headerblock__logoimg" href="http://footyball.ru/">&nbsp;</a>
        </div>
        <div class="col s6 m2 headerblock__rightblock">
            <a class="headerblock__rightblocklink modal-trigger" href="#modal1">
                записаться бесплатно
            </a>
        </div>
    </div>
</div>
<div id="conten" class="contentblock">
    <div class="row contentblock_line1">
        <div class="col s12 contentblock_line1__text">Добро пожаловать в мир Footyball</div>
    </div>
    <div class="row contentblock_line2">
        <div class="col s12 contentblock_line2__img">
            <a href="http://footyball.ru/" class="contentblock_line2__logo"></a>
        </div>
    </div>
    <div class="row contentblock_line3">
        <div class="col s12 contentblock_line3__text">
            Мир детcких улыбок и счастливых моментов!
        </div>
    </div>
    <div class="contentblock_line7">
        <div class="row">
            <div class="col s12 contentblock_line7__balckbg">
                <div class="container contentblock_line7__videocont">
                    <video id="footyballrspvideo" class="contentblock_line7__video center-block" poster="assets/img/Player-video.jpg" preload="none" tabindex="0" style="width: 100%;height: 100%;">
                        <source src="video/Insta-59sec-v3.mp4" type="video/mp4" />
                        <source src="video/Insta-59sec-v3.webm" type="video/webm" />
                        <source src="video/Insta-59sec-v3.ogg" type="video/ogg" />
                    </video>
                </div>
            </div>
        </div>
    </div>
    <div class="container contentblock_line4 fade-into-view">
        <div class="contentblock_line4__bgimg">
            <img class="contentblock_line4bannerimg" src="assets/img/banner.jpg" style="" alt="" >
            <div class="contentblock_line4__round wow flipInX" >
                <span class="contentblock_line4__roundtxt1">мы заинтересовали</span>
                <span id="counter" class="contentblock_line4__roundtxt2">0</span>
                <span class="contentblock_line4__roundtxt3">детей</span>
            </div>
        </div>
    </div>
    <div class="contentblock_line5">
        <div class="row">
            <div class="col s10 offset-s1 m12 contentblock_line5__nameblock">
                <h3>почему?</h3>
            </div>
        </div>
    </div>
    <div class="container contentblock_line6">
        <div class="row">
            <div class="col s12 m4 wow bounceInUp" style="min-height: 200px">
                <div class="row contentblock_line6whyblock">
                    <div class="col s3 contentblock_line6whyblock__number contentblock_line6whyblock__none"></div>
                    <div class="col s9 contentblock_line6whyblocktext">
                        <h4 class="contentblock_line6whyblocktext__h4">Мы не простая футбольная школа</h4>
                        <p class="contentblock_line6whyblocktext__cont">Очень много внимания мы уделяем личностному росту детей, сотрудников  и родителей воспитанников footyball   </p>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 wow bounceInUp" style="min-height: 200px">
                <div class="row contentblock_line6whyblock">
                    <div class="col s3 contentblock_line6whyblock__number contentblock_line6whyblock__nsec"></div>
                    <div class="col s9 contentblock_line6whyblocktext">
                        <h4 class="contentblock_line6whyblocktext__h4">После тренировок дети не хотят идти домой</h4>
                        <p class="contentblock_line6whyblocktext__cont">Внимание к деталям во всем - от организации помещений до спортивного инвентаря. Мы стараемся, чтобы посещения школ были приятными и комфортными для вас</p>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 wow bounceInUp" style="min-height: 200px">
                <div class="row contentblock_line6whyblock">
                    <div class="col s3 contentblock_line6whyblock__number contentblock_line6whyblock__nthir"></div>
                    <div class="col s9 contentblock_line6whyblocktext">
                        <h4 class="contentblock_line6whyblocktext__h4">Профессионализм в каждой детали</h4>
                        <p class="contentblock_line6whyblocktext__cont">Начиная с оформления помещений и заканчивая спортивным инвентарем - мы старемся сделать посещения наших школ приятным.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="contentblock_line6">
        <div class="row">
            <div class="col s12 contentblock_line6name">
                одно видео, больше тысячи слов
            </div>
        </div>
    </div>-->

    <div class="contentblock_line8">
        <div class="row">
            <div class="col s12 contentblock_line8name">
                Первой ступенью в мире Footyball<br>
                является программа<br>
                РАЗБУДИ ПОБЕДИТЕЛЯ
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="contentblock_line8rsplogo wow jackInTheBox"></div>
            </div>
        </div>
    </div>
    <div class="contentblock_line9">
        <div class="row">
            <div class="col s10 offset-s1 m12 contentblock_line9name">
                <h3> Что это?</h3>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m4 contentblock_line9txtblock wow zoomIn" style="margin-bottom: 40px">
                    <div class="contentblock_line9txtblockline1">Во-первых:</div>
                    <div class="contentblock_line9txtblockline2 contentblock_line9txtblockline2__str1">
                        Это шанс все увидеть собственными глазами,  принять участие в бесплатной тренировке которая длится:
                    </div>
                    <div class="contentblock_line9txtblockline3 valign-wrapper">
                        <span class="contentblock_line9txtblockline3time">45</span>
                        <span class="contentblock_line9txtblockline3min ">минут</span>
                    </div>
                    <div class="contentblock_line9txtblockline4l">
                        <a href="#modal1" class="contentblock_line9txtblockline4butt modal-trigger">узнать больше</a>
                    </div>
                </div>
                <div class="col s12 m4 contentblock_line9txtblock wow zoomIn" style="margin-bottom: 40px">
                    <div class="contentblock_line9txtblockline1">Во-вторых:</div>
                    <div class="contentblock_line9txtblockline2 contentblock_line9txtblockline2__str2">
                        Возможность примерить настоящую футбольную форму особенного дизайна.
                    </div>
                    <div class="contentblock_line9txtblockline3cloth "></div>
                    <div class="contentblock_line9txtblockline4">
                        <a href="" id="showcoll" class="contentblock_line9txtblockline4collectio">посмотреть коллекции</a>
                    </div>
                </div>
                <div class="col s12 m4 contentblock_line9txtblock wow zoomIn">
                    <div class="contentblock_line9txtblockline1">В-третьих:</div>
                    <div class="contentblock_line9txtblockline2 contentblock_line9txtblockline2__str3">
                        Это очень весело. Достаточно посмотреть на радостные лица детей и их родителей.
                    </div>
                    <div class="contentblock_line9txtblockline3photogall">
                        <section class="slider">
                            <img width="100%" height="100%" src="photo/photo1.jpg" class="left"/>
                            <img width="100%" height="100%" src="photo/photo2.jpg" class="center"/>
                            <img width="100%" height="100%" src="photo/photo3.jpg" class="right"/>
                        </section>
                    </div>
                    <div class="contentblock_line9txtblockline4r">
                        <a href="" class="contentblock_line9txtblockline4butt contentblock_line9txtblockline4butt__str3" id="showallphoto">посмотреть фото</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contentblock_line10">
        <div class="contentblock_line10line">&nbsp;</div>
        <div class="contentblock_line10buttonsubmitout">
            <a href="#modal1" class="modal-trigger btn btn-large contentblock_line10buttonsubmit pulse">Записаться</a>
        </div>
    </div>

    <div class="contentblock_line11">
        <div class="row">
            <div class="col s12 contentblock_line8name contentblock_line11name">
                <h3>А на десерт.... десерт</h3>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col s12 wow zoomIn">
                    <img src="assets/img/cake.jpg" style="width: 100%" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col s12 contentblock_line11text">В каждой из наших школ есть уютное кафе, с полноценным меню здорового питания. Дети могут пополнить запасы энергии после удачной тренировки.</div>
            </div>
        </div>
        
    </div>

    <div class="contentblock_line12">
        <div class="container">
            <div class="row">
                <div class="col s12 contentblock_line11text contentblock_line12text">А если ваш ребёнок хочет дольше поиграть с новыми друзьями, то он может остаться с ними в одной из наших игровых комнат бесплатно.</div>
            </div>
            <div class="row">
                <div class="col s12 wow zoomIn">
                    <img src="assets/img/ch-room.jpg" style="width: 100%" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="contentblock_line13">
        <div class="row">
            <div class="col s12 contentblock_line8name contentblock_line13name">
                <h3>У вас остались еще вопросы?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col s12 contentblock_line13text">звоните нам, а лучше приходите и задавайте их лично. Запишитесь на бесплатную тренировку через форму ниже.</div>
        </div>
    </div>

    <div class="contentblock_line10">
        <div class="contentblock_line10line">&nbsp;</div>
        <div class="contentblock_line10buttonsubmitout">
            <a href="#modal1" class="modal-trigger btn btn-large contentblock_line10buttonsubmit pulse">Записаться</a>
        </div>
    </div>


	<!-- Block9 -->
	<section class="b9" id="adr">
		<div class="adress">
			<div class="top">
				<h2>Адреса клубов</h2>
				<p class="num">Позвоните нам: <b>8-800-707-88-99</b></p>
			</div>
			<div class="slider">
				<div class="item">
					<div class="box">
						<div class="oct">1</div>
						<p class="title">Футбольный клуб "Парк Победы"</p>
						<p class="adr"> Кутузовский проспект, 36, стр 51, г. Москва </p>
						<p class="graph"> Пн-Пт 8-00 - 22-00</p>
					</div>
				</div>
				<div class="item">
					<div class="box">
						<div class="oct">2</div>
						<p class="title">Футбольный клуб "Перово"</p>
						<p class="adr">1й проезд Перова поля, д. 9 стр. 5, г. Москва</p>
						<p class="graph"> Пн-Пт 8-00 - 22-00</p>
					</div>
				</div>
				<div class="item">
					<div class="box">
						<div class="oct">3</div>
						<p class="title">Футбольный клуб "Беларусский"</p>
						<p class="adr">
							ул. Правды, 24, стр.8, г. Москва
						</p>
						<p>Заезд с ул. 5-ого Ямского поля. Через КПП №5". Мы находимся возле "Бодрый день" и кафе "Рецептор"</p>
						<p class="graph"> Пн-Пт 8-00 - 22-00</p>
					</div>
				</div>
				<div class="item">
					<div class="box">
						<div class="oct">4</div>
						<p class="title">Футбольный клуб "Ленинский"</p>
						<p class="adr">5-й Донской проезд 15, стр. 5, 11 подъезд, г. Москва</p>
						<p class="graph">Пн-Вс 8:00-22:00</p>
					</div>
				</div>
			</div>
		</div>
		<div id="map"></div>
	</section>
	<!-- //Block9 -->


	<div class="container-fluid nopadding" id="mainblockfooter">
		<footer>
			<div class="wrapper">
				<div class="logo">
					<img src="http://footyball.ru/assets-landing/img/logo.svg" alt="">
					<p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
				</div>
				<div class="polit">
					<p>
						ОГРН: 1147746697119 <br>
						<a href="#polit" class="fancy">Политика конфиденциальности</a>
					</p>
					<div class="footsocblock">
						<a target="_blank" id="footsoclinkfb" class="footsoclink fa" href="//www.facebook.com/footyballru">
						</a>
						<a target="_blank" id="footsoclinkin" class="footsoclink fa" href="//instagram.com/footyball_russia"></a>
						<a target="_blank" id="footsoclinkyt" class="footsoclink fa" href="//www.youtube.com/channel/UCL8tDsCfvfDWxG8WEiDDrDA"></a>
						<a target="_blank" id="footsoclinkvk" class="footsoclink fa" href="//vk.com/footyballru"></a>
					</div>
				</div>
				<div class="cont">
					<p class="cont__text">Позвоните нам</p>
					<p class="cont__num">8-800-707-88-99</p>
				</div>
			</div>
		</footer>
	</div>


    <!-- Modal Structure -->

    <div id="modal1" class="modal">

        <div class="modal-content">

            <div class="row">
                <div class="subscrform">
                    <div class="row">
                        <div class="col s12">
                            <h2> ЗАПИШИТЕСЬ НА БЕСПЛАТНУЮ ПРОБНУЮ ТРЕНИРОВКУ</h2>
                        </div>
                    </div>
                    <div class="row">
                        <form id="formfreetraining" class="col s12" onsubmit="return validateForm()" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
                            <div class="row">
                                <div class="col input-field s12">
                                    <input type="hidden" name="oid" value="00D20000000onaw" />
                                    <input type="hidden" name="retURL" value="http://footyball.ru/36-thankyou.php" />
                                    <input type="hidden" name="lead_source" value="Web form" />
                                    <input type="hidden" name="last_name" value="Не известно" />
                                    <input type="hidden" id="email" name="email"  value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="first_name"  id="first_name" size="20" maxlength="100" class="namechildinp validate" required >
                                    <label for="first_name">Имя и фамилия Ребенка</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="00N200000098YTY" id="00N200000098YTY" size="20" maxlength="100" class="namechildinp validate" required >
                                    <label for="00N200000098YTY">Имя и Фамилия Родителя</label>
                                </div>
                            </div>
                            <div class="row impyers">
                                <div class="col s12 m2">
                                    <div class="implabel">Возраст</div>
                                </div>
                                <div class="col s12 m10 ">
                                    <div>
                                        <input class="agechildinp" placeholder="Возраст" name="00N200000098YTQ" id="00N200000098YTQ" value="3">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col input-field s12">
                                    <input type="tel" class="phonenubminp validate" autocomplete="off" name="phone" size="17" maxlength="18" id="freetrain_phone" required >
                                    <label for="freetrain_phone">Телефон</label>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col s12">
                                    <input type="checkbox" id="00N0J000009xwo5" name="00N0J000009xwo5" value="1" />
                                    <label class="checkboxrights" for="00N0J000009xwo5">Я уведомлен и выражаю своё согласие на обработку моих персональных данных администрацией Сайта в целях оказания мне услуг Компании.<br>Я уведомлен и выражаю своё согласие на получение смс-уведомлений от администрации Сайта в целях оказания мне услуг Компании.</label>

	                                  <input type="hidden" id="00N200000098lUX" maxlength="255" name="00N200000098lUX" size="20" value="<?=@$_GET['utm_campaign'];?>"/>
                                    <input type="hidden" id="00N200000098lUc" maxlength="255" name="00N200000098lUc" size="20" value="<?=@$_GET['utm_content'];?>"/>
                                    <input type="hidden" id="00N200000098lUh" maxlength="255" name="00N200000098lUh" size="20" value="<?=@$_GET['utm_medium'];?>"/>
                                    <input type="hidden" id="00N200000098lUm" maxlength="255" name="00N200000098lUm" size="20" value="<?=@$_GET['utm_source'];?>"/>
                                    <input type="hidden" id="00N200000098lUr" maxlength="255" name="00N200000098lUr" size="20" value="<?=@$_GET['utm_term'];?>"/>
                                    <input type="hidden" id="URL" maxlength="80" name="URL" size="20" value="http://<?=@$_SERVER["SERVER_NAME"];?>/" />
                                </div>
                                <!--<div class="input-field col s10 subscrformaatext">

                                </div>-->
                            </div>



                            <input type="submit" class="buttonsend btn" id="sendWebToLeadbutton" value="отправить">
                            <i class="small modal-close material-icons closebutton">clear</i>
                        </form>
                    </div>


                </div>
            </div>


        </div>

    </div>


	<!-- Popups -->
	<div class="hide">

		<div id="thanks">
			<h2>Ваше сообщение успешно отправлено.<br>Наш менеджер свяжется с вами в ближайшее время.
			</h2>
		</div>

		<div id="polit">
			<h2>Политика конфиденциальности</h2>
			<div class="text">
				<p>Соблюдение Вашей конфиденциальности важно для нас. По этой причине, мы разработали Политику Конфиденциальности, которая описывает, как мы используем и храним Вашу информацию. Пожалуйста, ознакомьтесь с нашими правилами соблюдения конфиденциальности и сообщите нам, если у вас возникнут какие-либо вопросы.</p>
				<h6>Сбор и использование персональной информации</h6>
				<p>Под персональной информацией понимаются данные, которые могут быть использованы для идентификации определенного лица либо связи с ним.</p>
				<p>От вас может быть запрошено предоставление вашей персональной информации в любой момент, когда вы связываетесь с нами.</p>
				<p>Ниже приведены некоторые примеры типов персональной информации, которую мы можем собирать, и как мы можем использовать такую информацию.</p>
				<p>Какую персональную информацию мы собираем:</p>
				<ul>
					<li>Когда вы оставляете заявку на сайте, мы можем собирать различную информацию, включая ваши имя, номер телефона, адрес электронной почты и т.д.</li>
				</ul>
				<p>Как мы используем вашу персональную информацию:</p>
				<ul>
					<li>Собираемая нами персональная информация позволяет нам связываться с вами и сообщать об уникальных предложениях, акциях и других мероприятиях и ближайших событиях.</li>
					<li>Время от времени, мы можем использовать вашу персональную информацию для отправки важных уведомлений и сообщений.</li>
					<li>Мы также можем использовать персональную информацию для внутренних целей, таких как проведения аудита, анализа данных и различных исследований в целях улучшения услуг предоставляемых нами и предоставления Вам рекомендаций относительно наших услуг.</li>
					<li>Если вы принимаете участие в розыгрыше призов, конкурсе или сходном стимулирующем мероприятии, мы можем использовать предоставляемую вами информацию для управления такими программами.</li>
				</ul>
				<h6>Раскрытие информации третьим лицам</h6>
				<p>Мы не раскрываем полученную от Вас информацию третьим лицам.</p>
				<p>Исключения:</p>
				<ul>
					<li>В случае если необходимо — в соответствии с законом, судебным порядком, в судебном разбирательстве, и/или на основании публичных запросов или запросов от государственных органов на территории РФ — раскрыть вашу персональную информацию. Мы также можем раскрывать информацию о вас если мы определим, что такое раскрытие необходимо или уместно в целях безопасности, поддержания правопорядка, или иных общественно важных случаях.</li>
					<li>В случае реорганизации, слияния или продажи мы можем передать собираемую нами персональную информацию соответствующему третьему лицу – правопреемнику.</li>
				</ul>
				<h6>Защита персональной информации</h6>
				<p>Мы предпринимаем меры предосторожности — включая административные, технические и физические — для защиты вашей персональной информации от утраты, кражи, и недобросовестного использования, а также от несанкционированного доступа, раскрытия, изменения и уничтожения.</p>
				<h6>Соблюдение вашей конфиденциальности на уровне компании</h6>
				<p>Для того чтобы убедиться, что ваша персональная информация находится в безопасности, мы доводим нормы соблюдения конфиденциальности и безопасности до наших сотрудников, и строго следим за исполнением мер соблюдения конфиденциальности.</p>
			</div>
		</div>

	</div>
	<!-- //Popups -->







</div>

<!--
<div class="input-field col s12">
    <input placeholder="Placeholder" id="first_name00" type="text" class="validate">
    <label for="first_name">First Name</label>
</div>






    <div class="capchaout" style="width: 304px; margin: 20px auto;">
    </div>
    <div>

    </div>
    <input type="submit" class="buttonsend" id="sendWebToLeadbutton" value="отправить" style="margin-bottom: 20px">
</form>

-->












<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script src="assets/js/materialize.min.js"></script>
<script src="assets/js/ion.rangeSlider.min.js"></script>
<script src="assets/js/mediaelement-and-player.min.js"></script>
<script src="assets/js/fancybox.min.js"></script>
<script src="assets/js/counter.min.js"></script>
<script src="assets/js/wow-anim.min.js"></script>
<script src="assets/js/mwslider.min.js"></script>
<script defer="defer" src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>


<script>
     function validateForm(){
         if($("#first_name").val().length<3||$("#first_name").val().length>100){
             //console.log("first_name")
             $("#first_name").addClass("invalid").focus();
             return false
         }
         if($("#00N200000098YTY").val().length<3||$("#00N200000098YTY").val().length>100){
             //console.log("00N200000098YTY")
             $("#00N200000098YTY").addClass("invalid").focus();
             return false
         }
         var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
         if (!filter.test($("#freetrain_phone").val())) {
             $("#freetrain_phone").addClass("invalid").focus();
             return false
         }
         var termstatus = $('#00N0J000009xwo5').is(':checked');
         if(termstatus==false){
             $(".checkboxrights").css("color","red");
             return false;
         }
     }




     function initMap(adress) {

	     $("#map").html('');

	     var myGeocoder = ymaps.geocode(adress);
	     myGeocoder.then(
		     function(res) {
			     var pos = res.geoObjects.get(0).geometry.getCoordinates();
			     var myMap = new ymaps.Map('map', {
				     center: pos,
				     zoom: 17
			     });
			     var bpos = {
				     lat: pos[0],
				     lng: pos[1]
			     };
			     var ppos = {
				     lat: pos[0],
				     lng: pos[1] -= 0.003
			     };

			     var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
				     balloonContentHeader: '<p class="adr">' + adress + '</p>'
			     },{
				     iconLayout: 'default#image',
				     iconImageHref: 'http://free.footyball.ru/build/img/marker.png',
				     iconImageSize: [28, 33]
			     });
			     setPlacemark();


			     myMap.geoObjects.add(placemark);

			     myMap.controls
				     .add('zoomControl')
				     .add('typeSelector')
				     .add('mapTools');

			     myMap.events.add('click', function(e) {
				     myMap.balloon.close();
			     });

			     window.onresize = function() {
				     setPlacemark();
			     };

			     function setPlacemark() {
				     if (window.innerWidth < 740) {
					     myMap.setCenter([bpos.lat,bpos.lng]);
				     } else {
					     myMap.setCenter([ppos.lat,ppos.lng]);
				     }
			     }

		     }
	     );

     }



    $(document).ready(function() {




        //iOS 11 bug with inputs
        $(".modal-trigger").on('click', function () {
            var ua = navigator.userAgent,
                iOS = /iPad|iPhone|iPod/.test(ua),
                iOS11 = /OS 11_0_0|OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);
            if ( iOS && iOS11 ) {
                $('body').addClass('bodyAddClass');
            }

        });
        $(".closebutton").on('click', function () {
            var ua = navigator.userAgent,
                iOS = /iPad|iPhone|iPod/.test(ua),
                iOS11 = /OS 11_0_0|OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);
            if ( iOS && iOS11 ) {
                $('body').removeClass('bodyAddClass');
            }
        });






        Materialize.updateTextFields();
        $("#00N200000098YTQ").ionRangeSlider({
            min: 2,
            max: 7,
            from: 3,
            grid: true,
            values: [2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7],
            max_postfix: " лет"
        });

        $('#footyballrspvideo').mediaelementplayer({
            alwaysShowControls: true ,
            features: ['playpause','progress','volume','fullscreen'],
            //videoVolume: 'horizontal',
            startVolume: 0.7,
            lang:"ru",
            enableKeyboard: false
        });


        $('.modal').modal({
            startingTop: '4%',
            endingTop: '2%'
        });

        new WOW().init();


        $(".slider").MWslider();
       $("#showallphoto").on("click",function () {
           $.fancybox.open([
               {
                   src  : 'photo/photo1-big.jpg'
               },
               {
                   src  : 'photo/photo2-big.jpg'
               },
               {
                   src  : 'photo/photo3-big.jpg'
               }
           ], {
               loop : true,
               keyboard : false,
               buttons : [
                   'thumbs',
                   'close'
               ]
           });
           return false;
       });
       $("#showcoll").on("click",function () {
           $.fancybox.open([
               {src  : 'photo/form-1.jpg'},
               {src  : 'photo/form-2.jpg'},
               {src  : 'photo/form-3.jpg'},
               {src  : 'photo/form-4.jpg'},
               {src  : 'photo/form-5.jpg'},
               {src  : 'photo/form-6.jpg'}
           ], {
               loop : true,
               keyboard : false,
               buttons : [
                   'close'
               ]
           });
           return false;
       });


        var counterUp = $("#counter");
        counterUp.counter({
            //autoStart: true,
            duration: 7000,
            countTo: 74567,
            placeholder: 0,
            //easing: "easeOutCubic",
            onStart: function() {
                //document.getElementById("trigger").innerHTML = "Running.."
            },
            onComplete: function() {
                //document.getElementById("trigger").innerHTML = "Restart counting!"
            }
        });

        //.contentblock_line9txtblockline2__str3
        var timerId = setInterval(function() {
            //console.log( "тик" );
            $(".contentblock_line9txtblockline2__str3").css("back")
        }, 2000);
        var images = Array(
        	"assets/img/form-1.jpg",
	        "assets/img/form-2.jpg",
	        "assets/img/form-3.jpg",
	        "assets/img/form-4.jpg",
	        "assets/img/form-5.jpg",
	        "assets/img/form-6.jpg"
        );
        var currimg = 0;
        function loadimg(){
            $(".contentblock_line9txtblockline3cloth").animate({ opacity: 1 }, 500,function(){
                //finished animating, minifade out and fade new back in
                $(".contentblock_line9txtblockline3cloth").animate({ opacity: 0.2 }, 500,function(){
                    currimg++;
                    if(currimg > images.length-1){
                        currimg=0;
                    }
                    var newimage = images[currimg];
                    //swap out bg src
                    $(".contentblock_line9txtblockline3cloth").css("background-image", "url("+newimage+")");
                    //animate fully back in
                    $(".contentblock_line9txtblockline3cloth").animate({ opacity: 1 }, 1000,function(){
                        //set timer for next
                        setTimeout(loadimg,3000);
                    });
                });
            });
        }
        setTimeout(loadimg,3000);



        //check form
        $("#formfreetraining").submit(function(){
            //check terms


        });

	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });
	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
			    //console.log( $(this).find('.adr').html());
		    });
		    //initMap(  $('.b9 .item:first-of-type').find('.adr').html()  );
		    $('.b9 .item')[0].click();
		    //console.log(   $('.b9 .item:first-of-type').find('.adr').html()   );
	    };







    })
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47020212 = new Ya.Metrika({
                    id:47020212,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47020212" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
