<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Footyball - Работа</title>
	<meta name="keywords"  content=""/>
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<meta name="theme-color" content="#000">
	<meta name="msapplication-navbutton-color" content="#000">
	<meta name="apple-mobile-web-app-status-bar-style" content="#000">
	<style>
		body{
			-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
			filter: alpha(opacity=0);
			-moz-opacity: 0;
			-khtml-opacity: 0;
			opacity: 0;
		}
		hr {
			margin-top: 1rem;
			margin-bottom: 1rem;
			border: 0;
			border-top: 1px solid rgba(0, 0, 0, 0.1);
			width:100%;
			height:2px;
		}
		.listnumber{
			color:#00aeef;
			display:block;
			float:left;
			padding:0 20px 0 0;
			font-size:34px;
		}
		.inpmartop5{
			position:relative;
			top:10px
		}
	</style>
	<link href="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.css" type="text/css" rel="stylesheet"/>

	<link rel="stylesheet" href="build/css/style.min.css">
</head>
<body>

<header id="header" class="headerblock">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<a href="#" class="headerblock__logo"></a>
			</div>
			<div class="col-sm-4">
				<h1 class="headerblock__slogan">
					Сеть футбольных клубов для дошкольников №1
				</h1>
			</div>
		</div>
	</div>
</header>
<div class="content">
	<div class="content__upline"></div>
	<div class="container" style="padding-bottom:20px">

		<div class="row">
			<div class="col-sm-12">

				<div class="content__form">
					<h1 class="content__formname">
						Заполни анкету и присоединяйся к нашей команде!
					</h1>
					<form class="content__formsubmit form-horizontal" id="content__formsubmit">



							<div class="row" style="margin:40px 0">
								<div class="col-lg-12 ">

									<div class="form-group row">
										<label for="question1" class="col-lg-6 col-form-label">
											<span class="listnumber">1</span>
											<span class="h-100" style="display: flex; align-items: center;">
												Вы продавали что - нибудь? Если да, приведите примеры.
											</span>
										</label>
										<div class="col-lg-6 inpmartop5">
											<input type="text" class="form-control-plaintext" name="question1" id="question1" required="required">
										</div>
										<hr/>
									</div>
									<div class="form-group row">
										<label for="question3" class="col-lg-6 col-form-label">
											<span class="listnumber">2</span>
											<span class="h-100" style="display: flex; align-items: center;">
												Что такое ответственность? Приведите пример, когда вы брали на себя ответственность?
											</span>
										</label>
										<div class="col-lg-6 inpmartop5">
											<input type="text" class="form-control-plaintext" name="question2" id="question2" required="required">
										</div>
										<hr/>
									</div>
									<div class="form-group row">
										<label for="question3" class="col-lg-6 col-form-label">
											<span class="listnumber">3</span>
											<span class="h-100" style="display: flex; align-items: center;">
												Какие пять основных ценностей в жизни вы можете назвать?
											</span>
										</label>
										<div class="col-lg-6 inpmartop5">
											<input type="text" class="form-control-plaintext" name="question3" id="question3" required="required">
										</div>
										<hr/>
									</div>

									<div class="form-group row">
										<label for="question4" class="col-lg-6 col-form-label">
											<span class="listnumber">4</span>
											<span class="h-100" style="display: flex; align-items: center;">
												Сколько вы хотите зарабатывать в месяц?
											</span>
										</label>
										<div class="col-lg-6 inpmartop5">
											<input type="text" class="form-control-plaintext" name="question4" id="question4" required="required">
										</div>
										<hr/>
									</div>
									<div class="form-group row">
										<label for="question5" class="col-lg-6 col-form-label">
											<span class="listnumber">5</span>
											<span class="h-100" style="display: flex; align-items: center;">
												Почему нам не стоит вас нанимать?
											</span>
										</label>
										<div class="col-lg-6 inpmartop5">
											<input type="text" class="form-control-plaintext" name="question5" id="question5" required="required">
										</div>
										<hr/>
									</div>




									<div class="row">
										<div class="col-sm-6">
											<button  class="btn btn-danger content__formbutsubm" onclick="
											history.back();
											let answers = {}
											for(let i=1;i<6;i++){
												let q = `question${i}`;
												let a = $(`#question${i}`).val();
												if (a.length<1) a = '(none)'
												answers[`${q}`] = a;
											}
											localStorage.setItem('answers', JSON.stringify(answers));
											return false;
											">Назад</button>
										</div>
										<div class="col-sm-6">
											<button type="submit" class="btn btn-primary content__formbutsubm" id="formbutsubm">Отправить</button>
										</div>

									</div>

							</div>

						</div>



					</form>
				</div>
			</div>

		</div>

		
	</div>

	<!--<footer>
		<div class="row"
				style="
						background:#fff;
						border-top: 3px solid #00aeef;
						-webkit-box-shadow: 2px 2px 4px #333;
						-moz-box-shadow: 2px 2px 4px #333;
						box-shadow: 2px 2px 4px #333;
				">
			<div class="col-sm-4" >
				<div class="content__addrdeskr" style=" padding:3%;">
					<span style="font-weight:900; font-size:18px">Футбольный клуб FootyBall по адресу: <br> м. Савёловская - 5-я ул. Ямского Поля д. 9.</span> <br><br>
					Выход из метро один. Из стеклянных дверей - направо, выход в город - налево. Двигайтесь в сторону эстакады вдоль ж\д путей.<br><br>
					Сворачиваете налево на ул. Бумажный проезд. Далее 350  м. прямо. Поворот направо на 5-я ул. Ямского Поля.<br><br>
					С правой стороны будет вход на территорию (КПП №5, проходной пункт со шлагбаумом).<br><br>
					На 5-я ул. Ямского Поля Footyball находится с левой стороны от КПП №5 (ориентир: Кафе “Бодрый День” кофе с собой, Бизнес Центр Solutions).<br>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="content__addrmap">
					<iframe src="https://yandex.ru/map-widget/v1/-/CBeu4XCG1C" frameborder="0" style="border:0;width:100%;height:auto; min-height:450px" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</footer>-->

	</div>

<a href="#" id="mailok" class="mailformpopup" data-featherlight="<p>Отправлено! Ваша заявка отправлена. Менеджер с вами свяжется.<p>"></a>
<a href="#" id="mailerror" class="mailformpopup" data-featherlight="<p>Ошибка! Заполните все поля и нажмите кнопку Отправить</p>"></a>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script defer="defer" src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js"></script>

<script src="build/js/main.min.js"></script>


<script>


function fillInputs() {
	let answers = JSON.parse(localStorage.getItem('answers'));
	for (let i=1;i<6;i++) {
		//let qi = ;
		let a = answers[`question${i}`];
		if(a!==undefined && a !== "(none)"){
			//$('#question14').attr("value",answers.question14)
			$(`#question${i}`).attr("value",a);
			console.log(a);
		}else{
			$(`#question${i}`).attr("value","");
		}

	}

}


function submitForm() {

	console.log("submitForm");

	let answers = {}
	for(let i=1;i<6;i++){
		let q = `question${i}`;
		let a = $(`#question${i}`).val();
		if (a.length<1) a = "(none)"
		answers[`${q}`] = a;
	}
	console.log(answers);

	const userData  = JSON.parse(localStorage.getItem('userData'));
	const utmData  = JSON.parse(localStorage.getItem('utmData'));


	//babel
	const _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	const allData = _extends({}, userData, utmData, answers);
	localStorage.setItem('answers', JSON.stringify(answers));


	//const allData = {...userData,...utmData,...answers};
	console.log(allData);
	//return false;

	$.ajax({
		type: "POST",
		url: "sendmail.php",
		data: allData,
		success: function (data) {
			console.log(data)
			//alert("Ваше сообщение отправлено!");
			if (data == "Ok") {
				//alert("Ваше сообщение отправлено!")
				$('#mailok').click()
				localStorage.removeItem('userData');
				localStorage.removeItem('utmData');
				localStorage.removeItem('answers');
				for (let i=1;i<6;i++) {
					$(`#question${i}`).attr("value","");
				}
			} else {
				//alert("Ошикба отпрвления сообщения!")
				$('#mailerror').click()
			}
		}
	});
}
	$(document).ready(function() {
		//$('#phone').mask('+7(999) 999 99 99');

		fillInputs();

		$('#content__formsubmit').on('submit', function (e) {
			e.preventDefault();
			submitForm()
		});
		$('#formbutsubm').on('click', function (e) {
			e.preventDefault();
			submitForm()
		})
		/*WebFont.load({
			google: {
				families: ['Open Sans']
			}
		});*/
		//addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");



	});
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter47330712 = new Ya.Metrika({
					id:47330712,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47330712" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!--[if lt IE 9]>
<p class="browserupgrade">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</span>
<![endif]-->
</body>
</html>
