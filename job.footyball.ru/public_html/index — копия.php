<?

if( isset($_GET['utm_campaign'])&&!empty($_GET['utm_campaign'])){
	$utm_campaign = $_GET['utm_campaign'];
}else{
	$utm_campaign='(none)';
}
if( isset($_GET['utm_content'])&&!empty($_GET['utm_content'])){
	$utm_content = $_GET['utm_content'];
}else{
	$utm_content='(none)';
}
if( isset($_GET['utm_medium'])&&!empty($_GET['utm_medium'])){
	$utm_medium = $_GET['utm_medium'];
}else{
	$utm_medium='(none)';
}
if( isset($_GET['utm_term'])&&!empty($_GET['utm_term'])){
	$utm_term = $_GET['utm_term'];
}else{
	$utm_term='(none)';
}
if( isset($_GET['utm_source'])&&!empty($_GET['utm_source'])){
	$utm_source = $_GET['utm_source'];
}else{
	$utm_source='(none)';
}

?> <!DOCTYPE html><html lang="ru"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title>1Footyball - Работа</title><meta name="keywords" content=""><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no"><link rel="icon" href="img/favicon.ico" type="image/x-icon"><meta name="theme-color" content="#000"><meta name="msapplication-navbutton-color" content="#000"><meta name="apple-mobile-web-app-status-bar-style" content="#000"><style>body{-moz-opacity:0;-khtml-opacity:0;opacity:0}</style><link href="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.css" type="text/css" rel="stylesheet"><link rel="stylesheet" href="build/css/style.min.css"></head><body><header id="header" class="headerblock"><div class="container"><div class="row"><div class="col-sm-6"><a href="#" class="headerblock__logo"></a></div><div class="col-sm-4"><h1 class="headerblock__slogan">Сеть футбольных клубов для дошкольников №1</h1></div></div></div></header><div class="content"><div class="content__upline"></div><div class="container"><div class="row"><div class="col-sm-6 content__heroblock"><div class="content__hero"><img src="build/img/hero2.png" class="img-fluid" alt="footyman"></div><div class="content__herotext"><p class="content__herotextinner">«Хочешь зарабатывать и не пропускать учебу? Приходи в Footyball! Благодаря гибкому графику можно легко совмещать учебу, работу и даже любимое хобби.» <span>Агент F</span></p></div></div><div class="col-sm-6 content__formblock"><div class="content__form"><h3 class="content__formname">Заполни анкету и присоединяйся<br>к нашей команде!</h3><form class="content__formsubmit" id="content__formsubmit"><div class="row"><div class="col-sm-12"><div class="form-group"><label for="name">Фамилия, Имя</label><input type="text" class="form-control" id="name" placeholder="Агент F" required></div></div></div><div class="row"><div class="col-sm-6"><div class="form-group"><label for="age">Возраст</label><select class="form-control" id="age" required><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option></select></div></div><div class="col-sm-6"><div class="form-group"><label for="citizenship">Гражданство</label><select class="form-control" id="citizenship" name="citizenship" required><option value="Россия">Россия</option><option value="Украина">Украина</option><option value="Казахстан">Казахстан</option><option value="Армения">Армения</option><option value="Азербайджан">Азербайджан</option><option value="Беларусь">Беларусь</option><option value="Киргизия">Киргизия</option></select></div></div></div><div class="row"><div class="col-sm-12"><div class="form-group"><label for="name">Мобильный</label><input type="tel" class="form-control" id="phone" placeholder="+7" required></div></div></div><div class="row"><div class="col-sm-12"><div class="form-group"><label for="name">Ссылка на ваш профиль VK</label><input type="text" class="form-control" id="vkpofile" placeholder="https://vk.com/" required><!--<div class="col-auto">
										<label for="vkpofile">Профиль VK</label>
										<div class="input-group mb-2">
											<div class="input-group-prepend">
												<div class="input-group-text">http://vk.com/</div>
											</div>
											<input type="text" class="form-control" id="vkpofile" placeholder="username" required>
										</div>
									</div>--></div></div></div><div class="row"><div class="col-sm-12"><button type="submit" class="btn btn-primary content__formbutsubm" id="formbutsubm">отправить</button></div></div></form></div></div></div><div class="row"><div class="col-sm-12"><div class="content__maintext"><h3 class="content__maintextname">Мы предлагаем стать представителем детского футбольного клуба!</h3><ul class="content__maintextlist"><li>Возраст 16+</li><li>Город Москва, заработная плата от 30 000 рублей</li><li>Оклад + Бонусы - фиксированная ставка за каждый собранный ликвидный контакт</li><li>Выплаты каждую неделю</li><li>Работа в удобном районе</li><li>Наставник, который обучит всему с нуля</li><li>Карьерный рост в быстрорастущей компании</li></ul><h5 class="content__maintextsubname">Основная задача:</h5><p class="content__maintextsubtext">Приглашение родителей с детьми в возрасте от 2,5 до 6.6 лет на бесплатные детские футбольные праздники, которые мы устраиваем в клубах Footyball</p></div></div></div></div><footer><div class="row" style="background:#fff;border-top:3px solid #00aeef;-webkit-box-shadow:2px 2px 4px #333;-moz-box-shadow:2px 2px 4px #333;box-shadow:2px 2px 4px #333"><div class="col-sm-4"><div class="content__addrdeskr" style="padding:3%"><span style="font-weight:900;font-size:18px">Футбольный клуб FootyBall по адресу:<br>м. Савёловская - 5-я ул. Ямского Поля д. 9.</span><br><br>Выход из метро один. Из стеклянных дверей - направо, выход в город - налево. Двигайтесь в сторону эстакады вдоль ж\д путей.<br><br>Сворачиваете налево на ул. Бумажный проезд. Далее 350 м. прямо. Поворот направо на 5-я ул. Ямского Поля.<br><br>С правой стороны будет вход на территорию (КПП №5, проходной пункт со шлагбаумом).<br><br>На 5-я ул. Ямского Поля Footyball находится с левой стороны от КПП №5 (ориентир: Кафе “Бодрый День” кофе с собой, Бизнес Центр Solutions).<br></div></div><div class="col-sm-8"><div class="content__addrmap"><iframe src="https://yandex.ru/map-widget/v1/-/CBeu4XCG1C" frameborder="0" style="border:0;width:100%;height:auto;min-height:450px" allowfullscreen></iframe></div></div></div></footer></div><a href="#" id="mailok" class="mailformpopup" data-featherlight="<p>Отправлено! Ваша заявка отправлена. Менеджер с вами свяжется.<p>"></a> <a href="#" id="mailerror" class="mailformpopup" data-featherlight="<p>Ошибка! Заполните все поля и нажмите кнопку Отправить</p>"></a><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script><script defer="defer" src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js"></script><script src="build/js/main.min.js"></script><script>function submitForm() {
	console.log("submitForm");

	const userData = {
		name: $("#name").val(),
		age: $("#age").val(),
		phone: $("#phone").val(),
		citizenship: $("#citizenship").val(),
		vkpofile: $("#vkpofile").val(),
	};

	const utmData ={
		utm_campaign: '<?=$utm_campaign;?>',
		utm_content: '<?=$utm_content;?>',
		utm_medium: '<?=$utm_medium;?>',
		utm_term: '<?=$utm_term;?>',
		utm_source: '<?=$utm_source;?>'
	}

	localStorage.setItem('userData', JSON.stringify(userData));
	localStorage.setItem('utmData', JSON.stringify(utmData));



	//localStorage.setItem("aboutUser", data);
	/*$.ajax({
		type: "POST",
		url: "sendmail.php",
		data: data,
		success: function (data) {
			console.log(data)
			//alert("Ваше сообщение отправлено!");
			if (data == "Ok") {
				//alert("Ваше сообщение отправлено!")
				$('#mailok').click()
			} else {
				//alert("Ошикба отпрвления сообщения!")
				$('#mailerror').click()
			}
		}
	});*/
}
	$(document).ready(function() {
		$('#phone').mask('+7(999) 999 99 99');

		$('#content__formsubmit').on('submit', function (e) {
			e.preventDefault();
			submitForm()
		});
		$('#formbutsubm').on('click', function (e) {
			e.preventDefault();
			submitForm()
		})
		/*WebFont.load({
			google: {
				families: ['Open Sans']
			}
		});*/
		//addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");



	});</script><!-- Yandex.Metrika counter --><script type="text/javascript">!function(e,t,a){(t[a]=t[a]||[]).push(function(){try{t.yaCounter47330712=new Ya.Metrika({id:47330712,clickmap:!0,trackLinks:!0,accurateTrackBounce:!0})}catch(e){}});var c=e.getElementsByTagName("script")[0],n=e.createElement("script"),r=function(){c.parentNode.insertBefore(n,c)};n.type="text/javascript",n.async=!0,n.src="https://mc.yandex.ru/metrika/watch.js","[object Opera]"==t.opera?e.addEventListener("DOMContentLoaded",r,!1):r()}(document,window,"yandex_metrika_callbacks")</script><noscript><div><img src="https://mc.yandex.ru/watch/47330712" style="position:absolute;left:-9999px" alt=""></div></noscript><!-- /Yandex.Metrika counter --><!--[if lt IE 9]>
<p class="browserupgrade">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]--></body></html>