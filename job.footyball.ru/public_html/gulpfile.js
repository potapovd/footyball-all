const 	gulp           = require('gulp'),
				watch          = require('gulp-watch'),

				sass           = require('gulp-sass'),
				uglify         = require('gulp-uglify'),
				htmlmin 		= require('gulp-htmlmin'),
				cleanCSS       = require('gulp-clean-css'),
				sourcemaps     = require('gulp-sourcemaps'),
				rename         = require('gulp-rename'),
				imagemin       = require('gulp-imagemin'),
				pngquant       = require('imagemin-pngquant'),
				cache          = require('gulp-cache'),
				autoprefixer   = require('gulp-autoprefixer'),
				rigger         = require('gulp-rigger'),
				rimraf         = require('rimraf'),
				size           = require('gulp-size'),


				ftp            = require('vinyl-ftp'),
				browserSync    = require('browser-sync');


const path = {
	build: {
		php     : '',
		html    : '',
		js      : 'build/js/',
		css     : 'build/css/',
		img     : 'build/img/',
		fonts   : 'build/fonts/',
		maps    : 'build/maps/'
	},
	src: {
		html    : 'src/*.html',
		php     : 'src/*.php',
		js      : 'src/js/main.min.js',
		style   : 'src/scss/style.scss',
		img     : 'src/img/**/*.*',
		fonts   : 'src/fonts/**/*.*'
	},
	watch: {
		php     : 'src/*.php',
		html    : 'src/*.html',
		js      : 'src/js/**/*.js',
		style   : 'src/scss/**/*.scss',
		img     : 'src/img/**/*.*',
		fonts   : 'src/fonts/**/*.*'
	},
	clean: './build/**/*.*'
};



gulp.task('html:build', function () {
	gulp.src(path.src.html) //Выберем файлы по нужному пути
		.pipe(rigger()) //Прогоним через rigger
		.pipe(htmlmin({
			collapseWhitespace: true,
			useShortDoctype:true,
			minifyCSS:true,
			minifyJS:true
		}))
		.pipe(size({title:' ',showFiles:true}))
		.pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
		//.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('php:build', function () {
	gulp.src(path.src.php) //Выберем файлы по нужному пути
		.pipe(rigger()) //Прогоним через rigger
		.pipe(htmlmin({
			collapseWhitespace: true,
			useShortDoctype:true,
			minifyCSS:true,
			minifyJS:true
		}))
		.pipe(size({title:' ',showFiles:true}))
		.pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
	//.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
	gulp.src(path.src.js) //Найдем наш main файл
		.pipe(rigger()) //Прогоним через rigger
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
		.pipe(uglify()) //Сожмем наш js
		.pipe(sourcemaps.write('/')) //Пропишем карты
		.pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
		.pipe(size({title:' ',showFiles:true}))
		//.pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', function () {
	gulp.src(path.src.style) //Выберем наш main.scss
		.pipe(sourcemaps.init()) //То же самое что и с js
		.pipe(sass()) //Скомпилируем
		.pipe(autoprefixer(['last 15 versions'])) //Добавим вендорные префиксы
		.pipe(cleanCSS()) //Сожмем
		.pipe(rename({suffix: '.min', prefix : ''}))
		.pipe(sourcemaps.write('/')) //Пропишем карты
		.pipe(size({title:' ',showFiles:true}))
		.pipe(gulp.dest(path.build.css)) //И в build
		//.pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
	gulp.src(path.src.img) //Выберем наши картинки
		.pipe(
				imagemin([ //Сожмем их
					imagemin.gifsicle({interlaced: true}),
					imagemin.jpegtran({progressive: true}),
					imagemin.optipng({optimizationLevel: 5}),
					imagemin.svgo({
						plugins: [
							{removeViewBox: true},
							{cleanupIDs: false}
						]
					})
				])
		)
		.pipe(size({title:' ',showFiles:true}))
		.pipe(gulp.dest(path.build.img)) //И бросим в build
		//.pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
	gulp.src(path.src.fonts)
		.pipe(size({title:' ',showFiles:true}))
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('build', [
	'clean',
	'html:build',
	'php:build',
	'js:build',
	'style:build',
	'fonts:build',
	'image:build'
]
);




gulp.task('watch', function(){
	watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.php], function(event, cb) {
		gulp.start('php:build');
	});
	watch([path.watch.style], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	watch([path.watch.img], function(event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
});








