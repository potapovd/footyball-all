<!DOCTYPE html>
<html lang="ru">

<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
  <title>Сеть футбольных клубов для дошкольников № 1</title>
  <meta name="keywords"
          content="футбол для детей от 3 лет, футбол для детей, футбольные клубы для детей в москве, детская футбольная школа, детские футбольные школы москвы, футбол для детей от 3, футбольный клуб для детей в москве, детские футбольные клубы москвы, детский футбольный клуб, детский футбол в москве"/>
    <meta name="description"
          content="Сеть футбольных клубов для детей от 0 до 7 лет в Москве - запись на бесплатное занятие. "/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.6, maximum-scale=1.6" />
  <link rel="stylesheet" href="build/css/style.min.css">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <meta name="yandex-verification" content="a2cb9c45aa8dd6e5" />
  <meta name="google-site-verification" content="cN_nSeAYvAkNfSks07bQ85_h5k6bupZVjEX3v1yyq_U" />
</head>

<body>
  

  <!-- Nav -->
  <nav>
    <div class="wrapper">
      <div class="logo">
        <img src="build/img/logo.svg" alt="">
        <p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
      </div>
      <div class="menu">
        <a class="menu__button"><hr><hr><hr></a>
      </div>
      <a href="#callme" class="callme fancy">Записаться на<br> бесплатное занятие</a>
    </div>
  </nav>
  <!-- //Nav -->
  <!-- Header -->
  <header>
	  <div class="box">
		  <img class="icon" src="build/img/loggo.png" alt="">
		  <p class="num">+7(495)-191-17-01</p>
		  <p class="text">Узнайте, насколько подходят <br>занятия футболом вашему сыну</p>
		  <hr class="line">
		  <h1>Футбольный клуб для мальчиков<br>&nbsp; от 2 до 7 лет&nbsp;<br>с профессиональными тренерами!</h1><br>
		  <ul class="advantage">
			  <li class="advantagepic1">развитие<br> лидерских качеств</li>
			  <li class="advantagepic2">социализация<br> в команде</li>
			  <li class="advantagepic3">мужской<br> характер</li>
			  <li class="advantagepic4">умение<br> добиваться цели</li>
		  </ul>
		  <p class="text--2">Осталось всего <b>5</b> бесплатных мест на следующую тренировку</p>
		  <a href="#callme" class="try fancy"><div>Записаться на бесплатное<br> пробное занятие</div></a>
	  </div>
  </header>
  <!-- //Header -->

  


  <!-- Block9 -->
  <section class="b9" id="adr">
    <div class="adress">
      <div class="top">
        <h2>Адреса клубов</h2>
        <p class="num">Позвоните нам: <b>8-800-707-88-99</b></p>
      </div>
      <div class="slider">
        <div class="item">
          <div class="box">
            <div class="oct">1</div>
            <p class="title">Футбольный клуб "Парк Победы"</p>
            <p class="adr"> Кутузовский проспект, 36, стр 51, г. Москва </p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
        <div class="item">
          <div class="box">
            <div class="oct">2</div>
            <p class="title">Футбольный клуб "Перово"</p>
            <p class="adr">1й проезд Перова поля, д. 9 стр. 5, г. Москва</p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
		    <div class="item">
          <div class="box">
            <div class="oct">3</div>
            <p class="title">Футбольный клуб "Белорусский"</p>
            <p class="adr"> 
              ул. Правды, 24, стр.8, г. Москва 
            </p>
            <p>Заезд с ул. 5-ого Ямского поля. Через КПП №5". Мы находимся возле "Бодрый день" и кафе "Рецептор"</p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
        <div class="item">
          <div class="box">
            <div class="oct">4</div>
            <p class="title">Футбольный клуб "Ленинский"</p>
            <p class="adr">5-й Донской проезд 15, стр. 5, 11 подъезд, г. Москва</p>
            <p class="graph">Пн-Вс 8:00-22:00</p>
          </div>
        </div>
      </div>
    </div>
    <div id="map"></div>
  </section>
  <!-- //Block9 -->
  <!-- Block10 -->
  <section class="b10 dynamic addback">
    <div class="wrapper">
      <div class="im"><img src="build/img/1.gif" class="dynamic" data-src="build/img/cup1.png" alt=""></div>
      <div class="text">
        <p>Мы не для всех - для тех, кто по<br> настоящему любит своих детей</p>
      </div>
      <a href="#callme" class="call fancy">Вы с нами?</a>
    </div>
  </section>
  <!-- //Block10 -->
  <!-- Footer -->
  <footer>
    <div class="wrapper">
      <div class="logo">
        <img class="dynamic" src="build/img/1.gif" data-src="build/img/logo.svg" alt="">
        <p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
      </div>
      <div class="polit">
        <a href="#polit" class="fancy">Политика конфиденциальности</a>
		<p>ОГРН: 1147746697119</p>
      </div>
      <div class="cont">
        <p class="cont__text">Позвоните нам</p>
        <p class="cont__num">+7(495)-191-17-01</p>
      </div>
    </div>
  </footer>
  <!-- //Footer -->
  <!-- Popups -->
  <div class="hide">

    


       <div id="callme">
      <h2>Запишитесь на бесплатную пробную тренировку</h2>
      <p class="subtitle">в удобное время</p>

          <form class="feedback" action="https://salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
			     <input type="hidden" name="lead_source" value="youtube">
            <input type="hidden" name="oid" value="00D20000000onaw">
            <input type="hidden" name="retURL" value="http://first.footyball.ru/index.php#thanks">
            <input  id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="Имя ребенка"/><br>
            <!-- <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" placeholder="Фамилия ребенка"/><br>
            <input  id="email" maxlength="80" name="email" size="20" type="text" placeholder="Email"/><br> -->
            <input  id="00N200000098YTY" maxlength="255" name="00N200000098YTY" size="20" type="text" placeholder=" Имя родителя"/><br>
            <input  id="00N200000098YTQ" maxlength="10" name="00N200000098YTQ" size="20" type="text" placeholder="Возраст"/><br>
            <input  id="phone" maxlength="40" name="phone" class="phone" size="20" type="text" placeholder="Телефон"/><br>
            <input type="hidden" id="00N200000098lUX" maxlength="255" name="00N200000098lUX" size="20" value="<?=@$_GET['utm_campaign'];?>"/>
            <input type="hidden" id="00N200000098lUc" maxlength="255" name="00N200000098lUc" size="20" value="<?=@$_GET['utm_content'];?>"/>
            <input type="hidden" id="00N200000098lUh" maxlength="255" name="00N200000098lUh" size="20" value="<?=@$_GET['utm_medium'];?>"/>
            <input type="hidden" id="00N200000098lUm" maxlength="255" name="00N200000098lUm" size="20" value="<?=@$_GET['utm_source'];?>"/>
            <input type="hidden" id="00N200000098lUr" maxlength="255" name="00N200000098lUr" size="20" value="<?=@$_GET['utm_term'];?>"/>
            <input type="hidden" id="URL" maxlength="80" name="URL" size="20" value="http://<?=@$_SERVER["SERVER_NAME"];?>/" />
            <p class="desc" style="max-width:320px; margin:20px auto; ">
              <input type="checkbox" id="00N0J000009xwo5" name="00N0J000009xwo5" value="1"  style="float:left;width: 20px;"/>
              <label class="checkboxrights" for="00N0J000009xwo5" style="font-size:11px; text-align:justify">
                Я уведомлен и выражаю своё согласие на обработку моих персональных данных администрацией 
                Сайта в целях оказания мне услуг Компании.<br>
                Я уведомлен и выражаю своё согласие на получение смс-уведомлений от администрации
                 Сайта в целях оказания мне услуг Компании.
                </label>
            </p>
            <input type="submit" class="feedback__button" id="sendWebToLeadbutton" value="отправить" 
                  style="padding-right:0;padding-left:0; background:#C81688"
            >
          </form>
          
    </div>




    <div id="thanks">
      <h2>Ваше сообщение успешно отправлено.<br>Наш менеджер свяжется с вами в ближайшее время.</h2>
    </div>



    <div id="polit">
      <h2>Политика конфиденциальности</h2>
      <div class="text">
				<p>Соблюдение Вашей конфиденциальности важно для нас. По этой причине, мы разработали Политику Конфиденциальности, которая описывает, как мы используем и храним Вашу информацию. Пожалуйста, ознакомьтесь с нашими правилами соблюдения конфиденциальности и сообщите нам, если у вас возникнут какие-либо вопросы.</p>
				<h6>Сбор и использование персональной информации</h6>
				<p>Под персональной информацией понимаются данные, которые могут быть использованы для идентификации определенного лица либо связи с ним.</p>
				<p>От вас может быть запрошено предоставление вашей персональной информации в любой момент, когда вы связываетесь с нами.</p>
				<p>Ниже приведены некоторые примеры типов персональной информации, которую мы можем собирать, и как мы можем использовать такую информацию.</p>
				<p>Какую персональную информацию мы собираем:</p>
				<ul>
					<li>Когда вы оставляете заявку на сайте, мы можем собирать различную информацию, включая ваши имя, номер телефона, адрес электронной почты и т.д.</li>
				</ul>
				<p>Как мы используем вашу персональную информацию:</p>
				<ul>
				    <li>Собираемая нами персональная информация позволяет нам связываться с вами и сообщать об уникальных предложениях, акциях и других мероприятиях и ближайших событиях.</li>
				    <li>Время от времени, мы можем использовать вашу персональную информацию для отправки важных уведомлений и сообщений.</li>
				    <li>Мы также можем использовать персональную информацию для внутренних целей, таких как проведения аудита, анализа данных и различных исследований в целях улучшения услуг предоставляемых нами и предоставления Вам рекомендаций относительно наших услуг.</li>
				    <li>Если вы принимаете участие в розыгрыше призов, конкурсе или сходном стимулирующем мероприятии, мы можем использовать предоставляемую вами информацию для управления такими программами.</li>
				</ul>
				<h6>Раскрытие информации третьим лицам</h6>
				<p>Мы не раскрываем полученную от Вас информацию третьим лицам.</p>
				<p>Исключения:</p>
				<ul>
				    <li>В случае если необходимо — в соответствии с законом, судебным порядком, в судебном разбирательстве, и/или на основании публичных запросов или запросов от государственных органов на территории РФ — раскрыть вашу персональную информацию. Мы также можем раскрывать информацию о вас если мы определим, что такое раскрытие необходимо или уместно в целях безопасности, поддержания правопорядка, или иных общественно важных случаях.</li>
				    <li>В случае реорганизации, слияния или продажи мы можем передать собираемую нами персональную информацию соответствующему третьему лицу – правопреемнику.</li>
				</ul>
				<h6>Защита персональной информации</h6>
				<p>Мы предпринимаем меры предосторожности — включая административные, технические и физические — для защиты вашей персональной информации от утраты, кражи, и недобросовестного использования, а также от несанкционированного доступа, раскрытия, изменения и уничтожения.</p>
				<h6>Соблюдение вашей конфиденциальности на уровне компании</h6>
				<p>Для того чтобы убедиться, что ваша персональная информация находится в безопасности, мы доводим нормы соблюдения конфиденциальности и безопасности до наших сотрудников, и строго следим за исполнением мер соблюдения конфиденциальности.</p>
      </div>
    </div>

  </div>
  <!-- //Popups -->

  <script src="build/js/main.min.js"></script>

	
  </script>
  


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47020197 = new Ya.Metrika({
                    id:47020197,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47020197" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>
