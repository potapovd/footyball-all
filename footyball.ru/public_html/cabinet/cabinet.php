<?php

include_once("lib/cms_view_inc.php");

function generateCode($length=8) {
    //$chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPRQSTUVWXYZ123456789";
    $chars = "0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}
function clearphone($phonenumb){
    $phonebadsimb=array("(",")"," ");
    $phonenumb =  cleardata(trim(str_replace($phonebadsimb,"",$phonenumb)));
    return $phonenumb;
}

function reset_url($url) {
    $value = str_replace ( "http://", "", $url );
    $value = str_replace ( "https://", "", $value );
    $value = str_replace ( "www.", "", $value );
    $value = explode ( "/", $value );
    $value = reset ( $value );
    return $value;
}

$_SERVER['HTTP_REFERER'] = reset_url ( @$_SERVER['HTTP_REFERER'] );
$_SERVER['HTTP_HOST'] = reset_url ( @$_SERVER['HTTP_HOST'] );

if ($_SERVER['HTTP_HOST'] != $_SERVER['HTTP_REFERER']) {
    die ( "Wanna hack a website?" );
}



//проверка данных
$errorflag = 0;
$resresp = "";
if(!isset($_POST["action"])||empty($_POST["action"])){
    die("Wanna hack a website?");
}
$action = cleardata($_POST["action"]);
if($action!=="reg" && $action!=="rec" && $action!=="sub"  && $action!=="frz" && $action!=="nxtt"&& $action!=="ufz" && strlen($action) !== 3){
    die("Wanna hack a website?");
}


if( ($action!=="frz")&&($action!=="nxtt")&&($action!=="ufz") ){
    if(!isset($_POST['phone']) && empty($_POST['phone'])) {
        $errorflag = 1;
        $resresp = "Ошибка! Введите номер телефона";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }
}
if(!isset($_POST['action']) && empty($_POST['action'])) {
    $errorflag = 1;
    $resresp = "Ошибка! Обратитесь к оператору";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}






if( ($action!=="frz")&&($action!=="nxtt")&&($action!=="ufz")  ) {
    $phone = cleardata($_POST["phone"]);
    if (strlen($phone) !== 17) {
        $errorflag = 1;
        $resresp = "Ошибка! Вы ввели неправильный номер телефона";
        echo json_encode(array("errorflag" => $errorflag, "answertxt" => $resresp));
        die();
    }
}



if($action=="reg"){

    /*REGISRATION*/
    sleep(2);
    //проверка на существования в белом списке и в списке зареганных

    $phone = clearphone($phone);
    // проверка по уже зареганых
    $rescheck = checknuminexist($phone);
    if($rescheck == true ){
        $errorflag = 1;
        $resresp = "Ошибка! Вы уже зарегистрированы";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }

    // проверка по базе потенциальных
    $rescheck = checknuminpot($phone);
    if($rescheck == false ){
        $errorflag = 1;
        $resresp = "Ошибка! Вы не можете быть зарегистрированы";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }

    // регистрация
    if($errorflag==0){
        $password = generateCode(8);
        $newuserreg = newuser($phone,$password,$rescheck);
        if ($newuserreg!==false){
            $resresp = "Вы успешно зарегистрированы! Вам будет выслано СМС с паролем";
            /*ТЕСТ ОТВЕТА*/
            //$resresp = $newuserreg;
        }else{
            $errorflag = 1;
            $resresp = "Ошибка! Произошла ошибка при регистрации. Повторите попытку попозже.";

        }
        /*ТЕСТ ОТВЕТА*/
        //print_r($resresp);
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }
}elseif ($action=="rec"){

    /*RESTORE*/
    sleep(1);
    $phone = clearphone($phone);
    $rescheck = checknuminexist($phone);
    $password = generateCode(8);
    if($rescheck == true ){
        //$resresp = restoreuser($phone,$password);
        //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        //die();
        if(restoreuser($phone,$password)){
            $resresp = "Вам будет выслано СМС с новым паролем";
        }else{
            $errorflag = 1;
            $resresp = "Ошибка! Произошла ошибка при восстановлении пароля. Повторите попытку попозже.";
        }


    }else{
        $rescheck = checknuminpot($phone);
        if($rescheck == true ){
            $errorflag = 1;
            $resresp = "Ошибка! Вы должны пройти процедуру регистрации";
        }else{
            $errorflag = 1;
            $resresp = "Ошибка! Вы не можете восстановить пароль";
        }
    }
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}elseif ($action=="sub"){
    //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$phone." ".$action." ВСЕ ОК ИДЕМ ДАЛЬШЕ!"));

    /*AUTORIZATION*/
    sleep(1);
    if(!isset($_POST['pass']) || empty($_POST['pass'])) {
        $errorflag = 1;
        $resresp = "Ошибка! Заполните поле пароля";
        //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        //die();
    }
    $pass = cleardata($_POST["pass"]);
    if(strlen($pass) <= 7 or strlen($pass) >= 9){
        $errorflag = 1;
        $resresp = "Ошибка! Вы ввели неправильный пароль";
        //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        //die();
    }

    $hash = md5(generateCode(10));
    $phone = clearphone($phone);
    $reschek = checkphonepass($phone,$pass,$hash);

    //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$reschek));
    //die();


    if ($reschek){
        $errorflag = 0;
        //$redirect = SITE_URL."version2/cabinet/index.php";
        $redirect = SITE_URL."cabinet/index.php";
        //echo $redirect;
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp, "redirect"=>$redirect));

    }else{
        $errorflag = 1;
        $resresp = "Ошибка! Неправильный телефон или пароль";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }






}else if($action=="frz"){
    //$resresp = var_dump($_POST);
    $userid  = cleardata($_POST["useridcms"]);
    $progrid  = cleardata($_POST["progridcms"]);
    $freeztype  = cleardata($_POST["freeztype"]);
    $freznexttran  = cleardata($_POST["freznexttran"]);
    $freezingres = freezing($userid,$progrid,$freeztype,$freznexttran);


    if($freezingres){
        $resresp = "Заморозка включена успешно";
    }else{
        $errorflag = 1;
        $resresp = "Произошла ошибка при включении заморозки";
    }

    /*ТЕСТ ОТВЕТА*/
    //$resresp = $freezingres;


    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}else if($action=="ufz"){
    //$resresp = var_dump($_POST);
    $userid  = cleardata($_POST["useridcms"]);
    $progrid  = cleardata($_POST["progridcms"]);
    $freeztype  = cleardata($_POST["freeztype"]);
    $freznexttran  = cleardata($_POST["freznexttran"]);
    $freezingres = unfreezing($userid,$progrid,$freeztype);


    if($freezingres){
        $resresp = "Заморозка отключена успешно";
    }else{
        $errorflag = 1;
        $resresp = "Произошла ошибка при отключении заморозки";
    }

    /*ТЕСТ ОТВЕТА*/
    //$resresp = $freezingres;


    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}else if($action=="nxtt"){
    //$resresp = var_dump($_POST);
    $userid  = cleardata($_POST["useridcms"]);
    $progrid  = cleardata($_POST["progridcms"]);
    $netxtrainig = netxtrainig($progrid);

    /*ТЕСТ ОТВЕТА*/
    $resresp = $netxtrainig;
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();

}else{

    $errorflag = 1;
    $resresp = "Ошибка! Не допустимое действие. Свяжитесь с администрацией";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}


//echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>"ВСЕ ОК ИДЕМ ДАЛЬШЕ!"));
//die();

