<?
include_once 'cms_config_inc.php';
//include_once 'cms_lang_inc.php';

/*
 * ОБЩИЕ ФУНКИЦИИ 
 */

//защита от "плохих" запросов
$get_ar = array_values($_GET);
$c_a_g = count($get_ar);
for ($i = 0;$i < $c_a_g;$i++){
	if(preg_match('/union(\.*)select/',$get_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
	if(preg_match('/order(\.*)by/',$get_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
}
$post_ar = array_values($_POST);
$c_a_p = count($post_ar);
for ($i = 0;$i < $c_a_p;$i++){
	if(preg_match('/union(\.*)select/',$post_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
	if(preg_match('/order(\.*)by/',$post_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
}

//подключение и отправка запросов в SF
function callsaleforce($sfquery,$param=""){

    $paramsauth = "grant_type=password"
        . "&client_id=" . CLIENT_ID
        . "&client_secret=" . CLIENT_SECRET
        . "&username=" . USER_NAME
        . "&password=" . PASSWORD;

    //$loginurl="https://test.salesforce.com/services/oauth2/token";
    $loginurl="https://login.salesforce.com/services/oauth2/token";
    $curlauth = curl_init($loginurl);


    curl_setopt($curlauth, CURLOPT_HEADER, false);
    curl_setopt($curlauth, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlauth, CURLOPT_POST, true);

    curl_setopt($curlauth, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlauth, CURLOPT_SSLVERSION, 6);

    curl_setopt($curlauth, CURLOPT_POSTFIELDS, $paramsauth);
    curl_setopt($curlauth, CURLOPT_SSL_VERIFYPEER, false);
    $json_response = curl_exec($curlauth);
    $responseauth = json_decode($json_response, true);
    //return $json_response;
    if( !isset($responseauth['access_token']) || empty($responseauth['access_token'])){
        return false;
    }
    $access_token = $responseauth['access_token'];
    $instance_url = $responseauth['instance_url'];


    //updatefrom ГГГГ-ММ-ДД ЧЧ:ММ:СС
    //https://test.salesforce.com/services/oauth2/token/services/apexrest/SiteWebservice/?method=getSyncData

    if($sfquery=="getRSPVisits"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=getRSPVisits";
    }elseif ($sfquery=="getRSPDate"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=getRSPDate";
    }elseif ($sfquery=="getSyncData"){
        //$url = "$instance_url/services/apexrest/SiteWebservice/?method=getSyncData&updatefrom=1487579966";
        //$url = "$instance_url/services/apexrest/SiteWebservice/?method=getSyncData&updatefrom=1496231096000";
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=getSyncData&".$param;
    }elseif($sfquery=="sendSMS"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=sendSMS&".$param;
    }elseif($sfquery=="getRecord"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=getRecord&".$param;
    }elseif($sfquery=="updateRecord"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=updateRecord&".$param;
    }elseif($sfquery=="getNextClass"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=getNextClass&".$param;
    }elseif($sfquery=="setFreeze"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=setFreeze&".$param;
    }elseif($sfquery=="delFreeze"){
        $url = "$instance_url/services/apexrest/SiteWebservice/?method=delFreeze&".$param;
    }
    else{
        return false;
    }

  //return $url ;
    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
        array(
            "Authorization: OAuth $access_token",
            "Content-Type:  application/json"
        )
    );
    $json_response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($json_response, true);
    //$response = json_encode($response, JSON_PRETTY_PRINT);

    return $response;
}


//концертация объекта БД в массив
function fromdb2array ($data){
    $infoarray = array();
    while ($row = mysqli_fetch_assoc($data)){
        $infoarray[] = $row;
    }
    return $infoarray;
}
//Обработка запроса и отправка ответа на концертацию в массив
function fromquery2db($sqlquery){
    $link = mysqli_connect(DB_HOST,DB_LOGIN,DB_PASSWORD);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link,"utf8");
    mysqli_select_db($link, DB_NAME);

    if ( $result = mysqli_query($link,$sqlquery) or die( mysqli_error($link) ) ){
        $result = fromdb2array($result);
      //  CleanUpDB();
        return $result;
    }else{
        echo('Ошибка в MySQL запросе');
        //CleanUpDB();
        return false;
    }
    //mysqli_close($link);
}

//отправка запроса в БД (add,upd,del)
function simplequery2db($sqlquery){

    $link = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASSWORD);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    mysqli_select_db($link, DB_NAME);


    if (mysqli_query($link, $sqlquery) or die(mysqli_error($link))) {
        //$result = fromdb2array($result);
        return true;
    } else {
        echo('Ошибка в MySQL запросе');
        //CleanUpDB();
        return false;
    }

    //mysqli_close($link);
}


//отчистка вводимых юзером данных
function cleardata($data, $type = "s"){
    if ($type == "s"){
        if(is_array($data))
            return array_map(__METHOD__, $data);
        if(!empty($data) && is_string($data)) {
            $data = trim(strip_tags(stripslashes($data))) ; 
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $data);
        }
    }else{
        $data = (int)$data;
    }
    return $data;
}


/*CABINET*/
/*registr_new_user*/
//проверка на существование в таблице потенциальных ; возващает ID  или false
function checknuminpot($phonenumb){
    $sql="SELECT id, phone, sfid
          FROM user_potenc
          WHERE phone = '$phonenumb' LIMIT 1";
    $allnumb = fromquery2db($sql);
    $resultcount = count($allnumb);

    if($resultcount==0){
        return false;
    }else{
        //return true;
        return $allnumb[0]["sfid"];
    }
}
//проверка на существоание в таблице зарегестирован
function checknuminexist($phonenumb){
    $sql="SELECT id, phone_reg
          FROM user_reg
          WHERE phone_reg = '$phonenumb'";
    $allnumb = fromquery2db($sql);
    $result = count($allnumb);

    if($result>0){
        return true;
    }else{
        return false;
    }
}
//восстановление пароля
function restoreuser($phone,$password){
    //$phoneparam = "phone=$phone&;text=$password";

    /*НОВЫЙ МЕТОД*/
    if (PROJECTSTATUS == "local") {
        $dbpassw = md5(md5($password . "footyballpass"));
        $sql = "UPDATE user_reg
        SET ip ='$password', passw ='$dbpassw'
        WHERE phone_reg =  '$phone'  LIMIT 1 ";
        if (simplequery2db($sql)) {
            return true;
        } else {
            return false;
        }

    }else{
        $phoneparam = "phone=$phone&text=$password";
        
        $saleforceresp = callsaleforce( "sendSMS", $phoneparam);
        $dbpassw = md5(md5($password . "footyballpass"));
        if ($saleforceresp) {
            $sql = "UPDATE user_reg
            SET ip ='$password', passw ='$dbpassw'
            WHERE phone_reg =  '$phone'  LIMIT 1 ";
            if (simplequery2db($sql)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

        //$resultssaleforce = json_decode($saleforceresp, true);

    //return true;





}
//Регистрация новго пользов.
function newuser($phone,$password,$sfid){


    //отправка SMS
    if(PROJECTSTATUS!=="local"){
        $phoneparam = "phone=$phone&text=$password";
        $saleforceresp = callsaleforce("sendSMS",$phoneparam);
        $resultssaleforce = json_decode($saleforceresp, true);
    }
    /**/

    $phoneparam = "id=".$sfid;
    $saleforceresp = callsaleforce("getRecord",$phoneparam);
    $resultssaleforce = json_decode($saleforceresp, true);
    $resultssaleforce = json_decode($resultssaleforce["record"], true);

    /*ТЕСТ ОТВЕТА*/
    //return $resultssaleforce;
    $dbPrograms = $resultssaleforce["programs"];
    $dbPhone =  clearphone($resultssaleforce["Phone"]);
    $dbParentName =  $resultssaleforce["ParentName"];
    $dbIduser = $resultssaleforce["Id"];
    $dbPhonefull = $resultssaleforce["Phone"];
    $dbEmail =  $resultssaleforce["Email"];
    $dbChildLastName =  $resultssaleforce["ChildLastName"];
    $dbChildFirstName =  $resultssaleforce["ChildFirstName"];
    //$dbid =  $resultssaleforce["ID"];

    if(stristr($dbPhone, '+') === FALSE) {
        $dbPhone = "+".$dbPhone;
    }
    if(stristr($dbPhonefull, '+') === FALSE) {
        $dbPhonefull = "+".$dbPhonefull;
    }



    /*
            - ParentName - строка как ее ввел клиент или оператор (может быть как Иван, Иван Петров, Иван Николаевич, Петров Иван и т.п.),
            - ChildName - имя ребенка (задается строго Имя <пробел> Фамилия)
            - Phone
            - Email
            - Id
            - Programs - список программ у ребенка (поля указаны ниже)

            "Visited":2 //кол-во посещенных занятий
            ,"TrainingsAmount":2 // всего возможных тренировок предусмотренных программой
            ,"Status":"Finished" //статус программы (из списка Finished, Freezed, Not started, In progress, Not paid, Partially paid)
            ,"Passed":2 //общее количество использованных занятий (посещенных и пропущенных)
            ,"Name":"Старт Победителя" //наименование программы (внутреннее название типа "Фундамент Победителя 80 (акция)")
            ,"Missed":0 //кол-во пропущенных занятий
            ,"Left":0 //кол-во оставшихся для посещения занятий
            ,"ID":"a047E000001jMkRQAU" //ИД абонемента по данной программе
            ,"FreezedUnlim":0 //указывают сколько заморозок было использовано
            ,"Freezed":0 //указывают сколько заморозок было использовано
            ,"FreezedMaxUnlim":0 //макс. кол-во безлимитных заморозок
            ,"FreezedMaxDaily":0 // Максимальное количество однодневных (разовых) заморозок по данной программе
     */

    $allprogr_idsf ="";






    $allprogr_idsf = substr($allprogr_idsf, 0, -1);
    //return $allprogr_idsf;

    /*if($dbIduser=="00Q0J00000tnQ3QUAU"){
        $dbpassw = "228878e915f98e5507da1c7d514da0a3"; // 7(985)2515252 ST9r9uTK -> md5(md5(ST9r9uTKfootyballpass))
    }else if($dbIduser=="00Q7E000004veilUAA"){
        $dbpassw = "ce2733981beeccaab9e12bbc2a4124d6"; // 7(905)5934637 W8Y7QHwP -> md5(md5(W8Y7QHwPfootyballpass))
    }else if($dbIduser=="00Q7E00000BTU8oUAH"){
        $dbpassw = "ce2733981beeccaab9e12bbc2a4124d6"; // 7(905)5934637 W8Y7QHwP -> md5(md5(W8Y7QHwPfootyballpass))
    }*/
    $dbDateReg = date("Y-m-d");
    //$dbpassw = $password;
    //$dbpassw = md5(md5($password));
    //$dbpassw = md5(md5($password."footyballpass"));
    $dbpassw = md5(md5($password."footyballpass"));


    $sql = "INSERT INTO `user_reg` (
              `phone_reg`,`name_par`,`name_chil`,
              `passw`,`email`,`phone_cab`,
              `progr_idsf`,`ip`,
              `date_reg`, `sfid`)
            VALUES (
            '$dbPhone','$dbParentName','$dbChildFirstName',
            '$dbpassw','$dbEmail','$dbPhonefull',
            '$allprogr_idsf','$password',
             '$dbDateReg','$dbIduser') ";
        //return $sql;
    if(simplequery2db($sql)){
        foreach($dbPrograms as $d){
            //ID
            if($d["Status"]!=="Finished"){
                $allprogr_idsf  .= $d["ID"].",";

                $idsf = $d["ID"];
                $idusersf = $dbIduser;
                $visited  = $d["Visited"];
                $countbyprogr  = $d["TrainingsAmount"];
                $status  = $d["Status"];
                $passed  = $d["Passed"];
                $name  = $d["Name"];
                $missed  = $d["Missed"];
                $countleft  = $d["Left"];
                $freezed  = $d["Freezed"];
                $freezedunlim  = $d["FreezedUnlim"];
                $maxfreezed  = $d["FreezedMaxDaily"];
                $maxfreezedunlim  = $d["FreezedMaxUnlim"];


                $sql = "INSERT INTO `user_progr` (
                        `idsf`, `idusersf`, `visited`, 
                        `countbyprogr`, `status`, `passed`, 
                        `name`, `missed`, `countleft`, 
                        `freezed`, `freezedunlim`, `maxfreezed`, 
                        `maxfreezedunlim`) 
                    VALUES (
                      '$idsf','$idusersf',$visited,
                      $countbyprogr,'$status',$passed,
                      '$name',$missed,$countleft,
                      $freezed,$freezedunlim,$maxfreezed,
                      $maxfreezedunlim)";



                simplequery2db($sql);
                //return simplequery2db($sql);


                /*return $d["Name"];
                $dbProgramsName = $d["Name"];
                $dbProgramsStat = $d["Status"];
                $dbProgramsAll = $d["Passed"];
                $dbProgramsLeft = $d["Left"];
                $dbProgramsFreezLim = $d["Freezed"];
                $dbProgramsFreezUnlim = $d["FreezedUnlim"];*/
            }
        }

        return true;
    }


}

/*restore_pass*/


/*login*/
//авторизация
function checkphonepass($phone, $pass, $hash){
    $sql = "SELECT id, passw FROM user_reg WHERE phone_reg='" . $phone . "' LIMIT 1";

    $data = fromquery2db($sql);
    if (!empty($data)) {
        //if ($data[0]['passw'] === $pass) {
        if ($data[0]['passw'] === md5(md5($pass."footyballpass"))) {
            $sql = "UPDATE user_reg SET usrhash='" . $hash . "' WHERE id='" . $data[0]['id'] . "'";
            if (simplequery2db($sql)) {
                //вход успешный
                setcookie("id", $data[0]['id'], time() + 60 * 60 * 24 * 30);
                setcookie("hash", $hash, time() + 60 * 60 * 24 * 30);
                return true;
            } else {
                //ошибка при входе
                $sql = "UPDATE user_reg SET usrhash='' WHERE id='" . $data[0]['id'] . "'";
                simplequery2db($sql);
                setcookie("id", "", time() + 60 * 60 * 24 * 30);
                setcookie("hash", "", time() + 60 * 60 * 24 * 30);
                return false;
            }
        } else {
            //print "Вы ввели неправильный логин/пароль";
            return false;
        }
    }else{
        return false;
    }
}
//проверка логина и пароля на внутр. страницах
function getchecklogin($id,$hash){
    $sql="SELECT * FROM user_reg WHERE id = '".$id."' LIMIT 1";
    $data  = fromquery2db($sql);

    //$allnumb = fromquery2db($sql);
    $resultcount = count($data);

    if ($resultcount>0){


        if(
            ($data[0]['usrhash'] !== $hash) or
            ($data[0]['id'] !== $id)
        ){
            //ошибка
            setcookie("id", "", time() - 3600*24*30*12, "/");
            setcookie("hash", "", time() - 3600*24*30*12, "/");
            return false;
        }else {
            //все ок
            return true;
        }
    }else{
        return false;
    }
}
//информация о пользователе
function getuserinfo($id){
    $sql="SELECT 
          `id`, `phone_reg`, `passw`, `photo`, `name_par`, `name_chil`, `phone_cab`, `email`, `ip`, `sfid`
            FROM `user_reg`
            WHERE id = $id
            LIMIT 1";
    return fromquery2db($sql);
}
//информация о программах пользателей
function getuserprogramms($id){
    $sql="SELECT 
            `status`,`idsf`,`freeztype`, `freezon`, `freezoff`         
            FROM `user_progr`
            WHERE idusersf = '$id' AND status='In progress'";
    $sqlres = fromquery2db($sql);

    //return $sqlres;
    foreach($sqlres as $key => $val){
        //echo date('Y-m-d')." ".$sqlres[$key]["freezoff"];

        if( (date('Y-m-d')>=$sqlres[$key]["freezoff"])&&($sqlres[$key]["freeztype"]=="Day") ) {
            $sql = "UPDATE user_progr
                    SET 
                    freeztype = NULL,
                    freezon = NULL,
                    freezoff = NULL
                    WHERE idsf = '".$sqlres[$key]["idsf"]."' LIMIT 1";
            simplequery2db($sql);
            //echo $sql;
        }

    }


    $sql="SELECT
             `idsf`, `idusersf`, `visited`, `countbyprogr`, `status`, `passed`, `name`, `missed`, `countleft`, `freezed`, `freezedunlim`, `maxfreezed`, `maxfreezedunlim`, `freeztype`, `freezoff`         
            FROM `user_progr`
            WHERE idusersf = '$id' ";
    return fromquery2db($sql);
}
// получение логотипа программы
function getprogrlogo($programname){


    $programname =  mb_strtolower($programname,"utf-8");
    if(mb_stripos($programname, "фундамент",0, 'utf-8')!==false){
        $imgname = "logo4cab_fp";
    }elseif (mb_stripos($programname, "старт",0, 'utf-8')!==false){
        $imgname = "logo4cab_sp";
    }elseif (
        (mb_stripos($programname, "путь",0, 'utf-8')!==false )||
        (mb_stripos($programname, "пп",0, 'utf-8')!==false )
    ){
        $imgname = "logo4cab_pp";
    }elseif (mb_stripos($programname, "профи",0, 'utf-8')!==false){
        $imgname = "logo4cab_pro";
    }elseif (mb_stripos($programname, "звезда",0, 'utf-8')!==false){
        $imgname = "logo4cab_zv";
    }elseif (mb_stripos($programname, "english",0, 'utf-8')!==false){
        $imgname = "logo4cab_ec";
    }else{
        return false;
    }


    $fllimg = '<img src="../assets/img/'.$imgname.'.jpg" class="img-responsive img-circle" alt="">';

    return $fllimg;
}
// получение названия программы
function getprogrname($programname){

    $programname =  mb_strtolower($programname,"utf-8");
    if(mb_stripos($programname, "фундамент",0, 'utf-8')!==false){
        $progname = "Фундамент <br> Победителя";
    }elseif (mb_stripos($programname, "старт",0, 'utf-8')!==false){
        $progname = "Старт <br> Победителя";
    }elseif (
        (mb_stripos($programname, "путь",0, 'utf-8')!==false )||
        (mb_stripos($programname, "пп",0, 'utf-8')!==false )
    ){
        $progname = "Путь <br> Победителя";
    }elseif (mb_stripos($programname, "профи",0, 'utf-8')!==false){
        $progname = "Профи";
    }elseif (mb_stripos($programname, "звезда",0, 'utf-8')!==false){
        $progname = "Звезда";
    }elseif (mb_stripos($programname, "english",0, 'utf-8')!==false){
        $progname = "English <br> Club";
    }else{
        $progname = "";
    }

    return $progname;
}
//обновление иформации о пользователе на сервере SF
function updateuserinfosf($id,$value){
    $param ="id=".$id."&body=".$value;
    $saleforceresp = callsaleforce("updateRecord",$param);
    return $saleforceresp;
}
//обновление иформации о пользователе локально
function updateuserinfo($id,$sfid,$field,$value){
    if($field=="email"){
        $strupd='{"Email":"'.$value.'"}';
    }else if($field=="phone"){
        $strupd='{"Phone":"'.$value.'"}';
    }else if($field=="newpassword"){
        $newpass = md5(md5($value."footyballpass"));
        $sql = "UPDATE user_reg
            SET ip='$value', passw ='$newpass'
            WHERE sfid = '$sfid' LIMIT 1 ";
        if (simplequery2db($sql)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }

    $answupdateuserinfosf = updateuserinfosf($sfid,$strupd);
    $answupdateuserinfosf = json_decode($answupdateuserinfosf, true);
    if($answupdateuserinfosf["record"] == "Updated successfull"){
        $sql = "UPDATE user_reg
            SET $field='$value'
            WHERE sfid = '$sfid' LIMIT 1 ";
        if (simplequery2db($sql)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }




}
//заморозка включение
function freezing($userid,$progrid,$freeztype,$freznexttran){
    // /?method=setFreeze&progId=a047E000001jEIE&frDate=2017-05-30&frType=Unlimited
    // "{"record":{"msg":"a0S7E000000OxDeUAK","result":"success"}}"
    // "{"record":{"msg":"Date is empty","result":"error"}}"

    if($freeztype=="limfreez"){
        $freeztype="Day";
        $frDate = $freznexttran;
    }else if($freeztype=="unlimfreez"){
        $freeztype="Unlimited";
        $frDate = date("Y-m-d");
    }else{
        return false;
    }
    //$frDate="2017-06-19";
    $param ="progId=".$progrid."&frDate=".$frDate."&frType=".$freeztype;
    /* GETTT ON*/
    $resultssaleforce = callsaleforce("setFreeze",$param);
    $saleforceresp = json_decode($resultssaleforce, true);
    //return $saleforceresp;
    if($saleforceresp["record"]["result"]=="error"){
        return false;
    }else{
        $freezidsf  = $saleforceresp["record"]["msg"];
        if($freeztype=="Day"){
            $freezon = date('Y-m-d');
            $freezoff = $freznexttran;
            $sql = "UPDATE user_progr
                    SET 
                    freezed = freezed+1,
                    freeztype ='$freeztype',
                    freezon = '$freezon',
                    freezoff = '$freezoff',
                    freezidsf = '$freezidsf'
                    WHERE idsf = '$progrid' LIMIT 1
            ";
        }else{
            $freezon = date('Y-m-d');
            $sql = "UPDATE user_progr
                    SET 
                    freezedunlim = freezedunlim+1,
                    freeztype ='$freeztype',
                    freezon = '$freezon',
                    freezidsf = '$freezidsf'
                    WHERE idsf = '$progrid' LIMIT 1
            ";
        }
        if (simplequery2db($sql)){
            return true;
        }else{
            return false;
        }

    }



    return false;
}//заморозка включение
//заморозка отлючение
function unfreezing($userid,$progrid,$freeztype){
    // /services/apexrest/SiteWebservice?method=delFreeze&frId=a0S7E000000OxDe
    //"{"record":{"msg":"Freeze ID is empty","result":"error"}}"
    //"{"record":{"msg":"a0S7E000000OxDeUAK","result":"success"}}"

    $sql = "SELECT freezidsf  FROM user_progr WHERE idsf = '$progrid' LIMIT 1";
    $data = fromquery2db($sql);
    $frId = $data[0]["freezidsf"];

    $param ="frId=".$frId;
    /* GETTT ON*/
    $saleforceresp = callsaleforce("delFreeze",$param);
    $saleforceresp = json_decode($saleforceresp, true);

    if($saleforceresp["record"]["result"]=="success") {

        if($freeztype=="limunfreez"){
            $freeztype="Day";
        }else if($freeztype=="unlimunfreez"){
            $freeztype="Unlimited";
        }else{
            return false;
        }
        //return $freeztype;
        if($freeztype=="Day"){

            $sql = "SELECT * 
                    FROM user_progr
                    WHERE idsf = '$progrid' LIMIT 1";
            $getprogr = fromquery2db($sql);
            if(date('Y-m-d')<$getprogr[0]["freezoff"]){
                $sql = "UPDATE user_progr
                    SET 
                    freezed = freezed-1,
                    freeztype = NULL,
                    freezon = NULL,
                    freezoff = NULL,
                    freezidsf = NULL
                    WHERE idsf = '$progrid' LIMIT 1";
            }else{
                $sql = "UPDATE user_progr
                    SET 
                    freeztype = NULL,
                    freezon = NULL,
                    freezoff = NULL,
                    freezidsf = NULL
                    WHERE idsf = '$progrid' LIMIT 1";
            }
        }else{
            $sql = "UPDATE user_progr
                    SET 
                    freeztype =NULL,
                    freezon = NULL,
                    freezidsf = NULL 
                    WHERE idsf = '$progrid' LIMIT 1
            ";
        }

        if (simplequery2db($sql)){
            return true;
        }else{
            return false;
        }


    }else{
        return false;
    }

    return false;
}
//получение даты след. тренеровоки (для разовой заморозки)
function netxtrainig($progrid){
    // /services/apexrest/SiteWebservice/?method=getNextClass&progId=a047E000002Bdf3

    $param ="progId=".$progrid;
    $saleforceresp = callsaleforce("getNextClass",$param);

    
    $saleforceresp = json_decode($saleforceresp, true);
    //$saleforceresp = json_decode($resultssaleforce["record"], true);
    
    
    $saleforceresp = $saleforceresp["record"];
    return $saleforceresp;
    //$saleforceresp  = substr($saleforceresp,3);

    //return $saleforceresp;
    
}
//удаление тестовых юзеров
function clearusers(){
    $sql = "DELETE FROM user_reg
        WHERE sfid = '00Q7E000004veilUAA'
        LIMIT 1";
    simplequery2db($sql);
    $sql = "DELETE FROM user_progr
        WHERE idusersf = '00Q7E000004veilUAA'";
    simplequery2db($sql);


    return true;

}




?>