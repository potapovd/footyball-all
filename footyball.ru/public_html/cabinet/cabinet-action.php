<?php
header('Content-type: text/html; charset=utf-8');
include_once("lib/cms_view_inc.php");



//проверка данных
$errorflag = 0;
$resresp = "";
if(!isset($_POST['iduser']) && empty($_POST['action'])) {
    $errorflag = 1;
    $resresp = "Ошибка! Неправильное действие";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}

if(!isset($_POST['field']) && empty($_POST['field'])) {
    $errorflag = 1;
    $resresp = "Ошибка! Пустое значение";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}
if(!isset($_POST['value']) && empty($_POST['value'])) {
    $errorflag = 1;
    $resresp = "Ошибка! Поле не может быть пустым";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}

$iduser = cleardata($_POST["iduser"],"s");
$idsf = cleardata($_POST["idusercms"],"s");
$action = cleardata($_POST["action"]);
$field = cleardata($_POST["field"],"s");
$value = cleardata($_POST["value"],"s");

if($action!=="update"){
    $errorflag = 1;
    $resresp = "Ошибка! Не правильное действие";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();

}
//if($action!=="reg" && $action!=="rec" && $action!=="sub" && strlen($action) !== 3){

if($field!=="privphone" &&  $field!=="privpmail"&&  $field!=="newpassword"){
    $errorflag = 1;
    $resresp = "Ошибка! Недопустимое поле";
    echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
    die();
}






if($action=="update"){
    /*UPDATE*/

    //phone
    if( $field == "privphone" ){
        $field = "phone_cab";
    }else if($field == "privpmail"){
        $field = "email";
    }else if($field == "newpassword"){
        $field = "newpassword";
    }

    $updateuserinfores = updateuserinfo($iduser,$idsf,$field,$value);

    /*ТЕСТ ЗАПРОСА*/
    //echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$updateuserinfores));
    //die();

    if( $updateuserinfores==true ){
        $errorflag = 0;
        $resresp = "Данные успешно обновлены!";
        /*ТЕСТ ЗАПРОСА*/
        //$resresp = $updateuserinfores;
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }else{
        $errorflag = 1;
        $resresp = "Ошибка! Данные не были записаны. Обратитесь к администратору.";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }

    /*$rescheck = checknuminpot($phone);
    if($rescheck == false ){
        $errorflag = 1;
        $resresp = "Ошибка! Вы не можете быть зарегистрированы";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }
    $rescheck = checknuminexist($phone);
    if($rescheck == true ){
        $errorflag = 1;
        $resresp = "Ошибка! Вы уже зарегистрированы";
        echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
        die();
    }
    if($errorflag==0){
        if (newuser($phone)){
            $resresp = "Вы успешно зарегистрированы! Вам будет выслано СМС с паролем";
            echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>$resresp));
            die();
        }
    }*/


}


//echo json_encode(array("errorflag"=>$errorflag ,"answertxt"=>"ВСЕ ОК ИДЕМ ДАЛЬШЕ!"));
//die();

