<?
include_once("lib/cms_view_inc.php");

//phpinfo();

// удаление юзеров cabinet/index.php?&clearusers=1
if(isset($_GET["clearusers"])){
    //echo "clearusers";
    if(clearusers()){
        die("<h1>OK!</h1>");
    }
}
if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])){
    $checklogin = getchecklogin($_COOKIE['id'],$_COOKIE['hash']);
    //print_r ($_COOKIE);
    //print_r ($checklogin);
    if($checklogin){
        $userinfo = getuserinfo($_COOKIE['id']);
        $userprogr = getuserprogramms($userinfo[0]["sfid"]);
        //print_r($userinfo[0]);
        //print_r($userprogr);
    }else{
        //echo "EXIT!";
        header("Location: /index.php"); exit();
    }
}else{
    setcookie("id", "", time() - 3600*24*30*12, "/");
    setcookie("hash", "", time() - 3600*24*30*12, "/");
    //echo "STOP!";
    header("Location: /index.php"); exit();
}


//print_r($userinfo[0]["sfid"]);
//echo"<br>";
//print_r($userprogr);
?>

<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Личный кабинет</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="../assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.css">
    <link rel="stylesheet" href="../assets/css/style-tmpl.css">
    <link rel="stylesheet" href="../assets/css/style-cabinet.css">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">


    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="author" content="PotapovD">

</head>
<body>
    <header>
        <?include_once("../inc/header.inc.php");?>
    </header>
    <div id="cabinetpage">
        <div class="container-fluid bluebg">
            <div class="container" >
                <div class="row">
                    <div class="col-sm-12 col-sx-12">
                        <div class="textname">
                            контактные данные
                        </div>

                        <form action="/" method="post" id="cabinetuserinfoform">
                            <input type="hidden" id="code" value="<?=$userinfo[0]['id']?>" readonly="readonly">
                            <div class="col-sm-offset-1 col-sm-2 photocabinetblock">
                                <? if(!empty($userinfo[0]['photo'])){?>
                                    <img src="/photo/cabinet/<?=$userinfo[0]['photo']?>" class="img-responsive img-circle cabinetuserhoto"  alt="">
                                <?}else{?>
                                    <img src="/photo/cabinet/no-img.png" class="img-responsive img-circle cabinetuserhoto"  alt="">
                                <?}?>




                               <!-- <div id="kv-avatar-errors-2" class="center-block" style="display:none"></div>
                                <form class="text-center" action="/avatar_upload.php" method="post" enctype="multipart/form-data">
                                    <div class="kv-avatar center-block">
                                        <input id="avatar-2" name="avatar-2" type="file" class="file-loading" accept="image/*" data-show-preview="false">
                                    </div>
                                </form>-->





                            </div>
                            <div class="col-sm-4 col-xs-12 inputcabinetblock">

                                <div class="rowblocked">
                                    <input type="text" class="form-control inputcabinet inputcabinetdis inputcabinetnameparen" disabled value="<?=$userinfo[0]['name_par']?>" autocomplete="off" placeholder="Имя родителя" >
                                </div>
                                <input type="hidden" id="idcms" value="<?=$userinfo[0]['sfid']?>">
                                <div class="rowblockedtxt">
                                    <span class="rowblockedtxtlinetxt">сын</span>
                                    <span class="rowblockedtxtline"></span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="rowblocked">
                                    <input type="text" class="form-control inputcabinet inputcabinetdis inputcabinetchildparen" disabled value="<?=$userinfo[0]['name_chil']?>" autocomplete="off" placeholder="Имя ребенка" >
                                </div>

                            </div>
                            <div class="col-sm-4 col-xs-12">

                                <div class="rowblocked inputcabinetblock">
                                    <input type="text" class="form-control inputcabinet inputcabinetdis "  disabled value="<?=$userinfo[0]['phone_cab']?>" id="privphone" name="privphone" autocomplete="off" placeholder="Номер телефона" >
                                </div>
                                <div class="rowblockedtxt">
                                    <span class="rowblockedtxtlinetxt">&nbsp;</span>
                                    <span class="rowblockedtxtline"></span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="rowunblocked">
                                    <input type="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" class="form-control inputcabinet inputcabinetdis"  value="<?=$userinfo[0]['email']?>" id="privpmail" name="privpmail" autocomplete="off" placeholder="E-mail" >
                                </div>
                                <div class="rowunblocked">
                                    <input type="password" class="form-control inputcabinet inputcabinetdis"  id="newpassword" name="newpassword" autocomplete="off" placeholder="Изменить пароль" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" >
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-sm-12 nopadding">
                <div class="textnamewithline">
                    <span>программы</span>
                </div>
            </div>
        </div>

        <div class="container">
                <?
                $progrnumber = 0;
                foreach($userprogr as $key => $val){?>
                    <div id="progrline<?=$progrnumber;?>" class="progrline" style="">
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-5">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="proramlogo">
                                            <?=getprogrlogo($userprogr[$key]['name']);?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="proramname">
                                            <?=getprogrname($userprogr[$key]['name']);?>
                                            <?//=getprogrname("Старт победителя 3 ");?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 prorline">
                                <input type="text" id="countertrenslider" class="countertrenslider" name="countertren" data-min="1" data-max="<?=$userprogr[$key]['countbyprogr'];?>" data-from="<?=$userprogr[$key]['countbyprogr']-$userprogr[$key]['countleft'];?>" />
                                <div class="proramnamestatus">
                                    статус тренеровок
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-4 col-sm-7">
                            <!--
                            <?/*if(!empty($userinfo[0]['tren_nxt'])&&$userinfo[0]['tren_nxt']!=="0000-00-00"){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks">
                                    <div class="cabinetlinetxtname">
                                        следующая аттестация
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row nopadding">
                                            <div class="col-sm-4 hidden-xs">&nbsp;</div>
                                            <div class="col-sm-8 grayborder cabinetlinebox">
                                                <span class="cabinetlinenexattdt">31</span>
                                                <span class="cabinetlinenexattmont">августа</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?}*/?>
                            -->
                            <?//if( $userinfo[0]['tren_frz_lm'] >= 0){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks frizblock">
                                    <div class="cabinetlinetxtname">
                                        разовая заморозка
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row  mobgrayborder nopadding">
                                            <div class="col-sm-5 col-xs-5 cabinetlinenfreez countlimfreez">
                                                <span class="countlimfreeznumb"><?=$userprogr[$key]['maxfreezed']-$userprogr[$key]['freezed'];?></span>
                                                <span class="cabinetlinenfreezaddtext">шт</span>
                                            </div>
                                            <div class="col-sm-7 col-xs-7 desctrayborder cabinetlinebox" id="candlestickoneoutbox">
                                                <div class="freezcounterslid">
                                                    <span class="freezcounterslidinntxt freezcounterslidinntxtunlim">&nbsp;</span>
                                                    <div class="freezcounterslidinn">

                                                        <?if($userprogr[$key]['status']=="Freezed"){?>
                                                            <input class="js-candlestickone id-js-candlestick" id="" type="checkbox" name="candlestickone" data-progrnumb="<?=$progrnumber;?>" data-type="freezed" data-id="<?=$userprogr[$key]['idsf'];?>" disabled>
                                                        <?}else{?>
                                                            <?if($userprogr[$key]['maxfreezed']-$userprogr[$key]['freezed']>0){?>
                                                                <input class="js-candlestickone id-js-candlestick" id="" type="checkbox" name="candlestickone" data-progrnumb="<?=$progrnumber;?>" data-type="freezed" data-id="<?=$userprogr[$key]['idsf'];?>"
                                                                <?
                                                                //`freeztype`, `freezoff`
                                                                if(
                                                                        ($userprogr[$key]["freeztype"]=="Day")&&
                                                                        ($userprogr[$key]["freezoff"]>=date("Y-m-d"))
                                                                )

                                                                {echo "checked";}
                                                                if($userprogr[$key]["freeztype"]=="Unlimited"){echo "disabled";}?>
                                                                >
                                                            <?}else{?>
                                                                <input class="js-candlestickone id-js-candlestick" id="" type="checkbox" name="candlestickone" data-progrnumb="<?=$progrnumber;?>" data-type="freezed" data-id="<?=$userprogr[$key]['idsf'];?>" disabled>

                                                            <?}?>
                                                        <?}?>

                                                    </div>
                                                    <div class="locked"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?//}?>

                            <?//if( $userinfo[0]['tren_frz_unl'] >= 0){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks unlimfrizblock">
                                    <div class="cabinetlinetxtname">
                                        безлимтная заморозка
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row mobgrayborder nopadding">
                                            <div class="col-sm-5 col-xs-5 cabinetlinenfreez countunlimfreez">
                                                <span class="countunlimfreeznumb"><?=$userprogr[$key]['maxfreezedunlim']-$userprogr[$key]['freezedunlim'];?></span>
                                                <span class="cabinetlinenfreezaddtext">шт</span>
                                            </div>
                                            <div class="col-sm-7 col-xs-7 desctrayborder cabinetlinebox" id="candlestickunlimoutbox">

                                                <div class="freezcounterslid">
                                                    <span class="freezcounterslidinntxt freezcounterslidinntxtunlim">&nbsp;</span>
                                                    <div class="freezcounterslidinn">

                                                        <?if($userprogr[$key]['status']=="Freezed"){?>
                                                            <input class="js-candlestickunlim id-js-candlestickunlim" id="" type="checkbox" name="candlestickunlim" data-progrnumb="<?=$progrnumber;?>" data-type="freezedunlim" data-id="<?=$userprogr[$key]['idsf'];?>" disabled>
                                                        <?}else{?>
                                                            <?if($userprogr[$key]['maxfreezedunlim']-$userprogr[$key]['freezedunlim']>0){?>
                                                                <input class="js-candlestickunlim id-js-candlestickunlim" id="" type="checkbox" name="candlestickunlim" data-progrnumb="<?=$progrnumber;?>" data-type="freezedunlim" data-id="<?=$userprogr[$key]['idsf'];?>"
                                                                    <?
                                                                    //`freeztype`, `freezoff`
                                                                    if(
                                                                        $userprogr[$key]["freeztype"]=="Unlimited"
                                                                    ){echo "checked";}
                                                                    if($userprogr[$key]["freeztype"]=="Day"){echo "disabled";}?>

                                                                >
                                                            <?}
                                                            else if(
                                                                    ($userprogr[$key]['maxfreezedunlim']-$userprogr[$key]['freezedunlim']==0)&&
                                                                    $userprogr[$key]["freeztype"]=="Unlimited"
                                                            ){?>
                                                                <input class="js-candlestickunlim id-js-candlestickunlim" id="" type="checkbox" name="candlestickunlim" data-progrnumb="<?=$progrnumber;?>" data-type="freezedunlim" data-id="<?=$userprogr[$key]['idsf'];?>"
                                                                    <?
                                                                    //`freeztype`, `freezoff`
                                                                    if(
                                                                        $userprogr[$key]["freeztype"]=="Unlimited"
                                                                    ){echo "checked";}?>

                                                                >
                                                            <?}else{?>
                                                                <input class="js-candlestickunlim id-js-candlestickunlim" id="" type="checkbox" name="candlestickunlim" data-progrnumb="<?=$progrnumber;?>" data-type="freezedunlim" data-id="<?=$userprogr[$key]['idsf'];?>" disabled>
                                                            <?}?>

                                                        <?}?>

                                                    </div>
                                                </div>
                                                <div class="locked"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?//}?>

                        </div>
                        </div>
                        <?if($userprogr[$key]['status']=="Freezed"){?>
                            <div class="progrlinelock">&nbsp;</div>
                        <?}?>
                    </div>
                <?
                    $progrnumber++;
                }?>
                <!--<div class="row">
                    <div class="col-sm-offset-1 col-sm-5">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="proramlogo">
                                    <img src="../assets/img/logo4cab_fp.jpg" class="img-responsive img-circle" alt="">
                                </div>

                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="proramname">
                                    <?=$userinfo[0]['progr_name']?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 prorline">
                        <input type="text" id="countertrenslider" name="countertren" data-min="1" data-max="<?=$userinfo[0]['tren_all']+$userinfo[0]['tren_left']?>" data-from="<?=$userinfo[0]['tren_left']?>" />
                        <div class="proramnamestatus">
                            статус тренеровок
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-4 col-sm-7">

                            <?if(!empty($userinfo[0]['tren_nxt'])&&$userinfo[0]['tren_nxt']!=="0000-00-00"){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks">
                                    <div class="cabinetlinetxtname">
                                        следующая аттестация
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row nopadding">
                                            <div class="col-sm-4 hidden-xs">&nbsp;</div>
                                            <div class="col-sm-8 grayborder cabinetlinebox">
                                                <span class="cabinetlinenexattdt">31</span>
                                                <span class="cabinetlinenexattmont">августа</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                            <?if( $userinfo[0]['tren_frz_lm'] >= 0){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks">
                                    <div class="cabinetlinetxtname">
                                        разовая заморозка
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row  mobgrayborder nopadding">
                                            <div class="col-sm-5 col-xs-5 cabinetlinenfreez">
                                                <?=$userinfo[0]['tren_frz_lm'];?>
                                                <span>шт</span>
                                            </div>
                                            <div class="col-sm-7 col-xs-7 desctrayborder cabinetlinebox" id="candlestickoneoutbox">
                                                <div class="freezcounterslid">
                                                    <span class="freezcounterslidinntxt freezcounterslidinntxtunlim">&nbsp;</span>
                                                    <div class="freezcounterslidinn">
                                                        <input class="js-candlestickone" id="id-js-candlestick" type="checkbox" name="candlestickone">
                                                    </div>
                                                    <div class="locked"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>

                            <?if( $userinfo[0]['tren_frz_unl'] >= 0){?>
                                <div class="col-sm-4 col-xs-12 lastrowblocks">
                                    <div class="cabinetlinetxtname">
                                        безлимтная заморозка
                                    </div>
                                    <div class="cabinetlinenexatt">
                                        <div class="row mobgrayborder nopadding">
                                            <div class="col-sm-5 col-xs-5 cabinetlinenfreez">
                                                <?=$userinfo[0]['tren_frz_unl'];?>
                                                <span>шт</span>
                                            </div>
                                            <div class="col-sm-7 col-xs-7 desctrayborder cabinetlinebox" id="candlestickunlimoutbox">

                                                <div class="freezcounterslid">
                                                    <span class="freezcounterslidinntxt freezcounterslidinntxtunlim">&nbsp;</span>
                                                    <div class="freezcounterslidinn">
                                                        <input class="js-candlestickunlim" id="id-js-candlestickunlim" type="checkbox" name="candlestickunlim">
                                                    </div>
                                                </div>
                                                <div class="locked"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>

                    </div>
                </div>-->

                <div class="row">
                    <div id="allwarnings">
                        <div id="tofreezblokunlimouton">
                            <div class="row warningfreezing">
                                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                                    <div class="warhead">Включить безлимитную заморозку?</div>
                                    <div class="warline1">
                                        Вы можете включить заморозку на неограниченное время, но <b>место в группе не
                                            будет зарезервировано</b> за вами.<br>
                                        После выключения заморозки, вам нужно будет сконтактировать с call-центром
                                        и выбрать свободную дату и место в группе. <br><br>
                                        У вас осталось еще
                                        <span id="tofreezblokunlimcoutfreez" class="coutfreezwarline1"></span>
                                        заморозки.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tofreezblokunlimoutoff">
                            <div class="row warningfreezing">
                                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                                    <div class="warhead">Выключить безлимитную заморозку?</div>
                                    <div class="warline1">
                                        Вы можете выключить заморозку, но вам нужно будет позвонить в сall-центр по номеру 8-800-707-88-99 и выбрать свободную дату и место в группе.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tofreezblokouton">
                            <div class="row warningfreezing">
                                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                                    <div class="warhead">Включить разовую заморозку?</div>
                                    <div class="warline1">
                                        Ваше место в группе останется за вами.<br>
                                        За одно включение разовой заморозки, вы можете пропустить одно занятие. Подтверждение о включении прийдет в СМС<br>

                                        Дата заморозки заняитя <div id="freznexttran" style="font-weight: 900; color: #137ACD"></div>
                                        У вас осталось еще
                                        <span id="tofreezblokcoutfreez" class="coutfreezwarline1"></span>
                                        заморозок.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tofreezblokoutoff">
                            <div class="row warningfreezing">
                                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                                    <div class="warhead">Выключить разовую заморозку?</div>
                                    <div class="warline1">
                                        Вы можете выключить заморозку или она отключится автоматически.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="container-fluid nopadding" id="infoblock">
            <div class="col-sm-12 nopadding">
                <div class="infoblockout">
                    <!--<div class="infoblockin errortxt"></div>
                    <div class="infoblockin noerrortxt"></div>-->
                    <div class="infoblockin"></div>
                </div>
            </div>
        </div>

        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("../inc/footer.inc.php");?>
        </div>
    </div>

    <script src="../assets/js/jquery-1.12.4.min.js"></script>

    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.sticky.js"></script>
    <script src="../assets/js/jPushMenu.js"></script>
    <script src="../assets/js/metisMenu.min.js"></script>

    <script src="../assets/js/ion.rangeSlider.min.js"></script>
    <script src="../assets/js/jquery.mask.min.js"></script>
    <script src="../assets/js/lc_switch.js"></script>
    <script src="../assets/js/vex.combined.min.js"></script>

    <script src="../assets/js/fileinput.min.js"></script>
    <script src="../assets/js/fileinput.ru.js"></script>

    <script src="../assets/js/jquery.cookie.js"></script>



<script>

    function showViewPortSize(display) {
        if(display) {
            var height = jQuery(window).height();
            var width = jQuery(window).width();
            jQuery('body').prepend('<a href="log.html" id="viewportsize" style="z-index:9999;position:fixed;bottom:10px;right:5px;color:#fff;background:#000;padding:3px;text-align:center; font-size: 10px;opacity:0.5;display:block;">'+width+' x '+height+'<br>0.1.4</a>');
            jQuery(window).resize(function() {
                height = jQuery(window).height();
                width = jQuery(window).width();
                $('#viewportsize').html(width+' x '+height+'');
                //$(".irs-addtext").css("left",$(".irs-slider").css("left"))
            });
            //$(".col-menu,.col-widgets .widgetsinner").css("height",$(".col-conten").outerHeight()+32 );
        }
    }

    function randomInteger(min, max) {
        var rand = min + Math.random() * (max - min);
        rand = Math.round(rand);
        return rand;
    }

    function updateuserinfo($iduser, $idusercms, $action, $field, $value) {
        //alert($action+" "+$field+" "+$value);
        $.ajax({
            statusCode: {
                404: function () {
                    viewmMsg("Произошла ошибка при отключении заморозки", 1);
                }
            },
            url: 'cabinet-action.php',
            type: 'post',
            data: {'iduser': $iduser, 'idusercms': $idusercms, 'action': $action, 'field': $field, 'value': $value},
            dataType: 'JSON',
            crossDomain: false,
            timeout: 10000,
            beforeSend: function (res) {
                viewmMsg("Выполняется обновление данных...", 0);
                console.log("обновление данных - отправление запроса");
            }, success: function (res) {
                if (res.errorflag == 0) {
                    viewmMsg(res.answertxt, 0)
                    console.log("Данные успешно обновлены");
                } else {
                    viewmMsg(res.answertxt, 1)
                    console.log("Произошла ошибка при обновлении данных");
                    //location.reload();
                }
            },error: function (res) {
                viewmMsg("Произошла ошибка при обновлении данных - Timeout", 1);
                console.log("отлючение безлимитной заморозки - запрос провалился(тайаут)");
                console.log(res);
                //location.reload();
            }
        });
        return true
    }

    function viewmMsg(msg,errorflag){
        //$(".infoblockin").fadeOut("errortxt").removeClass("noerrortxt");
        $("#infoblock").stop(true,true).fadeOut();
        $(".infoblockin").stop(true,true).html("").removeClass("errortxt").removeClass("noerrortxt");
        if(errorflag==0){
            $(".infoblockin").html(msg).addClass("noerrortxt");

        }else{
            $(".infoblockin").html(msg).addClass("errortxt");
        }
        $("#infoblock").fadeIn("slow").delay(9000).queue(function () {
            $(this).fadeOut();
            $(".infoblockin").stop(true,true).html("").removeClass("errortxt").removeClass("noerrortxt");
            $(this).dequeue();
        });
        return true;
    }



    $(document).ready(function() {
        //initialise();



        /*TOP MENU*/
        $("#topmenu").sticky({
            topSpacing: 0,
            zIndex:200,
            center:true,
            responsiveWidth:true
        });
        $('#topmenu').on('sticky-start', function() {
            $('#topmenulogo,#topmenulogomob').attr("src","/assets/img/logo-small.svg");
            $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='/assets/img/logo-small.png'");
        });
        $('#topmenu').on('sticky-end', function() {
            $('#topmenulogo,#topmenulogomob').attr("src","/assets/img/logo.svg");
            $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='/assets/img/logo.png'");
            $("#topmenu").sticky({
                topSpacing: 0,
                zIndex:200,
                center:true,
                responsiveWidth:true
            });
        });
        var lastScrollTop = 0;
        $(window).scroll(function(event){
            var st = $(this).scrollTop();
            if (st > lastScrollTop){
                var st2 = st - lastScrollTop;
                if (st2>10){
                    $("#mandectopmaenu").slideUp();
                }
                //console.log("down");
            } else {
                $("#mandectopmaenu").slideDown();
                //console.log("up");
            }
            lastScrollTop = st;
        });
        $('.toggle-menu').jPushMenu({
            closeOnClickLink: false
        });
        $('#menu').metisMenu();

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

            $(".topmenumobile").css("display","block");

            $(".desctfooter").css("display","none");
            $("#mobilefooter").show("slow");
            $(".socblock").css("padding-bottom","60px");
            $("#infoblock").css("bottom","50px");
        }else{
            $(".topmenudesctop").css("display","block");
            //$("#mobilefooter").show("slow");
        }
        $('#mapmob').click(function(){
            var collapse_content_selector = $(this).attr('href');
            $(collapse_content_selector).slideToggle(function(){
                if($(this).css('display')=='none'){
                    //toggle_switch.html('Show');
                }else{
                    //toggle_switch.html('Hide');
                }
            });
            return false;
        });


        $(".countertrenslider").ionRangeSlider({
            //min: 1,
            //max: 80,
            //grid: true,
            //from:randomInteger(1, 80),
            //from:81,
            disable: true
            //postfix:"тренировок пройдено"
            //from: 550
        });



        var timerid2;
        $('#privphone').mask('+7(000) 000 00 00',{

            /*onKeyPress: function(value){
                //var value = a;
                if($(this).data("lastval")!= value){

                    $(this).data("lastval",value);
                    clearTimeout(timerid2);

                    timerid2 = setTimeout(function() {
                        //change action
                        //alert(inpidvalue+" "+value);
                        if(value.length<17){
                            //console.log("too schort");
                            $(".infoblockin").html("Неправильный номер телефона").addClass("errortxt").stop();
                            $("#infoblock").stop().fadeIn("slow").delay(3000).queue(function () {
                                $(this).stop().fadeOut().stop();
                                $(".infoblockin").stop().delay(100).html("").removeClass("errortxt").stop();
                                $(this).dequeue();
                            });
                            return false
                        }else{
                            console.log(" updateuserinfo(1,update,privphone,"+value+") ");
                            updateuserinfo(1,'update','privphone',value);
                            return true
                        }
                    },1500);
                }
            }*/


        });

        var timerid;
        $(".inputcabinetdis").on("input",function(e){
            var value = $(this).val();
            var inpidvalue = $(this).attr("id");
            if(inpidvalue!=="privphone"){
                if($(this).data("lastval")!= value){

                    $(this).data("lastval",value);
                    clearTimeout(timerid);
                    timerid = setTimeout(function() {

                        //console.log("update #"+inpidvalue+": "+value)
                        $usercode  = $("#code").val();
                        $usercodecms  = $("#idcms").val();
                        if(inpidvalue=="privpmail"){

                            if(! (/^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/.test(value))){
                                viewmMsg("Вы ввели неверный E-MAIL!", 1)
                                //alert('Пароль должен состоять из латинских букв и цифр!');
                                value.focus();
                                return false;
                            }else{
                                updateuserinfo($usercode,$usercodecms,'update','privpmail',value);
                            }
                        }else if(inpidvalue=="newpassword"){
                            if(value.length<7){
                                viewmMsg("Пароль должен состоять мининум из 7 символов", 1)
                                value.focus();
                                return false;
                            }
                            if(! (/^[a-zA-Z0-9]+$/.test(value))){
                                viewmMsg("Пароль должен состоять из латинских букв и цифр!", 1)
                                //alert('Пароль должен состоять из латинских букв и цифр!');
                                value.focus();
                                return false;
                            }else{
                                updateuserinfo($usercode,$usercodecms,'update','newpassword',value);
                            }
                        }


                        return true;


                    },1000);
                }
            }
        });




        //var btnCust = '<button type="button" class="btn btn-default" title="Add picture tags" ' +
        //    'onclick="alert(\'Call your custom code here.\')">' +
        //    '<i class="glyphicon glyphicon-tag"></i>' +
        //    '</button>';
        var imgupload = "imgimg.jpg";
        /*
        */
        $("#avatar-2").fileinput({
            uploadUrl: "cabinet-avatar-upload.php", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            language: 'ru',
            showDelete: false,
            showZoom: false,
            //showBrowse: false,
            showCancel: false,
            autoReplace: true,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            //browseOnZoneClick: true,
            //removeLabel: '',
            //removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            //removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: ' <img src="/photo/cabinet/no-img.png" class="img-responsive img-circle cabinetuserhoto" alt="">',
            //layoutTemplates: {main2: '{preview}  {browse}'},
            allowedFileExtensions: ["jpg", "png"],
            initialPreviewAsData: true,
            uploadExtraData: function() {
                return {
                    code: $("#code").val()
                }
            }
            //maxFilePreviewSize: 50
        }).on('fileuploaded', function(event, file, previewId, index, reader) {
            console.log(event);
            console.log(file);
            console.log(previewId);
            console.log(index);
            console.log(reader);
            //$('.photocabinetblock img').attr("src","").attr("src","/photo/cabinet/<?=$userinfo[0]['photo']?>.jpg");
            //$('.photocabinetblock img').attr("src","/photo/cabinet/<?=$userinfo[0]['photo']?>.jpg");
            console.log("fileloaded");
        }).on("filebatchselected", function(event, files) {
            // trigger upload method immediately after files are selected
            $("#avatar-2").fileinput("upload");
            console.log(event);
            console.log(files);
            //$('.photocabinetblock img').attr("src","").attr("src","/photo/cabinet/<?=$userinfo[0]['photo']?>.jpg");
        }).on('fileuploaded', function(event, data, previewId, index) {
            var response = data.response;
            //alert(response.uploaded)
            //alert(response.filename)
            $('img.cabinetuserhoto ').attr("src","").attr("src","/photo/cabinet/"+response.filename);
            //$('img.cabinetuserhoto ').attr("src","/photo/cabinet/"+response.filename);
            /*alert(response.uploaded);
            if(response.ret ) {
                alert("upload success!"+data.response.data);
            }else{
                alert("upload failed!"+response.errmsg)
            }
            alert('File uploaded triggered'+form+"response:"+response);
            console.info(response.data);*/
        });






        //cabinetlogout
        $("#cabinetlogout").on("click",function(e){
            //alert('logout');
            $.removeCookie('id');
            $.removeCookie('hash');
            location.reload();
            //header("Location: /index.php"); exit();
            return false
        });


        /*//var $status = $('#status'),
        var phonebox = $('#privpmail'),
            timeoutId;

        phonebox.on('input', function() {
            //alert("1");
            // do something
            if (timeoutId) clearTimeout(timeoutId);
            //alert("2");

            // Set timer that will save comment when it fires.
            timeoutId = setTimeout(function () {
                alert("3");
                // Make ajax call to save data.
                //$status.attr('class', 'saved').text('changes saved');
                console.log("SAVED!")
            }, 750);
        });*/




        function checklockfreez(){
            /*if($(".id-js-candlestickunlim").is(':checked')){
                //alert("  candlestickunlim  ");
                $("#candlestickoneoutbox .locked").css("z-index","100");

            }else{
                $("#candlestickoneoutbox .locked").css("z-index","-999");
            }

            if($(".id-js-candlestick").is(':checked')){
                //alert("   candlestick   ");
                $("#candlestickunlimoutbox .locked").css("z-index","100");
            }else{
                $("#candlestickunlimoutbox .locked").css("z-index","-999");
            }*/

            $(".id-js-candlestickunlim").each(function(indx){
                //heights.push($(this).height());



                if($(this).is(':checked')){
                    //$(this).parent().hide();
                    //alert("   candlestick   ");
                    //$("#candlestickunlimoutbox .locked").css("z-index","100");
                }else{
                    //$("#candlestickunlimoutbox .locked").css("z-index","-999");
                }



            });




        }

        vex.defaultOptions.className = 'vex-theme-plain vex-custom';
        vex.defaultOptions.showCloseButton = true;
        vex.defaultOptions.closeClassName = "iconmenuclose";


        //БЕЗЛИМИТНАЯ ЗАМОРОЗКА
        $('.id-js-candlestickunlim').lc_switch('вкл', 'выкл');
        $('body').delegate('.id-js-candlestickunlim', 'lcs-statuschange', function () {
            var status = ($(this).is(':checked')) ? 'checked' : 'unchecked';
            var programnumber = $(this).attr("data-progrnumb");

            //отключение безлимитной заморозки
            if (status == "unchecked") {
                var str = $("#tofreezblokunlimoutoff").html();
                vex.dialog.buttons.YES.text = 'Выключить';
                vex.dialog.buttons.NO.text = 'Отмена';
                vex.dialog.open({
                    input: str,
                    message: "",
                    callback: function (data) {
                        //console.log(data);
                        if (data === false) {
                            $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                        } else {
                            console.log("отлючение безлимитной заморозки - переключение кнопки");
                            var useraction = "ufz";
                            var useridcms = $("#idcms").val();
                            var freznexttran = "";
                            var progridcms = $("#progrline" + programnumber + " .frizblock input").attr("data-id");
                            var freeztype = "unlimunfreez";
                            $.ajax({
                                data: {
                                    'action': useraction,
                                    'useridcms': useridcms,
                                    'progridcms': progridcms,
                                    'freeztype': freeztype,
                                    'freznexttran': freznexttran
                                },
                                statusCode: {
                                    404: function () {
                                        viewmMsg("Произошла ошибка при отключении заморозки", 1);
                                    }
                                },
                                url: 'cabinet.php',
                                type: 'post',
                                cache: false,
                                dataType: 'JSON',
                                crossDomain: false,
                                timeout: 10000,
                                beforeSend: function (res) {
                                    viewmMsg("Выполняется проверка данных...", 0);
                                    console.log("отлючение безлимитной заморозки - отправление запроса");
                                    $("#progrline" + programnumber + " .id-js-candlestickunlim").attr('disabled', 'disabled').lcs_destroy();
                                    $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                }, success: function (res) {
                                    var response = res.answertxt;
                                    if (res.errorflag == 0) {
                                        console.log("отлючение безлимитной заморозки - запрос прошел успешно");
                                        viewmMsg(response, 0);
                                        var numbcabinetlinenfreez = $("#progrline" + programnumber + " .countunlimfreez").html();
                                        numbcabinetlinenfreez = numbcabinetlinenfreez.toString().replace(/[^\d.]/ig, '').trim();
                                        $("#progrline" + programnumber + " .id-js-candlestick").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                        if (numbcabinetlinenfreez - 1 <= 0) {
                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").attr('disabled', 'disabled').lcs_destroy();
                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                        }
                                    } else {
                                        viewmMsg(response, 1);
                                        console.log("отлючение безлимитной заморозки - запрос провалился");
                                        $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                    }
                                }, error: function (res) {
                                    viewmMsg("Произошла ошибка при отключении заморозки - Timeout", 1);
                                    console.log("отлючение безлимитной заморозки - запрос провалился(тайаут)");
                                    console.log(res);
                                    $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                    $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                    $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                }
                            });
                        }
                    }
                });
            } else {
                //включение безлимитной заморозки
                if ($("#progrline" + programnumber + "  .countunlimfreeznumb").html() > 0) {
                    var numbcabinetlinenfreez = $("#progrline" + programnumber + " .countunlimfreez").html();
                    numbcabinetlinenfreez = numbcabinetlinenfreez.toString().replace(/[^\d.]/ig, '').trim();
                    $("#tofreezblokunlimouton #tofreezblokunlimcoutfreez").html("").html(numbcabinetlinenfreez);
                    var str = $("#tofreezblokunlimouton").html();
                    vex.dialog.buttons.YES.text = 'Включить';
                    vex.dialog.buttons.NO.text = 'Отмена';
                    vex.dialog.open({
                        input: str,
                        message: "",
                        callback: function (data) {
                            if (data === false) {
                                $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                            } else {
                                console.log("включение безлимитной заморозки - переключение кнопки");
                                var useraction = "frz";
                                var useridcms = $("#idcms").val();
                                var freznexttran = "";
                                var progridcms = $("#progrline" + programnumber + " .frizblock input").attr("data-id");
                                var freeztype = "unlimfreez";
                                $.ajax({
                                    data: {
                                        'action': useraction,
                                        'useridcms': useridcms,
                                        'progridcms': progridcms,
                                        'freeztype': freeztype,
                                        'freznexttran': freznexttran
                                    },
                                    statusCode: {
                                        404: function () {
                                            viewmMsg("Произошла ошибка при включении заморозки", 1);
                                        }
                                    },
                                    url: 'cabinet.php',
                                    type: 'post',
                                    cache: false,
                                    dataType: 'JSON',
                                    crossDomain: false,
                                    timeout: 10000,
                                    beforeSend: function (res) {
                                        viewmMsg("Выполняется проверка данных...", 0);
                                        console.log("включение безлимитной заморозки - отправление запроса");
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").attr('disabled', 'disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                    }, success: function (res) {
                                        var response = res.answertxt;
                                        if (res.errorflag == 0) {
                                            viewmMsg(response, 0);
                                            console.log("включение безлимитной заморозки - запрос прошел успешно");
                                            $("#progrline" + programnumber + "  .countunlimfreeznumb").html(numbcabinetlinenfreez - 1);
                                            $("#progrline" + programnumber + " .id-js-candlestick").attr('disabled', 'disabled').lcs_destroy();
                                            $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');

                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                        } else {
                                            viewmMsg(response, 1);
                                            console.log("включение безлимитной заморозки - запрос провалился");
                                            $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                            $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');
                                        }
                                        return true;
                                    }, error: function (res) {
                                        viewmMsg("Произошла ошибка при включении заморозки - Timeout", 1);
                                        console.log("включение безлимитной заморозки - запрос провалился(тайаут)");
                                        console.log(res);
                                        $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');

                                    }
                                });
                            }
                        }
                    });
                } else {
                    viewmMsg("Произошла ошибка при включении заморозки 1", 1);
                    $(".id-js-candlestickunlim[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                }
            }
        });

        //ОДНОРАЗОВАЯ ЗАМОРОЗКА
        $('.id-js-candlestick').lc_switch('вкл', 'выкл');
        $('body').delegate('.id-js-candlestick', 'lcs-statuschange', function () {
            var status = ($(this).is(':checked')) ? 'checked' : 'unchecked';
            var programnumber = $(this).attr("data-progrnumb");

            //отключение разовой заморозки
            if (status == "unchecked") {
                var str = $("#tofreezblokoutoff").html();
                vex.dialog.buttons.YES.text = 'Выключить';
                vex.dialog.buttons.NO.text = 'Отмена';
                vex.dialog.open({
                    input: str,
                    message: "",
                    callback: function (data) {
                        if (data === false) {
                            $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                        } else {
                            console.log("отлючение разовой заморозки - переключение кнопки");
                            var useraction = "ufz";
                            var useridcms = $("#idcms").val();
                            var freznexttran = "";
                            var progridcms = $("#progrline" + programnumber + " .frizblock input").attr("data-id");
                            var freeztype = "limunfreez";
                            $.ajax({
                                data: {
                                    'action': useraction,
                                    'useridcms': useridcms,
                                    'progridcms': progridcms,
                                    'freeztype': freeztype,
                                    'freznexttran': freznexttran
                                },
                                url: 'cabinet.php',
                                type: 'post',
                                cache: false,
                                statusCode: {
                                    404: function () {
                                        viewmMsg("Произошла ошибка при отключении заморозки", 1);
                                    }
                                },
                                dataType: 'JSON',
                                timeout: 20000,
                                crossDomain: false,
                                beforeSend: function (res) {
                                    viewmMsg("Выполняется проверка данных...", 0);
                                    console.log("отлючение разовой заморозки - отправление запроса");
                                    $("#progrline" + programnumber + " .id-js-candlestick").attr('disabled', 'disabled').lcs_destroy();
                                    $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл')
                                },
                                success: function (res) {
                                    var response = res.answertxt;

                                    if (res.errorflag == 0) {
                                        var numbcabinetlinenfreez = $("#progrline" + programnumber + " .countlimfreeznumb").html();
                                        numbcabinetlinenfreez = parseInt(numbcabinetlinenfreez);
                                        viewmMsg(response, 0);
                                        console.log("отлючение разовой заморозки - успешная обработка ");


                                        $("#progrline" + programnumber + " .countlimfreeznumb").html(numbcabinetlinenfreez + 1);

                                        $("#progrline" + programnumber + " .id-js-candlestick").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                        $("#progrline" + programnumber + " .js-candlestickunlim").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .js-candlestickunlim").lc_switch('вкл', 'выкл');


                                    } else {
                                        //$("#errortxtresform span").html("");
                                        viewmMsg(response, 1);
                                        console.log("включение разовой заморозки - запрос провалился сервер вернул ошибку ");
                                        $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                        $("#progrline" + programnumber + " .id-js-candlestick").removeAttr('disabled').lcs_destroy();
                                        $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                    }

                                }, error: function (res) {
                                    viewmMsg("Произошла ошибка при отлючении заморозки - Timeout", 1);
                                    console.log("включение разовой заморозки - запрос провалился(тайаут)");
                                    console.log(res);
                                    $("#progrline" + programnumber + " .id-js-candlestick").removeAttr('disabled').lcs_destroy();
                                    $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                    $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                }
                            });
                        }
                    }
                });
            } else {
                //включение разовой заморозки
                if ($("#progrline" + programnumber + " .countlimfreeznumb").html() > 0) {
                    var useraction = "nxtt";
                    var useridcms = $("#idcms").val();
                    var progridcms = $("#progrline" + programnumber + " .frizblock input").attr("data-id");
                    viewmMsg("Выполняется проверка данных...", 0);
                    $("#progrline" + programnumber + " .id-js-candlestick").attr('disabled', 'disabled').lcs_destroy();
                    $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');

                    setTimeout(function () {
                        $.ajax({
                            data: {'action': useraction, 'useridcms': useridcms, 'progridcms': progridcms},
                            statusCode: {
                                404: function () {
                                    viewmMsg("Произошла ошибка при включении заморозки", 1);
                                }
                            },
                            url: 'cabinet.php',
                            type: 'post',
                            cache: false,
                            dataType: 'JSON',
                            //crossDomain: false,
                            //timeout: 10000,
                            async: false,
                            beforeSend: function (res) {
                                console.log("включение разовой заморозки - отправление запроса даты");
                                $("#progrline" + programnumber + " .id-js-candlestick").attr('disabled', 'disabled').lcs_destroy();
                                $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                            }, success: function (res) {
                                var response = res.answertxt;
                                if (res.answertxt == "No records found") {
                                    viewmMsg("Ошибка! Не удалось получить дату следующей тренеровки", 1);
                                    console.log("включение разовой заморозки - провал запроса нет даты след. тренеровки");
                                    $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                    $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                    $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel("checked");
                                    return false;
                                } else {
                                    $("#freznexttran").html(response);
                                    var numbcabinetlinenfreez = $("#progrline" + programnumber + " .countlimfreez").html();
                                    numbcabinetlinenfreez = numbcabinetlinenfreez.toString().replace(/[^\d.]/ig, '').trim();
                                    $("#tofreezblokouton #tofreezblokcoutfreez").html("").html(numbcabinetlinenfreez);
                                    var str = $("#tofreezblokouton").html();
                                    vex.dialog.buttons.YES.text = 'Включить';
                                    vex.dialog.buttons.NO.text = 'Отмена';
                                    vex.dialog.open({
                                        input: str,
                                        message: "",
                                        callback: function (data) {
                                            if (data === false) {
                                                $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                                $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                                $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel(status);
                                            } else {
                                                console.log("включение разовой заморозки - отправление запроса заморозки");
                                                var useraction = "frz";
                                                var useridcms = $("#idcms").val();
                                                var freznexttran = $("#freznexttran").html();
                                                var progridcms = $("#progrline" + programnumber + " .frizblock input").attr("data-id");
                                                var freeztype = "limfreez";
                                                setTimeout(function () {
                                                    $.ajax({
                                                        data: {
                                                            'action': useraction,
                                                            'useridcms': useridcms,
                                                            'progridcms': progridcms,
                                                            'freeztype': freeztype,
                                                            'freznexttran': freznexttran
                                                        },
                                                        statusCode: {
                                                            404: function () {
                                                                viewmMsg("Произошла ошибка при включении заморозки", 1);
                                                            }
                                                        },
                                                        url: 'cabinet.php',
                                                        type: 'post',
                                                        cache: false,
                                                        dataType: 'JSON',
                                                        crossDomain: false,
                                                        timeout: 10000,
                                                        beforeSend: function (res) {
                                                            viewmMsg("Выполняется заморозка....", 0);
                                                            console.log("включение разовой заморозки - отправление запроса даты");
                                                        }, success: function (res) {
                                                            var response = res.answertxt;
                                                            console.log(res.answertxt + " " + res.errorflag);

                                                            if (res.errorflag == 0) {

                                                                viewmMsg(response, 0);
                                                                console.log("включение разовой заморозки - запрос был успешный");
                                                                $("#progrline" + programnumber + "  .countlimfreeznumb").html(numbcabinetlinenfreez - 1);
                                                                $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                                                $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                                                $("#progrline" + programnumber + " .id-js-candlestickunlim").attr('disabled', 'disabled').lcs_destroy();
                                                                $("#progrline" + programnumber + " .id-js-candlestickunlim").lc_switch('вкл', 'выкл');

                                                            } else {
                                                                viewmMsg(response, 1);
                                                                console.log("включение разовой заморозки - запрос провалился сервер вернул ошибку");
                                                                $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel("checked");
                                                                $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                                                $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                                            }
                                                        }, error: function (res) {
                                                            viewmMsg("Произошла ошибка при включении заморозки - Timeout", 1);
                                                            console.log("включение разовой заморозки - запрос провалился(тайаут)");
                                                            console.log(res);
                                                            $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel("checked");
                                                            $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                                            $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                                                        }
                                                    });
                                                }, 500);
                                            }
                                        }
                                    });
                                }
                            }, error: function (res) {
                                viewmMsg("Произошла ошибка при включении заморозки - Timeout", 1);
                                console.log("включение разовой заморозки - запрос провалился(тайаут)");
                                console.log(res);
                                $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel("checked");
                                $("#progrline" + programnumber + " .id-js-candlestick").removeAttr("disabled").lcs_destroy();
                                $("#progrline" + programnumber + " .id-js-candlestick").lc_switch('вкл', 'выкл');
                            }
                        });
                    }, 500);
                } else {
                    viewmMsg("Произошла ошибка при включении заморозки", 1);
                    $(".id-js-candlestick[data-progrnumb='" + programnumber + "']").lcs_cancel("checked");
                }
            }
        });

        checklockfreez();

        //setTimeout(showViewPortSize(1), 5000);
    })
    $("#freezdate").click(function(){
        alert("GO!8")
    })

</script>

</body>
</html>
