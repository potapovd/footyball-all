<?php
function getlang($lang){

	$langru = array(
		"lang"							    =>	"ru",
		"htmllang"						    =>	"ru",
		"all-up" 						    =>	"ВВЕРХ",
		"clients" 						    =>	"Наши клиенты",
		"mmabout" 						    =>	"О компании",
		"mmservices" 						=>	"Услуги",
		"mmportfilio" 						=>	"Портфолио",
        "mmcontacts" 						=>	"Контакты",
        "mmnews" 						    =>	"Новости",
        "mmvideo" 						    =>	"Видео",
        "mmvacancy" 						=>	"Вакансии",

        "popm1" 						    =>	"О нас",
        "popm2" 						    =>	"«UAT»",
        "popm3" 						    =>	"Цели и принципы",
        "popm4" 						    =>	"Виды работ",

        "pops1" 						    =>	"Архитектура ",
        "pops2" 						    =>	"Генеральное проектирование",
        "pops3" 						    =>	"Дизайн интерьера",
        "pops4" 						    =>	"Сферы деятельности",
        "pops5" 						    =>	"Дью Дилидженс",

        "blogmore" 						    =>	"подробнее...",

        "portfallcat" 						=>	"Все"
	);
	$langen = array(
		"lang"							    =>	"en",
		"htmllang"						    =>	"en",
		"all-up" 						    =>	"UP",
		"clients" 						    =>	"Our customers",
        "mmabout" 						    =>	"About us",
        "mmservices" 						=>	"Services",
        "mmportfilio" 						=>	"Portfolio",
        "mmcontacts" 						=>	"Contacts",
        "mmnews" 						    =>	"News",
        "mmvideo" 						    =>	"Video",
        "mmvacancy" 						=>	"Vacancy",

        "popm1" 						    =>	"About us",
        "popm2" 						    =>	"«UAT»",
        "popm3" 						    =>	"Objectives and principles",
        "popm4" 						    =>	"Types of work",

        "pops1" 						    =>	"Architecture",
        "pops2" 						    =>	"General planning",
        "pops3" 						    =>	"Interior Design",
        "pops4" 						    =>	"Sphere of activity",
		"pops5" 						    =>	"Due diligence report",

        "blogmore" 						    =>	"more...",

        "portfallcat" 						=>	"All"


	);
    $langch = array(
        "lang"							=>	"en",
        "htmllang"						=>	"en",
        "all-up" 						=>	"UP",
        "all-up" 						=>	"UP",

    );
    $langar = array(
        "lang"							=>	"en",
        "htmllang"						=>	"en",
        "all-up" 						=>	"UP",
        "all-up" 						=>	"UP",

    );
	
	if($lang=="ru"){
		return $langru;
	}elseif($lang=="en"){
		return $langen;
	}elseif($lang=="ch"){
        return $langch;
    }elseif($lang=="ar"){
        return $langar;
    }else{
        return $langru;
    }

}
?>