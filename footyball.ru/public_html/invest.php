<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Вложи Свои Деньги Под 24.50% Годовых</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-video.css">
    <link rel="stylesheet" href="assets/css/style-invest.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">


    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody ">

        <div class="container-fluid blu
        ebg">
            <div class="container">
                <div class="row">
                    <div class=" col-sm-offset-1 col-sm-10 col-sx-12">

                        <div class="blok_00-2 ">
                            <div class="blok_01">
                                Заставь Свои Деньги Работать <br>
                                Максимально Эффективно
                            </div>

                            <div class="calcblock">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="calclftblock">
                                            <div class="calcnameblock">Хочу Заработать</div>
                                            <div role="tabpanel">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" id="razvgodtab">
                                                        <a href="#razvgod" aria-controls="razvgod" role="tab" data-toggle="tab" id="razvgodbutt">капитализация <br>процентов</a>
                                                    </li>
                                                    <li role="presentation" class="active" id="ejemesiachnotab">
                                                        <a href="#ejemesiachno" aria-controls="ejemesiachno" role="tab" data-toggle="tab" id="ejemesiachnobutt">выплата процентов <br>ежемесячно</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane" id="razvgod">

                                                        <div class="calcline linevklad">
                                                            <div class="calcslider-wrapper">
                                                                <div class="row">
                                                                    <div class="col-sm-1">
                                                                        <div class="calclinename">Вклад</div>
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <div class="calcslider-wrapperinner">
                                                                            <input id="razvgodsumm" class="calcslid" value="500000">
                                                                            <span id="razvgodsummcopy" class="datacopy">0</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <input id="razvgodsummmanual" class="caclcdisplay-box manualchangeinp" step="10000" max="5000000" min="500000">
                                                                        <div class="manualchangeinppostfix">руб</div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="calcline linesrok">
                                                            <div class="calcslider-wrapper">
                                                                <div class="calcslider-wrapper">
                                                                    <div class="row">
                                                                        <div class="col-sm-1">
                                                                            <div class="calclinename">Срок</div>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="calcslider-wrapperinner">
                                                                                <input class="calcslid" id="razvgodmes" value="6">
                                                                                <span class="datacopy" id="razvgodmessummcopy">0</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <input class="pull-left caclcdisplay-box manualchangeinp" id="razvgodmesmmanual" readonly max="18" min="6">
                                                                            <div class="manualchangeinppostfix">мес</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="calcline">
                                                            <div class="row">
                                                                <div class="col-sm-3 col-xs-6">
                                                                    <div class="calclinename calclinename-perc">Процентная ставка</div>
                                                                </div>
                                                                <div class="col-sm-9 col-xs-6">
                                                                    <div class="calcpercnumb">24,5% <span>годовых</span></div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div role="tabpanel" class="tab-pane active" id="ejemesiachno">


                                                        <div class="calcline linevklad">
                                                            <div class="calcslider-wrapper">
                                                                <div class="row">
                                                                    <div class="col-sm-1">
                                                                        <div class="calclinename">Вклад1</div>
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <div class="calcslider-wrapperinner">
                                                                            <input id="razvgodsumm2" class="calcslid" value="500000"/>
                                                                            <span id="razvgodsummcopy2" class="datacopy">0</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <input id="razvgodsummmanual2" class="pull-left caclcdisplay-box manualchangeinp" step="10000" max="5000000" min="500000"/>
                                                                        <div class="manualchangeinppostfix">руб</div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="calcline linesrok">
                                                            <div class="calcslider-wrapper">
                                                                <div class="calcslider-wrapper">
                                                                    <div class="row">
                                                                        <div class="col-sm-1">
                                                                            <div class="calclinename">Срок</div>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="calcslider-wrapperinner">
                                                                                <input class="calcslid" id="razvgodmes2" value="6">
                                                                                <span class="datacopy" id="razvgodmessummcopy2">0</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <input class="pull-left  caclcdisplay-box manualchangeinp" id="razvgodmesmmanual2" readonly max="18" min="6">
                                                                            <div class="manualchangeinppostfix">мес</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="calcline">
                                                            <div class="row">
                                                                <div class="col-sm-3 col-xs-6">
                                                                    <div class="calclinename calclinename-perc">Процентная ставка</div>
                                                                </div>
                                                                <div class="col-sm-9 col-xs-6">
                                                                    <div class="calcpercnumb">22% <span>годовых</span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="calcrghtblock">
                                            <div class="calcchart">

                                                <div id="chart">
                                                    <ul id="bars">
                                                        <li>
                                                            <div data-percentage="1" class="bar" id="chartvklad">
                                                                <div class="calclvkaldblocktxt">
                                                                    Вклад <br/>
                                                                    <span>0</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div data-percentage="1" class="bar" id="chartdoh">
                                                                <div class="calclvkardblocktxt">
                                                                    Доход <br/>
                                                                    <span>0</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>
                                            <div class="calcbueblock">
                                                <div class="calcchistprib">Мой доход за весь период</div>
                                                <div class="calcchistpribdeng">0</div>
                                                <div class="calcrubmout">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <span>руб</span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </div>
                                            <div class="calcblueline"></div>
                                            <div class="calcgraygadblock">
                                                <div class="calcnamedoh">Доход за месяц</div>
                                                <div class="calcsummzaper"><span>0</span> руб/мес</div>
                                            </div>
                                            <div class="calcgraygadblockshadow"></div>


                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div
                            class="blok_03 scrollme animateme"
                            data-when="enter"
                            data-from="0.5"
                            data-to="0"
                            data-crop="false"
                            data-opacity="0"
                            data-scale="1.5">
                            <div class="blok_03_02" style="text-align:center;"></div>
                        </div>

                        <div
                            class="blok_03 scrollme animateme"
                            data-when="enter"
                            data-from="0.5"
                            data-to="0"
                            data-crop="false"
                            data-opacity="0"
                            data-scale="1.5">
                            <div class="blok_03_01">Почему Мы Готовы Платить Вам Такие Высокие Проценты?</div>
                            <div class="blok_03_02">
                                <br>
                                <p>За несколько лет занятий нашим бизнесом мы создали и отточили нашу бизнес-модель настолько, что она позволяет нам зарабатывать с каждого вложенного рубля в течении года - один рубль чистой прибыли. И 24,5 копейками мы готовы поделиться с Вами. Другими словами, мы зарабатываем 100 % в год и 24,5% делимся с Вами.</p>
                            </div>
                        </div>

                        <div class="blok_10 scrollme animateme"
                             data-when="enter"
                             data-from="0.5"
                             data-to="0"
                             data-crop="false"
                             data-opacity="0"
                             data-scale="1.5">
                            <div class="blok_10_01">
                                Зачем Нужно <br> Инвестировать Именно В Footyball?
                            </div>
                            <div class="blok_10_02">
                                <p>Это Ваш шанс не просто заработать хорошие деньги, но и прикоснуться к развитию одной из самых динамичных, инновационных, успешных и социально значимых компаний России.</p>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid nopadding">



            <div class="blok_05_02">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8 col-sx-12 effects plain clearfix scrollme">
                            <div class="playerbg effect_box effect_box_scale animateme"
                                 data-when="view"
                                 data-from="0.55"
                                 data-to="0.05"
                                 data-scale="0">
                                <div class="steps_result_video2 no-svg ">
                                    <video style="width: 100%; height: 100%;" poster="/photo/tv/3-1481021918-nagibinkorotkijsajt.jpg" id="videoinv4" controls="controls" preload="none">
                                        <source src="//footyball.ru/video/tv/3-1481021918-nagibinkorotkijsajt.webm" type="video/webm">
                                        <source src="//footyball.ru/video/tv/3-1481021918-nagibinkorotkijsajt.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <div class="blok_05">
                <div class="container">
                    <div class="row">
                        <div class="col-sx-offset-1 col-sm-12 col-sx-10">
                            <div class="blok_05_01  scrollme animateme"
                                 data-when="enter"
                                 data-from="0.5"
                                 data-to="0"
                                 data-crop="false"
                                 data-opacity="0"
                                 data-scale="1.5">CМИ О Нас</div>
                        </div>
                    </div>
                </div>

                <div class="blok_05_02">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8 col-sx-12 effects plain clearfix scrollme">
                                <div class="playerbg effect_box effect_box_scale animateme"
                                     data-when="view"
                                     data-from="0.55"
                                     data-to="0.05"
                                     data-scale="0">
                                    <div class="steps_result_video2 no-svg ">
                                        <video style="width: 100%; height: 100%;" poster="/photo/tv/8-1479717758-kamedi_futi_sajt.jpg" id="videoinv3" controls="controls" preload="none">
                                            <source src="//footyball.ru/video/tv/8-1479717758-kamedi_futi_sajt.webm" type="video/webm">
                                            <source src="//footyball.ru/video/tv/8-1479717758-kamedi_futi_sajt.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container">
            <div class="blok_10 scrollme animateme"
                 data-when="enter"
                 data-from="0.5"
                 data-to="0"
                 data-crop="false"
                 data-opacity="0"
                 data-scale="1.5"
                 style="border-top:none;">
                <div class="blok_10_01">
                    Для Чего Мы Привлекаем Инвестиции?
                </div>
                <div class="blok_10_02">
                    <p>Тщательно проанализировав все возможные вклады на рынке, мы с уверенностью можем сказать, что наше предложение выгодное, надежное и социально значимое.</p><br>
                    <p>Мы предлагаем Вам такой высокий процент, так как научились эффективно развивать и масштабировать наш бизнес, мы научились зарабатывать и хотим поделиться прибылью с теми, кто помогает нам расти быстрее. </p><br>
                    <p>Наша бизнес-модель успешно протестирована на трёх школах в г.Киев и четырех школах в г.Москва. Сейчас идет активный запуск новых школ.


                </div>
            </div>
        </div>




        <div class="container-fluid nopadding">

            <div class="blok_05">
                <div class="container">
                    <div class="row">
                        <div class="col-sx-offset-1 col-sm-12 col-sx-10">
                            <div class="blok_05_01  scrollme animateme"
                                 data-when="enter"
                                 data-from="0.5"
                                 data-to="0"
                                 data-crop="false"
                                 data-opacity="0"
                                 data-scale="1.5">Валерий Газзаев О Нас</div>
                        </div>
                    </div>
                </div>

                <div class="blok_05_02">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8 col-sx-12 effects plain clearfix scrollme">
                                <div class="playerbg effect_box effect_box_scale animateme"
                                     data-when="view"
                                     data-from="0.55"
                                     data-to="0.05"
                                     data-scale="0">
                                    <div class="steps_result_video2 no-svg ">
                                        <video style="width: 100%; height: 100%;" poster="/photo/tv/3-1458722316-gazzaevSajt.jpg" id="videoinv1" controls="controls" preload="none">
                                            <source src="//footyball.ru/video/tv/3-1458722316-gazzaevSajt.webm" type="video/webm">
                                            <source src="//footyball.ru/video/tv/3-1458722316-gazzaevSajt.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="blok_06">
                <div class="blok_06_01">
                    <div class="blok_06_02">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-offset-4 col-sm-2 col-xs-6">
                                    <a href="http://instagram.com/footyball_russia" target="_blank" class="impubbutt animateme scrollme"
                                       data-when="enter"
                                       data-from="0.75"
                                       data-to="0"
                                       data-opacity="0"
                                       data-translatex="-400">
                                        <img src="assets/img/in.png" class="img-responsive" alt="">
                                    </a>
                                </div>
                                <div class="col-sm-2  col-xs-6">
                                    <a href="https://vk.com/footyballru" target="_blank" class="vkpubbutt animateme scrollme"
                                       data-when="enter"
                                       data-from="0.75"
                                       data-to="0"
                                       data-opacity="0"
                                       data-translatex="400">
                                        <img src="assets/img/vk.png" class="img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="blok_05">
                <div class="blok_05_01  scrollme animateme"
                     data-when="enter"
                     data-from="0.5"
                     data-to="0"
                     data-crop="false"
                     data-opacity="0"
                     data-scale="1.5">Гарик Мартиросян О Нас</div>
                <div class="blok_05_02">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8 col-sx-12 effects plain clearfix scrollme">
                                <div class="playerbg effect_box effect_box_scale animateme"
                                     data-when="view"
                                     data-from="0.55"
                                     data-to="0.05"
                                     data-scale="0">
                                    <div class="steps_result_video2 no-svg ">
                                        <video style="width: 100%; height: 100%;" poster="//footyball.ru/photo/tv/3-1454922927-mart_sajt1.jpg" id="videoinv2" controls="controls" preload="none">
                                            <source src="//footyball.ru/video/tv/3-1454922927-mart_sajt1.webm" type="video/webm">
                                            <source src="//footyball.ru/video/tv/3-1454922927-mart_sajt1.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" style="height:40px"></div>

        </div>


        <div class="container ">
            <div class="row">
                <div class="col-sx-offset-1 col-sm-12 col-sx-10">
                    <div class="blok_05_01  scrollme animateme"
                         data-when="enter"
                         data-from="0.5"
                         data-to="0"
                         data-crop="false"
                         data-opacity="0"
                         data-scale="1.5">Отзывы Родителей</div>
                </div>
            </div>
            <div class="row">
                <div class="blok_12 scrollme animateme"
                 data-when="enter"
                 data-from="0.5"
                 data-to="0"
                 data-crop="false"
                 data-opacity="0"
                 data-scale="1.5"
            >
                <div class="col-sm-4 col-sx-12">
                    <div class="feedbackblock">
                        <div class="feedbackblockinner">
                            <div class="nameblock">
                                <div class="feedbackphoto">
                                    <img class="feetback img-circle img-responsive" alt="Константин Терехов" src="//footyball.ru/photo/feedback/terehov-sm.jpg">
                                </div>
                                <div class="feedbackname">
                                    Константин Терехов <br><span>о нашем клубе </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="200">



                                <div class="dynamic-wrap">
                                    Очень хорошая школа. Начиная от парковки и раздевалок и заканчивая реквизитом и методикой преподавания. Сын ходит четыре месяца - двух одинаковых занятий не было! Не знаю, как у Фёдора будет с учебой, когда он в общую школу пойдет, но что при игре в футбол со сверстниками он будет среди лидеров - я уверен.    </div>
                                <div class="align-center">
                                    <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="Закрыть...">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-sx-12">
                    <div class="feedbackblock">
                        <div class="feedbackblockinner">
                            <div class="nameblock">
                                <div class="feedbackphoto">
                                    <img class="feetback img-circle img-responsive" alt="Карина Емельянова" src="//footyball.ru/photo/feedback/Karina_Emelyanova1442584775.jpg">
                                </div>
                                <div class="feedbackname">
                                    Карина Емельянова <br><span>о нашем клубе </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="200">



                                <div class="dynamic-wrap">
                                    Спасибо огромное Footyball!!!!! Вы настоящее открытие и теперь мы в ваших рядах !! :)) Мой ребенок в восторге, а для меня - это главный показатель. С нетерпением ждем следующего занятия!!                                   </div>
                                <div class="align-center">
                                    <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="Закрыть...">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-sx-12">
                    <div class="feedbackblock">
                        <div class="feedbackblockinner">
                            <div class="nameblock">
                                <div class="feedbackphoto">
                                    <img class="feetback img-circle img-responsive" alt="Светлана Бочина " src="//footyball.ru/photo/feedback/Svetlana_Bochina1451045535.jpg">
                                </div>
                                <div class="feedbackname">
                                    Светлана Бочина <br><span>о нашем клубе </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="200">



                                <div class="dynamic-wrap">

                                    Хочу в преддверии Нового года тоже оставить свой отзыв об этом замечательном клубе! Мы узнали про него случайно, когда нас записали на пробную тренировку на празднике день города на ВДНХ. У меня два сына с небольшой разницей и оба занимаются с сентября 2015 года. Спасибо команде тренеров за индивидуальный подход и терпение. Ведь характер у обоих непростой! Но теперь оба бегут на тренировку с удовольствием, а мы за них болеем на аттестациях всей семьёй. Даня(5 лет) и Денис(4 года).

                                </div>
                                <div class="align-center">
                                    <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="Закрыть...">...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="container-fluid bluebg">

            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="blok_00-2  scrollme animateme"
                             data-when="enter"
                             data-from="0.5"
                             data-to="0"
                             data-crop="false"
                             data-opacity="0"
                             data-scale="1.7">
                            <div class="blok_12">
                                <div class="blok_12_block"
                                     id="bottomform">
                                    <div class="blok_12_01">
                                        <div class="blok_12_02">Для Дополнительной Информации <br> Нажмите Кнопку Ниже</div>
                                        <a href="#" class="knowmorebutton">УЗНАТЬ БОЛЬШЕ</a>

                                        <div class="clearfix"></div>

                                        <div id="knowmoreblock" >
                                            <div class="knowmoreblockinner" >
                                                <div class="howwork  scrollme animateme"
                                                     data-when="enter"
                                                     data-from="0.5"
                                                     data-to="0"
                                                     data-crop="false"
                                                     data-opacity="0"
                                                     data-scale="1.7">
                                                    <div class="blockname">Как Это Работает</div>
                                                    <div class="howworkinner">
                                                        <div class="row">
                                                            <div class="col-sm-offset-2 col-sm-2">
                                                                <img src="assets/img/invstep1.png" class="img-responsive center-block" alt="">
                                                                <span class="howworktxt">Вы оставляете свои данные </span></div>
                                                            <div class="col-sm-2">
                                                                <img src="assets/img/invstep2.png" class="img-responsive center-block" alt="">
                                                                <span class="howworktxt">Мы перезваниваем Вам в рабочее время и приглашаем на встречу к нам в офис</span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <img src="assets/img/invstep3.png" class="img-responsive center-block" alt="">
                                                                <span class="howworktxt">Вы приезжаете на встречу и узнаете все подробности</span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <img src="assets/img/invstep4.png" class="img-responsive center-block" alt="">
                                                                <span class="howworktxt">Вы начинаете зарабатывать</span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="bigform  scrollme animateme"
                                                     data-when="enter"
                                                     data-from="0.5"
                                                     data-to="0"
                                                     data-crop="false"
                                                     data-opacity="0"
                                                     data-scale="1.7">
                                                    <div class="blockname">Заполните Данные Ниже</div>
                                                    <div class="bigforminner">
                                                        <form action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" id="sendform">
                                                            <input type="hidden" name="oid" value="00D20000000onaw">
                                                            <input type="hidden" name="retURL" value="http://footyball.ru/36-thankyou.php">
                                                            <input type="hidden" id="00N200000099lqG" name="00N200000099lqG" type="checkbox" value="1" />
                                                            <input type="hidden" id="lead_source" name="lead_source" value="Web form" />
                                                            <div class="bigformhr"></div>
                                                            <div class="bigformform">

                                                                <div class="row">
                                                                    <div class="col-sm-offset-3 col-sm-6">
                                                                        <div class="bigformlft">
                                                                            <div class="bigformformsmname">Вкладчик: </div>
                                                                            <input id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="ИМЯ" required="required" data-required="true" />

                                                                            <input id="last_name" maxlength="80" name="last_name" size="20" type="hidden" placeholder="ФАМИЛИЯ"  />

                                                                            <input id="phone" maxlength="40" name="phone" size="20" type="text" placeholder="ТЕЛЕФОН" required="required" data-required="true" />
                                                                            <!--<div class="bigformformsmname">
                                                                                <input type="checkbox" id="checkbox-1-1"  name="bigformformmorefils" value="true" class="regular-checkbox" /><label for="checkbox-1-1"></label>
                                                                                Я Рекомендатель<br>
                                                                                <span style="font-size:16px;">(Рекомендатель получает 3% от вклада моментально)</span>
                                                                            </div>--->

                                                                            <div id="bigformformmorefils">
                                                                                <div class="bigformformmorefilshr"></div>
                                                                                <div class="bigformformsmname">Данные Рекомендателя:</div>
                                                                                <input id="00N200000099sLl" maxlength="255" name="00N200000099sLl" size="20" placeholder="ИМЯ" type="text" />

                                                                                <input id="00N200000099sLm" maxlength="255" name="00N200000099sLm" size="20" placeholder="ФАМИЛИЯ" type="text" />

                                                                                <input id="00N200000099sLn" maxlength="255" name="00N200000099sLn" size="20" placeholder="ТЕЛЕФОН" type="text" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="bigformhr"></div>
                                                            <div class="bigformtxt">Все сведения/персональные данные, предоставленные получателями услуг, считаются строго конфиденциальными
                                                            </div>
                                                            <input class="bigformsend" type="submit" name="submit" value="ОТПРАВИТЬ">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

		    <div class="container-fluid nopadding">
			    <?include_once("inc/map.inc.php");?>
		    </div>
        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
		    <div class="container-fluid nopadding" id="mainblockfooter">
			    <?include_once("inc/footer-new.inc.php");?>
			    <?include_once("inc/hide-popups.inc.php");?>
		    </div>

    </div>


    <?include_once("inc/allmainjs.php");?>

    <script src="assets/js/jquery.dynamicmaxheight.min.js" defer="defer"></script>
    <script src="assets/js/iLightInputNumber.min.js" defer="defer"></script>
    <script src="assets/js/accounting.min.js" defer="defer"></script>
    <script src="assets/js/jquery-letterfx.min.js" defer="defer"></script>
    <script src="assets/js/mediaelement-and-player.min.js" defer="defer"></script>
    <script src="assets/js/scripts_invest.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


    <script>
    var ismob = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        ismob = true;
    }
    if(ismob===false){
        document.write('<script src="assets/js/jquery.scrollme.min.js" defer="defer"><\/script>');
    }
    </script>


<script>



	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}

    $(document).ready(function() {


	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });

	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
		    });
		    $('.b9 .item:first-of-type').click();
	    };


        $('#razvgodbutt').trigger('click');
        $('#ejemesiachnobutt').trigger('click');


        $('.dynamic-max-height').dynamicMaxHeight(
            { trigger : '.dynamic-show-more'}
        );




        $("#razvgodsumm,#razvgodsumm2").ionRangeSlider({
            min: 500000,
            max: 5000000,
            from: 500000,
            grid: true,
            step: 10000,
            prettify_enabled: true,
            postfix: " руб",
            hide_min_max: true,
            //values:[100000,500000,1000000,2000000,3000000],
            onFinish: function (data) {
                var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                //console.log(tabactual)
                $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(data.from);
                //$('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(  );
                $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html(data.from);
                chart();
            }
        });
        $("#razvgodmes,#razvgodmes2").ionRangeSlider({
            min: 6,
            max: 18,
            from: 6,
            grid: true,
            step: 1,
            postfix: " месяцев",
            hide_min_max: true,
            //values:[1,3,6,9,12,15,18],
            onFinish: function (data) {
                var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                //console.log(tabactual)
                $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .manualchangeinp').val(data.from);
                $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html(data.from);
                chart();
            }
        });


        $('.manualchangeinp').iLightInputNumber({});

        chart();
        updatedata();

        var ismob = false;
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            ismob = true;
        }

        if(ismob===false){
            setTimeout(show1line, 500);
            setTimeout(show2line, 5000)
        }


        $('#videoinv1,#videoinv2,#videoinv3,#videoinv4').mediaelementplayer({
            alwaysShowControls: true ,
            features: ['playpause','progress','volume','fullscreen'],
            videoVolume: 'horizontal',
            startVolume: 0.7,
            enableKeyboard: false
        });

        $("#checkbox-1-1").prop("checked",false);



    })
</script>

</body>
</html>
