<?
header("Location: http://rsp.footyball.ru");
die();

include_once("lib/cms_view_inc.php");


$contfilename = "RSPVisits.txt";
$filesting = file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
$filearray = explode(";", $filesting);
$filecout = $filearray[1];
$filecout = $filecout * 1;
if ($filecout>1) {
   $cout = $filecout;
}else{
    $cout = 52000;
}

?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт - Разбуди в Сыне Победителя!</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-programms.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">

        <div class="container">

            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">

                    <ul class="mySlideshow">
                        <li>
                            <img src="photo/banner/rsp/1.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/rsp1-logo.png" class="img-responsive center-block sliderlogo" alt="">
                        </li>
                    </ul>

                </div>
            </div>

            <div class="row rspline2">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        Бесплатная Эксклюзивная <br>
                        45-Минутная Программа <br>
                        "Разбуди в Сыне Победителя!" <br>
                        <span>УЖЕ В МОСКВЕ!</span>
                    </div>
                </div>
            </div>

            <div class="row rspline3">
                <div class="col-xs-12">
                    <div class="buttblock">
                        <div class="buttout">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                                <div class="col-sm-4 col-xs-12 butout">
                                    <a href="#" class="butt toggle-menu menu-top">записаться на тренировку</a>
                                </div>
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            </div>
                        </div>
                        <div class="butline"></div>
                    </div>
                </div>
            </div>

        </div>
        
        <div class="container-fluid nopadding rspline4" >
            <div class="container">
                <div class="rsptextline">
                    Почему это бесплатно?
                </div>
            </div>
        </div>

        <div class="container">

            <div class="rspline5">
                <div class="rsptextline">
                    <p>Наша миссия – создать больше успешных, целеустремленных людей по всему миру. И лучший способ – это вживую показать вам весь процесс, раскрыв сильные стороны вашего сына и предоставив ценную практическую информацию о воспитании.</p>
                    <p>Мы также понимаем, что если вам у нас понравится, вы вернетесь к нам снова и снова. Вот почему мы инвестируем в вас на первом этапе и даем максимальную ценность, которую вы нигде больше не найдете.</p>
                </div>
            </div>

            <div class="row rspline6">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        Это не просто<br>
                        футбольная тренировка
                    </div>
                </div>
            </div>

            <div class="row rspline7">
                <div class="col-sm-4 col-xs-12 ">
                    <div class="imgblock imgblockl">
                        <img src="assets/img/rsp-block1.jpg" class="img-responsive" alt="" style="margin-bottom: 20px">
                        <span class="imgblocktxt">уникальная методика</span>
                    </div>
                    <div class="imgblock imgblockl">
                        <img src="assets/img/rsp-block2.jpg" class="img-responsive" alt="" style="margin-bottom: 10px">
                        <span class="imgblocktxt">знакомство со спортом</span>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-12 imgblockс">
                    <img src="assets/img/rsp-block3.jpg" class="img-responsive" alt="" style="margin-bottom: 10px">
                    <span class="imgblocktxt">первая форма победителя</span>
                </div>

                <div class="col-sm-4 col-xs-12">
                    <div class="imgblock imgblockr">
                    <img src="assets/img/rsp-block4.jpg" class="img-responsive" alt="" style="margin-bottom: 20px">
                    <span class="imgblocktxt">профессиональное оборудование</span>
                </div>
                <div class="imgblock imgblockr">

                    <img src="assets/img/rsp-block5.jpg" class="img-responsive" alt="">
                    <span class="imgblocktxt">абсолютно бесплатно</span>

                </div>
            </div>
            </div>

            <div class="row rspline8">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        нас уже посетили более
                    </div>
                </div>
            </div>

            <div class="row rspline9">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        <div class="incremental-counter" data-value="<?=$cout?>"></div>
                    </div>
                </div>
            </div>

            <div class="row rspline10">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        детей россии
                    </div>
                </div>
            </div>

            <div class="row rspline11">
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9325.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9325_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9328.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9328_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9364.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9364_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9418.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9418_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9435.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9435_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9461.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9461_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9487.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9487_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/rsp/6K0F9512.jpg" class="big">
                        <img src="photo/gallery/rsp/6K0F9512_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>

            </div>

            <div class="row rspline12">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        Подумай о Сыне, Запиши Его на РСП
                    </div>
                </div>
            </div>

            <div class="row rspline13">
                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="rsptextline rsptextlinel">
                            от <span>2</span> лет
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <img src="assets/img/rsp1-logo.jpg" class="center-block"  alt="">
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="rsptextline rsptextliner">
                            до <span>6,5</span> лет
                        </div>
                    </div>
                </div>
            </div>

            <div class="row rspline14">
            <div class="col-xs-12">
                <div class="buttblock">
                    <div class="buttout">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            <div class="col-sm-4 col-xs-12 butout">
                                <a href="#" class="butt toggle-menu menu-top">узнать подробности</a>
                            </div>
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                        </div>
                    </div>
                    <div class="butline"></div>
                </div>
            </div>
        </div>

        </div>


		    <div class="container-fluid nopadding">
			    <?include_once("inc/map.inc.php");?>
		    </div>
        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
	    <div class="container-fluid nopadding" id="mainblockfooter">
		    <?include_once("inc/footer-new.inc.php");?>
		    <?include_once("inc/hide-popups.inc.php");?>
	    </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/simple-lightbox.min.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/jquery.incremental-counter.min.js"></script>
    <script src="assets/js/allpages-functions.js"></script>



<script>

	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}

    $(document).ready(function() {




        $('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });


        $(".incremental-counter").incrementalCounter({"digits": 6});

        $(".rspline11 a").simpleLightbox();


	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });

	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
		    });
		    $('.b9 .item:first-of-type').click();
	    };




    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>
</body>
</html>