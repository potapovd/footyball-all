<?
include_once("../lib/cms_view_inc.php");

$pageinfo = getpageinfo(7);
//print_r($_SERVER);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?include_once("../inc/head.inc.php")?>
</head>

<body>
    <a id="link-top" href="#">&and;</a>

    <header>
        <div id="fixedupnav" class="headermenu">
            <div class="container">

                <div class="row">
                    <div class="col-sm-1 col-xs-2">
                        <div class="menurleft">
                            <button class="toggle-menu menu-left navbar-toggle">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-8">
                        <a href="../index.php" class="logositeout">
                            <img class="logosite pull-left" width="171" height="124" src="../assets/img/logo.svg" onerror="this.onerror=null; this.src='../assets/img/logo.png'" alt="FootyBall logo">
                        </a>
                    </div>
                    <div class="col-sm-7 hidden-xs">
                        <div class="sitelogan">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ ДЛЯ ДОШКОЛЬНИКОВ №1</div>
                    </div>
                    <div class="col-sm-1 col-xs-2">
                        <div class="menuright">
                            <button class="toggle-menu menu-right navbar-toggle">
                                <i class="fa fa-users"></i>
                            </button>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix"></div>

                <!-- Left menu element-->
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
                    <h3>
                        <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
                        <span>Меню</span>
                    </h3>
                    <a href="../index.php"><span>Главная</span></a>
                    <a href="../club.php"><span>О нас</span></a>
                    <a href="../programms.php"><span>Программы</span></a>
                    <a href="../feedback.php"><span>Отзывы</span></a>
                    <a href="../footyballtv.php"><span>FootyballTV</span></a>
                    <a href="../invest/"><span>Инвeстиции</span></a>
                </nav>
                <!-- Right menu element-->
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">

                    <h3>
                        <span>Личный кабинет</span>
                        <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
                    </h3>
                    <div class="subscrform">
                        <form action="/" method="POST">
                            <input class="namechildinp" type="text" placeholder="Логин" required="required" maxlength="10" size="10">
                            <input class="nameparentinp" type="text" placeholder="Пароль" required="required" maxlength="10" size="10">
                            <input class="buttonsend" type="submit" value="Войти" >
                        </form>
                    </div>
                </nav>


            </div>
        </div>
    </header>
        
    <div class="blok_00-2 bluebg">
        <div class="conteiner">
            <div class="blok_01">
                <span>Развивайте сеть клубов<br/> вместе с нами!</span>
                <img src="36p/img/map.png" alt="" class="farnmap">
                <div class="farntxt">
                    Мы динамично развиваемся и открываем наши школы в самых разных уголках РФ.
                    Если вы готовы это делать вместе с нами, тогда присоеденяйтесь   к нашей программе Франшизы. О подробностях и условиях Вы можете
                    <br>узнать  <br><br>
                </div>
             </div>
            
            <a href="#" class="knowmorebutton">заполнив форму</a>
            
            <div id="knowmoreblock" >
                <div class="conteiner">
                    <div class="knowmoreblockinner" >
                        <div class="howwork  scrollme animateme"
                             data-when="enter"
                             data-from="0.5"
                             data-to="0"
                             data-crop="false"
                             data-opacity="0"
                             data-scale="1.7">
                        </div>
                        <div class="bigform  scrollme animateme"
                             data-when="enter"
                             data-from="0.5"
                             data-to="0"
                             data-crop="false"
                             data-opacity="0"
                             data-scale="1.7">
                            <div class="blockname">Заполните Данные Ниже</div>
                            <div class="bigforminner">
                                <form action="" method="POST" id="sendform">
                                    <div class="bigformhr"></div>
                                    <div class="bigformform">
                                        <div class="bigformlft">
                                            <input id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="ИМЯ" required="required" data-required="true" />

                                            <input id="last_name" maxlength="80" name="last_name" size="20" type="text" placeholder="ФАМИЛИЯ"  />

                                            <input id="phone" maxlength="40" name="phone" size="20" type="text" placeholder="ТЕЛЕФОН" required="required" data-required="true" />

                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="bigformhr"></div>
                                    <div class="bigformtxt">Все сведения/персональные данные, предоставленные получателями услуг, считаются строго конфиденциальными </div>
                                    <input class="bigformsend" type="submit" id="sendformsubmin" name="submit" value="ОТПРАВИТЬ">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <?include_once("../inc/footer.inc.php")?>

    <div class="modal-overlay"></div>
    <?include_once("../inc/modal-form.inc.php")?>
    <div class="modal-wrapper">
        <div class="modal-window js-blur" id="modalwindowlogin" style="width:550px; height:350px">
            <button class="close-modal">X</button>
            <h2>ПЕРСОНАЛЬНЫЙ КАБИНЕТ</h2>
            <div class="subscrform">
                <form action="#">
                    <input type="text" placeholder="Логин">
                    <input type="text" placeholder="Пароль">
                    <input type="submit" class="buttonsend" value="Войти">
                </form>
            </div>
        </div>
    </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" defer="defer"></script>
    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.0.8/jquery.mCustomScrollbar.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/0.9.0/jquery.mask.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.0.8/jquery.placeholder.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/h5Validate/0.8.4/jquery.h5validate.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/tooltipster/3.0.5/js/jquery.tooltipster.min.js" defer="defer"></script>-->
    
   
    <script src="../assets/js/jquery.sticky.js" defer="defer"></script>
    
    <script src="../assets/js/TweenMax.min.js" defer="defer"></script>
<!--    <script src="../assets/js/motionblur.js" defer="defer"></script>-->
    
    <script src="36p/js/jquery.mask.min.js" defer="derfer"></script>
    <script src="36p/js/jquery.maskedinput.js" defer="derfer"></script>
    <script src="36p/js/ion.rangeSlider.min.js" defer="derfer"></script>
    <script src="../assets/js/mediaelement-and-player.min.js" ></script>
    <script src="36p/js/jquery.scrollme.min.js" defer="defer"></script>
    <script src="../assets/js/jPushMenu.js"></script>



    <script>
    var ismob = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        ismob = true;
    }
    if(ismob===false){
       document.write('<script src="36p/js/jquery.scrollme.min.js" defer="defer"><\/script>');
    }
    </script>

    
    
    <!--<script src="36p/js/jquery.scrollme.min.js" defer="defer"></script>
<script src="36p/js/jquery-letterfx.min.js" defer="defer"></script>
    <script src="36p/js/jquery-letterfx.min.js" defer="defer"></script>
    <script src="36p/js/accounting.min.js" defer="defer"></script>
    <script src="36p/js/mediaelement-and-player.min.js" defer="defer"></script>
    <script src="36p/js/iLightInputNumber.min.js" defer="defer"></script>
    <script src="36p/js/ion.rangeSlider.min.js" defer="defer"></script>
<script src="36p/js/scripts_new.js"></script>-->

    <script>



        $('#fixedupnav').on('sticky-start', function() {
            //console.log("Started");
            $('#link-top').fadeIn("slow");
            $('.logosite').attr("src","../assets/img/logo-small.svg");
            $('.logosite').attr("onerror","this.onerror=null; this.src='../assets/img/logo-small.png'");
        });
        $('#fixedupnav').on('sticky-end', function() {
            //console.log("Ended");
            $('#link-top').fadeOut("slow");
            $('.logosite').attr("src","../assets/img/logo.svg");
            $('.logosite').attr("onerror","this.onerror=null; this.src='../assets/img/logo.png'");
        });

        
    $(function() {

        $('.toggle-menu').jPushMenu({
            closeOnClickLink: false
        });


        $("#fixedupnav").sticky({
            topSpacing: 0,
            zIndex:10,
            center:true,
            responsiveWidth:true
        });
        
        $('input[name="phone"]').mask('+7(999) 999 99 99');
        $(".agechildinp").ionRangeSlider({
            min: 2,
            max: 7,
            from: 3,
            grid: true,
            values: [2, 2.5, 3, 3.5, 4, 4.5, 5,5.5,6,6.5,7],
            max_postfix: " лет"
        });
        
        


        $('#link-top').on('click',function(e){
            e.preventDefault();
            //$('html,body').stop().animate({scrollTop: 0},2000);
            $('html,body').stop().animate({scrollTop: $("body").offset().top},1000);
            return false;
        });
        
        $(".knowmorebutton").toggle(function(){
            $("#knowmoreblock").animate({height:530},1000);
            $('html,body').stop().animate({scrollTop: $("#knowmoreblock").offset().top},1000);
        },function(){
            $('html,body').stop().animate({scrollTop: $(".knowmorebutton").offset().top},1000);
            $("#knowmoreblock").animate({height:0},1000);
        });
        
        $('#maincontent, .headerline2  .clearfix, .blok_00-2').on('click',function(e){
            //alert("1");
            if( $(".footer").hasClass('footeron')  ){
                $(".footer").removeClass('footeron')
                $('html,body').stop().animate({scrollTop: $(".footmapblock").offset().top},1500);
                $(".footer2line").animate({height:0},1500);
                $('.footmapblock').removeClass('footmapblockactive');
            }
        });
        
        $(".footmapblock").toggle(function(){
            $(".footer").addClass('footeron')
            $('#footertabmap1 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2249.873720377262!2d37.44017121028134!3d55.67379605147178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x63e9cf9fc167b361!2z0J7Rh9Cw0LrQvtCy0L4!5e0!3m2!1sru!2sua!4v1431008228089"  width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap2 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.4418665347575!2d37.772959!3d55.750826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414acaa351f2331f%3A0xd39dfa163bf5112a!2zMS3QuSDQn9C10YDQvtCy0LAg0J_QvtC70Y8g0L_RgC3QtCwgOdGBNSwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDExMTE0MQ!5e0!3m2!1sru!2sde!4v1429531355805" width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap3 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.9658692295566!2d37.523714!3d55.741721999999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54be40b9041f5%3A0x4d8e1849ab11e958!2z0JrRg9GC0YPQt9C-0LLRgdC60LjQuSDQv9GA0L7RgdC_LiwgMzbRgTUxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTIxMTcw!5e0!3m2!1sru!2sde!4v1431090886045"  width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap4 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2243.271348910248!2d37.58248731550191!3d55.788525996704244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b549f6391b2b55%3A0xae00b01ed57b1dd!2z0J_RgNCw0LLQtNGLINGD0LsuLCAyNNGBNSwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDEyNzEzNw!5e0!3m2!1sru!2sde!4v1458800006210"  width="593" height="449" frameborder="0" style="border:0"></iframe>');


            $(".footer2line").animate({height:460},1500);
            $('html,body').stop().animate({scrollTop: $(".footer2line").offset().top},1500);
            //footmapblockactive
            $('.footmapblock').addClass('footmapblockactive');
        },function(){
            $(".footer").removeClass('footeron')
            $('html,body').stop().animate({scrollTop: $(".footmapblock").offset().top},1500);
            $(".footer2line").animate({height:0},1500);
            $('.footmapblock').removeClass('footmapblockactive');
        });
        
        
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        
        var $modal = $(".modal-window"),
            $overlay = $(".modal-overlay"),
            blocked = false,
            unblockTimeout = null;

        TweenMax.set($modal, {
            autoAlpha: 0
        });

        var isOpen = false;

        function openModal(modalname) {
            var $modal = $("#"+modalname+"");
            //alert(modal)

            if (!blocked) {
                $modal.addClass("openedwindow");
                block(400);
                //alert($modal)

                TweenMax.to($overlay, 0.3, {
                    autoAlpha: 1
                });

                TweenMax.fromTo($modal, 0.5, {
                    x: (-$(window).width() - $modal.width()) / 2 - 50,
                    scale: 0.9,
                    autoAlpha: 1
                }, {
                    delay: 0.1,
                    rotationY: 0,
                    scale: 1,
                    opacity: 1,
                    x: 0,
                    z: 0,
                    ease: Elastic.easeOut,
                    easeParams: [0.4, 0.3],
                    force3D: false
                });
               // $.startUpdatingBlur(800);
            }
            return false;
        }

        function closeModal(modalname) {
            var $modal = $("#"+modalname+"");
            if (!blocked) {
                
                if(modalname=="modalwindowtv"){
                    var player = new MediaElementPlayer('#footyballtv');
                    player.pause(); 
                }
                if(modalname=="modalwindowprogramm"){
                    var player = new MediaElementPlayer('#programvideo');
                    player.pause();
                }
                if(modalname=="modalwindowstep3"){
                    var player = new MediaElementPlayer('#programvideo');
                    player.pause();
                }
                
                
                $modal.removeClass("openedwindow");
                block(400);
                TweenMax.to($overlay, 0.3, {
                    delay: 0.3,
                    autoAlpha: 0
                });
                TweenMax.to($modal, 0.3, {
                    x: ($(window).width() + $modal.width()) / 2 + 100,
                    scale: 0.9,
                    ease: Quad.easeInOut,
                    force3D: false,
                    onComplete: function () {
                        TweenMax.set($modal, {
                            autoAlpha: 0
                        });
                    }
                });
               // $.startUpdatingBlur(400);
            }
            return false;
        }

        function block(t) {
            blocked = true;
            if (unblockTimeout !== null) {
                clearTimeout(unblockTimeout);
                unblockTimeout = null;
            }
            unblockTimeout = setTimeout(unblock, t);
            return false;
        }

        function unblock() {
            blocked = false;
            return false;
        }
        $(".open-modal").click(function () {
            openModal( $(this).attr("data-modawindowid") );

        });
        $(".close-modal, .modal-overlay").click(function () {            
            closeModal(  $(".openedwindow").attr("id")  );
        });
       
        $("body").animate({ opacity: "1" }, "fast");
        $(".tmpsprite").show("scale", {percent: 100}, 500 );
        
        
        
        $('#sendformsubmin').click(function(){

            $.post("sendmail.php", $("#sendform").serialize(),  function(response) {
                //$('#success').html(response);
                alert(response);
                //$('#success').hide('slow');
            });
            return false;

        });
        
        //$(".blok_01 span").letterfx({"fx":"fall","backwards":false,"timing":100,"fx_duration":"1000ms","letter_end":"restore","element_end":"restore"})

        /*$('#tooltip-map').tooltipster({
        	content: $('<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2246.0143872610997!2d37.524420899999996!3d55.740879!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54be46bcbf56d%3A0x7062ca5108095b99!2z0JrRg9GC0YPQt9C-0LLRgdC60LjQuSDQv9GA0L7RgdC_LiwgMzYsINCc0L7RgdC60LLQsCwg0KDQvtGB0YHQuNGPLCAxMjExNzA!5e0!3m2!1sru!2sua!4v1428438447833" width="400" height="300" frameborder="0" style="border:0"></iframe>'),
        	hideOnClick:true,
        	trigger:'click',
        	//autoClose:false,
        	theme:"tooltipster-light"
        });

        var ismob = false;
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            ismob = true;
        }    

        $('#razvgodbutt').trigger('click');
        $('#ejemesiachnobutt').trigger('click');

        if(ismob===false){
            setTimeout(show1line, 500)
            setTimeout(show2line, 5000)
        }
        

        $("#razvgodsumm,#razvgodsumm2").ionRangeSlider({
                min: 100000,
                max: 5000000,
                from: 100000,
                grid: true,
                step: 10000,
                prettify_enabled: true,
                postfix: " руб",
                hide_min_max: true,
                //values:[100000,500000,1000000,2000000,3000000],
                onFinish: function (data) {
                    var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                    //console.log(tabactual)
                     $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(data.from);
                     //$('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(  );
                     $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html(data.from);
                    chart();
                }
            });
        $("#razvgodmes,#razvgodmes2").ionRangeSlider({
                min: 6,
                max: 18,
                from: 6,
                grid: true,
                step: 1,
                postfix: " месяцев",
                hide_min_max: true,
                //values:[1,3,6,9,12,15,18],
                onFinish: function (data) {
                      var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                     //console.log(tabactual)
                      $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .manualchangeinp').val(data.from);
                      $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html(data.from);
                    chart();
                }
            });
        $('.manualchangeinp').iLightInputNumber({});

        $('.js-lazyYT').lazyYT();

        updatedata();
        chart();

        $('#sendform').h5Validate();
        $('input[name="phone"],input[name="00N200000099sLn"]').mask('+7(999) 9999999');
        $("#checkbox-1-1").prop("checked",false);
        $('input').placeholder();
            */

        


    });
        
        

        $('.tvlistitem').on('click',function(e){
            e.preventDefault();

            $(".tvlistblock ul li").removeClass("tvlistitemactive");
            $(this).parent().addClass("tvlistitemactive");
            //$("#blockcitat .blockfeatbtxt p").html("<img style='margin:20px 90px' src='assets/img/ajax-loader.gif'> " ) ;

            $.ajax({
                type: 'POST',
                url: '../inc/functio-gettvvideos.inc.php',
                data: 'numb='+$(this).attr("data-tvnumb"),
                dataType: 'json',
                async:true,
                cache: false,
                success: function(result) {

                    //console.log(result);
                    var player = new MediaElementPlayer('#footyballtv');
                    player.pause(); 
                    $("#tvvideoblok .tvvideobloknamevieo").html(result[1]) ;

                    var videoiiner = //"<h3 class='tvvideobloknamevieo'>"+result[1]+"</h3>"+
                        "<div class='no-svg blockvideo'>"+
                        "<video width='560' poster='/assets/video/simple.jpg' height='315' id='footyballtv' controls='controls' preload='none'>"+
                        "<source src='/assets/video/"+result[0]+".webm' type='video/webm'>"+
                        "<source src='/assets/video/"+result[0]+".mp4' type='video/mp4'>"+
                        "</video>"+
                        //   "<div class='tvdesctblock'>"+
                        //                result[2]+
                        //    "</div>"+
                        "</div>";

                    $(".tvdesctblock").html(result[2]);
                    $(".blockvideo").html(videoiiner);
                    $(".tvdesctblock .mCSB_container").html(result[2]) ;


                }
            });

            $(document).ajaxSuccess(function(){
                // alert("11");
                $('#footyballtv').mediaelementplayer({  
                    alwaysShowControls: true ,
                    features: ['playpause','progress','volume','fullscreen'],
                    videoVolume: 'horizontal',
                    startVolume: 0.7,
                    enableKeyboard: false
                });
                $("#tvvideoblok ").mCustomScrollbar("update");
            });

            return false;
        });
    </script>



    <noscript>
        <style>
            <!--
            body {opacity:1;}
            .calcblock{display:none;}
            .blok_09_02 .feedbackinner{overflow: auto;}
            .blok_12_02,.knowmorebutton{display:none;}
            #knowmoreblock{display:block;height:1130px;}
            #bigformformmorefils{display:block;}
            -->
        </style>
        <div id="jsdisable">У Вашего браузера отключен JAVASCPRIT или он не поддерживает эту технологию.
            <a href="http://anonym.to/?https://support.google.com/answer/23852">Как включить JAVASCPRIT</a>.
            <a href="http://anonym.to/?http://browser-update.org/update-browser.html">Новые браузеры </a>
        </div>
    </noscript>
</body>
</html>
