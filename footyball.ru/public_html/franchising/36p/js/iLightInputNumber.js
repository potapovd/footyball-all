;(function ($) {

$.fn.iLightInputNumber = function (options) {

var inBox = '.input-number-box',
    newInput = '.caclcdisplay-box',
    moreVal = '.input-number-more',
    lessVal = '.input-number-less';

this.each(function () {

    var el = $(this);
    $('<div class="' + inBox.substr(1) + '"></div>').insertAfter(el);
    var parent = el.find('+ ' + inBox);
    parent.append(el);
    var classes = el.attr('class');
    parent.append('<input class="' + newInput.substr(1) + '" type="text">');
    el.hide();
    var newEl = el.next();
    newEl.addClass(classes);
    var attrValue;

    function setInputAttr(attrName) {
        if (el.attr(attrName)) {
            attrValue = el.attr(attrName);
            newEl.attr(attrName, attrValue);
        }
    }

    setInputAttr('value');
    setInputAttr('placeholder');
    setInputAttr('min');
    setInputAttr('max');
    setInputAttr('step');
    setInputAttr('readonly');

    parent.append('<div class=' + moreVal.substr(1) + '></div>');
    parent.append('<div class=' + lessVal.substr(1) + '></div>');

}); //end each

var value,
    step,
    readonly;

var interval = null,
    timeout = null;

    function ToggleValue(input) {
        input.val(parseInt(input.val(), 10) + d);
        console.log(input);
    }

    $('body').on('mousedown', moreVal, function () {
        var el = $(this);
        var input = el.siblings(newInput);
        moreValFn(input);
        //console.log(newInput);
        timeout = setTimeout(function(){
            interval = setInterval(function(){ moreValFn(input); }, 50);
        }, 200);

    });

    $('body').on('mousedown', lessVal, function () {
        var el = $(this);
        var input = el.siblings(newInput);
        lessValFn(input);
        //console.log(input.value);
        timeout = setTimeout(function(){
            interval = setInterval(function(){ lessValFn(input); }, 50);
        }, 200);
    });

    $(moreVal +', '+ lessVal).on("mouseup mouseout", function() {
        clearTimeout(timeout);
        clearInterval(interval);
    });

    function changeValSlider(newValue){
        //копирование данныех в скрыте поля и обновление слайдера
        var tabactual = $(".calclftblock ul.nav li.active").attr("id");

        $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html(newValue)
        var slider = $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .calcslid').data("ionRangeSlider");
        slider.update({ from: newValue});


        //построение графиков
        var maxvklad=5000000;
        var maxdohod= Math.round(parseInt(   ((maxvklad*0.4752)/12)*18   ));
        var tabactual = $(".calclftblock ul.nav li.active").attr("id");
        var strsumvloj = Math.round(parseInt(   $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html()   )) ;
        var strsrokvloj = Math.round(parseInt(  $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html()     ));
        var percvlkad = (strsumvloj/maxvklad)*100;
        var itogdohodzames = 0;
        var itogdohod = 0;
        var itogdohodzavesper = 0;
        if(tabactual=="razvgodtab"){
            //капитализация
            $(".calcgraygadblock").slideUp("slow").css("opacity","0");
            itogdohod = Math.round(parseInt(   ((strsumvloj*0.4752)/12)*strsrokvloj   ));
            /*for(var i=0; i<strsrokvloj; i++) {
                sumskapatalizats += (sumskapatalizats*0.36)/12;
            } */
        }else{
            //без капитализации
             $(".calcsummzaper span").html( accounting.formatMoney(Math.round(parseInt(((strsumvloj*0.36)/12))), {symbol:"",format:"%v %s",thousand:" ",precision:0} )  );
            itogdohod = Math.round(parseInt(((strsumvloj*0.36)/12)*strsrokvloj   ));
            $(".calcgraygadblock").slideDown("slow").css("opacity","1");
        }
        var perdohod = (itogdohod/maxdohod)*100;
        if(perdohod<1){perdohod=1;}
        if(percvlkad<1){percvlkad=1;}
        $('#chartvklad .calclvkaldblocktxt span').html( accounting.formatMoney(strsumvloj, {symbol:"руб",format:"%v %s",thousand:" ",precision:0} )  );
        $("#chartvklad").animate({'height' : percvlkad + 'px'},800);
        $('#chartdoh .calclvkardblocktxt span').html( accounting.formatMoney(itogdohod, {symbol:"руб",format:"%v %s",thousand:" ",precision:0} )  );
        $("#chartdoh").animate({'height' : perdohod + 'px'},800);
        $(".calcbueblock .calcchistpribdeng").html( accounting.formatMoney(strsumvloj+itogdohod, {symbol:"",format:"%v %s",thousand:" ",precision:0} )  );
    }

    function moreValFn(input){
        if(input.attr('readonly')!=="readonly"){
            var max = input.attr('max');
            checkInputAttr(input);
            var newValue = value + step;
            var newValue4chek = value + step*2;
            if (newValue4chek >= max) {
                newValue = max;
            }
            changeValSlider(newValue)
            changeInputsVal(input, newValue);

        }
    }

    function lessValFn(input){
        if(input.attr('readonly')!=="readonly"){
            var min = input.attr('min');
            checkInputAttr(input);
            var newValue = value - step;
            if (newValue <= min) {
                newValue = min;
            }
            changeValSlider(newValue);
            changeInputsVal(input, newValue);
        }
    }

    function changeInputsVal(input, newValue){
        input.val(newValue);
        var inputNumber = input.siblings(this);
        inputNumber.val(newValue);
    }

    function checkInputAttr(input) {
        if (input.attr('value')) {
            value = parseFloat(input.attr('value'));
        } else if (input.attr('placeholder')) {
            value = parseFloat(input.attr('placeholder'));
        }
        if (!( $.isNumeric(value) )) {
            value = 0;
        }
        if (input.attr('step')) {
            step = parseFloat(input.attr('step'));
        } else {
            step = 1;
        }
    }

    $(newInput).change(function () {
        var input = $(this);
        var value = parseFloat(input.val());
        var min = input.attr('min');
        var max = input.attr('max');
        if (value < min) {
            value = min;
        } else if (value > max) {
            value = max;
        }
        if (!( $.isNumeric(value) )) {
            value = '';
        }
        input.val(value);
        input.siblings(this).val(value);
        changeValSlider(value);
        //console.log("cahnger"+value)
    });

    $(newInput).keydown(function(e){
        var input = $(this);
        var k = e.keyCode;
        if( k == 38 ){
            moreValFn(input);
        }else if( k == 40){
            lessValFn(input);
        }
    });
     if (typeof changeVal == 'function') { // make sure the callback is a function
        changeVal.call(this); // brings the scope to the callback
    }
}})(jQuery);
