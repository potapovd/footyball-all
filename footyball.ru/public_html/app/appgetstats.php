<?php
include_once("appconfig.php");

$user = json_encode($_GET);
$fh = fopen("log.txt","a");
fwrite($fh,( date("j/n/Y H:i:s")."\n"));  
fwrite($fh,($user."\n"));
//fclose($fh);


$curl = curl_init($loginurl);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 

$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ( $status != 200 ) {
    die("Error: call to URL failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
}

curl_close($curl);

$response = json_decode($json_response, true);

//echo "<pre>";
//print_r($response);
//echo "</pre>";


$access_token = $response['access_token'];
$instance_url = $response['instance_url'];
//echo "<br>=======<br>";
//echo "access_token ".$access_token ;
//echo "<br>";
//echo "instance_url ".$instance_url ;
//echo "<br>=======<br>";

if (!isset($access_token) || $access_token == "") {
    die("Error - access token missing from response!");
}

if (!isset($instance_url) || $instance_url == "") {
    die("Error - instance URL missing from response!");
}






session_start();
$_SESSION['access_token'] = $access_token;
$_SESSION['instance_url'] = $instance_url;


function show_accounts($instance_url, $access_token) {
    //$query = "SELECT Name, Id from Account LIMIT 100";
    //$url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);
    //https://cs17.salesforce.com/services/apexrest/SalesforceAgent/

    //echo "<pre>";
    //print_r($_GET);
    //echo "</pre>";
    $employeeId = $_GET["employeeId"];
    $datestart = $_GET["datestart"];
    $dateend = $_GET["dateend"];

    //$employeeId = "a0Ng0000002sBFE";
    //$employeeId = "a0Ng0000002PGxaEAG";
    //$datestart = 1420070400000;
    //$dateend = 1446336000000;


    $url = "$instance_url/services/apexrest/SalesforceAgent/?method=getStats&employeeId=".$employeeId."&datestart=".$datestart."&dateend=".$dateend." ";

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
    curl_setopt($curl, CURLOPT_HTTPHEADER,
                  array(
                    "Authorization: OAuth $access_token",
                    "Content-Type:  application/json"
                  )
                );

    $json_response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ( $status != 200 ) {
        die("Error: call to URL failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    }
    curl_close($curl);


    //var $jsonObject = json_decode($json_response);
    //var $response = $jsonObject.data.result;


    $response = json_decode($json_response, true);
    //$response = json_encode(serialize($json_response));
    //$response = json_encode(($json_response));




    //echo "<br>=======<br>"; 
    //echo "<pre>";
    echo($response);
    //echo "</pre>";
    $fh = fopen("log.txt","a");
    fwrite($fh,($response."\r\n")); // add newline for next time
    fwrite($fh,("\n")); // add newline for next time
    fclose($fh);

  
}



echo (show_accounts($instance_url, $access_token));


?>