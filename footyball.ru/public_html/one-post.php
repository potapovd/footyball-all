<?
include_once("lib/cms_view_inc.php");

//if(isset($_GET["cat"])&&isset($_GET["id"])) {
if(isset($_GET["id"])) {
    //$cat = cleardata($_GET["cat"]);
    $id = cleardata($_GET["id"],"i");
    if($id<1){
        header("Location: http://".$_SERVER['HTTP_HOST']);
        exit();
    }else{
        $blogonepost = getoneblogpost($id);
        updatebogpostcounter($id);
        if(empty($blogonepost)){
            header("Location: http://".$_SERVER['HTTP_HOST']);
            exit();
        }
    }
}else{
    header("Location: http://".$_SERVER['HTTP_HOST']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$blogonepost[0]["blogpostsname"];?></title>
    <meta name="description" content="<?=$blogonepost[0]["blogpostsname"];?> Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-blog.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row" id="blog-one-post">
                <div class="col-sm-6 col-sm-offset-3">
                    <article id="one-post-inner">
                        <header>
                            <div class="blog-all-onelpost-line1">
                                <a href="one-post.php">
                                    <img src="photo/blog/<?=$blogonepost[0]["imgurl"];?>" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="blog-all-onelpost-line2">
                                <span class="blog-all-onelpost-line2-date"><?=$blogonepost[0]["date"];?></span>
                                |
                                <span class="blog-all-onelpost-line2-cat"><?=$blogonepost[0]["name"];?></span>
                            </div>
                            <div class="blog-all-onelpost-line3">
                                <h1 class="blog-all-onelpost-line2-postanme">
                                    <?=$blogonepost[0]["blogpostsname"];?>
                                </h1>
                                <!-- AddToAny BEGIN -->
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin: 20px 0">
                                    <!--<a class="a2a_dd" href="https://www.addtoany.com/share"></a>-->
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_vk"></a>
                                    <a class="a2a_button_odnoklassniki"></a>
                                </div>
                                <!-- AddToAny END -->
                            </div>
                        </header>
                        <div class="opne-post-maincontent">
                            <?=$blogonepost[0]["text"];?>
                        </div>
                    </article>

                </div>
            </div>
        </div>
        
        <div class="row rspline3">
            <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                <div class="buttblock">
                    <div class="buttout">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            <div class="col-sm-4 col-xs-12 butout">
                                <a href="#" class="butt toggle-menu menu-top">записаться на тренировку</a>
                            </div>
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                        </div>
                    </div>
                    <div class="butline"></div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row" id="blog-one-post">
                <div class="col-sm-6 col-sm-offset-3">
                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin: 20px 0">
                        <!--<a class="a2a_dd" href="https://www.addtoany.com/share"></a>-->
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_vk"></a>
                        <a class="a2a_button_odnoklassniki"></a>
                    </div>
                    <!-- AddToAny END -->
                </div>
            </div>
        </div>


		    <div class="container-fluid nopadding">
			    <?include_once("inc/map.inc.php");?>
		    </div>
        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
		    <div class="container-fluid nopadding" id="mainblockfooter">
			    <?include_once("inc/footer-new.inc.php");?>
			    <?include_once("inc/hide-popups.inc.php");?>
		    </div>

    </div>

    <?include_once("inc/allmainjs.php");?>
    <script async src="//static.addtoany.com/menu/page.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>

	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}
    $(document).ready(function() {


        $('.buttback a').click(function(){
            parent.history.back();
            return false;
        });

        $("#one-post-inner .opne-post-maincontent img").addClass("img-responsive");


	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });

	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
		    });
		    $('.b9 .item:first-of-type').click();
	    };


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>
