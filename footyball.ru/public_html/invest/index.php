<?
include_once("../lib/cms_view_inc.php");

$pageinfo = getpageinfo(6);

?><!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <?include_once("../inc/head.inc.php")?>
</head>

<body>
<a id="link-top" href="#">&and;</a>
<header>
    <div id="fixedupnav" class="headermenu">
        <div class="container">

            <div class="row">
                <div class="col-sm-1 col-xs-2">
                    <div class="menurleft">
                        <button class="toggle-menu menu-left navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-8">
                    <a href="../index.php" class="logositeout">
                        <img class="logosite pull-left" width="171" height="124" src="../assets/img/logo.svg" onerror="this.onerror=null; this.src='../assets/img/logo.png'" alt="FootyBall logo">
                    </a>
                </div>
                <div class="col-sm-7 hidden-xs">
                    <div class="sitelogan">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ ДЛЯ ДОШКОЛЬНИКОВ №1</div>
                </div>
                <div class="col-sm-1 col-xs-2">
                    <div class="menuright">
                        <button class="toggle-menu menu-right navbar-toggle">
                            <i class="fa fa-users"></i>
                        </button>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>

            <!-- Left menu element-->
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
                <h3>
                    <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
                    <span>Меню</span>
                </h3>
                <a href="../index.php"><span>Главная</span></a>
                <a href="../club.php"><span>О нас</span></a>
                <a href="../programms.php"><span>Программы</span></a>
                <a href="../feedback.php"><span>Отзывы</span></a>
                <a href="../footyballtv.php"><span>FootyballTV</span></a>
                <a href="/invest/" class="activemanimenu"><span>Инвeстиции</span></a>
            </nav>
            <!-- Right menu element-->
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">

                <h3>
                    <span>Личный кабинет</span>
                    <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
                </h3>
                <div class="subscrform">
                    <form action="/" method="POST">
                        <input class="namechildinp" type="text" placeholder="Логин" required="required" maxlength="10" size="10">
                        <input class="nameparentinp" type="text" placeholder="Пароль" required="required" maxlength="10" size="10">
                        <input class="buttonsend" type="submit" value="Войти" >
                    </form>
                </div>
            </nav>


        </div>
    </div>
    </header>



    <div class="blok_00-2 bluebg">
        <div class="blok_01">Инвестиции в Footyball</div>
    
    
        <div class="calcblock">
            <div class="calclftblock">
                <div class="calcnameblock">Хочу Заработать</div>
                <div role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" id="razvgodtab">
                            <a href="#razvgod" aria-controls="razvgod" role="tab" data-toggle="tab" id="razvgodbutt">капитализация <br>процентов</a>
                        </li>
                        <li role="presentation" class="active" id="ejemesiachnotab">
                            <a href="#ejemesiachno" aria-controls="ejemesiachno" role="tab" data-toggle="tab" id="ejemesiachnobutt">выплата процентов ежемесячно</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade " id="razvgod">

                            <div class="calcline"></div>
                            <div class="calcline linevklad">
                                <div class="calcslider-wrapper">
                                    <div class="calclinename pull-left">Вклад</div>
                                    <div class="calcslider-wrapperinner pull-left">
                                        <input id="razvgodsumm" class="calcslid" value="500000">
                                        <span id="razvgodsummcopy" class="datacopy">0</span>
                                    </div>
                                    <input id="razvgodsummmanual" class="pull-left caclcdisplay-box manualchangeinp" step="10000" max="5000000" min="500000">
                                    <div class="manualchangeinppostfix pull-left">руб</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="calcline linesrok">
                                <div class="calcslider-wrapper">
                                    <div class="calcslider-wrapper  pull-left">
                                        <div class="calclinename pull-left">Срок</div>
                                        <div class="calcslider-wrapperinner pull-left">
                                            <input class="calcslid" id="razvgodmes" value="6">
                                            <span class="datacopy" id="razvgodmessummcopy">0</span>
                                        </div>
                                        <input class="pull-left  caclcdisplay-box manualchangeinp" id="razvgodmesmmanual" readonly max="18" min="6">
                                        <div class="manualchangeinppostfix pull-left">мес</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calcline">
                                <div class="calclinename calclinename-perc">Процентная ставка</div>
                                <div class="calcpercnumb">28% <span>годовых</span></div>
                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane fade active" id="ejemesiachno">
                            <div class="calcline"></div>
                            <div class="calcline linevklad">
                                <div class="calcslider-wrapper">
                                    <div class="calclinename pull-left">Вклад</div>
                                    <div class="calcslider-wrapperinner pull-left">
                                        <input id="razvgodsumm2" class="calcslid" value="500000">
                                        <span id="razvgodsummcopy2" class="datacopy">0</span>
                                    </div>
                                    <input id="razvgodsummmanual2" class="pull-left caclcdisplay-box manualchangeinp" step="10000" max="5000000" min="500000">
                                    <div class="manualchangeinppostfix pull-left">руб</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="calcline linesrok">
                                <div class="calcslider-wrapper">
                                    <div class="calcslider-wrapper  pull-left">
                                        <div class="calclinename pull-left">Срок</div>
                                        <div class="calcslider-wrapperinner pull-left">
                                            <input class="calcslid" id="razvgodmes2" value="6">
                                            <span class="datacopy" id="razvgodmessummcopy2">0</span>
                                        </div>
                                        <input class="pull-left  caclcdisplay-box manualchangeinp" id="razvgodmesmmanual2" readonly max="18" min="6">
                                        <div class="manualchangeinppostfix pull-left">мес</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calcline">
                                <div class="calclinename calclinename-perc">Процентная ставка</div>
                                <div class="calcpercnumb">25% <span>годовых</span></div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--<a href="#garantblock" class="strahovka pull-left">
                    Все ваши вложения застрахованы<br>
                    <span style="font-size:18px; color:#616161">Принимаем Вклады От 100.000 Рублей</span>
                </a>-->
            </div>
            <div class="calcrghtblock">
                <div class="calcchart">

                    <div id="chart">
                        <ul id="bars">
                            <li>
                                <div data-percentage="1" class="bar" id="chartvklad">
                                    <div class="calclvkaldblocktxt">
                                        Вклад <br/>
                                        <span>0</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div data-percentage="1" class="bar" id="chartdoh">
                                    <div class="calclvkardblocktxt">
                                        Доход <br/>
                                        <span>0</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>

                </div>
                <div class="calcbueblock">
                    <div class="calcchistprib">Мой доход за весь период</div>
                    <div class="calcchistpribdeng">0</div>
                    <div class="calcrubmout">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span>руб</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div class="calcblueline"></div>
                <div class="calcgraygadblock">
                    <div class="calcnamedoh">Доход за месяц</div>
                    <div class="calcsummzaper"><span>0</span> руб/мес</div>
                </div>
                <div class="calcgraygadblockshadow"></div>
                <!--<a href="#bottomform" class="calclinktoform gotobottomform">оставить заявку</a>-->

            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    
    </div>

   <div class="whitebg">

    <div
        class="blok_03 scrollme animateme"
        data-when="enter"
        data-from="0.5"
        data-to="0"
        data-crop="false"
        data-opacity="0"
        data-scale="1.5">
        <div class="blok_03_02" style="text-align:center;"></div>
    </div>

    <div
        class="blok_03 scrollme animateme"
        data-when="enter"
        data-from="0.5"
        data-to="0"
        data-crop="false"
        data-opacity="0"
        data-scale="1.5">
        <div class="blok_03_01">Почему Мы Готовы Платить Вам Такие Высокие Проценты?</div>
        <div class="blok_03_02">
        <br>
            <p>За несколько лет занятий нашим бизнесом мы создали и отточили нашу бизнес-модель настолько, что она позволяет нам зарабатывать с каждого вложенного рубля в течении года - один рубль чистой прибыли. И 28 копейками мы готовы поделиться с Вами. Другими словами, мы зарабатываем 100 % в год и 28% делимся с Вами.</p>
        </div>
    </div>

    <div class="blok_10 scrollme animateme"
         data-when="enter"
         data-from="0.5"
         data-to="0"
         data-crop="false"
         data-opacity="0"
         data-scale="1.5">
        <div class="blok_10_01">
            Зачем Нужно <br> Инвестировать Именно В
            <span class="questsimb">?</span>
            <a href="//footyball.ru/index.php" class="logotomainsite"></a>
            <div class="clearfix"></div>
        </div>
        <div class="blok_10_02">
            <p>Это Ваш шанс не просто заработать хорошие деньги, но и прикоснуться к развитию одной из самых динамичных, инновационных, успешных и социально значимых компаний России.</p>
        </div>
    </div>

    <div class="blok_10 scrollme animateme"
         data-when="enter"
         data-from="0.5"
         data-to="0"
         data-crop="false"
         data-opacity="0"
         data-scale="1.5"
         style="border-top:none;">
        <div class="blok_10_01">
            Для Чего Мы Привлекаем Инвестиции?
        </div>
        <div class="blok_10_02">
            <p>Тщательно проанализировав все возможные вклады на рынке, мы с уверенностью можем сказать, что наше предложение выгодное, надежное и социально значимое.</p><br>
            <p>Мы предлагаем Вам такой высокий процент, так как научились эффективно развивать и масштабировать наш бизнес, мы научились зарабатывать и хотим поделиться прибылью с теми, кто помогает нам расти быстрее. </p><br>
            <p>Наша бизнес-модель успешно протестирована на трёх школах в г.Киев и двух школах в г.Москва. Сейчас идет активный запуск новых школ.

             <!--на <span id="tooltip-map">Кутузовском проспекте д36</span>. В наших, планах до конца следующего года, открытие 10 школ на территории Москвы и Московской области. </p><br>


            <p>Инвестиции целевые, на открытие новых школ.</p>-->
        </div>
    </div>

    <div class="blok_05">
        <div class="blok_05_01  scrollme animateme"
             data-when="enter"
             data-from="0.5"
             data-to="0"
             data-crop="false"
             data-opacity="0"
             data-scale="1.5">Валерий Газзаев О Нас</div>
        <div class="blok_05_02">
            <div class="playerbg">
                <div class="steps_result_video2 no-svg ">
                    <!--<video id="youtube1" width="690" height="360" preload="none">
                        <source src="http://www.youtube.com/watch?v=G1EGBSCT0DQ" type="video/youtube" >
                    </video>
                    <div class="js-lazyYT" data-youtube-id="G1EGBSCT0DQ" data-width="690" data-height="360" data-ratio="16:9"></div>-->
                    <video width="640" height="360" poster="/photo/tv/3-1458722316-gazzaevSajt.jpg" id="mainvideo" controls="controls" preload="none">
                        <source src="/video/tv/3-1458722316-gazzaevSajt.webm" type="video/webm">
                        <source src="/video/tv/3-1458722316-gazzaevSajt.mp4" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>

    <div class="blok_06">
        <div class="blok_06_01">
            <div class="blok_06_02">
                <a href="http://instagram.com/footyball_russia" target="_blank" class="impubbutt animateme scrollme"
                   data-when="enter"
                   data-from="0.75"
                   data-to="0"
                   data-opacity="0"
                   data-translatex="-400">
                </a>
                <a href="https://vk.com/footyballru" target="_blank" class="vkpubbutt animateme scrollme"
                   data-when="enter"
                   data-from="0.75"
                   data-to="0"
                   data-opacity="0"
                   data-translatex="400">
                </a>
            </div>
        </div>
    </div>

    <div class="blok_07">
        <div class="blok_07_01">Отзывы Наших Клиентов</div>
    </div>

    <div class="clearfix"></div>


        <div style="width: 900px; margin: 0 auto" data-when="enter"
             data-from="0.5"
             data-to="0"
             data-crop="false"
             data-opacity="0"
             data-scale="1.5"
             class="scrollme animateme"

        >
            <div class="dragdiv tmpsprite blockfeatbpage" style="z-index: 1;">
               <div class="nameblock nameblockfeatbpage">
                   <span style="font-size:25px;line-height:20px">ОТЗЫВЫ</span> <span style="font-size:17px">Клиентов:</span>
               </div>
               <div class="blockfeatbinner">
                   <img class="blockfeatbimg" alt="Денис и Ирина Никифоровы" src="/video/feedback/Denis_i_Irina_Nikiforovy1454577567.jpg">
                   <div class="blockfeatbtxtnname" style="height:205px">
                       <div class="blockfeatbtxt2">
                           <a data-videofeedbid="Denis_i_Irina_Nikiforovy1454577567" data-modawindowid="modalwindowvideofeed" class="openvideopop open-modal" href="#">
                               Сообщение от Денис и Ирина Никифоровы                                </a>
                       </div>
                       <div class="blockfeatbname">Денис и Ирина Никифоровы</div>
                   </div>
               </div>
            </div>


            <div class="dragdiv tmpsprite blockfeatbpage" style="z-index: 1;">
               <div class="nameblock nameblockfeatbpage">
                   <span style="font-size:25px;line-height:20px">ОТЗЫВЫ</span> <span style="font-size:17px">Клиентов:</span>
               </div>
               <div class="blockfeatbinner">
                   <img class="blockfeatbimg" alt="Сергей Кирьяков" src="/video/feedback/Sergej_Kiryakov1454405780.jpg">
                   <div class="blockfeatbtxtnname" style="height:205px">
                       <div class="blockfeatbtxt2">
                           <a data-videofeedbid="Sergej_Kiryakov1454405780" data-modawindowid="modalwindowvideofeed" class="openvideopop open-modal" href="#">
                               Сообщение от Сергей Кирьяков                                </a>
                       </div>
                       <div class="blockfeatbname">Сергей Кирьяков</div>
                   </div>
               </div>
            </div>

            <div class="dragdiv tmpsprite blockfeatbpage" style="z-index: 1;">
               <div class="nameblock nameblockfeatbpage">
                   <span style="font-size:25px;line-height:20px">ОТЗЫВЫ</span> <span style="font-size:17px">Клиентов:</span>
               </div>
               <div class="blockfeatbinner">
                   <img class="blockfeatbimg" alt="Яна Козлова" src="/video/feedback/YAna_Kozlova1454405062.jpg">
                   <div class="blockfeatbtxtnname" style="height:205px">
                       <div class="blockfeatbtxt2">
                           <a data-videofeedbid="YAna_Kozlova1454405062" data-modawindowid="modalwindowvideofeed" class="openvideopop open-modal" href="#">
                               Сообщение от Яна Козлова                                </a>
                       </div>
                       <div class="blockfeatbname">Яна Козлова</div>
                   </div>
               </div>
            </div>
        </div>



       <div class="clearfix"></div>



       <!--<div class="blok_05">
           <div class="blok_05_01  scrollme animateme"
                data-when="enter"
                data-from="0.5"
                data-to="0"
                data-crop="false"
                data-opacity="0"
                data-scale="1.5">Гарик Мартиросян О Нас</div>
           <div class="blok_05_02">
               <div class="playerbg">
                   <div class="steps_result_video2 no-svg ">
                       <video width="640" height="360" poster="/photo/tv/3-1454922927-mart_sajt1.jpg" id="mainvideo" controls="controls" preload="none">
                           <source src="/video/tv/3-1454922927-mart_sajt1.webm" type="video/webm">
                           <source src="/video/tv/3-1454922927-mart_sajt1.mp4" type="video/mp4">
                       </video>
                   </div>
               </div>
           </div>
       </div>-->
       <div class="clearfix" style="height:40px"></div>



    <!--<div class="blok_00  scrollme animateme"
         data-when="enter"
         data-from="0.5"
         data-to="0"
         data-crop="false"
         data-opacity="0"
         data-scale="1.5">
        <div class="blok_09">
            <div class="blok_09_01">
                <img src="36p/img/feed02.png" alt="" width="99" height="99" border="0">
            </div>
            <div class="blok_09_02">
                <div class="feedbackinner">
                    <p>Ярослав дома отрабатывает пас мячом, теперь мы спим с мячом, едим с мячом, гуляем с мячом, у нас даже кружка в форме футбольного мяча :) Футибол &ndash; спасибо Вам огромное, за такие эмоции, ребенок мой счастлив!</p>
                </div>
                <span class="blok_09_t">Элина Сержантова, сын Ярослав, 5 лет 9 мес</span>
            </div>
            <div class="blok_09_03">
                <a href="https://vk.com/album-78067910_210483563" target="_blank" class="feedbackimg feedback02">
                    <img src="36p/img/feed_cos02-2.png" alt="" width="272" height="116" border="0" >
                </a>
                <span class="blok_09_tt">источник: <a href="https://vk.com/album-78067910_210483563" target="_blank">vk.com</a></span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="blok_00  scrollme animateme"
         data-when="enter"
         data-from="0.5"
         data-to="0"
         data-crop="false"
         data-opacity="0"
         data-scale="1.5">
        <div class="blok_09">
            <div class="blok_09_01">
                <img src="36p/img/feed03.png" alt="" width="99" height="99" border="0" >
            </div>
            <div class="blok_09_02">
                <div class="feedbackinner">
                    <p>
                    Ходим на Футибол с конца октября, сыну 4г7мес ))) самое главное на мой взгляд - это искренняя заинтересованность ребенка и полное его самостоятельное решение ходить именно в этот клуб. После первых трех занятий ребенок буквально зафанател футболом! Изменения очень видны, сын научился не просто пинать мячик, а выполнять различные манипуляции, передавать пас, обводить конусы и мяч к моему великому удивлению от него не укатывается! Спасибо огромное тренерскому составу за подход и позитивный настрой во всём ))) у сына усилились лидерские качества, при этом командный дух тоже имеет место. Удивительно но это так. Английские слова - приятный бонус к занятиям футболом, чтобы получать звезды ребенок готов их учить по доброй воле и желании ))) Безусловно очень радует окружающий сервис и отзывчивость к пожеланиям родителей! Предусмотрено практически всё, даже уборная есть специальная детская для наших чемпионов. Родителям не менее комфортная зона ожидания, сопровождающим сестрам и малышам - игровая. Недостатком является оплата за весь период обучения сразу, думаю если б ввели ежемесячную оплату было бы удобнее. Это пожалуй смущало больше всего - особенно при первых встречах. Однако стоит заметить, что мы ни разу не пожалели о сделанном выборе и я согласна что за комфорт и качество нужно платить. Огромное спасибо всей команде футибола за позитив, за моего чемпиона, за комфорт и возможность быть услышанными )))
                    </p>
                </div>
                <span class="blok_09_t">Юлия Плетнёва, сын Артём, 4 года и 7 месяцев</span>
            </div>
            <div class="blok_09_03">
                <a href="https://www.facebook.com/footyballru/reviews" target="_blank" class="feedbackimg feedback02">
                    <img src="36p/img/feed_cos03.png" alt="" width="272" height="116" border="0" >
                </a>
                <span class="blok_09_tt">источник: <a href="https://www.facebook.com/footyballru/reviews" target="_blank">facebook.com</a></span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>-->

   
    <div class="blok_00  scrollme animateme"
         data-when="enter"
         data-from="0.5"
         data-to="0"
         data-crop="false"
         data-opacity="0"
         data-scale="1.5">
       <!-- <div id="garantblock" class="blok_11">
            <div class="blok_11_01">
                <div class="linebg"></div>
                <span>Гарантии</span>
            </div>
            <div class="garnatinner">
                <div class="garanttxt garantlimg">
                    <p>Все вклады застрахованы <br> надежной страховой <br> компанией</p>
                </div>
                <div class="garanttxt garantrimg">
                    <p>Каждый заключенный договор регистрируется официально (по желанию вкладчика)</p>
                </div>
                <div class="clearfix"></div>
                <div class="garanttxt garantlimg">
                    <p>Собственники компании несут <br> полную личную ответственность. <br>В том числе и уголовную согласно законодательству РФ</p>
                </div>
                <div class="garanttxt garantrimg">
                    <p>Надежность вклада обеспечена высокодоходной бизнес моделью, которая успешно протестирована в сложных экономических условиях</p>
                </div>
            </div>
        </div>-->
    </div>


    <div class="blok_00-2  scrollme animateme"
             data-when="enter"
             data-from="0.5"
             data-to="0"
             data-crop="false"
             data-opacity="0"
             data-scale="1.7">
        <div class="blok_12">
            <div class="blok_12_block"
              id="bottomform">
                <div class="blok_12_01">
                    <div class="blok_12_02">Для Дополнительной Информации <br> Нажмите Кнопку Ниже</div>
                    <a href="#" class="knowmorebutton">УЗНАТЬ БОЛЬШЕ</a>

                    <div class="clearfix"></div>

                    <div id="knowmoreblock" >
                        <div class="knowmoreblockinner" >
                            <div class="howwork  scrollme animateme"
                                 data-when="enter"
                                 data-from="0.5"
                                 data-to="0"
                                 data-crop="false"
                                 data-opacity="0"
                                 data-scale="1.7">
                                <div class="blockname">Как Это Работает</div>
                                <div class="howworkinner">
                                    <span class="howworktxt howworktxt1">Вы оставляете свои данные </span>
                                    <span class="howworktxt howworktxt2">Мы перезваниваем Вам в рабочее время и приглашаем на встречу к нам в офис</span>
                                    <span class="howworktxt howworktxt3">Вы приезжаете на встречу и узнаете все подробности</span>
                                    <span class="howworktxt howworktxt4">Вы начинаете зарабатывать</span>
                                </div>
                            </div>
                            <div class="bigform  scrollme animateme"
                                 data-when="enter"
                                 data-from="0.5"
                                 data-to="0"
                                 data-crop="false"
                                 data-opacity="0"
                                 data-scale="1.7">
                                <div class="blockname">Заполните Данные Ниже</div>
                                <div class="bigforminner">
                                    <form action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" id="sendform">
                                    <!--<form action="/" method="POST" >-->
                                        <input type="hidden" name="oid" value="00D20000000onaw">
                                        <input type="hidden" name="retURL" value="http://footyball.ru/36-thankyou">
                                        <input type="hidden" id="00N200000099lqG" name="00N200000099lqG" type="checkbox" value="1" />
                                        <input type="hidden" id="lead_source" name="lead_source" value="Web form" />
                                        <div class="bigformhr"></div>
                                        <div class="bigformform">
                                            <div class="bigformlft">
                                                <div class="bigformformsmname">Вкладчик: </div>
                                                <input id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="ИМЯ" required="required" data-required="true" />
                                                
                                                <input id="last_name" maxlength="80" name="last_name" size="20" type="hidden" placeholder="ФАМИЛИЯ"  />
                                                
                                                <input id="phone" maxlength="40" name="phone" size="20" type="text" placeholder="ТЕЛЕФОН" required="required" data-required="true" />
                                                 <div class="bigformformsmname">
                                                    <input type="checkbox" id="checkbox-1-1"  name="bigformformmorefils" value="true" class="regular-checkbox" /><label for="checkbox-1-1"></label>
                                                    Я Рекомендатель<br>
                                                    <span style="font-size:16px;">(Рекомендатель получает 3% от вклада моментально)</span>
                                                </div>

                                                <div id="bigformformmorefils">
                                                    <div class="bigformformmorefilshr"></div>
                                                    <div class="bigformformsmname">Данные Рекомендателя:</div>
                                                    <input id="00N200000099sLl" maxlength="255" name="00N200000099sLl" size="20" placeholder="ИМЯ" type="text" />
                                                    
                                                    <input id="00N200000099sLm" maxlength="255" name="00N200000099sLm" size="20" placeholder="ФАМИЛИЯ" type="text" />
                                                    
                                                    <input id="00N200000099sLn" maxlength="255" name="00N200000099sLn" size="20" placeholder="ТЕЛЕФОН" type="text" />
</div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="bigformhr"></div>
                                        <div class="bigformtxt">Все сведения/персональные данные, предоставленные получателями услуг, считаются строго конфиденциальными
                                        </div>
                                        <input class="bigformsend" type="submit" name="submit" value="ОТПРАВИТЬ">
                                    </form>
                                </div>
                            </div>
                       </div>
                    </div>

                </div>
            </div>
        </div>
        
        


    </div>
    
    </div>
    

<?include_once("../inc/footer.inc.php")?>

<div class="modal-overlay"></div>
<?include_once("../inc/modal-form.inc.php")?>
    <div class="modal-wrapper">
        <div class="modal-window js-blur" id="modalwindowlogin" style="width:550px; height:350px">
            <button class="close-modal">X</button>
            <h2>ПЕРСОНАЛЬНЫЙ КАБИНЕТ</h2>
            <div class="subscrform">
                <form action="#">
                    <input type="text" placeholder="Логин">
                    <input type="text" placeholder="Пароль">
                    <input type="submit" class="buttonsend" value="Войти">
                </form>
            </div>
        </div>
    </div>

    <div class="modal-wrapper">
        <div class="modal-window js-blur" id="modalwindowvideofeed" style="width:730px; height:480px">
            <button class="close-modal">X</button>
            <div id="videofeetbinner"></div>

        </div>
    </div>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.0.8/jquery.mCustomScrollbar.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/0.9.0/jquery.mask.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.0.8/jquery.placeholder.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/h5Validate/0.8.4/jquery.h5validate.min.js" defer="defer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/tooltipster/3.0.5/js/jquery.tooltipster.min.js" defer="defer"></script>
    
    <!--
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.8/jquery.mCustomScrollbar.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/jquery.ion.rangeslider/2.0.6/js/ion.rangeSlider.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/accounting.js/0.3.2/accounting.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.mask/1.11.4/jquery.mask.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/jquery.placeholder/2.1.1/jquery.placeholder.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/jquery.h5validate/0.9.0/jquery.h5validate.min.js" defer="defer"></script>
    <script src="//cdn.jsdelivr.net/jquery.transition/1.7.2/jquery.transition.min.js"></script>
    -->
    <script>
    var ismob = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        ismob = true;
    }
    if(ismob===false){
        document.write('<script src="36p/js/jquery.scrollme.min.js" defer="defer"><\/script>');
    }
    </script>

    <!--<script src="36p/js/jquery.scrollme.min.js" defer="defer"></script>-->
    <script src="36p/js/jquery-letterfx.min.js" defer="defer"></script>
    <script src="36p/js/accounting.min.js" defer="defer"></script>
    <script src="36p/js/mediaelement-and-player.min.js" defer="defer"></script>
    <script src="36p/js/iLightInputNumber.min.js" defer="defer"></script>
    <script src="36p/js/ion.rangeSlider.min.js" defer="defer"></script>
    <script src="36p/js/scripts_new.js"></script>
    

    <script src="../assets/js/jquery.sticky.js" defer="defer"></script>
    <script src="../assets/js/mediaelement-and-player.min.js" ></script>
    <script src="../assets/js/TweenMax.min.js" defer="defer"></script>

    <script src="../assets/js/TweenMax.min.js" defer="defer"></script>
    <script src="../assets/js/jPushMenu.js"></script>
    <!--<script src="../assets/js/motionblur.js" defer="defer"></script>-->
<!--<script src="../assets/js/modal.js"  defer="defer"></script>-->

    <script>

        $('#fixedupnav').on('sticky-start', function() {
            //console.log("Started");
            $('#link-top').fadeIn("slow");
            $('.logosite').attr("src","../assets/img/logo-small.svg");
            $('.logosite').attr("onerror","this.onerror=null; this.src='../assets/img/logo-small.png'");
        });
        $('#fixedupnav').on('sticky-end', function() {
            //console.log("Ended");
            $('#link-top').fadeOut("slow");
            $('.logosite').attr("src","../assets/img/logo.svg");
            $('.logosite').attr("onerror","this.onerror=null; this.src='../assets/img/logo.png'");
        });
        
    $(function() {  
        
        //ranbombg();
        $('.toggle-menu').jPushMenu({
            closeOnClickLink: false
        });


        $('video').mediaelementplayer({
            //  alwaysShowControls: true ,
            features: ['playpause','progress','volume','fullscreen'],
            videoVolume: 'horizontal',
            startVolume: 0.7,
            enableKeyboard: false
        });
        
        $('input[name="phone"]').mask('+7(999) 999 99 99');
        $(".agechildinp").ionRangeSlider({
            min: 2,
            max: 7,
            from: 3,
            grid: true,
            values: [2, 2.5, 3, 3.5, 4, 4.5, 5,5.5,6,6.5,7],
            max_postfix: " лет"
        });


        $('.openvideopop').on('click',function(e){
            e.preventDefault();

            var  videofeedbid = $(this).attr("data-videofeedbid");
            var videoiiner = //"<h5 class='tvvideobloknamevieo'></h5>"+
                "<div class='no-svg blockvideo' style='margin-top:40px'>"+
                "<video width='640' height='360' poster='/assets/video/simple.jpg' id='videofeed' controls='controls' preload='none' autoplay='autoplay'>"+
                "<source src='/video/feedback/"+videofeedbid+".webm' type='video/webm'>"+
                "<source src='/video/feedback/"+videofeedbid+".mp4' type='video/mp4'>"+
                "</video>"+
                    //   "<div class='tvdesctblock'>"+
                    //                result[2]+
                    //    "</div>"+
                "</div>";

            $("#modalwindowvideofeed #videofeetbinner").html( videoiiner )
            $('#videofeed').mediaelementplayer({
                alwaysShowControls: true ,
                features: ['playpause','progress','volume','fullscreen'],
                videoVolume: 'horizontal',
                startVolume: 0.7,
                enableKeyboard: false
            });

            return false;
        });
        
        
    	$('#tooltip-map').tooltipster({
            content: $('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.9658692295566!2d37.523714!3d55.741721999999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54be40b9041f5%3A0x4d8e1849ab11e958!2z0JrRg9GC0YPQt9C-0LLRgdC60LjQuSDQv9GA0L7RgdC_LiwgMzbRgTUxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTIxMTcw!5e0!3m2!1sru!2sde!4v1431090886045" width="400" height="300" frameborder="0" style="border:0"></iframe>'),
        	hideOnClick:true,
        	trigger:'click',
        	//autoClose:false,
        	theme:"tooltipster-light"
        });

        var ismob = false;
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            ismob = true;
        }    

        $('#razvgodbutt').trigger('click');
        $('#ejemesiachnobutt').trigger('click');

        if(ismob===false){
            setTimeout(show1line, 500)
            setTimeout(show2line, 5000)
        }
        

        $("#razvgodsumm,#razvgodsumm2").ionRangeSlider({
                min: 500000,
                max: 5000000,
                from: 500000,
                grid: true,
                step: 10000,
                prettify_enabled: true,
                postfix: " руб",
                hide_min_max: true,
                //values:[100000,500000,1000000,2000000,3000000],
                onFinish: function (data) {
                    var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                    //console.log(tabactual)
                     $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(data.from);
                     //$('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(  );
                     $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html(data.from);
                    chart();
                }
            });
        $("#razvgodmes,#razvgodmes2").ionRangeSlider({
                min: 6,
                max: 18,
                from: 6,
                grid: true,
                step: 1,
                postfix: " месяцев",
                hide_min_max: true,
                //values:[1,3,6,9,12,15,18],
                onFinish: function (data) {
                      var tabactual = $(".calclftblock ul.nav li.active").attr("id");
                     //console.log(tabactual)
                      $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .manualchangeinp').val(data.from);
                      $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html(data.from);
                    chart();
                }
            });
        $('.manualchangeinp').iLightInputNumber({});

        $('.js-lazyYT').lazyYT();

        updatedata();
        chart();

        $('#sendform').h5Validate();
        $('input[name="phone"],input[name="00N200000099sLn"]').mask('+7(999) 9999999');
        $("#checkbox-1-1").prop("checked",false);
        $('input').placeholder();
        
        
        
        $("#fixedupnav").sticky({
            topSpacing: 0,
            zIndex:10,
            center:true,
            responsiveWidth:true
        });

        $('#maincontent, .headerline2  .clearfix, .blok_00-2, .blok_03, .blok_10, .blok_05, .blok_06, .blok_07,.blok_00').on('click',function(e){
            //alert("1");
            if( $(".footer").hasClass('footeron')  ){
                $(".footer").removeClass('footeron')
                $('html,body').stop().animate({scrollTop: $(".footmapblock").offset().top},1500);
                $(".footer2line").animate({height:0},1500);
                $('.footmapblock').removeClass('footmapblockactive');
            }
        });
     

        $(".footmapblock").toggle(function(){
            $(".footer").addClass('footeron')
            $('#footertabmap1 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2249.873720377262!2d37.44017121028134!3d55.67379605147178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x63e9cf9fc167b361!2z0J7Rh9Cw0LrQvtCy0L4!5e0!3m2!1sru!2sua!4v1431008228089"  width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap2 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.4418665347575!2d37.772959!3d55.750826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414acaa351f2331f%3A0xd39dfa163bf5112a!2zMS3QuSDQn9C10YDQvtCy0LAg0J_QvtC70Y8g0L_RgC3QtCwgOdGBNSwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDExMTE0MQ!5e0!3m2!1sru!2sde!4v1429531355805" width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap3 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.9658692295566!2d37.523714!3d55.741721999999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54be40b9041f5%3A0x4d8e1849ab11e958!2z0JrRg9GC0YPQt9C-0LLRgdC60LjQuSDQv9GA0L7RgdC_LiwgMzbRgTUxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTIxMTcw!5e0!3m2!1sru!2sde!4v1431090886045"  width="593" height="449" frameborder="0" style="border:0"></iframe>');
            $('#footertabmap4 .mapinner').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2243.271348910248!2d37.58248731550191!3d55.788525996704244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b549f6391b2b55%3A0xae00b01ed57b1dd!2z0J_RgNCw0LLQtNGLINGD0LsuLCAyNNGBNSwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDEyNzEzNw!5e0!3m2!1sru!2sde!4v1458800006210"  width="593" height="449" frameborder="0" style="border:0"></iframe>');


            $(".footer2line").animate({height:460},1500);
            $('html,body').stop().animate({scrollTop: $(".footer2line").offset().top},1500);
            //footmapblockactive
            $('.footmapblock').addClass('footmapblockactive');
        },function(){
            $(".footer").removeClass('footeron')
            $('html,body').stop().animate({scrollTop: $(".footmapblock").offset().top},1500);
            $(".footer2line").animate({height:0},1500);
            $('.footmapblock').removeClass('footmapblockactive');
        });


        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        
        
        var $modal = $(".modal-window"),
            $overlay = $(".modal-overlay"),
            blocked = false,
            unblockTimeout = null;

        TweenMax.set($modal, {
            autoAlpha: 0
        });

        var isOpen = false;

        function openModal(modalname) {
            var $modal = $("#"+modalname+"");
            //alert(modal)

            if (!blocked) {
                $modal.addClass("openedwindow");
                block(400);
                //alert($modal)

                TweenMax.to($overlay, 0.3, {
                    autoAlpha: 1
                });

                TweenMax.fromTo($modal, 0.5, {
                    x: (-$(window).width() - $modal.width()) / 2 - 50,
                    scale: 0.9,
                    autoAlpha: 1
                }, {
                    delay: 0.1,
                    rotationY: 0,
                    scale: 1,
                    opacity: 1,
                    x: 0,
                    z: 0,
                    ease: Elastic.easeOut,
                    easeParams: [0.4, 0.3],
                    force3D: false
                });
               // $.startUpdatingBlur(800);
            }
            return false;
        }

        function closeModal(modalname) {
            var $modal = $("#"+modalname+"");
            if (!blocked) {
                
                if(modalname=="modalwindowtv"){
                    var player = new MediaElementPlayer('#footyballtv');
                    player.pause(); 
                }
                if(modalname=="modalwindowprogramm"){
                    var player = new MediaElementPlayer('#programvideo');
                    player.pause();
                }
                if(modalname=="modalwindowstep3"){
                    var player = new MediaElementPlayer('#programvideo');
                    player.pause();
                }
                
                
                $modal.removeClass("openedwindow");
                block(400);
                TweenMax.to($overlay, 0.3, {
                    delay: 0.3,
                    autoAlpha: 0
                });
                TweenMax.to($modal, 0.3, {
                    x: ($(window).width() + $modal.width()) / 2 + 100,
                    scale: 0.9,
                    ease: Quad.easeInOut,
                    force3D: false,
                    onComplete: function () {
                        TweenMax.set($modal, {
                            autoAlpha: 0
                        });
                    }
                });
               // $.startUpdatingBlur(400);
            }
            return false;
        }

        function block(t) {
            blocked = true;
            if (unblockTimeout !== null) {
                clearTimeout(unblockTimeout);
                unblockTimeout = null;
            }
            unblockTimeout = setTimeout(unblock, t);
            return false;
        }

        function unblock() {
            blocked = false;
            return false;
        }
        $(".open-modal").click(function () {
            openModal( $(this).attr("data-modawindowid") );

        });
        $(".close-modal, .modal-overlay").click(function () {            
            closeModal(  $(".openedwindow").attr("id")  );
        });
        

        $("body").animate({ opacity: "1" }, 1000);

    });
        

        
    </script>


    <!-- Google Tag Manager -->

    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TN4KB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5TN4KB');</script>
    <!-- End Google Tag Manager -->

    <script>
    //google-analytics
    !function(e,a,t,n,c,o,s){e.GoogleAnalyticsObject=c,e[c]=e[c]||function(){(e[c].q=e[c].q||[]).push(arguments)},e[c].l=1*new Date,o=a.createElement(t),s=a.getElementsByTagName(t)[0],o.async=1,o.src=n,s.parentNode.insertBefore(o,s)}(window,document,"script","//www.google-analytics.com/analytics.js","ga"),ga("create","UA-60426825-1","auto"),ga("send","pageview");
    </script>
    <!-- Yandex.Metrika informer -->

    <a href="https://metrika.yandex.ua/stat/?id=28879840&amp;from=informer" target="_blank" rel="nofollow" style="display:none;visibility:hidden"><img src="//bs.yandex.ru/informer/28879840/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: дані за сьогодні  (перегляди, візити та унікальні відвідувачі)" onclick="try{Ya.Metrika.informer({i:this,id:28879840,lang:'ua'});return false}catch(e){}" /></a>
    <!-- /Yandex.Metrika informer -->
        <!-- Yandex.Metrika counter -->

    <script>
    !function(t,e,a){(e[a]=e[a]||[]).push(function(){try{e.yaCounter28879840=new Ya.Metrika({id:28879840,webvisor:!0,clickmap:!0,trackLinks:!0,accurateTrackBounce:!0})}catch(t){}});var c=t.getElementsByTagName("script")[0],n=t.createElement("script"),r=function(){c.parentNode.insertBefore(n,c)};n.type="text/javascript",n.async=!0,n.src=("https:"==t.location.protocol?"https:":"http:")+"//mc.yandex.ru/metrika/watch.js","[object Opera]"==e.opera?t.addEventListener("DOMContentLoaded",r,!1):r()}(document,window,"yandex_metrika_callbacks");
    </script>
    <noscript>
    <div><img src="//mc.yandex.ru/watch/28879840" style="position:absolute; left:-9999px;" alt="" />
    </div>
    </noscript>

    <!-- /Yandex.Metrika counter -->


    <noscript>
        <style>
            <!--
            body {opacity:1;}
            .calcblock{display:none;}
            .blok_09_02 .feedbackinner{overflow: auto;}
            .blok_12_02,.knowmorebutton{display:none;}
            #knowmoreblock{display:block;height:1130px;}
            #bigformformmorefils{display:block;}
            -->
        </style>
        <div id="jsdisable">У Вашего браузера отключен JAVASCPRIT или он не поддерживает эту технологию.
            <a href="http://anonym.to/?https://support.google.com/answer/23852">Как включить JAVASCPRIT</a>.
            <a href="http://anonym.to/?http://browser-update.org/update-browser.html">Новые браузеры </a>
        </div>
    </noscript>
</body>
</html>
