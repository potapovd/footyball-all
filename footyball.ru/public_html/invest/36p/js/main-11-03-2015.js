// JavaScript Document

$(function() {
	'use strict';
	
	$('#youtube1').mediaelementplayer({features: ['playpause','current','progress','duration','volume','fullscreen']});
	
	var slideStep = 222,
		slideTime = 700,
		isSliding = false,
        d = new Date(),
        h = d.getHours(),
        m = d.getMinutes(),
        s = d.getSeconds(),
		timeForAction = 24 * 60 * 60 - (h * 60 * 60 + m * 60 + s) ; // 12 hours
	
	$('.menu_button').on('click', showPopup);
	$('.popup_bg, .popup_close').on('click', hidePopup);
	
	$('.slider_arrow_left').on('click', slideLeft);
	$('.slider_arrow_right').on('click', slideRight);
	
	$('input[name="phone"]').mask('+7(999) 9999999');
	$('input').on('blur', onInputBlur);
	
	$('.ordering_count_decrease').on('click', decreaseCount);
	$('.ordering_count_increase').on('click', increaseCount);	
	$('form.ordering_form').on('submit', formOrderSubmit);
    $('form.popup').on('submit', formPopupSubmit);

	$('#google_map').attr('width', $('body').width());
	
	$('.rules').on('click', function () {
		$('.popup_bg, .popup_rules').removeClass('hidden');		
	});

    
	/*
	$('.steps_result_video_play').on('click touchstart', function () {
        $('.steps_result_video object').css({
            display : 'block'
        });
        $('.steps_result_video_play').hide();
        $('#placeForVideo').css({
            'z-index' : 1
        });
		tryToPlay();
    });
	*/
	
	if (getParameterByName('actionpay')) {
		var actionpay = getParameterByName('actionpay').split('.');
		$.cookie('click', actionpay[0], {path : '/', expires: 30});
		$.cookie('source', actionpay[1], {path : '/', expires: 30});		
	}	
	
	setTimeout(updateTimer, 1000);


    /*$('a[href*=#]').on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });
	
	
	function tryToPlay() {
		if (player && player.playVideo) {
			player.playVideo();
		} else {
			setTimeout(tryToPlay, 100);
		}
	}
	*/
	
	function showPopup() {
		$('.popup_bg, .popup_container').removeClass('hidden');		
	}
	
	function hidePopup() {
		$('.popup_bg, .popup_container, .popup_rules').addClass('hidden');
	}
	
	function slideLeft () {
        var $slider = $('.slider_container_items'),
            $slide = $('.slider_container_item:last-child');

		if (!isSliding) {
			isSliding = true;
			setTimeout(function() {
				isSliding = false;
			}, slideTime);
            $slide.clone().prependTo($slider)
                .css('width', 0)
                .animate({width: '188px'});
            $slide.remove();
		}
	}

	function slideRight () {
		var $slider = $('.slider_container_items'),
            $slide = $('.slider_container_item:first-child');

		if (!isSliding) {
			isSliding = true;
			setTimeout(function() {
				isSliding = false;
			}, slideTime);
            $slide.clone().appendTo($slider);
            $slide.animate({width: 0, 'margin-right': 0, 'padding' : 0}, slideTime, function () {
                $slide.remove();
            });
		}
			
	}	
	
	function increaseCount (e) {
		var $el = $(e.currentTarget),
			$count = $el.closest('form').find('.ordering_count'),
			value = parseInt($count.text(), 10);
			
			if (value < 9) {
				$count.text(value + 1);
			}
	}
	
	function decreaseCount (e) {
		var $el = $(e.currentTarget),
			$count = $el.closest('form').find('.ordering_count'),
			value = parseInt($count.text());
			
		if (value > 1) {
			$count.text(value - 1);
		}		
	}
	
	function formOrderSubmit (e)	{
        var $form = $(e.currentTarget),
            $name = $form.find('[name="name"]'),
            $phone = $form.find('[name="phone"]'),
            $color = $form.find('input[type="radio"]:checked'),
            $count = $form.find('.ordering_count'),
            $price = $form.find('[name="price"]');

        if ($name.val() && $phone.val()) {
		
			if (localStorage) {
				var orderInfo = {
					color : $color.data('color'),
					count : $form.find('.ordering_count').text(),
					price : $price.val()
				};
				localStorage.setItem('orderInfo', JSON.stringify(orderInfo));
			}	
			
			var data = {
                name : $name.val(),
                phone : $phone.val().replace(/[\s\(\)\-]/g, ''),
                color: $color.data('color'),
                count : $count.text(),
                price : $price.val(),
                article : $form.find('input[type="radio"]:checked').data('article')
            };			

			if (getParameterByName('actionpay')) {
				var actionpay = getParameterByName('actionpay').split('.');
				data.click = actionpay[0];
				data.source = actionpay[1];		
			}

            $.getJSON('/post-mail.php', data, function (r) {
                if (r.response === 'OK') {
                    document.location.href = '/submit.html?price=' + r.price + '&number=' + r.number;
                }
            }, 'json');

        } else {
            alert('Пожалуйста, заполните оба поля!');
        }
        e.preventDefault();
    }

    function formPopupSubmit (e)	{
        var $form = $(e.currentTarget),
            $name = $form.find('[name="name"]'),
            $phone = $form.find('[name="phone"]');

        if ($name.val() && $phone.val()) {

            $.getJSON('/call.php', {
                name : $name.val(),
                phone : $phone.val().replace(/[\s\(\)\-]/g, '')
            }, hidePopup, 'json');

        } else {
            alert('Пожалуйста, заполните оба поля!');
        }
        e.preventDefault();
    }
	
	function onInputBlur (e) {
		var $el = $(e.currentTarget);
		
		if ($el.val().length) {
			$el.addClass('valid');
		} else {
			$el.removeClass('valid');
		}
	}
	
	function updateTimer () {
		var $hour = $('.ordering_counter_hour'),
			$minute = $('.ordering_counter_minute'),
			$second = $('.ordering_counter_second'),
			hour, minute, second;
			
		timeForAction--;
		second = timeForAction % 60;
		minute = ((timeForAction - second) / 60) % 60;
		hour = (timeForAction - second - minute * 60) / 3600;
		
		$hour.text(hour < 10 ? '0' + hour : hour);
		$minute.text(minute < 10 ? '0' + minute : minute);
		$second.text(second < 10 ? '0' + second : second);	
		
		if (timeForAction > 1) {
			setTimeout(updateTimer, 1000);
		}
	}
	
	function getParameterByName(name, source) {
        var s = source || window.location.search,
            match = new RegExp('[?&]' + name + '=([^&]*)').exec(s);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    };	
	
	
	
	
	//$('.phone_with_ddd').mask('+7(000)0000000');
	//$('.placeholder').mask("+7(000)0000000", {
	//	placeholder: "+7(___)_______"
	//});

	/*
	$('pre').each(function (i, e) {
		hljs.highlightBlock(e)
	});
	*/
			
		

	//link to top
	  $('#link-top').on('click',function(e){
		$('html,body').stop().animate ({scrollTop: 0},1000);
		 e.preventDefault();
		 return false;
	  });

	  



	  window.onscroll = function() { 
		var scrolled = window.pageYOffset || document.documentElement.scrollTop;
		 var style =  $('#link-top').css("display");
		if(scrolled>200){
		  //if( $('#link-top').css("display")== )
		 
		  if(style == 'none'){
			$('#link-top').fadeIn("slow");
		  }
		}else{
		  //alert(style);
		  if(style == 'block'){
			$('#link-top').fadeOut("slow");
		  }
		}
	  //document.getElementById('showScroll').innerHTML = scrolled + 'px';
	  }		
			
	
});

/*
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


function onYouTubeIframeAPIReady() {
    window.player = new YT.Player('placeForVideo', {
        height: '375',
        width: '668',
        videoId: 'G1EGBSCT0DQ',
        playerVars: {
            'controls': 0,
            'showinfo': 0,
        }
    });
}

*/
