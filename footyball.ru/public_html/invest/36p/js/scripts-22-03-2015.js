function updatedata(){

	var tabactual = $(".calclftblock ul.nav li.active").attr("id");

	//скрытыее поля
	$('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html(  Math.round( parseInt( $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .calcslid').val() )) );
	$('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html(  Math.round( parseInt( $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .calcslid').val() )) );

	//ручной инпут
	$('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .manualchangeinp').val(Math.round( parseInt( $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .calcslid').val() )) );
	$('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .manualchangeinp').val(Math.round( parseInt( $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .calcslid').val() )) );

   // $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .manualchangeinp').val(data.from);


}
function chart(){
	var maxvklad=3000000;
	var maxdohod= Math.round(parseInt(   ((maxvklad*0.4752)/12)*16   ));
	var tabactual = $(".calclftblock ul.nav li.active").attr("id");
	var strsumvloj = Math.round(parseInt(   $('#'+tabactual.substring(0, tabactual.length - 3)+' .linevklad .datacopy').html()   )) ;
	var strsrokvloj = Math.round(parseInt(  $('#'+tabactual.substring(0, tabactual.length - 3)+' .linesrok .datacopy').html()     ));
	var percvlkad = (strsumvloj/maxvklad)*100;
	var itogdohodzames = 0;
	var itogdohod = 0;
	var itogdohodzavesper = 0;
	if(tabactual=="razvgodtab"){
		//капитализация
		$(".calcgraygadblock").slideUp("slow").css("opacity","0");
		itogdohod = Math.round(parseInt(   ((strsumvloj*0.4752)/12)*strsrokvloj   ));
		/*for(var i=0; i<strsrokvloj; i++) {
			sumskapatalizats += (sumskapatalizats*0.36)/12;
		} */
	}else{
		//без капитализации
		 $(".calcsummzaper span").html( accounting.formatMoney(Math.round(parseInt(((strsumvloj*0.36)/12))), {symbol:"",format:"%v %s",thousand:" ",precision:0} )  );
		itogdohod = Math.round(parseInt(((strsumvloj*0.36)/12)*strsrokvloj   ));
		$(".calcgraygadblock").slideDown("slow").css("opacity","1");
	}
	var perdohod = (itogdohod/maxdohod)*100;
	if(perdohod<1){perdohod=1;}
	if(percvlkad<1){percvlkad=1;}
	$('#chartvklad .calclvkaldblocktxt span').html( accounting.formatMoney(strsumvloj, {symbol:"руб",format:"%v %s",thousand:" ",precision:0} )  );
	$("#chartvklad").animate({'height' : percvlkad + '%'},800);
	$('#chartdoh .calclvkardblocktxt span').html( accounting.formatMoney(itogdohod, {symbol:"руб",format:"%v %s",thousand:" ",precision:0} )  );
	$("#chartdoh").animate({'height' : perdohod + '%'},800);
	$(".calcbueblock .calcchistpribdeng").html( accounting.formatMoney(strsumvloj+itogdohod, {symbol:"",format:"%v %s",thousand:" ",precision:0} )  );
}

function checkscrolling(){
	var scrolled = window.pageYOffset || document.documentElement.scrollTop;
	var style =  $('#link-top').css("display");
	// console.log(scrolled);
	if(scrolled>200){
		if(style == 'none'){
			$('#link-top').fadeIn("slow");
		}
		/*if( (scrolled>500)&&(scrolled<2400)  ){
			$(".footer").fadeIn("slow");
		}else{
			$(".footer").fadeOut("slow");
		}*/
	}else{
		if(style == 'block'){
			$('#link-top').fadeOut("slow");
		}
	}

}