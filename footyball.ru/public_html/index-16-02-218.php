<?

//phpinfo();
if(  !isset($_COOKIE['counttrain'])||empty($_COOKIE['counttrain'])  ){
	$counttrain = rand(1, 5) ;
	setcookie( "counttrain",$counttrain , strtotime( '+30 days' ) );
}else{
	$counttrain = $_COOKIE['counttrain'];
}


include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(1);

//$ranfeet = getrandomfeetback(0);
//$fivevideo = getfivevideo();
//$sliderphoto = getsliderphoto('index');

/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ВЫВОД СЧЕТЧИКА*/
function newfilevisit() {

	$newtimeforfile = gmdate('j.n.Y G:i', time());
	$saleforceresp = callsaleforce("getRSPVisits");
	//print_r($saleforceresp);
	$resultssaleforce = json_decode($saleforceresp, true);
	$resultssaleforce = $resultssaleforce['RSPVisits'] * 1;
	if ($resultssaleforce > 0) {
		$cout = $resultssaleforce;
		$newstr = $newtimeforfile . ";" . $cout;
		file_put_contents("RSPVisits.txt", $newstr);
	} else {
		$cout = 82000;
	}
	return true;
}

$contfilename = "RSPVisits.txt";
$filesting = @file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
if ($filesting === false) {
	$cout = 82000;
	newfilevisit();
}
$nowdate = gmdate('j.n.Y G:i', time()); //текуща дата
$nowdate = strtotime($nowdate);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate + (60 * 60 * 4); // время файла +4ч
if ($nowdate < $olddateplus24hour) {
	//echo ("читаем файл");
	$cout = $filearray[1];
} else {
	//echo ("пишем в файл");

	if (!newfilevisit()) {
		$cout = 82000;
	}


}


/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ДАТЫ СЛЕД. ТРЕНЕРОВКИ*/
function newfiledate() {

	$newtimeforfile = gmdate('j.n.Y G:i', time());
	$saleforceresp = callsaleforce("getRSPDate");
	//print_r($saleforceresp);
	$resultssaleforce = json_decode($saleforceresp, true);
	$newdate = $resultssaleforce['RSPDate'];

	$datestr = explode("/", $newdate);
	$datestrdd = $datestr[0];
	//$datestrmm =  $datestr[1];
	$datestrdd = $datestrdd * 1;

	if ($datestrdd > 0) {
		$newstr = $newtimeforfile . ";" . $resultssaleforce['RSPDate'];
		file_put_contents("RSPDate.txt", $newstr);
	} else {
		$datestrdd = gmdate('j', time() + (60 * 60 * 24));
		$datestrmm = gmdate('n', time());
	}
	return true;
}

$contfilename = "RSPDate.txt";
$filesting = @file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
if ($filesting === false) {

	$datestrdd = gmdate('j', time() + (60 * 60 * 24));
	$datestrmm = gmdate('n', time());
	newfiledate();

}
$nowdateshort = gmdate('j.n.Y G:i', time()); //текуща дата
$nowdate = strtotime($nowdateshort);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate + (60 * 60 * 6); // время файла +6ч


if ($nowdate < $olddateplus24hour) {
	//echo ("читаем файл");
	$datestr = $filearray[1];
	$datestrfull = explode("/", $datestr);
	$datestrdd = $datestrfull[0];
	$datestrmm = $datestrfull[1];
} else {
	//echo ("пишем в файл");

	if (!newfiledate()) {
		$datestrdd = gmdate('j', time() + (60 * 60 * 24));
		$datestrmm = gmdate('n', time());
	}


}

?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?= $pageinfo[0]["metatitle"]; ?></title>
	<meta name="description" content="<?= $pageinfo[0]["metadescr"]; ?>">
	<meta name="keywords" content="<?= $pageinfo[0]["metakeyw"]; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="assets/ico/favicon.ico">
	<link rel="icon" href="assets/ico/favicon.ico">
	<link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<link rel="stylesheet" href="assets/css/style-tmpl.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

	<!--[if IE]>
	<script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
	<![endif]-->


	<link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
	<meta property="og:locale" content="ru_RU">
	<meta property="og:title" content="Footyball - Официальный сайт">
	<meta property="og:site_name" content="Footyball - официальный сайт">
	<meta property="og:url" content="http://footyball.ru/">
	<meta property="og:description" content="Официальный сайт компании Footyball">
	<meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="200"/>
	<meta property="og:image:height" content="200"/>
	<meta property="og:type" content="website">
	<meta property="fb:app_id" content="826699460747158"/>

	<meta name="author" content="PotapovD">

</head>

<body>
<header>
	<? include_once("inc/header.inc.php"); ?>
</header>
<div id="mainbody">

	<div class="container-fluid nopadding">


		<!-- Header -->
		<div class="div-header">
			<div class="box">
				<h1>Футбольный клуб для мальчиков<br>&nbsp; от 2 до 7 лет&nbsp;<br>с профессиональными тренерами!</h1><br>
				<ul class="advantage">
					<li class="advantagepic1">развитие<br> лидерских качеств</li>
					<li class="advantagepic2">социализация<br> в команде</li>
					<li class="advantagepic3">мужской<br> характер</li>
					<li class="advantagepic4">умение<br> добиваться цели</li>
				</ul>
				<p class="text--2">Осталось всего <b><?=$counttrain;?></b> бесплатных мест на следующую тренировку</p>
				<a href="#callme" class="try fancy"><div>Записаться на бесплатное<br> пробное занятие</div></a>
			</div>
		</div>
		<!-- //Header -->

		<!-- Block7 -->
		<section class="b7 dynamic addback" id="comments">
			<div class="wrapper">
				<h2>Отзывы родителей</h2>
				<p class="subtitle">Успешные и состоятельные люди выбирают для своих детей Footyball</p>
				<div class="slider">
					<div class="item">
						<div class="im"> <iframe  src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/Ig1wrijKpMc" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="cont">
							<p class="text"> Гарик Мартиросян отдал своего сына Даниэля в FootyBall. Успехи маленького Чемпиона и отзыв звездного папы в нашем видеосюжете. </p>
							<p class="title">Гарик Мартиросян</p>
							<p class="desc">Ведущий, продюсер телевизионных проектов</p>
						</div>
					</div>
					<div class="item">
						<div class="im"><iframe  src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/-0OZGSzx1Jk" frameborder="0" allowfullscreen></iframe></div>
						<div class="cont">
							<p class="text"> В Footyball пополнение. Актеры Денис и Ирина Никифоровы привели в школу своего сына Сашу. Юному чемпиону 2,5 года, но он уже стремиться забить как можно больше голов </p>
							<p class="title">Денис и Ирина Никифоровы</p>
							<p class="desc">Актеры</p>
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe  src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/wki-MzGPIZ4" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text">  Футибол принял в свои ряды Мирослава Козлова. Сына известного футболиста, игрока московского "Динамо" и сборной России, Алексея Козлова. </p>
							<p class="title">Алексей Козлов</p>
							<p class="desc">Футболист "Динамо"</p>
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe   src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/sbxJTCOnkK4" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text"> В Футибол приходят чемпионы! Трехкратная олимпийская чемпионка по синхронному плаванию Наталья Ищенко с сыном Семеном на тренировке Footyball  </p>
							<p class="title">Наталья Ищенко</p>
							<p class="desc">Олимпийская чемпионка по синхронному плаванию</p>
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe   src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/DXF09OgKn8U" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text"> Кем мечтает стать сын миссис мира 2015 Марины Алексейчик?! И что он любит больше всего в футболе?! Расскажет и сам покажет Даниил Алексейчик </p>
							<p class="title">Марина Алексейчик</p>
							<p class="desc">Мисс мира</p>
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe   src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/qJzZFwkhNA0" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text">Сын игрока московского "Спартака" Александра Зотова продолжает осваивать футбольное мастерство в FootyBall. Тимофею 3,5 года и он уже стремится в группу PROFI. То ли еще будет! </p>
							<p class="title">Александр Зотов</p>
							<p class="desc">Футболист "Спартака"</p>
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe class="dynamic" src="assets-landing/img/1.gif" data-src="https://www.youtube.com/embed/gnTxNO4OU8Y" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text">Прабабушка юного чемпиона Влада Герасимова рассказывает о том, как FootyBall меняет представление о футболе </p>
							<!-- <p class="title">Марина Алексейчик</p>
							<p class="desc">Мисс мира</p> -->
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe class="dynamic" src="assets-landing/img/1.gif" data-src="https://www.youtube.com/embed/WNltdgrtrU8" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text">Отзыв дедушки юного чемпиона: "Полгода занимается, стал меньше болеть. Над техникой работает, удары пошли хорошо.Тут все поставлено на высоком уровне. Говорю как бывший футболист."</p>
							<!-- <p class="title">Марина Алексейчик</p>
							<p class="desc">Мисс мира</p> -->
						</div>
					</div>
					<div class="item">
						<div class="im"> <iframe class="dynamic" src="assets-landing/img/1.gif" data-src="https://www.youtube.com/embed/Rtm5OM79eow" frameborder="0" allowfullscreen></iframe> </div>
						<div class="cont">
							<p class="text">Два в одном - футбол и английский язык. Позитивное настроение и отношение ребенка. В прошлом году разыгрывали iPhone и наш ребенок выиграл, очень приятно!</p>
							<!-- <p class="title">Марина Алексейчик</p>
							<p class="desc">Мисс мира</p> -->
						</div>
					</div>
				</div>
				<div class="photos">
					<a data-pos="0"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/Ig1wrijKpMc/mqdefault.jpg" alt=""> </a>
					<a data-pos="1"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/-0OZGSzx1Jk/mqdefault.jpg" alt=""> </a>
					<a data-pos="2"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/wki-MzGPIZ4/mqdefault.jpg" alt=""> </a>
					<a data-pos="3"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/sbxJTCOnkK4/mqdefault.jpg" alt=""> </a>
					<a data-pos="4"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/DXF09OgKn8U/mqdefault.jpg" alt=""> </a>
					<a data-pos="5"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/qJzZFwkhNA0/mqdefault.jpg" alt=""> </a>
					<a data-pos="6"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/gnTxNO4OU8Y/mqdefault.jpg" alt=""> </a>
					<a data-pos="7"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/WNltdgrtrU8/mqdefault.jpg" alt=""> </a>
					<a data-pos="8"> <img src="assets-landing/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/Rtm5OM79eow/mqdefault.jpg" alt=""> </a>
				</div>
			</div>
		</section>
		<!-- //Block7 -->

		<!-- Block2 -->
		<section class="b2">
			<div class="wrapper">
				<h2>Почему тренер Сборной России по футболу<br> отдал своего внука в Footyball</h2>
				<div class="box">
					<div class="left">
						<iframe src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/qp0Vx3RsycM" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="right">
						<p class="title">Footyball - это будущее<br> Российского футбола </p>
						<p class="text">Валерий Георгиевич уверен в том, что малыш продолжит традиции своей футбольной семьи. Посмотрев тренировки в клубе, он сделал выводы о том, что у наших воспитанников есть все шансы стать настоящими профессиональными футболистами! Главное, верить в себя, тренироваться, идти вперед и не останавливаться на достигнутом.</p>
						<p class="text">В свою очередь, мы благодарим семью Газзаевых за выбор и доверие!</p>
					</div>
				</div>
			</div>
		</section>
		<!-- //Block2 -->

		<!-- Block4 -->
		<section class="b4">
			<div class="wrapper">
				<h2>Как проходят занятия</h2>
				<ul class="dynamic addback">
					<li>
						<div class="im"></div>
						<p class="text">Длительность<br> от 40-60 минут </p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Занятия в мини группах<br> по 6-10 человек</p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">2 раза в неделю в удобные<br> для вас дни</p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">15 залов по Москве -<br> места хватит всем!</p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Постоянная медицинская<br> поддержка - на месте<br> дежурит врач </p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Мини сессии английского языка в игровой форме </p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Тренировки открыты для<br> родителей - вы можете<br> наблюдать за ходом<br> тренировки из ФутКафе</p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Летом и зимой - в залах<br> и на открытых площадках</p>
					</li>
					<li>
						<div class="im"></div>
						<p class="text">Упражнения безопасны для<br> самых маленьких детей</p>
					</li>
				</ul>
				<p class="desc">Перед занятием ребенку выдается все необходимое - форма, дневник успеха. <br> С собой необходимо принести хорошее настроение!</p>
			</div>
		</section>
		<!-- //Block4 -->

		<!-- Block5 -->
		<section class="b5 dynamic addback" id="about">
			<div class="wrapper">
				<h2>Узнайте почему дети бегут на тренировку в Footyball?</h2>
				<div class="box">
					<div class="left">
						<iframe src="assets-landing/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/_w3Mb0-QrB0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="right">
						<ul class="dynamic addback">
							<li>
								<div class="im"></div>
								<p class="title">Дружелюбная атмосфера</p>
								<p class="text">Приходите и почувствуйте сами - <br> мы всегда рады гостям! </p>
							</li>
							<li>
								<div class="im"></div>
								<p class="title">Приятные тренеры</p>
								<p class="text">Наши тренеры очень любят детей<br> и помогают им раскрыть себя </p>
							</li>
							<li>
								<div class="im"></div>
								<p class="title">Возможность проявить себя</p>
								<p class="text">Каждый гол - это момент счастья для ребенка. Это возможность проявить себя! </p>
							</li>
						</ul>
						<a href="#" class="see toggle-menu menu-top">Увидеть тренировку<br> своими глазами</a>
					</div>
				</div>
			</div>
		</section>
		<!-- //Block5 -->

		<?include_once("inc/map.inc.php");?>

	</div>


	<nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
		<i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
		<div class="blockinnermenu">
			<div class="row">
				<div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
					<? include_once("inc/subscribeform.inc.php"); ?>
				</div>
			</div>
		</div>
	</nav>
	<div class="container-fluid nopadding" id="mainblockfooter">
		<? //include_once("inc/footer.inc.php"); ?>
		<?include_once("inc/footer-new.inc.php");?>
		<?include_once("inc/hide-popups.inc.php");?>
	</div>
</div>

<? include_once("inc/allmainjs.php"); ?>

<!--<script src="assets/js/jQuery.GI.Carousel.min.js"></script>
<script src="assets/js/jquery.dynamicmaxheight.min.js"></script>
<script src="assets/js/CounterCircleSlider.js"></script>
<script src="assets/js/jquery.edslider.js"></script>
<script src="assets/js/slick.min.js"></script>-->

<script src="assets-landing/js/plugins/fancy.js"></script>
<script src="assets-landing/js/plugins/slick.min.js"></script>



<script src="assets/js/cab-functions.js"></script>
<script src="assets/js/allpages-functions.js"></script>
<script defer="defer" src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>

<script>

	/*INDEX FUNCIONS*/

	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}

	$(document).ready(function () {
		$('.fancy').fancybox({
			padding: 0,
			margin: 0
		});




		window.onload = function() {
			$('.b9').on('click','.item',function() {
				$('.b9 .item').removeClass('slick-current');
				$(this).addClass('slick-current');
				initMap($(this).find('.adr').html());
			});
			$('.b9 .item:first-of-type').click();
		};



		$('.b7 .slider').slick({
			adaptiveHeight: true
		});
		$('.b7 .photos a').click(function () {
			$('.b7 .slider').slick('slickGoTo', $(this).data('pos'));
		});



		// -------------------------------------- Dynamic images and backgrounds
		showVisible();

		window.onscroll = function() {
			showVisible();
		};

		function showVisible() {
			var imgs = Array.prototype.slice.call(document.querySelectorAll('.dynamic'));
			if (imgs.length) {
				imgs.forEach(function(img) {
					if (isVisible(img)) {
						if (img.classList.contains('addback')) {
							img.classList.remove('addback');
							img.classList.add('backimg');
						} else {
							img.src = img.getAttribute('data-src');
							img.removeAttribute('data-src');
						}
						img.classList.remove('dynamic');
					}
				});
			}
		}

		function isVisible(elem) {
			var coords = elem.getBoundingClientRect();
			var windowHeight = document.documentElement.clientHeight;

			var topVisible = coords.top >= -windowHeight/2 && coords.top < windowHeight*1.5;
			var bottomVisible = coords.bottom < windowHeight*1.5 && coords.bottom > -windowHeight/2;

			return topVisible || bottomVisible;
		}
		// ----------------------------------- Dynamic images and backgrounds End


	})
</script>
<? include_once("inc/beforeclose.inc.php") ?>
</body>
</html>
