<?
include_once("lib/cms_view_inc.php");
checkpassandroll("index");

//$allcategories = getblogcatigoriesname();

if(!isset($_POST["id"])){ die();}

$slideid = $_POST["id"];
if($slideid>0){
    $nv = 1;
    $slideronephoto = getslideronephoto($slideid);
}
if(is_ajax2()){
?>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="slidelink">Ссылка <?=$slideid;?></label>

            <div class="col-sm-10">
                <input type="text" placeholder="" id="slidelink" name="slidelink" class="form-control" value="<?if(isset($nv))echo $slideronephoto[0]["link"];?>">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="slidetxt">Текст</label>

            <div class="col-sm-10">
                <input type="text" placeholder=""  id="slidetxt" name="slidetxt" class="form-control" value="<?if(isset($nv))echo $slideronephoto[0]["text"];?>">
            </div>
        </div>
    </div>
</div>







<?if($slideid==0){?>
<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <div class="col-sm-6">
                <label class="col-sm-9 control-label">Мобильный постер <span class="inpreq">*</span>
                    <button class="btn btn-default btn-xs popover-default"
                            data-original-title="Требование к постеру:"
                            data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                            2)Разрешение: 900x900 пикселей;&nbsp;&nbsp;"
                            data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                        <i class="fa fa-info"></i>
                    </button>
                </label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать мобильный постер</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="slidermobimg" id="slidermobimg">
            </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>
            </div>

            <div class="col-sm-6" >
                <label class="col-sm-9 control-label">Большой постер <span class="inpreq">*</span>
                    <button class="btn btn-default btn-xs popover-default"
                            data-original-title="Требование к постеру:"
                            data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                            2)Разрешение: 1170x540 пикселей;&nbsp;&nbsp;"
                            data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                        <i class="fa fa-info"></i>
                    </button>
                </label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать большой постер</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="sliderbigimg" id="sliderbigimg">
            </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>
            </div>

        </div>
    </div>
</div>
<?}else{?>
<br>
<div class="row">

</div>       
<br>
<div class="row">

    <div class="col-sm-4">
        <label class="col-sm-9 control-label">Мобильный постер <span class="inpreq">*</span>
            <button class="btn btn-default btn-xs popover-default"
                    data-original-title="Требование к постеру:"
                    data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                    2)Разрешение: 1170x540 пикселей;&nbsp;&nbsp;"
                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                <i class="fa fa-info"></i>
            </button>
        </label>
        <img src="/photo/banner/index/<?=$slideronephoto[0]["name"];?>-mobile.jpg" alt="" class="img-responsive">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать мобильный постер</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="slidermobimg" id="slidermobimg">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
               style="float: none">&times;</a>
        </div>
    </div>

    <div class="col-sm-8">
        <label class="col-sm-9 control-label">Большой <br>постер <span class="inpreq">*</span>
            <button class="btn btn-default btn-xs popover-default"
                    data-original-title="Требование к постеру:"
                    data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                    2)Разрешение: 900x900 пикселей;&nbsp;&nbsp;"
                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                <i class="fa fa-info"></i>
            </button>
        </label>
        <img src="/photo/banner/index/<?=$slideronephoto[0]["name"];?>.jpg" alt="" class="img-responsive">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать большой постер</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="sliderbigimg" id="sliderbigimg">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
               style="float: none">&times;</a>
        </div>
    </div>



</div>
<input type="hidden" name="sliderid" id="sliderid" value="<?=$slideid;?>">
<!--<input type="hidden" name="blogposturl" id="blogposturl" value="<?/*=$oneblogpost[0]["blogposturl"];*/?>">-->
<?}?>

<script type="text/javascript">

    //$('blogpost').mediaelementplayer(/* Options */);






    // Popovers and tooltips
    $('[data-toggle="popover"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'right'),
            trigger = attrDefault($this, 'trigger', 'click'),
            popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

        $this.popover({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.popover', function(ev)
        {
            var $popover = $this.next();

            $popover.addClass(popover_class);
        });
    });

    $('[data-toggle="tooltip"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'top'),
            trigger = attrDefault($this, 'trigger', 'hover'),
            popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

        $this.tooltip({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.tooltip', function(ev)
        {
            var $tooltip = $this.next();

            $tooltip.addClass(popover_class);
        });
    });






</script>
<?}?>

