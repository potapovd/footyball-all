<?
session_start();
include_once 'cms_config_inc.php';
//include_once 'cms_lang_inc.php';

/*
 * ОБЩИЕ ФУНКИЦИИ
 */
//защита от "плохих" запросов
/*
$get_ar = array_values($_GET);
$c_a_g = count($get_ar);
for ($i = 0;$i < $c_a_g;$i++){
    if(preg_match('/union(\.*)select/',$get_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
    if(preg_match('/order(\.*)by/',$get_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
}
$post_ar = array_values($_POST);
$c_a_p = count($post_ar);
for ($i = 0;$i < $c_a_p;$i++){
    if(preg_match('/union(\.*)select/',$post_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
    if(preg_match('/order(\.*)by/',$post_ar[$i])){ header("Location: http://google.gik-team.com/?q=%D0%B2%D0%B7%D0%BB%D0%BE%D0%BC+%D1%81%D0%B0%D0%B9%D1%82%D0%B0"); exit; }
}*/


//концертация объекта БД в массив
function fromdb2array($data)
{
    $infoarray = array();
    while ($row = mysqli_fetch_assoc($data)) {
        $infoarray[] = $row;
    }
    return $infoarray;
}

//Обработка запроса и отправка ответа на концертацию в массив
function fromquery2db($sqlquery)
{

    $link = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASSWORD);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    mysqli_select_db($link, DB_NAME);


    if ($result = mysqli_query($link, $sqlquery) or die(mysqli_error($link))) {
        $result = fromdb2array($result);
        //  CleanUpDB();
        return $result;
    } else {
        echo('Ошибка в MySQL запросе');
        //CleanUpDB();
        return false;
    }

    //mysqli_close($link);

}




//отправка запроса в БД (add,upd,del)
function simplequery2db($sqlquery)
{

    $link = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASSWORD);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    mysqli_select_db($link, DB_NAME);


    if (mysqli_query($link, $sqlquery) or die(mysqli_error($link))) {
        //$result = fromdb2array($result);
        return true;
    } else {
        echo('Ошибка в MySQL запросе');
        //CleanUpDB();
        return false;
    }

    //mysqli_close($link);
}


function img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 90){
    if (!file_exists($src)) return false;
    $size = getimagesize($src);
    if ($size === false) return false;
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
    $icfunc = "imagecreatefrom" . $format;
    if (!function_exists($icfunc)) return false;
    $x_ratio = $width / $size[0];
    $y_ratio = $height / $size[1];
    $ratio = min($x_ratio, $y_ratio);
    $use_x_ratio = ($x_ratio == $ratio);
    $new_width = $use_x_ratio ? $width : floor($size[0] * $ratio);
    $new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);
    $new_left = $use_x_ratio ? 0 : floor(($width - $new_width) / 2);
    $new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);
    $isrc = $icfunc($src);
    $idest = imagecreatetruecolor($width, $height);
    imagefill($idest, 0, 0, $rgb);
    imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
        $new_width, $new_height, $size[0], $size[1]);
    imagejpeg($idest, $dest, $quality);
    imagedestroy($isrc);
    imagedestroy($idest);
    return true;
}
// img_resize_png($uploadfile, $uploadfile, 211, 109 );
function img_resize_png($im, $uploadfile, $dst_width, $dst_height){
    $width = imagesx($im);
    $height = imagesy($im);

    $newImg = imagecreatetruecolor($dst_width, $dst_height);

    imagealphablending($newImg, false);
    imagesavealpha($newImg, true);
    $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
    imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $dst_width, $dst_height, $width, $height);

    imagepng($newImg, $uploadfile, 8.5);
    return true;
}

//транслитерация
function ru2lat($st){
    $translit = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'j',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'x',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
        'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'J',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
        'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
        'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',
        'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',

        ' ' => '_',   '&' => 'and',  '  ' => '_',
        '/' => '_'
    );
    $st = strtr($st, $translit);

    if (preg_match('/[^A-Za-z0-9._\-]/', $st)) {
        $st = preg_replace('/[^A-Za-z0-9._\-]/', '', $st);
    }
    return $st;
}

//добавление / удаледение слешей
function stripslashes_deep($value){
    $value = is_array($value) ?
        array_map('stripslashes_deep', $value) :
        stripslashes($value);

    return $value;
}

function addslashes_deep($str){
    $str = trim($str);
    if(strpos(str_replace("\'",""," $str"),"'")!=false)
        return addslashes($str);
    else
        return $str;
}





/*ALL VARS*/
//print_r($_POST);
//print_r($_FILES);


if (is_ajax()) {
    if (isset($_POST["formname"]) && !empty($_POST["formname"])) {
        $formname = $_POST["formname"];
        //echo (" formname ".$formname);
        switch ($formname) {
            //all
            case "removeelement":
                removedbelement();
                break;
            case "logout":
                logout();
                break;
            case "cleardebug":
                cleardebug();
                break;
            //users
            case "updatecurrenuser":
                updatecurrenuser();
                break;
            case "updateoneuser":
                updateoneuser();
                break;
            case "addoneuser":
                addoneuser();
                break;
            //index
            case "tabmapinp":
                updindexpamap();
            case "addoneslide":
                addoneslide();
            case "updateoneslide":
                updateoneslide();
                break;
            case "tabvideoinp":
                updtabvideo();
                break;
            case "tabphoto":
                newphotos();
                break;
            case "tabpartn":
                newpartner();
                break;
            case "tabprtnpartnupd":
                updatepartn();
                break;
            //tv
            case "updatevideo":
                updatevideotv();
                break;
            case "addvideo":
                addvideotv();
                break;
            //feedback
            case "updatefeedback":
                updatefeedback();
                break;
            case "addfeedback":
                addfeedback();
                break;
            //blog
            case "addblogpost":
                addblogpost();
                break;
            case "updateblogpost":
                updateblogpost();
                break;

        }
    }else {
        die("Ошибка данных");
    }
} else {
    die("Ошибка, запрос может быть только Ajax");
}

function is_ajax(){
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function logout(){

    $currentuser = $_SESSION["id"];
    $currdate = date("Y-m-d H:i:s");
    $ip = $_SERVER['REMOTE_ADDR'];
    $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate','<--- выход $ip')";
    simplequery2db($sqldeb);


    unset ( $_SESSION["id"] );
    unset ( $_SESSION["hash"] );
    session_destroy();
    header('Location: /_adminsite/login.php');
    exit;
}

function cleardebug(){
    $sqldeb = "TRUNCATE changes";
    simplequery2db($sqldeb);

}

function removedbelement(){
    //print_r($_POST);

    $object = $_POST["object"];
    $id = $_POST["id"];

    /*if($object=="removevideo"){
        $sqldb = "tvvideo";
    }*/

    switch ($object) {
        case "removevideo":
            $sqldb = "tvvideo";
            break;
        case "removefeetback":
            $sqldb = "pagefeedback";
            break;
        case "removeuser":
            $sqldb = "users";
            break;
        case "removephotohero":
            $sqldb = "widgetphoto";
            break;
        case "removephotopartn":
            $sqldb = "widgetparnter";
            break;
        case "removeblogpost":
            $sqldb = "blogposts";
            break;
        case "removeindexslide":
            $sqldb = "widgetgallery";
            break;
    }

    $currentuser = $_SESSION["id"];
    $currdate = date("Y-m-d H:i:s");
    $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate','---- удаление из $sqldb элемента с id = $id')";
    simplequery2db($sqldeb);


    $sql = "DELETE FROM $sqldb WHERE id = $id LIMIT 1 ";
    if (simplequery2db($sql)) {
        $output = json_encode(array('type' => 'success', 'text' => 'Объект удален успешно'));
        die($output);
    } else {
        $output = json_encode(array('type' => 'error', 'text' => 'Ошибка удаления объекта'));
        die($output);
    }


}


/*USERS*/
function updatecurrenuser(){
    $currentuserid = $_POST["currentuserid"];
    $newpas = $_POST["newpas"];

    $newpas = trim($newpas);

    if (!preg_match("/^[a-zA-Z0-9]+$/", $newpas)) {
        $output = json_encode(array('type' => 'error', 'text' => 'Пароль может содержать только латинские буквы и цифры'));
        die($output);
    }
    if (strlen($newpas) < 5 or strlen($newpas) > 30) {
        $output = json_encode(array('type' => 'error', 'text' => 'Пароль должен содержать от 5 до 30 символов'));
        die($output);
    }

    $newpas = md5(md5($newpas));

    $sql = "UPDATE users
                SET userpass='$newpas'
                WHERE id = $currentuserid LIMIT 1 ";

    $currentuser = $_SESSION["id"];
    $currdate = date("Y-m-d H:i:s");
    $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ~~~~ изменил свой пароль')";
    simplequery2db($sqldeb);


    if (simplequery2db($sql)) {
        $output = json_encode(array('type' => 'success', 'text' => 'Пароль обновлен успешно'));
        die($output);
    } else {
        $output = json_encode(array('type' => 'error', 'text' => 'Ошибка обновления пароля'));
        die($output);
    }

    //

}

function updateoneuser(){
    //print_r($_POST);
    $currentuserid = $_POST["currentuserid"];
    $newpas = $_POST["newpas"];
    $cbindex = $_POST["cbindex"];
    $cbtv = $_POST["cbtv"];
    $cbfeetback = $_POST["cbfeetback"];
    $msg = "";
    $flagerror = 0;

    if($newpas!==""){
        $newpas = trim($newpas);

        if (!preg_match("/^[a-zA-Z0-9]+$/", $newpas)) {
            $msg .= "Пароль должен содержать только латинские буквы и цифры";
            $flagerror = 1;
        }
        if (strlen($newpas) < 5 or strlen($newpas) > 30) {
            $msg .= "Пароль должен содержать от 5 до 30 символов";
            $flagerror = 1;
        }

        if ($flagerror==0){
            $newpas = md5(md5($newpas));

            $sql = "UPDATE users
                    SET userpass='$newpas'
                    WHERE id = $currentuserid LIMIT 1 ";

            $currentuser = $_SESSION["id"];
            $currdate = date("Y-m-d H:i:s");
            $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ~~~~ изменил пароль $currentuserid')";
            simplequery2db($sqldeb);


            if (simplequery2db($sql)) {
                $msg .= "Пароль обновлен успешно<br>";
            } else {
                $msg .= "Ошибка обновления пароля";
                $flagerror = 1;
            }
        }

    }

    $tables ="";

    if($cbindex==1){$tables.="index;";}
    if($cbtv==1){$tables.="tv;";}
    if($cbfeetback==1){$tables.="feedback;";}

    if (!empty($tables)){
        $sql = "UPDATE users
                SET tables='$tables'
                WHERE id = $currentuserid LIMIT 1 ";

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ~~~~ изменил права $currentuserid')";
        simplequery2db($sqldeb);



        if (simplequery2db($sql)) {
            $msg .= "Права обновлены успешно<br>";
        } else {
            $msg .= "Ошибка обновления прав";
            $flagerror = 1;
        }
    }


    if ($flagerror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);


}

function addoneuser(){
    //print_r($_POST);
    $username = $_POST["username"];
    $pasword = $_POST["newpas"];
    $cbindex = $_POST["cbindex"];
    $cbtv = $_POST["cbtv"];
    $cbfeetback = $_POST["cbfeetback"];
    $msg = "";
    $flagerror = 0;

        $newpas = trim($pasword);

    if (!preg_match("/^[a-zA-Z0-9]+$/", $username)) {
        $msg .= "Логин должен содержать только латинские буквы и цифры";
        $flagerror = 1;
    }
    if (strlen($username) < 5 or strlen($username) > 30) {
        $msg .= "Логин должен содержать от 5 до 30 символов";
        $flagerror = 1;
    }
    if (!preg_match("/^[a-zA-Z0-9]+$/", $pasword)) {
        $msg .= "Пароль должен содержать только латинские буквы и цифры";
        $flagerror = 1;
    }
    if (strlen($pasword) < 5 or strlen($pasword) > 30) {
        $msg .= "Пароль должен содержать от 5 до 30 символов";
        $flagerror = 1;
    }

    if ($flagerror==0){
        $pasword = md5(md5($pasword));
        $tables ="";
        if($cbindex==1){$tables.="index;";}
        if($cbtv==1){$tables.="tv;";}
        if($cbfeetback==1){$tables.="feedback;";}


        $sql = "INSERT INTO users (username,userpass,tables,role)
                VALUES ('$username','$pasword','$tables','manager')";

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
          VALUES ('$currentuser','$currdate',' ~~~~ добавил пользователя ')";
        simplequery2db($sqldeb);



        if (simplequery2db($sql)) {
            $msg .= "Пользователь добавлен успешно<br>";
        } else {
            $msg .= "Ошибка добавления пользователя";
            $flagerror = 1;
        }
    }







    if ($flagerror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);


}

/*SLIDER*/
function addoneslide(){

    //print_r($_POST);
    //print_r($_FILES);

    $slidetxt = $_POST["slidetxt"];
    $slidelink = $_POST["slidelink"];
    $slidermobimg = $_FILES["slidermobimg"];
    $sliderbigimg = $_FILES["sliderbigimg"];

    //print_r()
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/banner/index/';
    $msg = '';
    $uploaderror = 0;


    //if ( !isset($sliderbigimg) || !isset($slidermobimg) ) {
    if ( !isset($sliderbigimg) ) {
        $msg .= 'Ошибка. Постеры должны быть заполнены <br>';
        $uploaderror = 1;
    }

    if ($uploaderror == 0) {

        $slide4upload = md5(microtime().ru2lat($sliderbigimg['name']));

        $uploadfileslidbig = $uploaddirphoto . basename($slide4upload).".jpg";
        $uploadfileslidmob = $uploaddirphoto . basename($slide4upload)."-mobile.jpg";

        if ( move_uploaded_file($sliderbigimg['tmp_name'], $uploadfileslidbig) &&
            move_uploaded_file($slidermobimg['tmp_name'], $uploadfileslidmob)) {
            //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
            img_resize($uploadfileslidbig, $uploadfileslidbig, 1170, 540, $rgb = 0xffffff);
            img_resize($uploadfileslidmob, $uploadfileslidmob, 900, 900, $rgb = 0xffffff);
        } else {
            $msg .= 'Ошибка добавления постера <br>';
            $uploaderror = 1;
        }




        if($uploaderror == 0){

            $sql = "INSERT INTO widgetgallery (name, link, text )
                    VALUES ('$slide4upload', '$slidelink', '$slidetxt')";

            $currentuser = $_SESSION["id"];
            $currdate = date("Y-m-d H:i:s");
            $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ++++ добавил фото в слайдер на главной')";
            simplequery2db($sqldeb);


            if (simplequery2db($sql)) {
                $msg .= 'Новый слайд добавлен <br>';
            } else {
                $msg .= 'Ошибка добавления слайда <br>';
                $uploaderror = 1;
            }

        }

    }


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);

}

function updateoneslide(){

    //print_r($_POST);
    //print_r($_FILES);

    $slidetxt = $_POST["slidetxt"];
    $slidelink = $_POST["slidelink"];
    $slideid = $_POST["slideid"];
    @$slidermobimg = $_FILES["slidermobimg"];
    @$sliderbigimg = $_FILES["sliderbigimg"];

    $msg = '';
    $uploaderror = 0;
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/banner/index/';


    if( (isset($slidermobimg) && !empty($slidermobimg)) &&
        isset($sliderbigimg) && !empty($sliderbigimg)) {


        $slide4upload = md5(microtime().ru2lat($sliderbigimg['name']));

        $uploadfileslidbig = $uploaddirphoto . basename($slide4upload).".jpg";
        $uploadfileslidmob = $uploaddirphoto . basename($slide4upload)."-mobile.jpg";

        if ( move_uploaded_file($sliderbigimg['tmp_name'], $uploadfileslidbig) &&
            move_uploaded_file($slidermobimg['tmp_name'], $uploadfileslidmob)) {
            //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
            img_resize($uploadfileslidbig, $uploadfileslidbig, 1170, 540, $rgb = 0xffffff);
            img_resize($uploadfileslidmob, $uploadfileslidmob, 900, 900, $rgb = 0xffffff);
        } else {
            $msg .= 'Ошибка добавления постера <br>';
            $uploaderror = 1;
        }


        if($uploaderror == 0){

            $sql ="UPDATE widgetgallery
                    SET
                      name='$slide4upload',
                      link='$slidelink',
                      text= '$slidetxt'
                    WHERE id = '$slideid' LIMIT 1 ";


        }


    }else{
        $sql ="UPDATE widgetgallery
                    SET
                      link='$slidelink',
                      text= '$slidetxt'
                    WHERE id = $slideid LIMIT 1 ";
    }




    if (simplequery2db($sql)) {
        $msg .= 'Новый слайд добавлен <br>';

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ++++ обновил фото в слайдер на главной')";
        simplequery2db($sqldeb);

    } else {
        $msg .= 'Ошибка добавления слайда <br>';
        $uploaderror = 1;
    }



    //print_r()
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/banner/index/';



    //if ($uploaderror == 0) {



    //}


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);

}

/*PHOTOS*/
function newphotos(){
    //echo ("TAB PHOTO");
    //print_r($_POST);
    //print_r($_FILES);
    //print_r($_POST['newphotos']["FileList"]);

    $newfoto = $_FILES["newphotos"];
    $msg = '';
    $uploaderror = 0;
    $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/photo/index/';
    $photoname = md5(time());




    if (isset($newfoto)) {
        if ($newfoto["error"] == 0) {

            if ($newfoto["type"] == "image/jpeg") {
                $uploadfile = $uploaddir . basename($photoname) . ".jpg";
                if (move_uploaded_file($newfoto['tmp_name'], $uploadfile)) {
                    //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                    img_resize($uploadfile, $uploadfile, 960, 640);
                    $msg .= 'Фото добавленно на севрвер успешно <br>';
                } else {
                    $msg .= 'Ошибка обновления фото <br>';
                    $uploaderror = 1;
                }
            }else{
                $msg .= 'Ошибка в приложенном файле. Формат файла доджен быть .JPG <br>';
                $uploaderror = 1;
            }

        }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
    }

    if($uploaderror==0) {
        $sql = "INSERT INTO widgetphoto (name)
            VALUES ('$photoname')";

        if (simplequery2db($sql)) {
            $msg .= 'Информация добавлена успешно <br>';
        } else {
            $msg .= 'Ошибка обновления информации <br>';
        }
    }



    if($uploaderror==0){

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== добавил фоточемпиона $photoname')";
        simplequery2db($sqldeb);


        //if (simplequery2db($sql)) {
        //    $msg .= 'Информация обновлена успешно <br>';
        //} else {
        //    $msg .= 'Ошибка обновления информации <br>';
        //}
    }

    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg, 'src'=> $photoname));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);

}

function newpartner(){
    //echo ("TAB PHOTO");
    //print_r($_POST);
    //print_r($_FILES);
    //print_r($_POST['newphotos']["FileList"]);

    $newfoto = $_FILES["newphotos"];
    $msg = '';
    $uploaderror = 0;
    $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/photo/partner/';
    $photoname = md5(time());




    if (isset($newfoto)) {
        if ($newfoto["error"] == 0) {

            if ($newfoto["type"] == "image/png") {
                $uploadfile = $uploaddir . basename($photoname) . ".png";
                if (move_uploaded_file($newfoto['tmp_name'], $uploadfile)) {
                    //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                    //img_resize_png($uploadfile, $uploadfile, 211, 109 );
                    $msg .= 'Логотип добавленно на севрвер успешно <br>';
                } else {
                    $msg .= 'Ошибка обновления лого <br>';
                    $uploaderror = 1;
                }
            }else{
                $msg .= 'Ошибка в приложенном файле. Формат файла доджен быть .PNG <br>';
                $uploaderror = 1;
            }

        }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
    }

    if($uploaderror==0) {
        $photoname = $photoname.'.png';
        $sql = "INSERT INTO widgetparnter (name)
            VALUES ('$photoname')";

        if (simplequery2db($sql)) {
            $msg .= 'Информация добавлена успешно <br>';
        } else {
            $msg .= 'Ошибка обновления информации <br>';
        }
    }



    if($uploaderror==0){

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== добавил лого партнера $photoname')";
        simplequery2db($sqldeb);


        //if (simplequery2db($sql)) {
        //    $msg .= 'Информация обновлена успешно <br>';
        //} else {
        //    $msg .= 'Ошибка обновления информации <br>';
        //}
    }

    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg, 'src'=> $photoname));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);

}

/*FEEDBACK*/
function updatefeedback(){

    $feedbackid = $_POST["feedbackid"];
    $feedbacktype = $_POST["feedbacktype"];
    $feedbackfio = $_POST["feedbackfio"];
    $feedbacavaname = $_POST["feedbacavaname"];

    @$feedbacktxt = addslashes_deep($_POST["feedbacktxt"]);
    @$feedbackava = $_FILES["feedbackava"];

    $msg = '';
    $uploaderror = 0;


    if($feedbacktype==0){
        $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/photo/feedback/';
        $sql = "UPDATE pagefeedback
                SET name='$feedbackfio', content = '$feedbacktxt'
                WHERE id = $feedbackid LIMIT 1 ";
    }else{
        $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/video/feedback/';
        $sql = "UPDATE pagefeedback
                SET name='$feedbackfio'
                WHERE id = $feedbackid LIMIT 1 ";
    }

    if (isset($feedbackava) && !empty($feedbackava)) {
        if ($feedbackava["error"] == 0) {

            $uploadfile = $uploaddir . basename($feedbacavaname);
            if (move_uploaded_file($feedbackava['tmp_name'], $uploadfile)) {
                //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                img_resize($uploadfile, $uploadfile, 97, 97);
                $msg .= 'Фото обновлено успешно <br>';
            } else {
                $msg .= 'Ошибка обновления фото <br>';
                $uploaderror = 1;
            }

        }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
    }
    if($uploaderror==0){

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== обновил отзыв $feedbackid')";
        simplequery2db($sqldeb);


        if (simplequery2db($sql)) {
            $msg .= 'Информация обновлена успешно <br>';
        } else {
            $msg .= 'Ошибка обновления информации <br>';
        }
    }

    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);



}

function addfeedback(){
    //print_r($_POST);
    //print_r($_FILES);


    $feedbacktype = $_POST["feedbacktype"];
    $feedbackfio = $_POST["feedbackfio"];

    @$feedbacktxt = addslashes_deep($_POST["feedbacktxt"]);
    @$feedbackava = $_FILES["feedbackava"];

    @$feedbackvideomp4 = $_FILES["feedbackvideomp4"];
    @$feedbackvideowebm = $_FILES["feedbackvideowebm"];

    $msg = '';
    $uploaderror = 0;

    $feedbacavaname = ru2lat($feedbackfio);
    $feedbacavaname .= time();
    $feedbacavanamejpg = $feedbacavaname.".jpg";


    if($feedbacktype==0){
        $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/photo/feedback/';
        $sql = "INSERT INTO pagefeedback (type, photo, name, content)
                    VALUES (0, '$feedbacavanamejpg', '$feedbackfio', '$feedbacktxt')";
    }else{
        $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/video/feedback/';
        $sql = "INSERT INTO pagefeedback (type, photo, name, content)
                    VALUES (1, '$feedbacavanamejpg', '$feedbackfio', '$feedbacavaname')";

        if (  !isset($feedbackvideomp4)|| !isset($feedbackvideowebm)) {
            $msg .= 'Ошибка. Названиe, Категория, Видео .webm и mp4 - должны быть заполнены <br>';
            $uploaderror = 1;
        }

        if ($uploaderror == 0) {

            $uploadfilemp4 = $uploaddir . basename($feedbacavaname.".mp4");
            $uploadfilewebm = $uploaddir . basename($feedbacavaname.".webm");

            if (move_uploaded_file($feedbackvideomp4['tmp_name'], $uploadfilemp4)) {
                $msg .= $feedbacavaname.'.mp4 закачан на сервер успешно <br>';
            } else {
                $msg .= 'Ошибка добавления видео .mp4 <br>';
                $uploaderror = 1;
            }

            if (move_uploaded_file($feedbackvideowebm['tmp_name'], $uploadfilewebm)) {
                $msg .= $feedbacavaname.'.webm закачан на сервер успешно <br>';
            } else {
                $msg .= 'Ошибка добавления видео .webm <br>';
                $uploaderror = 1;
            }
        }


    }

    if (isset($feedbackava) && !empty($feedbackava)) {
        if ($feedbackava["error"] == 0) {

            $uploadfile = $uploaddir . basename($feedbacavanamejpg);
            if (move_uploaded_file($feedbackava['tmp_name'], $uploadfile)) {
                //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                img_resize($uploadfile, $uploadfile, 97, 97);
                $msg .= 'Фото добавлено успешно <br>';
            } else {
                $msg .= 'Ошибка обновления фото <br>';
                $uploaderror = 1;
            }

        }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
    }
    if($uploaderror==0){

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ++++ добавил отзыв ')";
        simplequery2db($sqldeb);


        if (simplequery2db($sql)) {
            $msg .= 'Информация добавлена успешно <br>';
        } else {
            $msg .= 'Ошибка обновления информации <br>';
        }
    }

    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);


}


/*VIDEO*/
function updatevideotv(){
    //print_r($_POST);

    $videoid = $_POST["videoid"];
    $namevideo = $_POST["namevideo"];
    $categoryvideo = $_POST["categoryvideo"];
    $videourl = $_POST["videourl"];
    $videodesription = addslashes_deep($_POST["videodesription"]);

    @$tvposter = $_FILES["tvposter"];
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/tv/';

    $msg = '';
    $uploaderror = 0;

    $datevideo = $_POST["datevideo"];
    $date_elements  = explode("/",$datevideo);
    $datevideo =  $date_elements[2]."-".$date_elements[1]."-".$date_elements[0];


    if (isset($tvposter) && !empty($tvposter)) {
        if ($tvposter["error"] == 0) {

            $uploadfile = $uploaddirphoto . basename($videourl).".jpg";
            if (move_uploaded_file($tvposter['tmp_name'], $uploadfile)) {
                //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                //img_resize($uploadfile, $uploadfile, 259, 140);
                //img_resize($uploadfile, $uploadfile, 248, 140, $rgb = 0xffffff);
                img_resize($uploadfile, $uploadfile, 640, 360, $rgb = 0xffffff);
                $msg .= 'Фото обновлено успешно <br>';
            } else {
                $msg .= 'Ошибка обновления фото <br>';
                $uploaderror = 1;
            }

        }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
    }




    $sql = "UPDATE tvvideo
            SET name='$namevideo',
            date='$datevideo',
              tvcategoryid='$categoryvideo',
              desription='$videodesription'
            WHERE id = $videoid LIMIT 1 ";

    $currentuser = $_SESSION["id"];
    $currdate = date("Y-m-d H:i:s");
    $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== обновил видео ТВ $videoid')";
    simplequery2db($sqldeb);

    if (simplequery2db($sql)) {
        $output = json_encode(array('type' => 'success', 'text' => 'Видео обновлено успешно'));
        die($output);
    } else {
        $output = json_encode(array('type' => 'error', 'text' => 'Ошибка обновления видео'));
        die($output);
    }

}

function addvideotv(){
    //print_r($_POST);
    //print_r($_FILES);

    $namevideo = $_POST["namevideo"];
    $categoryvideo = $_POST["categoryvideo"];
    $videodesription = addslashes_deep($_POST["videodesription"]);
    $datevideo = $_POST["datevideo"];
    $date_elements  = explode("/",$datevideo);
    $datevideo =  $date_elements[2]."-".$date_elements[1]."-".$date_elements[0];

    @$tvvideowebm = $_FILES["tvvideowebm"];
    @$tvvideomp4 = $_FILES["tvvideomp4"];
    @$tvposter = $_FILES["tvposter"];

    //print_r()

    $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/video/tv/';
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/tv/';
    $msg = '';
    $uploaderror = 0;


    if ( empty($namevideo) || $categoryvideo < 1 || !isset($tvvideowebm)|| !isset($tvvideomp4)) {
        $msg .= 'Ошибка. Названиe, Категория, Видео .webm и mp4 - должны быть заполнены <br>';
        $uploaderror = 1;
    }

    if ($uploaderror == 0) {

        $namevideo4upload = ru2lat($tvvideomp4['name']);
        $p = strrpos($namevideo4upload, '.');
        if ($p > 0) $clearname =  substr($namevideo4upload, 0, $p);
        $namevideo4upload = $categoryvideo."-".time()."-".$clearname;

        $uploadfilemp4 = $uploaddir . basename($namevideo4upload.".mp4");
        $uploadfilewebm = $uploaddir . basename($namevideo4upload.".webm");
        $uploadfiletvposter = $uploaddirphoto . basename($namevideo4upload.".jpg");

        if (move_uploaded_file($tvvideomp4['tmp_name'], $uploadfilemp4)) {
            //$msg .= $namevideo4upload.'.mp4 закачан на сервер успешно <br>';
        } else {
            $msg .= 'Ошибка добавления видео .mp4 <br>';
            $uploaderror = 1;
        }

        if (move_uploaded_file($tvvideowebm['tmp_name'], $uploadfilewebm)) {
            //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
        } else {
            $msg .= 'Ошибка добавления видео .webm <br>';
            $uploaderror = 1;
        }
        if (move_uploaded_file($tvposter['tmp_name'], $uploadfiletvposter)) {
            //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
            //img_resize($uploadfiletvposter, $uploadfiletvposter, 259, 140);
            //img_resize($uploadfiletvposter, $uploadfiletvposter, 248, 140, $rgb = 0xffffff);
            img_resize($uploadfiletvposter, $uploadfiletvposter, 640, 360, $rgb = 0xffffff);
        } else {
            $msg .= 'Ошибка добавления постера <br>';
            $uploaderror = 1;
        }


        if($uploaderror == 0){

            $sql = "INSERT INTO tvvideo (tvcategoryid, name, desription, date, videourl)
                    VALUES ($categoryvideo, '$namevideo', '$videodesription', '$datevideo','$namevideo4upload')";

            $currentuser = $_SESSION["id"];
            $currdate = date("Y-m-d H:i:s");
            $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ++++ добавил видео ТВ')";
            simplequery2db($sqldeb);


            if (simplequery2db($sql)) {
                $msg .= 'Видео добавленно в БД <br>';
            } else {
                $msg .= 'Ошибка добавления видео в БД <br>';
                $uploaderror = 1;
            }

        }

    }


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);





}

function updtabvideo(){
    //echo ("TAB VIDEO");
    //print_r($_POST);
    //print_r($_FILES);

    @$tabvideowebm = $_FILES["tabvideowebm"];
    @$tabvideomp4 = $_FILES["tabvideomp4"];
    @$tabvideposter = $_FILES["tabvideposter"];
    $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/assets/video/';
    $msg = '';
    $uploaderror = 0;


    if (!isset($tabvideposter) || !isset($tabvideomp4) || !isset($tabvideowebm)) {
        $msg .= 'Ошибка. Нет данных для обновления <br>';
        $uploaderror = 1;
    }

    if ($uploaderror == 0) {
        //обновление постера
        if (isset($tabvideposter) && !empty($tabvideposter)) {
            if ($tabvideposter["error"] == 0) {
                $uploadfile = $uploaddir . basename("FootyBall-main-poster.jpg");
                if (move_uploaded_file($tabvideposter['tmp_name'], $uploadfile)) {
                    //img_resize($src, $dest, $width, $height, $rgb = 0x111111, $quality = 100)
                    img_resize($uploadfile, $uploadfile, 561, 315);
                    $msg .= 'Постер обновлен успешно <br>';
                } else {
                    $msg .= 'Ошибка обновления постера <br>';
                    $uploaderror = 1;
                }
            }else {
            $msg .= 'Ошибка в приложенном файле <br>';
            $uploaderror = 1;
        }
        }
        //обноление видео
        if (isset($tabvideomp4) && !empty($tabvideomp4)) {
            if ($tabvideomp4["type"]!=="video/mp4"){
                $msg .= 'Ошибка неверный формат. Добавьте видео формата .mp4 <br>';
                $uploaderror = 1;
            }
            if (isset($tabvideowebm) && !empty($tabvideowebm)) {
                if ($tabvideowebm["type"]!=="video/webm"){
                    $msg .= 'Ошибка неверный формат. Добавьте видео формата .webm <br>';
                    $uploaderror = 1;
                }

                if ($uploaderror == 0) {
                    $uploadfilemp4 = $uploaddir . basename("FootyBall-main.mp4");
                    $uploadfilewebm = $uploaddir . basename("FootyBall-main.webm");

                    $currentuser = $_SESSION["id"];
                    $currdate = date("Y-m-d H:i:s");
                    $sqldeb = "INSERT INTO changes (user,date,action)
                              VALUES ('$currentuser','$currdate',' ==== обновил видео на главной ')";
                                    simplequery2db($sqldeb);

                    if (move_uploaded_file($tabvideomp4['tmp_name'], $uploadfilemp4)) {
                        $msg .= '.mp4 обновлен успешно <br>';
                    } else {
                        $msg .= 'Ошибка обновления видео .mp4 <br>';
                        $uploaderror = 1;
                    }

                    if (move_uploaded_file($tabvideowebm['tmp_name'], $uploadfilewebm)) {
                        $msg .= '.webm обновлен успешно <br>';
                    } else {
                        $msg .= 'Ошибка обновления видео .webm <br>';
                        $uploaderror = 1;
                    }


                }

            }else{
                $msg .= 'Ошибка добавьте видео формата .webm <br>';
                $uploaderror = 1;
            }
        }
    }


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);
    //return $output;


}


/*MAP*/
function updindexpamap(){
    //$output = json_encode(array('type'=>'success', 'text' => 'Карта обновлена'));
    //$output = json_encode(array(
    //    'type'=>'error',
    //    'text' => 'Ошибка, запрос может быть только Ajax POST'
    //));
    //die($output);

    $newcontent = $_POST["mapcode"];
    $sql = "UPDATE pageindex
          SET content='$newcontent'
          WHERE name = 'blockmap' LIMIT 1 ";

    $currentuser = $_SESSION["id"];
    $currdate = date("Y-m-d H:i:s");
    $sqldeb = "INSERT INTO changes (user,date,action)
                              VALUES ('$currentuser','$currdate',' ==== обновид карту на главной ')";
    simplequery2db($sqldeb);

    if (simplequery2db($sql)) {
        $output = json_encode(array('type' => 'success', 'text' => 'Карта обновлена успешно'));
        die($output);
    } else {
        $output = json_encode(array('type' => 'error', 'text' => 'Ошибка обновления карты'));
        die($output);
    }
}

function updatepartn(){

    $partnerlink = $_POST["partnerlink"];
    $partnertype = $_POST["partnertype"];
    $partnerid = $_POST["partnerid"];
    $msg = '';
    $uploaderror = 0;


    if (empty($partnerlink) || empty($partnertype) ) {
        $msg .= 'Ошибка. Ссылка и Тип - должны быть заполнены <br>';
        $uploaderror = 1;
    }

    //echo $partnerlink;
    //echo $partnertype;

    if($uploaderror == 0){

        $sql = "UPDATE widgetparnter
            SET link='$partnerlink',
              type='$partnertype'
            WHERE id = $partnerid LIMIT 1 ";

        $currentuser = $_SESSION["id"];
        $currdate = date("Y-m-d H:i:s");
        $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== обновил партнера c id = $partnerid')";
        simplequery2db($sqldeb);


        if (simplequery2db($sql)) {
            $msg .= 'Парнер обновлен <br>';
        } else {
            $msg .= 'Ошибка обновления партнера <br>';
            $uploaderror = 1;
        }

    }




    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);
}

/*BLOG*/
function addblogpost(){
    //print_r($_POST);
    //print_r($_FILES);

    $nameblogpost = $_POST["nameblogpost"];
    $categoryblogpost = $_POST["categoryblogpost"];
    $blogposttext = addslashes_deep($_POST["blogpostdesription"]);
    $dateblogpost = $_POST["dateblogpost"];
    $date_elements  = explode("/",$dateblogpost);
    $dateblogpost =  $date_elements[2]."-".$date_elements[1]."-".$date_elements[0];

    $blogposter = $_FILES["blogposter"];

    //print_r()
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/blog/';
    $msg = '';
    $uploaderror = 0;


    if ( empty($nameblogpost) || $categoryblogpost < 1 || !isset($blogposter) ) {
        $msg .= 'Ошибка. Названиe, Категория и Постер - должны быть заполнены <br>';
        $uploaderror = 1;
    }

    if ($uploaderror == 0) {

        $blogposter4upload = md5(microtime()).ru2lat($blogposter['name']);

        $uploadfiletvposter = $uploaddirphoto . basename($blogposter4upload);

        if (move_uploaded_file($blogposter['tmp_name'], $uploadfiletvposter)) {
            //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
            img_resize($uploadfiletvposter, $uploadfiletvposter, 570, 250, $rgb = 0xffffff);
        } else {
            $msg .= 'Ошибка добавления постера <br>';
            $uploaderror = 1;
        }


        if($uploaderror == 0){

            $sql = "INSERT INTO blogposts (name,blogcategoryid,text,date,imgurl)
                    VALUES ('$nameblogpost', $categoryblogpost, '$blogposttext','$dateblogpost','$blogposter4upload')";

            $currentuser = $_SESSION["id"];
            $currdate = date("Y-m-d H:i:s");
            $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ++++ добавил пост в блог')";
            simplequery2db($sqldeb);


            if (simplequery2db($sql)) {
                $msg .= 'Новый пост добавлен <br>';
            } else {
                $msg .= 'Ошибка добавления видео в БД <br>';
                $uploaderror = 1;
            }

        }

    }


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);

}

function updateblogpost(){


    $nameblogpost = $_POST["nameblogpost"];
    $blogpostid = $_POST["blogpostid"];
    $categoryblogpost = $_POST["categoryblogpost"];
    $blogposttext = addslashes_deep($_POST["blogpostdesription"]);
    $dateblogpost = $_POST["dateblogpost"];
    $date_elements  = explode("/",$dateblogpost);
    $dateblogpost =  $date_elements[2]."-".$date_elements[1]."-".$date_elements[0];

    @$blogposter = $_FILES["blogposter"];

    //print_r()
    $uploaddirphoto = $_SERVER["DOCUMENT_ROOT"] . '/photo/blog/';
    $msg = '';
    $uploaderror = 0;


    if ( empty($nameblogpost) || $categoryblogpost < 1 ) {
        $msg .= 'Ошибка. Названиe, Категория - должны быть заполнены <br>';
        $uploaderror = 1;
    }

    if ($uploaderror == 0) {

        if (isset($blogposter) && !empty($blogposter)) {
            //$blogposter4upload = ru2lat($blogposter['name']);
            $blogposter4upload = md5(microtime()).ru2lat($blogposter['name']);

            $uploadfiletvposter = $uploaddirphoto . basename($blogposter4upload);


            if (move_uploaded_file($blogposter['tmp_name'], $uploadfiletvposter)) {
                //$msg .= $namevideo4upload.'.webm закачан на сервер успешно <br>';
                img_resize($uploadfiletvposter, $uploadfiletvposter, 570, 250, $rgb = 0xffffff);
            } else {
                $msg .= 'Ошибка добавления постера <br>';
                $uploaderror = 1;
            }
        }



        if($uploaderror == 0){

            /*$sql = "INSERT INTO blogposts (name,blogcategoryid,text,date,imgurl)
                    VALUES ('$nameblogpost', $categoryblogpost, '$blogposttext','$dateblogpost','$blogposter4upload')";*/

            if(isset($blogposter4upload)&& !empty($blogposter4upload)){
                $sql = "UPDATE blogposts
                    SET
                      name='$nameblogpost',
                      date='$dateblogpost',
                      blogcategoryid=$categoryblogpost,
                      text='$blogposttext',
                      imgurl='$blogposter4upload'
                    WHERE id = $blogpostid LIMIT 1 ";

            }else{
                $sql = "UPDATE blogposts
                    SET
                      name='$nameblogpost',
                      date='$dateblogpost',
                      blogcategoryid=$categoryblogpost,
                      text='$blogposttext'
                    WHERE id = $blogpostid LIMIT 1 ";

            }

           /* $currentuser = $_SESSION["id"];
            $currdate = date("Y-m-d H:i:s");
            $sqldeb = "INSERT INTO changes (user,date,action)
              VALUES ('$currentuser','$currdate',' ==== обновил пост #'.$blogpostid.' блог')";
            simplequery2db($sqldeb);*/


            if (simplequery2db($sql)) {
                $msg .= 'Пост обновлен <br>';
            } else {
                $msg .= 'Ошибка обновления поста <br>';
                $uploaderror = 1;
            }

        }

    }


    if ($uploaderror == 0) {
        $output = json_encode(array('type' => 'success', 'text' => $msg));
    } else {
        $output = json_encode(array('type' => 'error', 'text' => $msg));
    }
    die($output);
}

?>