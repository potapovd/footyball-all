<?
include_once("lib/cms_view_inc.php");
checkpassandroll("tv");


$allcategories = getcatigoriesname();
$videos1 = getvideos("video-daidejst");
$videos2 = getvideos("perovo");
$videos3 = getvideos("park-pobedy");
$videos4 = getvideos("proekty");
$videos5 = getvideos("futy-cafe");
$videos6 = getvideos("tribuna-oratorov");
//$videos7 = getvideos("ekskluzivy");
$videos8 = getvideos("savelovskaya");

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/css/mediaelementplayer.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>
            FootyBall TV
            <button class="btn btn-success" href="#sample-modal" data-toggle="modal"
                    data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button"><i class="fa fa-plus"></i></button>
        </h2>


        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal form-groups-bordered">
                    <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->

                        <?
                        $i = 1;
                        foreach ($allcategories as $key => $val) {
                            ?>


                            <li <? if ($i == 1) {
                                echo 'class="active"';
                            } ?>>
                                <a href="#tab<?= $i; ?>" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-home"></i></span>
                                    <span class="hidden-xs"><?= $allcategories[$key]["name"]; ?></span>
                                </a>
                            </li>

                            <?
                            $i++;
                        }
                        ?>


                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid1">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos1 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos1[$key]["videoname"]; ?></td>
                                                <td><?= $videos1[$key]["cout"]; ?></td>
                                                <td><?= $videos1[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos1[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos1[$key]["videoid"];?>)" data-videoid ="<?=$videos1[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid2">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos2 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos2[$key]["videoname"]; ?></td>
                                                <td><?= $videos2[$key]["cout"]; ?></td>
                                                <td><?= $videos2[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos2[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos2[$key]["videoid"];?>)" data-videoid ="<?=$videos2[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid3">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos3 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos3[$key]["videoname"]; ?></td>
                                                <td><?= $videos3[$key]["cout"]; ?></td>
                                                <td><?= $videos3[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos3[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos3[$key]["videoid"];?>)" data-videoid ="<?=$videos3[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid4">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos4 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos4[$key]["videoname"]; ?></td>
                                                <td><?= $videos4[$key]["cout"]; ?></td>
                                                <td><?= $videos4[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos4[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos4[$key]["videoid"];?>)" data-videoid ="<?=$videos4[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab5">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid5">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos5 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos5[$key]["videoname"]; ?></td>
                                                <td><?= $videos5[$key]["cout"]; ?></td>
                                                <td><?= $videos5[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos5[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos5[$key]["videoid"];?>)" data-videoid ="<?=$videos5[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab6">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid6">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos6 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos6[$key]["videoname"]; ?></td>
                                                <td><?= $videos6[$key]["cout"]; ?></td>
                                                <td><?= $videos6[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos6[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos6[$key]["videoid"];?>)" data-videoid ="<?=$videos6[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab7">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid7">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($videos7 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $videos7[$key]["videoname"]; ?></td>
                                                <td><?= $videos7[$key]["cout"]; ?></td>
                                                <td><?= $videos7[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$videos7[$key]["videoid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removevideo',<?=$videos7[$key]["videoid"];?>)" data-videoid ="<?=$videos7[$key]["videoid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>




                    </div>
                    <button class="btn btn-success btn-icon" href="#sample-modal" data-toggle="modal"
                            data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button">Добавить видео <i class="fa fa-plus"></i></button>
                </form>
            </div>
        </div>


        <!-- Sample Modal (Default skin) -->
        <div class="modal fade" id="addedivdideo">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Видео</h4>
                    </div>

                    <div class="modal-body"> <h3>Загрузка...</h3> </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="savevideoinfo">Сохранить <i class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <script src="assets/js/mediaelement-and-player.min.js"></script>

    <script type="text/javascript">

        $("#logout").on("click",function() {
            $.post( "lib/cms_update_inc.php",{formname: "logout"});
            //return false;
        });


        function showAjaxModal(id){
            //var videoid = "id="+id;
            //alert (videoid);
            $('#addedivdideo').modal('show', {backdrop: 'static'});
            $.ajax({
                url: "page-tv-onevideo.php",
                type: "POST",
                async: false,
                cache: false,
                data:"id="+id,
                success: function(response)
                {
                    $('#addedivdideo .modal-body').html(response);
                }
            });
        }
        $("#savevideoinfo").on("click",function() {
            //var namevideo = $("#datevideo").val()
            //alert("namevideo: "+namevideo);


            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            if($("#videoid").val()!==undefined) {

                if ( $("#namevideo").val()!=="" && $("#datevideo").val()!== ""){
                    var formData = new FormData();
                    formData.append('formname', 'updatevideo');
                    formData.append('videoid', $("#videoid").val());
                    formData.append('videourl', $("#videourl").val());
                    formData.append('namevideo', $("#namevideo").val());
                    formData.append('datevideo', $("#datevideo").val());
                    formData.append('categoryvideo', $("#categoryvideo").val());
                    formData.append('videodesription', $("#videodesription").val());
                    formData.append('tvposter', $('input[name=tvposter]')[0].files[0]);

                    //var formData = new FormData($("#tabvideoform")[0]);
                    $.ajax({
                        url: "lib/cms_update_inc.php",
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        async: false,
                        cache: false,
                        //enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        success: function (results) {
                            if (results["type"] == 'error') {
                                toastr.error(results["text"], optstoastr);

                            } else {
                                toastr.success(results["text"], optstoastr);
                                $('#addedivdideo').modal('hide');
                            }
                        },
                        error: function (results) {
                            toastr.error('Ошибка! ' + results["responseText"]);
                        },
                        complete: function () {
                        }
                    });
                }else{
                    toastr.error('Ошибка! Названиe, Категория, Видео .webm и mp4 - должны быть заполнены');
                    return false;
                }
            }else{
                //alert("new video");

                if ( $("#namevideo").val()!=="" && $("#datevideo").val()!== "" && $('input[name=tvvideowebm]')[0].files[0]!==undefined && $('input[name=tvvideomp4]')[0].files[0]!==undefined && $('input[name=tvposter]')[0].files[0]!==undefined){
                    var formData = new FormData();
                    formData.append( 'formname', 'addvideo');
                    formData.append( 'namevideo', $("#namevideo").val());
                    formData.append( 'datevideo', $("#datevideo").val());
                    formData.append( 'categoryvideo', $("#categoryvideo").val());
                    formData.append( 'videodesription', $("#videodesription").val());
                    formData.append( 'tvvideowebm', $('input[name=tvvideowebm]')[0].files[0]);
                    formData.append( 'tvvideomp4', $('input[name=tvvideomp4]')[0].files[0]);
                    formData.append( 'tvposter', $('input[name=tvposter]')[0].files[0]);


                    $.ajax({
                        url: "lib/cms_update_inc.php",
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        //async: false,
                        cache: false,
                        //enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            toastr.success("Идет загрузка видео...", optstoastr);
                        },
                        success: function(results) {
                            if(results["type"] == 'error'){
                                toastr.error(results["text"], optstoastr);

                            }else{
                                toastr.success(results["text"], optstoastr);
                                $("#tableid"+$("#categoryvideo").val()+" tr:first").after("<tr><td>0</td><td>"+$("#namevideo").val()+"</td><td></td><td></td></tr>");
                                $('#addedivdideo').modal('hide');
                            }
                        },
                        error:function(results) {
                            toastr.error('Ошибка! '+results["responseText"]);
                        },
                        complete: function() {
                        }
                    });


                }else{
                    toastr.error('Ошибка! Названиe, Категория, Видео .webm, mp4 и постер - должны быть заполнены');
                    return false;
                }
            }



            //return false;

            //console.log(formData);


            //return false
        });



        function removevideos(object,id){
            var formData = {
                'object' : object,
                'id' : id,
                'formname' : "removeelement"
            };
            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                success: function(results) {
                    //var posts = JSON.parse(results);
                    if(results["type"] == 'error'){
                        toastr.error(results["text"], optstoastr);

                    }else{
                        toastr.success(results["text"], optstoastr);
                        $("* [data-videoid='"+id+"']").parent().parent().slideUp("slow");

                    }
                },
                error: function(results) {
                    toastr.error('Ошибка! '+results["responseText"]);
                    //console.log(results);
                }
            });


            return false;

        }


        $(document).ready(function ($) {
            $("#mainmenutv").addClass("active");
        });









    </script>

</div>
</body>
</html>