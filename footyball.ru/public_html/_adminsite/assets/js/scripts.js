$("#logout").on("click",function() {
    $.post( "lib/cms_update_inc.php",{formname: "logout"});
});

function showAjaxModalUserinfo(id){

    $('#currentuser').modal('show', {backdrop: 'static'});
    $.ajax({
        url: "page-user-currentuser.php",
        type: "POST",
        async: false,
        cache: false,
        data:"id="+id,
        success: function(response)
        {
            $('#currentuser .modal-body').html(response);
        }
    });
}
$("#savecurrentuser").on("click",function() {
    var optstoastr = {
        "closeButton": false,
        "debug": false,
        "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
        "toastClass": "black",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    if($("#newpas").val()!==""||$("#newpas2").val()!==""){
        if ($("#newpas").val()!==$("#newpas2").val()){
            toastr.error("Пароли не совпадают", optstoastr);
            return false;
        }else{
            var formData = new FormData();
            formData.append('formname', 'updatecurrenuser');
            formData.append('currentuserid', $("#currentuserid").val());
            formData.append('newpas', $("#newpas").val());
        }
        $.ajax({
            url: "lib/cms_update_inc.php",
            type: "POST",
            data: formData,
            dataType: 'json',
            async: false,
            cache: false,
            //enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            success: function (results) {
                if (results["type"] == 'error') {
                    toastr.error(results["text"], optstoastr);

                } else {
                    toastr.success(results["text"], optstoastr);
                    $('#currentuser').modal('hide');
                }
            },
            error: function (results) {
                toastr.error('Ошибка! ' + results["responseText"]);
            },
            complete: function () {
            }

        });

    }else{
        return false;
    }
});

$(document).ready(function () {
    $('.datatable').DataTable({
        language: {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "infoEmpty": "Записи с 0 до 0 из 0 записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "infoPostFix": "",
            "loadingRecords": "Загрузка записей...",
            "zeroRecords": "Записи отсутствуют.",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
            },
            "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
            }
        }
    });
});