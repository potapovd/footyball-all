<?
include_once("lib/cms_view_inc.php");
checkpassandroll("0");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/js/dropzone/dropzone.css">
    <link rel="stylesheet" href="assets/js/jcrop/jquery.Jcrop.min.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>У вас не хватает прав для просмотра данной страницы.</h2>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
            <script src="assets/js/dropzone/dropzone.js"></script>
            <script src="assets/js/jcrop/jquery.Jcrop.min.js"></script>

        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            $("#logout").on("click",function() {
                $.post( "lib/cms_update_inc.php",{formname: "logout"});
                //return false;
            });

            //$("#mainmenuindex").addClass("active");


        });
    </script>
</div>
</body>
</html>