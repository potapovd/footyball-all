<?
include_once("lib/cms_view_inc.php");
checkpassandroll("0");

if(!isset($_POST["id"])){ die();}

$username = $_POST["id"];
$userinfo = getuserinfo($username);

$perm = getperm($username);

$getrandpass = generateCode(12);


if(is_ajax2()){
?>
<form class="form-horizontal form-groups-bordered" role="form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Логин </label>
                <div class="col-sm-9"><input type="text" placeholder="" required id="namevideo" name="namevideo" class="form-control" disabled value="<?=$userinfo[0]["username"];?>"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Новый пароль <span class="inpreq">*</span> </label>
                <div class="col-sm-9"><input type="text" placeholder="" required id="newpas" name="newpas" class="form-control"  value=""></div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Повтор пароля <span class="inpreq">*</span> </label>
                <div class="col-sm-9"><input type="text" placeholder="" required id="newpas2" name="newpas2" class="form-control" value=""></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Случайный пароль</label>
                <div class="col-sm-9"><input type="text"  id="gennewpas" name="gennewpas" class="form-control"  value="<?=$getrandpass;?>"></div>
            </div>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <label class="col-sm-3 control-label" for="namevideo">Права доступа </label>
            <div class="col-sm-9">
                <?=$perm;?>
            </div>
        </div>
    </div>
    <input type="hidden" name="currentuserid" id="currentuserid" value="<?=$userinfo[0]["id"];?>">
</form>
<?}?>

