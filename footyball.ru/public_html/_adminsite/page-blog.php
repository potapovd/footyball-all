<?
include_once("lib/cms_view_inc.php");
checkpassandroll("blog");


$allcategories = getcatblogname();
$posts1 = getposts("statiii");
$posts2 = getposts("novosti-footyball");
$posts3 = getposts("aktsii");


?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <!--<link rel="stylesheet" href="assets/css/mediaelementplayer.css">-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>
            FootyBall Blog
            <button class="btn btn-success" href="#sample-modal" data-toggle="modal"
                    data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button"><i class="fa fa-plus"></i></button>
        </h2>


        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal form-groups-bordered">
                    <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->

                        <?
                        $i = 1;
                        foreach ($allcategories as $key => $val) {
                            ?>


                            <li <? if ($i == 1) {
                                echo 'class="active"';
                            } ?>>
                                <a href="#tab<?= $i; ?>" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-home"></i></span>
                                    <span class="hidden-xs"><?= $allcategories[$key]["name"]; ?></span>
                                </a>
                            </li>

                            <?
                            $i++;
                        }
                        ?>


                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid1">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($posts1 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $posts1[$key]["blogpostsname"]; ?></td>
                                                <td><?= $posts1[$key]["cout"]; ?></td>
                                                <td><?= $posts1[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$posts1[$key]["blogpostsid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removeblogpost',<?=$posts1[$key]["blogpostsid"];?>)" data-videoid ="<?=$posts1[$key]["blogpostsid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid2">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($posts2 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $posts2[$key]["blogpostsname"]; ?></td>
                                                <td><?= $posts2[$key]["cout"]; ?></td>
                                                <td><?= $posts2[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$posts2[$key]["blogpostsid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removeblogpost',<?=$posts2[$key]["blogpostsid"];?>)" data-videoid ="<?=$posts2[$key]["blogpostsid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover datatable" id="tableid3">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;
                                        foreach ($posts3 as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $posts3[$key]["blogpostsname"]; ?></td>
                                                <td><?= $posts3[$key]["cout"]; ?></td>
                                                <td><?= $posts3[$key]["date"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$posts3[$key]["blogpostsid"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removevideos('removeblogpost',<?=$posts3[$key]["blogpostsid"];?>)" data-videoid ="<?=$posts3[$key]["blogpostsid"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-success btn-icon" href="#sample-modal" data-toggle="modal"
                            data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button">Добавить пост <i class="fa fa-plus"></i></button>
                </form>
            </div>
        </div>


        <!-- Sample Modal (Default skin) -->
        <div class="modal fade" id="addedivdideo">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Пост</h4>
                    </div>

                    <div class="modal-body"> <h3>Загрузка...</h3> </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="savevideoinfo">Сохранить <i class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <!--<script src="assets/js/mediaelement-and-player.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>-->
    <script src="assets/js/summernote.js"></script>
    <script src="assets/js/summernote-ru-RU.js"></script>

    <script type="text/javascript">

        $("#logout").on("click",function() {
            $.post( "lib/cms_update_inc.php",{formname: "logout"});
            //return false;
        });


        function showAjaxModal(id){
            //var videoid = "id="+id;
            //alert (videoid);
            $('#addedivdideo').modal('show', {backdrop: 'static'});
            $.ajax({
                url: "page-blog-onepost.php",
                type: "POST",
                async: false,
                cache: true,
                data:"id="+id,
                success: function(response)
                {
                    $('#addedivdideo .modal-body').html("<h3>Загрузка...</h3>");
                    $('#addedivdideo .modal-body').html(response);
                }
            });
        }
        $("#savevideoinfo").on("click",function() {
            //var namevideo = $("#datevideo").val()
            //alert("namevideo: "+namevideo);


            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            if($("#blogpostid").val()!==undefined) {
                //alert("edit blogpost");
                if ( $("#namevideo").val()!=="" && $("#datevideo").val()!== ""){
                    var formData = new FormData();
                    formData.append('formname', 'updateblogpost');
                    formData.append('blogpostid', $("#blogpostid").val());
                    formData.append( 'nameblogpost', $("#nameblogpost").val());
                    formData.append( 'dateblogpost', $("#dateblogpost").val());
                    formData.append( 'categoryblogpost', $("#categoryblogpost").val());
                    formData.append( 'blogpostdesription', $("#blogpostdesription").val());
                    formData.append( 'blogposter', $('input[name=blogposter]')[0].files[0]);

                    //var formData = new FormData($("#tabvideoform")[0]);
                    $.ajax({
                        url: "lib/cms_update_inc.php",
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        async: false,
                        cache: false,
                        //enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            toastr.success("Идет обновление поста...", optstoastr);
                        },
                        success: function (results) {
                            if (results["type"] == 'error') {
                                toastr.error(results["text"], optstoastr);

                            } else {
                                toastr.success(results["text"], optstoastr);
                                $('#addedivdideo').modal('hide');
                            }
                        },
                        error: function (results) {
                            toastr.error('Ошибка! ' + results["responseText"]);
                        },
                        complete: function () {
                        }
                    });
                }else{
                    toastr.error('Ошибка. Названиe, Категория - должны быть заполнены');
                    return false;
                }
            }else{
                //alert("new blogpost");

                if ( $("#nameblogpost").val()!=="" && $("#dateblogpost").val()!== "" && $('input[name=blogposter]')[0].files[0]!==undefined){
                    var formData = new FormData();
                    formData.append( 'formname', 'addblogpost');
                    formData.append( 'nameblogpost', $("#nameblogpost").val());
                    formData.append( 'dateblogpost', $("#dateblogpost").val());
                    formData.append( 'categoryblogpost', $("#categoryblogpost").val());
                    formData.append( 'blogpostdesription', $("#blogpostdesription").val());
                    formData.append( 'blogposter', $('input[name=blogposter]')[0].files[0]);


                    $.ajax({
                        url: "lib/cms_update_inc.php",
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        //async: false,
                        cache: false,
                        //enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            toastr.success("Идет добавление поста...", optstoastr);
                        },
                        success: function(results) {
                            if(results["type"] == 'error'){
                                toastr.error(results["text"], optstoastr);

                            }else{
                                toastr.success(results["text"], optstoastr);
                                $("#tableid"+$("#categoryvideo").val()+" tr:first").after("<tr><td>0</td><td>"+$("#namevideo").val()+"</td><td></td><td></td></tr>");
                                $('#addedivdideo').modal('hide');
                            }
                        },
                        error:function(results) {
                            toastr.error('Ошибка! '+results["responseText"]);
                        },
                        complete: function() {
                        }
                    });


                }else{
                    toastr.error('Ошибка. Названиe, Категория и Постер - должны быть заполнены');
                    return false;
                }
            }



            //return false;

            //console.log(formData);


            //return false
        });



        function removevideos(object,id){
            var formData = {
                'object' : object,
                'id' : id,
                'formname' : "removeelement"
            };
            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                success: function(results) {
                    //var posts = JSON.parse(results);
                    if(results["type"] == 'error'){
                        toastr.error(results["text"], optstoastr);

                    }else{
                        toastr.success(results["text"], optstoastr);
                        $("* [data-videoid='"+id+"']").parent().parent().slideUp("slow");

                    }
                },
                error: function(results) {
                    toastr.error('Ошибка! '+results["responseText"]);
                    //console.log(results);
                }
            });


            return false;

        }


        $(document).ready(function ($) {
            $("#mainmenublogpage").addClass("active");
        });









    </script>

</div>
</body>
</html>