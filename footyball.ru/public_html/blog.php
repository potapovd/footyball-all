<?
include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(9);
$posts = getposts(0);
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$pageinfo[0]["metatitle"];?></title>
    <meta name="description" content="<?=$pageinfo[0]["metadescr"];?>">
    <meta name="keywords" content="<?=$pageinfo[0]["metakeyw"];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-blog.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <!--<div class="container-fluid" id="redline"></div>-->
        <div class="container">
            <div class="row" id="videofilters">
                <a href="#" class="filter categ" data-filter="all">все</a>
                <a href="#" class="filter categ" data-filter=".statiii">Статьи</a>
                <a href="#" class="filter categ" data-filter=".novosti-footyball">Новости FootyBall</a>
                <a href="#" class="filter categ" data-filter=".aktsii">Акции</a>
                <div class="clearfix"></div>
            </div>
            <div class="row" id="allvideos">

                <?foreach($posts as $key => $val){?>


                <article class="col-sm-6 col-sx-12 mix <?=$posts[$key]["unicname"];?>">
                    <div class="blog-all-onelpost">
                        <div class="blog-all-onelpost-line1">
                            <a href="one-post.php?id=<?=$posts[$key]["blogpostsid"];?>">
                                <img src="photo/blog/<?=$posts[$key]["imgurl"];?>" class="img-responsive" alt="">
                            </a>
                        </div>
                        <div class="blog-all-onelpost-line2">
                            <span class="blog-all-onelpost-line2-date"><?=$posts[$key]["date"];?></span>
                            |
                            <span class="blog-all-onelpost-line2-cat"><?=$posts[$key]["name"];?></span>
                        </div>
                        <div class="blog-all-onelpost-line3">
                            <a href="one-post.php">
                                <h3 class="blog-all-onelpost-line2-postanme">
                                    <a href="one-post.php?id=<?=$posts[$key]["blogpostsid"];?>">
                                        <?=$posts[$key]["blogpostsname"];?>
                                    </a>
                                </h3>
                            </a>

                        </div>
                        <div class="blog-all-onelpost-line4">
                            <div class="blog-all-onelpost-line2-poststext">
                                <?
                                $posttext = preg_replace("/<img[^>]+\>/i", "", $posts[$key]["text"]);
                                $posttext = strip_tags($posttext);
                                $posttext = mb_substr($posttext,0,200,'UTF-8');
                                $posttext = $posttext." ...";
                                echo $posttext;
                                ?>
                            </div>
                        </div>
                    </div>
                </article>


                <?} ?>


                

               

            </div>
            
        </div>
	    <div class="container-fluid nopadding">
		    <?include_once("inc/map.inc.php");?>
	    </div>
	    <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
		    <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
		    <div class="blockinnermenu">
			    <div class="row">
				    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
					    <?include_once("inc/subscribeform.inc.php");?>
				    </div>
			    </div>
		    </div>
	    </nav>
	    <div class="container-fluid nopadding" id="mainblockfooter">
		    <?include_once("inc/footer-new.inc.php");?>
		    <?include_once("inc/hide-popups.inc.php");?>
	    </div>
    </div>


    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/jquery.mixitup.min.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


    <script>



	    function initMap(adress) {

		    $("#map").html('');

		    var myGeocoder = ymaps.geocode(adress);
		    myGeocoder.then(
			    function(res) {
				    var pos = res.geoObjects.get(0).geometry.getCoordinates();
				    var myMap = new ymaps.Map('map', {
					    center: pos,
					    zoom: 17
				    });
				    var bpos = {
					    lat: pos[0],
					    lng: pos[1]
				    };
				    var ppos = {
					    lat: pos[0],
					    lng: pos[1] -= 0.003
				    };

				    var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					    balloonContentHeader: '<p class="adr">' + adress + '</p>'
				    },{
					    iconLayout: 'default#image',
					    iconImageHref: 'assets-landing/img/marker.png',
					    iconImageSize: [28, 33]
				    });
				    setPlacemark();


				    myMap.geoObjects.add(placemark);

				    myMap.controls
					    .add('zoomControl')
					    .add('typeSelector')
					    .add('mapTools');

				    myMap.events.add('click', function(e) {
					    myMap.balloon.close();
				    });

				    window.onresize = function() {
					    setPlacemark();
				    };

				    function setPlacemark() {
					    if (window.innerWidth < 740) {
						    myMap.setCenter([bpos.lat,bpos.lng]);
					    } else {
						    myMap.setCenter([ppos.lat,ppos.lng]);
					    }
				    }

			    }
		    );

	    }

    $(document).ready(function() {


        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var cat = getUrlParameter('cat');
        $('#allvideos').mixItUp();
        if(cat.length>0){
            if(cat!=="all"){
                $('#allvideos').mixItUp('filter', '.'+cat);
            }
        }

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $("#videofilters").hide();
        }else{
            $("#videofilters").show();
        }

	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });

	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
		    });
		    $('.b9 .item:first-of-type').click();
	    };



    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>
