$(document).ready(function() {
    /*ALL PAGES*/
    //Menu
    $("#topmenu").sticky({
        topSpacing: 0,
        zIndex:200,
        center:true,
        responsiveWidth:true
    });
    $('#topmenu').on('sticky-start', function() {
        $('#topmenulogo,#topmenulogomob').attr("src","assets/img/logo-small.svg");
        $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='assets/img/logo-small.png'");
    });
    $('#topmenu').on('sticky-end', function() {
        $('#topmenulogo,#topmenulogomob').attr("src","assets/img/logo.svg");
        $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='assets/img/logo.png'");
        $("#topmenu").sticky({
            topSpacing: 0,
            zIndex:200,
            center:true,
            responsiveWidth:true
        });
    });
    var lastScrollTop = 0;
    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            var st2 = st - lastScrollTop;
            if (st2>10){
                $("#mandectopmaenu").slideUp();
            }
            //console.log("down");
        } else {
            $("#mandectopmaenu").slideDown();
            //console.log("up");
        }
        lastScrollTop = st;
    });
    $('.toggle-menu').jPushMenu({
        closeOnClickLink: false
    });
    $('#menu').metisMenu();
    $('input[name="phone"]').mask('9(999) 999 99 99');
    $("#00N200000098YTQ").ionRangeSlider({
        min: 2,
        max: 7,
        from: 3,
        grid: true,
        values: [2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7],
        max_postfix: " лет"
    });
    //mobile correct
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $(".topmenumobile").css("display","block");
        $(".desctfooter, #adr").css("display","none");
        $("#mobilefooter").show("slow");
        $(".socblock").css("padding-bottom","60px");
        $("#topmenu-sticky-wrapper").removeClass("destopmenu").addClass("mobilemenu");
    }else{
        $(".topmenudesctop").css("display","block");
        $("#topmenu-sticky-wrapper").removeClass("mobilemenu").addClass("destopmenu")
    }
    $('#mapmob').click(function(){
        var collapse_content_selector = $(this).attr('href');
        $(collapse_content_selector).slideToggle(function(){
            if($(this).css('display')=='none'){
            }else{
            }
        });
        return false;
    });
    //check form
    $("#formfreetraining").submit(function(){
        //check terms
        var termstatus = $('#00N0J000009xwo5').is(':checked');
        if(termstatus==false){
            $(".subscrform .subscrformaatext").css("color","red");
            return false;
        }

    });






	//test function
    /*$("#sendWebToLeadbutton").on("click",function(e){

        var firstnamechildren = $("#first_name").val();
        var firstnameparent = $("#00N200000098YTY").val();
        var phone = $("#freetrain_phone").val();
        var age = $("#00N200000098YTQ").val();
        var errordiv  =  $("#errortxtsendWebToLeadbutton span");

        //проверка данных
        var agetest = /^[2-7.]+$/;
        var russnametest = /^[а-яА-ЯёЁ ]+$/;
        var phonetest = /[\+7][\(]\d{3}[\)][\ ]\d{3}[\ ]\d{2}[\ ]\d{2}/;

        if(firstnameparent.length<3 || firstnameparent=="" ||!russnametest.test(firstnameparent) ){
            response = "Ошибка! Вы ввели неправильное Имя и/или Фамилию Родителя";
            errordiv.html("").html(response).fadeIn().delay(3000).fadeOut("slow");
            return false
        }
        if(phone.length!==17|| phone=="" ||!phonetest.test(phone)){
            response = "Ошибка! Вы ввели Неправильный телефон";
            errordiv.html("").html(response).fadeIn().delay(3000).fadeOut("slow");
            return false
        }
        if(age.length>3||age.length<=0|| !agetest.test(age)){
            response = "Ошибка! Вы ввели Неправильный возраст";
            errordiv.html("").html(response).fadeIn().delay(3000).fadeOut("slow");
            return false
        }


        setTimeout(function() {

            $.ajax({
                statusCode : { 404: function(){alert('Not Found');} },
                url: 'sendWebToLead.php',
                type: 'post',
                data: {'namech':firstnamechildren,'namepar':firstnameparent,'phone':phone,'age':age},
                dataType:'JSON',
                crossDomain: false,
                timeout: 10000,
                cache:false,
                success: function(res){
                    response = res.answertxt;
                    response = res.answertxt.trim();
                    errordiv.html("").css("color","green").html(response).fadeIn().delay(3000).fadeOut("slow");
                    //return false;



                },
                error: function(error){
                    console.log("Error:");
                    console.log(error);
                    return false;
                }
            });

        }, 1000);


        return false;
    });*/



});