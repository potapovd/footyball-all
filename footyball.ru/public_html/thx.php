<?
include_once("lib/cms_view_inc.php");

$pageinfo = getpageinfo(8);
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
    <head>
        <?include_once("inc/head.inc.php")?>
    </head>

    <body>

        <?include_once("inc/header.inc.php")?>

        <!--<div id="maincontent" class="maincontentadmin headerlock">-->
        <div id="maincontent" class="maincontentadmin">
            <div class="headerlockdiv"></div>
                <div class="container">
                    
                    <div class="modal-overlay" style="visibility: inherit; opacity: 1;"></div>
                    <div class="modal-wrapper" style="visibility: visible !important; opacity: 1">
                        <div class="modal-window js-blur" id="modalwindowphoto" style="visibility: visible !important; opacity: 0" >
                            <a href="index.php" class="close-modal">X</a>
                            <div class="textpopupabout">
                                
                                <h3 style =" color:#00aeef; padding-top:80px;">Спасибо! Мы с Вами свяжемся в ближайшее время!</h3>
                                
                            </div>   
                        </div>
                    </div>
           
            </div>
        </div>
        

        <?include_once("inc/footer.inc.php")?>
        <?include_once("inc/modal-login.inc.php")?>

        <?include_once("inc/mainsctipts.inc.php")?>
        <script src="assets/js/slick.min.js" defer="defer"></script>
        <script src="assets/js/blockposition-index.js" defer="defer"></script>
        <script src="assets/js/scripts.js"></script>

        <script src="assets-landing/js/plugins/fancy.js"></script>
        <script defer="defer" src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>
        
        <script>
            $(window).load(function(){
                //$(".textpopup").mCustomScrollbar({alwaysShowScrollbar:2});
            });
            
            $(function() {
                ranbombg()
                
                $("#modalwindowphoto").animate({ opacity: "1" }, 1000);
                $("#modalwindowphoto").css("visibility","visible");
                
                  $('.fadeslider').slick({
                    dots: true,
                    infinite: true,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                });
                
                $('#navtabs a').on('shown.bs.tab', function (e) {
                    $('.fadeslider').resize();
                });
                
            });
        </script>

        <?include_once("inc/beforeclose.inc.php")?>
    </body>
</html>