<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">


    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <meta name="author" content="PotapovD">

    <style>
        .mainbannerout{
            margin: 40px auto;
	          /*display: flex;
	          align-items:center;
	          align-content:center;*/
        }
        .mainbanner{
            position: relative;
            overflow: hidden;
            margin: 50px auto;
            max-width: 850px;
        }
        .mainbanner img{
            margin: 5px;
            opacity: 0;

        }
        .txtblok{
            background: rgba(255,255,255, 1);
            width: 310px;

            text-align: center;


            position: absolute;
	        z-index: 1;
	        /*top: 0; left: 0; bottom: 0; right: 0;
	        margin: auto;*/


	        /*top: 30%;
					left: 35%;*/
	        top:50%;
	        left:50%;
	        margin:-90px 0 0 -155px;
        }
        .txtbloknumb{
            color: deeppink;
            font-size: 68px;
            font-weight: 900;
            line-height: 50px;
            margin: 10px auto;
        }
        .txtblokbg{
            display: inline-block;
            margin: auto auto;
            padding: 5px;
            font-size: 18px;
            background: deeppink;
            color: #fff;
            text-transform: uppercase;
            font-weight: 900;
        }
        .txtblolink{
            display: inline-block;
            border: 1px solid deeppink;
            color: deeppink;
            text-wrap: none;
            padding: 5px 20px;
            text-transform: uppercase;
            font-size: 18px;
            margin: 15px auto ;
        }
        .txtblolink:hover{
            background: deeppink;
            color: #fff;
        }
        .mainbanner img{
            border-radius:45%;
        }
    </style>
</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row">
                <div class=" col-sm-offset-1 col-sm-10 col-sx-12">



                    <div class="mainbannerout">
                        <div class="txtblok" style="display:none">
                            <div class="txtblokbg">
                                на наших бесплатных тренеровках:
                            </div>
                            <div class="txtbloknumb">&nbsp;</div>
                            <div class="txtblokbg">
                                улыбок, победителей, детей
                            </div>
                            <a class="txtblolink" href="#">
                                все подробности
                            </a>

                        </div>
                        <div class="mainbanner">


                            <?php

                            $dir = "photo/testgallery/";
                            $name = scandir($dir);
                            for($i=2; $i<=(sizeof($name)-1); $i++) {
                                echo "<img id='photo".$i."' src='".$dir.$name[$i]."'>";
                            }

                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/CounterCircleSlider.js"></script>
    <script src="assets/js/allpages-functions.js"></script>

<script>

    jQuery.fn.fadeDelay=function(e){var i=$(this),a=0,l=0,c=i.length,s={interval:50,fadeIn:!0,fadeTime:350,callback:null,hideVisible:!0,opaque:!0};return e=$.extend({},s,e),e.fadeIn?(e.hideVisible||(c=0,i.each(function(){e.opaque&&"0"==$(this).css("opacity")?c++:$(this).is(":visible")||c++})),i.each(function(){var i=!1;e.opaque&&(e.hideVisible||"0"==$(this).css("opacity"))?i=!0:(e.hideVisible||!$(this).is(":visible"))&&(i=!0),i&&(e.opaque?($(this).css({opacity:"0"}).delay(a).fadeTo(e.fadeTime,1,function(){l++,l>=c&&null!==e.callback&&e.callback()}),$(this).css({visibility:"visible"})):$(this).hide().delay(a).fadeIn(e.fadeTime,function(){l++,l>=c&&null!==e.callback&&e.callback()}),a+=e.interval)})):i.reverse().each(function(){e.opaque?$(this).delay(a).fadeTo(e.fadeTime,0,function(){l++,l>=c&&null!==e.callback&&e.callback(),$(this).css({display:"block",visibility:"hidden"})}):$(this).delay(a).fadeOut(e.fadeTime,function(){l++,l>=c&&null!==e.callback&&e.callback()}),a+=e.interval})},jQuery.fn.reverse=[].reverse;



   function animation() {

        var photoid =  Math.floor(Math.random() * (118 - 2)) + 2;
        var photoid2 =  Math.floor(Math.random() * (118 - 2)) + 2;
        $("#photo"+photoid+"").fadeTo(600, 0.1, function() {
            $(this).attr("src","").attr("src",$("#photo"+photoid2+"").attr("src"))
            $(this).fadeTo(2000, 1.0);
           // $("#photo"+photoid2+"").fadeTo(1000, 0.1, function() { $(this).fadeTo(1500, 1.0); });
        });

   }

    function startCounter() {
        //alert("111")
        var counterUp = $(".txtbloknumb");
        counterUp.counter({
            autoStart: false,
            duration: 7000,
            //countTo: newpeole,
            countTo: 75000,
            placeholder: "...",
            easing: "easeOutCubic",
            onComplete: function() {
                //cl.hide();
                //$('#blockcountchnumb').animate({width: 85},100).animate({fontSize: 34},2000);
            }
        });
        counterUp.counter("start");
    }


    $(document).ready(function() {

        //startCounter();




        $('.mainbanner img').fadeDelay({

            // Time between each item
            interval : 20,

            // Are we fading in or out?
            fadeIn : true,

            // How long each item takes to fade
            fadeTime : 350,

            // Function to call after completed
            callback : function () {
                //alert("0")
                //setInterval("$('.mainbanner').shuffle({durations: [1200, 1500, 1100, 1500, 1900]})",2200);
                //setInterval(animation(),2200);

                //bla
                //startCounter();
                $(".txtblok").fadeIn(1500,function () {
                    startCounter();
                });
                setTimeout(function () {

                },100)

                setInterval(function() {
                    //alert( "тик" );

                    setTimeout(animation(),Math.floor(Math.random() * (2000 - 500)) + 500)
                }, 1000);

            }


        });



        //setInterval("$('.mainbanner').shuffle({durations: [1200, 1500, 1100, 1500, 1900],times:0})",2000);
        /*$('.mainbanner').shuffle({
            durations: [500, 650, 750, 500, 900]
        })*/

    });

</script>

</body>
</html>
