<div class="modal-wrapper">
    <div class="modal-window js-blur" id="modalwindowtv" style="width:940px; height:600px">
        <button class="close-modal">X</button>
        
        <div class="tvvideoblokout" id="tvvideoblok">
           <div class="pull-left tvvideoblok">
               <h3 class="tvvideobloknamevieo"><?=$videos[1][1];?></h3>
               <div class="no-svg blockvideo">
                   <video width="560" height="315" poster="/assets/video/simple.jpg" id="footyballtv" controls="controls" preload="none">
                        <source src="/assets/video/<?=$videos[1][0];?>.webm" type="video/webm">
                        <source src="/assets/video/<?=$videos[1][0];?>.mp4" type="video/mp4">
                    </video>
                </div>
               <div class="tvdesctblock">
                   <?=$videos[1][2];?>
               </div>
            </div>
            
               <div class="pull-left tvlistblock">
                   <div class="tvlistblocktopbg" ></div>
                   <div class="tvlistblockbottombg" ></div>
                <ul>
                    <?
                        $i = 1;
                        foreach($videos as $key => $type)  {
                    ?>      
                    <li>
                        <a href="#" class="tvlistitem" data-tvnumb="<?=$i;?>">
                           <div class="tvlistprev">
                               <img src="/assets/video/<?=$type[0];?>.jpg" alt="">
                           </div>
                           <div class="tvlistname">
                               <?=$type[1];?>
                           </div>
                        </a>
                    </li>
                    <?
                        $i++;
                        }
                    ?>                    
                </ul>
            </div>
            <div class="clearfix"></div>
            
        </div>
            
      
        
    </div>
</div>