<!-- Footer -->
<footer>
	<div class="wrapper">
		<div class="logo">
			<img src="assets-landing/img/logo.svg" alt="">
			<p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
		</div>
		<div class="polit">
			<p>
				ОГРН: 1147746697119 <br>
				<a href="#polit" class="fancy">Политика конфиденциальности</a>
			</p>
			<div class="footsocblock">
				<a target="_blank" id="footsoclinkfb" class="footsoclink fa" href="//www.facebook.com/footyballru">
				</a>
				<a target="_blank" id="footsoclinkin" class="footsoclink fa" href="//instagram.com/footyball_russia"></a>
				<a target="_blank" id="footsoclinkyt" class="footsoclink fa" href="//www.youtube.com/channel/UCL8tDsCfvfDWxG8WEiDDrDA"></a>
				<a target="_blank" id="footsoclinkvk" class="footsoclink fa" href="//vk.com/footyballru"></a>
			</div>
		</div>
		<div class="cont">
			<p class="cont__text">Позвоните нам</p>
			<p class="cont__num">8-800-707-88-99</p>
		</div>
	</div>
</footer>

<div id="mobilefooter">
	<div class="container">
		<div class="row" id="addpanelmob">
			<div class="col-xs-12">
				<ul>
					<li><a href="https://goo.gl/maps/DYU9Az7jRCk">Футбольный клуб "Перово"</a></li>
					<li><a href="https://goo.gl/maps/ggtTmCMPEGF2">Футбольный клуб "Парк победы"</a></li>
					<li><a href="https://goo.gl/maps/M94KpzcXUHM2">Футбольный клуб "Белорусский"</a></li>
					<li><a href="https://goo.gl/maps/7JF2cF1V6AC2">Футбольный клуб "Ленинский"</a></li>
					<li><a href="https://goo.gl/maps/xy4sSdrcNoH2">Центральный офис</a></li>
				</ul>
			</div>
		</div>
		<div class="row" id="mainpanelmob">
			<div class="col-xs-6 nopadding">
				<a class="mobilnk" href="tel:88007078899">
					<i class="fa fa-phone buttoncall"></i> Позвонить
				</a>
			</div>
			<div class="col-xs-6 nopadding" id="mapmobout">
				<a class="mobilnk" href="#addpanelmob" id="mapmob">
					<i class="fa fa-map-marker buttoncall"></i> Доехать
				</a>
			</div>
		</div>
	</div>
</div>
<!-- //Footer -->