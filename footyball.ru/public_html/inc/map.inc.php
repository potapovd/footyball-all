<!-- Block9 -->
<section class="b9" id="adr">
	<div class="adress">
		<div class="top">
			<h2>Адреса клубов</h2>
			<p class="num">Позвоните нам: <b>8-800-707-88-99</b></p>
		</div>
		<div class="slider">
			<div class="item">
				<div class="box">
					<div class="oct">1</div>
					<p class="title">Футбольный клуб "Парк Победы"</p>
					<p class="adr"> Кутузовский проспект, 36, стр 51, г. Москва </p>
					<p class="graph"> Пн-Вс 8-00 - 22-00</p>
				</div>
			</div>
			<div class="item">
				<div class="box">
					<div class="oct">2</div>
					<p class="title">Футбольный клуб "Перово"</p>
					<p class="adr">1й проезд Перова поля, д. 9 стр. 5, г. Москва</p>
					<p class="graph"> Пн-Вс 8-00 - 22-00</p>
				</div>
			</div>
			<div class="item">
				<div class="box">
					<div class="oct">3</div>
					<p class="title">Футбольный клуб "Белорусский"</p>
					<p class="adr">
						ул. Правды, 24, стр.8, г. Москва
					</p>
					<p>Заезд с ул. 5-ого Ямского поля. Через КПП №5". Мы находимся возле "Бодрый день" и кафе "Рецептор"</p>
					<p class="graph"> Пн-Вс 8-00 - 22-00</p>
				</div>
			</div>
			<div class="item">
				<div class="box">
					<div class="oct">4</div>
					<p class="title">Футбольный клуб "Ленинский"</p>
					<p class="adr">5-й Донской проезд 15, стр. 5, 11 подъезд, г. Москва</p>
					<p class="graph">Пн-Вс 8:00-22:00</p>
				</div>
			</div>
		</div>
	</div>
	<div id="map"></div>
</section>
<!-- //Block9 -->