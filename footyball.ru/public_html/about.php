<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт - О нас</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">

        <div class="container">

            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">
                    <ul class="mySlideshow">
                        <li>
                            <img src="photo/banner/about/1.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/about-logo.png" class="img-responsive center-block sliderlogo" alt="">
                        </li>
                        <!--<li>
                            <img src="photo/banner/about/2.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/about-logo.png"" class="img-responsive center-block sliderlogo" alt="">
                        </li>-->
                    </ul>
                </div>
            </div>

            <div class="row abline1">
                <div class="col-sm-offset-2 col-sm-3 hidden-xs abredline "></div>
                <div class="col-sm-2 col-xs-12 txtblock nopadding">
                   <span>наша миссия</span>
                </div>
                <div class="col-sm-3 hidden-xs abredline nopadding"></div>
            </div>
            <div class="row abline2">
                <span class="txtblock">
                    ВОСПИТАНИЕ ЗДОРОВОГО, CЧАСТЛИВОГО И УСПЕШНОГО ОБЩЕСТВА
                </span>
            </div>
            <div class="row abline3">
                <div class="col-sm-offset-1 col-sm-4 hidden-xs abredline "></div>
                <div class="col-sm-2 col-xs-12 txtblock nopadding">
                    <span>наши цели</span>
                </div>
                <div class="col-sm-4 hidden-xs abredline nopadding"></div>
            </div>
            <div class="row abline4">
                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                    <div class="row">
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock1.png" class="img-responsive center-block" alt="">
                            <span>здоровье</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock2.png" class="img-responsive center-block" alt="">
                            <span>ЛЮБОВЬ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock3.png" class="img-responsive center-block" alt="">
                            <span>СЕМЬЯ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock4.png" class="img-responsive center-block" alt="">
                            <span>СИЛА</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock5.png" class="img-responsive center-block" alt="">
                            <span>ДЕТИ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock6.png" class="img-responsive center-block" alt="">
                            <span>РАЗВИТИЕ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock7.png" class="img-responsive center-block" alt="">
                            <span>СЧАСТЬЕ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock8.png" class="img-responsive center-block" alt="">
                            <span>ответственность</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock9.png" class="img-responsive center-block" alt="">
                            <span>УСПЕХ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock10.png" class="img-responsive center-block" alt="">
                            <span>ПОМОЩЬ ОБЩЕСТВУ</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockblue">
                            <img src="assets/img/about-icoblock11.png" class="img-responsive center-block" alt="">
                            <span>образование</span>
                        </div>
                        <div class="col-sm-2 col-xs-6 abicoblock abicoblockred">
                            <img src="assets/img/about-icoblock12.png" class="img-responsive center-block" alt="">
                            <span>спорт</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>

	    <div class="container-fluid nopadding">
		    <?include_once("inc/map.inc.php");?>
	    </div>
	    <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
		    <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
		    <div class="blockinnermenu">
			    <div class="row">
				    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
					    <?include_once("inc/subscribeform.inc.php");?>
				    </div>
			    </div>
		    </div>
	    </nav>
	    <div class="container-fluid nopadding" id="mainblockfooter">
		    <?include_once("inc/footer-new.inc.php");?>
		    <?include_once("inc/hide-popups.inc.php");?>
	    </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>


	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}

	$(document).ready(function() {


		$('.fancy').fancybox({
			padding: 0,
			margin: 0
		});

		window.onload = function() {
			$('.b9').on('click','.item',function() {
				$('.b9 .item').removeClass('slick-current');
				$(this).addClass('slick-current');
				initMap($(this).find('.adr').html());
			});
			$('.b9 .item:first-of-type').click();
		};

        $('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });

    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>