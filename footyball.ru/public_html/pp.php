<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт - Путь победителя</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-programms.css">
	<link rel="stylesheet" href="assets-landing/css/style.min.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">

        <div class="container">

            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">

                    <ul class="mySlideshow">
                        <li>
                            <img src="photo/banner/pp/1.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/pp1-logo.png" class="img-responsive center-block sliderlogo" alt="">
                        </li>
                    </ul>

                </div>
            </div>

            <div class="fpline2">
                <div class="rsptextline">
                    <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                        <p>
                            Программа рассчитана на обучение командному взаимодействию,что способствует воспитанию индивидуальной и командной ответственности!
                        </p>
                    </div>
                </div>
            </div>

            <div class="row ppline3">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        путь победителя это:<br>
                    </div>
                </div>
            </div>
            
        </div>


        <div class="container-fluid nopadding ppline4" >
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-xs-12">
                        <div class="pptextlinel0">
                            <div class="colr pull-right">
                                уникальных<br>
                                развивающих<br>
                                занятий
                            </div>
                            <div class="coll pull-right">80</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12"> </div>
                    <div class="col-sm-5 col-xs-12">
                        <div class="pptextliner0">
                            <div class="colr pull-right">
                                минутные<br>
                                тренировки
                            </div>
                            <div class="coll pull-right">60</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="rsptextline">
                        Ценности программы
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="fpline5">
                <div class="rsptextline">
                    <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                        <p>
                            Программа тренировок акцентирована на обучении профессиональным футбольным навыкам и включает тактические занятия в игровой форме.Это даст возможность полноценно погрузиться ребенку в захватывающий мир футбола.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row ppline6">
                <div class="col-sm-6 col-xs-12 imgblock imgblockl">
                    <img src="assets/img/pp-block1.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">новая форма с фамилией</span>
                </div>
                <div class="col-sm-6 col-xs-12 imgblock imgblockr">
                    <img src="assets/img/pp-block2.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">именной дневник</span>
                </div>
            </div>

            <div class="row ppline7">
                <div class="col-sm-offset-4 col-sm-4 hidden-xs line"></div>
            </div>

            <div class="row ppline8">
                <div class="col-sm-offset-3 col-sm-6 col-xs-12 imgblock imgblockl">
                    <img src="assets/img/pp-block3.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">9 индивидуальных аттестаций</span>
                </div>
            </div>

            <div class="row ppline9">
                <div class="col-xs-12">
                    <div class="buttblock">
                        <div class="buttout">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                                <div class="col-sm-4 col-xs-12 butout">
                                    <a href="#" class="butt toggle-menu menu-top">узнать подробности</a>
                                </div>
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            </div>
                        </div>
                        <div class="butline"></div>
                    </div>
                </div>
            </div>

            <div class="row fpline10">
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9019.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9019_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9028.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9028_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9036.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9036_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9042.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9042_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9044.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9044_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9047.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9047_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9080.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9080_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/pp/6K0F9092.jpg" class="big">
                        <img src="photo/gallery/pp/6K0F9092_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>




            </div>

            <div class="row ppline11">
            <div class="col-xs-12">
                <div class="buttblock">
                    <div class="buttout">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            <div class="col-sm-4 col-xs-12 butout">
                                <a href="#" class="butt toggle-menu menu-top">записаться на тренировку</a>
                            </div>
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                        </div>
                    </div>
                    <div class="butline"></div>
                </div>
            </div>
        </div>
        </div>


		    <div class="container-fluid nopadding">
			    <?include_once("inc/map.inc.php");?>
		    </div>
        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <div class="subscrform">
                            <?include_once("inc/subscribeform.inc.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
		    <div class="container-fluid nopadding" id="mainblockfooter">
			    <?include_once("inc/footer-new.inc.php");?>
			    <?include_once("inc/hide-popups.inc.php");?>
		    </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/simple-lightbox.min.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>
	function initMap(adress) {

		$("#map").html('');

		var myGeocoder = ymaps.geocode(adress);
		myGeocoder.then(
			function(res) {
				var pos = res.geoObjects.get(0).geometry.getCoordinates();
				var myMap = new ymaps.Map('map', {
					center: pos,
					zoom: 17
				});
				var bpos = {
					lat: pos[0],
					lng: pos[1]
				};
				var ppos = {
					lat: pos[0],
					lng: pos[1] -= 0.003
				};

				var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
					balloonContentHeader: '<p class="adr">' + adress + '</p>'
				},{
					iconLayout: 'default#image',
					iconImageHref: 'assets-landing/img/marker.png',
					iconImageSize: [28, 33]
				});
				setPlacemark();


				myMap.geoObjects.add(placemark);

				myMap.controls
					.add('zoomControl')
					.add('typeSelector')
					.add('mapTools');

				myMap.events.add('click', function(e) {
					myMap.balloon.close();
				});

				window.onresize = function() {
					setPlacemark();
				};

				function setPlacemark() {
					if (window.innerWidth < 740) {
						myMap.setCenter([bpos.lat,bpos.lng]);
					} else {
						myMap.setCenter([ppos.lat,ppos.lng]);
					}
				}

			}
		);

	}
    $(document).ready(function() {

        $('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });
        $(".fpline10 a").simpleLightbox();

	    $('.fancy').fancybox({
		    padding: 0,
		    margin: 0
	    });

	    window.onload = function() {
		    $('.b9').on('click','.item',function() {
			    $('.b9 .item').removeClass('slick-current');
			    $(this).addClass('slick-current');
			    initMap($(this).find('.adr').html());
		    });
		    $('.b9 .item:first-of-type').click();
	    };


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>