<?
include_once("lib/cms_view_inc.php");
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт - Footyball English club</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-programms.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">

        <div class="container">

            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">

                    <ul class="mySlideshow">
                        <li>
                            <img src="photo/banner/eng/1.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/eng-logo.png" class="img-responsive center-block sliderlogo" alt="">
                        </li>
                    </ul>

                </div>
            </div>

            <div class="fpline2">
                <div class="rsptextline">
                    <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                        <p>
                            Footyball English club - это специальная программа для изучения Английского языка для дошкольников от младшего до старшего возраста, основанная на игровой форме обучения: игры, песни, творческие задания.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row ppline3">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        Footyball English club Это:<br>
                    </div>
                </div>
            </div>

        </div>

        <div class="container-fluid nopadding ecline4" >
            <div class="container">
                <div class="rsptextline0">
                    <div class="colr pull-right">
                        занятий <br>
                        в группах <br>
                        до 6 детей <br>
                    </div>
                    <div class="coll pull-right">80</div>
                    <div class="clearfix"></div>
                </div>
                <div class="rsptextline">
                    Ценности программы
                </div>
            </div>
        </div>

        <div class="container">

            <div class="row ecline7">
                <div class="col-sm-6 col-xs-12 ">
                    <div class="imgblock imgblockl">
                        <img src="assets/img/ec-block1.jpg" class="img-responsive" alt="" style="margin-bottom: 20px">
                        <span class="imgblocktxt">
                            погружаем в мир английского<br>
                            через интересные игры,<br>
                            яркие картинки, весёлые задания<br>
                        </span>
                    </div>
                    <div class="imgblock imgblockl">
                        <img src="assets/img/ec-block2.jpg" class="img-responsive" alt="" style="margin-bottom: 10px">
                        <span class="imgblocktxt">
                            учим понимать английский, постепенно<br>
                            переходя каждое занятие<br>
                            только на английскую речь<br>
                        </span>
                    </div>
                    <div class="imgblock imgblockl">
                        <img src="assets/img/ec-block3.jpg" class="img-responsive" alt="" style="margin-bottom: 10px">
                        <span class="imgblocktxt" style="top:30%">
                            формируем запас<br>
                            английской лексики<br>
                        </span>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="imgblock imgblockr">
                        <img src="assets/img/ec-block4.jpg" class="img-responsive" alt="" style="margin-bottom: 20px; float: right">
                        <span class="imgblocktxt">
                            тренируем<br>
                            разговорный английский<br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="imgblock imgblockr">
                        <img src="assets/img/ec-block5.jpg" class="img-responsive" alt="" style=" margin-bottom: 10px;float: right">
                        <span class="imgblocktxt">
                            готовим к<br>
                            начальной школе<br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="imgblock imgblockr">
                        <img src="assets/img/ec-block6.jpg" class="img-responsive" alt="" style="margin-bottom: 10px;float: right">
                        <span class="imgblocktxt">
                            индивидуальный подход<br>
                            к каждому ребёнку<br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="row fpline9">
                <div class="col-xs-12">
                    <div class="buttblock">
                        <div class="buttout">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                                <div class="col-sm-4 col-xs-12 butout">
                                    <a href="#" class="butt toggle-menu menu-top">узнать подробности</a>
                                </div>
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            </div>
                        </div>
                        <div class="butline"></div>
                    </div>
                </div>
            </div>

            <div class="row rspline11">
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_170947.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_170947_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_172051.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_172051_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_173117.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_173117_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_173146.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_173146_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_173352.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_173352_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_173405.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_173405_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_173913.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_173913_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/ec/20170627_174157.jpg" class="big">
                        <img src="photo/gallery/ec/20170627_174157_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>

            </div>

            <div class="row fpline9">
                <div class="col-xs-12">
                    <div class="buttblock">
                        <div class="buttout">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                                <div class="col-sm-4 col-xs-12 butout">
                                    <a href="#" class="butt toggle-menu menu-top">записаться на тренировку</a>
                                </div>
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            </div>
                        </div>
                        <div class="butline"></div>
                    </div>
                </div>
            </div>
        </div>

        </div>


        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>


    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/simple-lightbox.min.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>

    $(document).ready(function() {

        $(".rspline11 a").simpleLightbox();
        $('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>
</body>
</html>