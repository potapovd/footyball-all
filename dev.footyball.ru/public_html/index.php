<?
include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(1);

$ranfeet = getrandomfeetback(0);
$fivevideo = getfivevideo();
$sliderphoto = getsliderphoto('index');

/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ВЫВОД СЧЕТЧИКА*/
function newfilevisit(){

    $newtimeforfile = gmdate('j.n.Y G:i',time());
    $saleforceresp = callsaleforce("getRSPVisits");
    //print_r($saleforceresp);
    $resultssaleforce = json_decode($saleforceresp, true);
    $resultssaleforce = $resultssaleforce['RSPVisits']*1;
    if($resultssaleforce>0){
        $cout = $resultssaleforce;
        $newstr = $newtimeforfile.";".$cout;
        file_put_contents("RSPVisits.txt", $newstr);
    }else{
        $cout = 82000;
    }
    return true;
}

$contfilename = "RSPVisits.txt";
$filesting = @file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
if($filesting===false){
    $cout = 82000;
    newfilevisit();
}
$nowdate = gmdate('j.n.Y G:i',time()); //текуща дата
$nowdate = strtotime($nowdate);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate+(60*60*4); // время файла +4ч
if($nowdate<$olddateplus24hour){
   //echo ("читаем файл");
    $cout = $filearray[1];
}else{
    //echo ("пишем в файл");

    if(!newfilevisit()){
        $cout = 82000;
    }

    
}



/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ДАТЫ СЛЕД. ТРЕНЕРОВКИ*/
function newfiledate(){

    $newtimeforfile = gmdate('j.n.Y G:i',time());
    $saleforceresp = callsaleforce("getRSPDate");
    //print_r($saleforceresp);
    $resultssaleforce = json_decode($saleforceresp, true);
    $newdate = $resultssaleforce['RSPDate'];

    $datestr = explode("/", $newdate);
    $datestrdd =  $datestr[0];
    //$datestrmm =  $datestr[1];
    $datestrdd = $datestrdd*1;

    if($datestrdd>0){
        $newstr = $newtimeforfile.";".$resultssaleforce['RSPDate'];
        file_put_contents("RSPDate.txt", $newstr);
    }else{
        $datestrdd =  gmdate('j',time()+(60*60*24));
        $datestrmm =  gmdate('n',time());
    }
    return true;
}
 $contfilename = "RSPDate.txt";
$filesting = @file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
if($filesting===false){

    $datestrdd =  gmdate('j',time()+(60*60*24));
    $datestrmm =  gmdate('n',time());
    newfiledate();

}
$nowdateshort = gmdate('j.n.Y G:i',time()); //текуща дата
$nowdate = strtotime($nowdateshort);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate+(60*60*6); // время файла +6ч


if($nowdate<$olddateplus24hour){
    //echo ("читаем файл");
    $datestr = $filearray[1];
    $datestrfull = explode("/", $datestr);
    $datestrdd =  $datestrfull[0];
    $datestrmm =  $datestrfull[1];
}else{
    //echo ("пишем в файл");

    if(!newfiledate()){
        $datestrdd =  gmdate('j',time()+(60*60*24));
        $datestrmm =  gmdate('n',time());
    }


}

?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$pageinfo[0]["metatitle"];?></title>
    <meta name="description" content="<?=$pageinfo[0]["metadescr"];?>">
    <meta name="keywords" content="<?=$pageinfo[0]["metakeyw"];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">
<!--                    <ul class="mySlideshow">
                        <li>
                            <span class="helper">
                                <img src="photo/banner/1.jpg"  alt="">
                                <div class="textbanner">Мы дарим детям сказку</div>
                            </span>
                        </li>
                    </ul>-->


                    <!-- 

                        child-talks.jpg - 31957ed53f507a816416187dd3a1dc7b
                        4-footy-site-2.jpg - 91b5b5d896ba32744b7d1146cd043811
                        IMG_3346.JPG - bc93f93dc530f193fe37249e7684b3f5
                        1.jpg - b55c7db8312da2bf0427af7ea8422e91

                    -->


                    <div class="demo">
                        <div class="GICarousel demo1 GI_C_wrapper">
                            <ul class="GI_IC_items mySlideshow">

                                <?foreach ($sliderphoto as $key=> $val){?>
                                    <li
                                        style="background-image:url('photo/banner/index/<?=$sliderphoto[$key]['name']?>.jpg')"
                                        class="slideshow<?=$sliderphoto[$key]['id']?>"
                                        data-mobsrc="photo/banner/index/<?=$sliderphoto[$key]['name']?>-mobile.jpg"

                                    >
                                        <a href="<?=$sliderphoto[$key]['link']?>">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"><?=$sliderphoto[$key]['text']?></div>
                                        </span>
                                        </a>
                                    </li>
                                <?}?>

                                
                                <!--<li style="background-image:url('photo/banner/index/31957ed53f507a816416187dd3a1dc7b.jpg')" class="slideshow4">
                                    <a href="one-post.php?id=6">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"></div>
                                        </span>
                                    </a>
                                </li>
                                <li style="background-image:url('photo/banner/index/91b5b5d896ba32744b7d1146cd043811.jpg')" class="slideshow3">
                                    <a href="https://www.instagram.com/p/BNPfAljDRrn/?taken-by=footyball_russia">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"></div>
                                        </span>
                                    </a>
                                </li>
                                <li style="background-image:url('photo/banner/index/bc93f93dc530f193fe37249e7684b3f5.jpg')" class="slideshow1">
                                    <a href="/video.php?cat=footytv&id=81">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"></div>
                                        </span>
                                    </a>
                                </li>
                                <li style="background-image:url('photo/banner/index/b55c7db8312da2bf0427af7ea8422e91.jpg')" class="slideshow2">
                                    <a href="rsp.php">
                                        <span class="helper">
                                            <div class="textbanner">Мы дарим детям сказку</div>
                                        </span>
                                    </a>
                                </li>-->
                            </ul>
                        </div>
                    </div>



                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <a href="http://rsp.footyball.ru/">
                        <div class="mainblocksbg" id="main-block2">
                            <div>
                                <span>абсолютно</span>
                                бесплатная незабываемая пробная тренировка
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="http://rsp.footyball.ru/">
                        <div class="mainblockspink">
                            <div class="textblock2"><span>детей</span></div>
                            <div class="textblock22">на бесплатных тренировках</div>
                            <div class="blockcounterall">
                                <div id="countloader"></div>
                                <div id="blockcountchnumb" class="nameblock">0</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="#" class="toggle-menu menu-top">
                        <div class="mainblockspink">
                            <div class="blockfreetranall">
                                <div class="textblock3"><span>выберите дату</span></div>
                                <div class="textblock22">бесплатной тренировки</div>
                                <div class="freetraindate">
                                    <div class="freetraindateday" id="freetraindateday"></div>
                                    <div class="freetraindatemonth"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <a href="sp.php">
                        <div class="mainblocksbg" id="main-block3">
                            <video id="main-block3-video" muted autoplay loop width='100%' height='100%' preload="auto" tabindex="0" poster="assets/img/main-block3-2.jpg">
                                <source src="video/index/sp.m4v" >
                                <source src="video/index/sp.webm" type="video/webm">
                                <source src="video/index/sp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>старт победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="fp.php">
                        <div class="mainblocksbg" id="main-block4">
                           <video id="main-block4-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block4-2.jpg">
                                <source src="video/index/fp.m4v" >
                                <source src="video/index/fp.webm" type="video/webm">
                                <source src="video/index/fp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>фундамент победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="pp.php">
                        <div class="mainblocksbg" id="main-block5">
                            <video id="main-block5-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block5-2.jpg">
                                <source src="video/index/pp.m4v" >
                                <source src="video/index/pp.webm" type="video/webm">
                                <source src="video/index/pp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>путь победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="profi.php">
                        <div class="mainblocksbg" id="main-block6">
                            <video id="main-block6-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block6-2.jpg">
                                <source src="video/index/profi.m4v" >
                                <source src="video/index/profi.webm" type="video/webm">
                                <source src="video/index/profi.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>PROFI</span></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="mainblocksblack">
            <div class="container containergallery">
                <div class="slider one-time">

                    <div class="multiple">
                        <a href="footytv.php?cat=all" class="videoblock">
                            <img alt="" class="img-responsive" src="assets/img/footytvindex2.png">
                        </a>
                    </div>
                    <?foreach($fivevideo as $key => $val){?>
                    <div class="multiple">
                        <a href="video.php?cat=footytv&id=<?=$fivevideo[$key]["id"];?>" class="videoblock">
                            <img alt="" class="img-responsive" src="//footyball.ru/photo/tv/<?=$fivevideo[$key]["videourl"];?>.jpg">
                            <span class="buttonplay"></span>
                        </a>
                        <div class="videoname"><?=$fivevideo[$key]["name"];?></div>
                    </div>
                    <?}?>



                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin-bottom: 15px">

                <div class="col-sm-6 col-xs-12">
                    <div class="mainblocksbg" id="main-block8">

                        <div class="feedbackblockinner">
                            <a href="feedback.php">
                                <div class="nameblock">
                                    <div class="feedbackphoto">
                                        <img class="feetback img-circle img-responsive" alt="<?=$ranfeet[0]["name"];?>" src="/photo/feedback/<?=$ranfeet[0]['photo'];?>">
                                    </div>
                                    <div class="feedbackname">
                                        <?=$ranfeet[0]["name"];?> <br><span>о нашем клубе </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="170">
                                <div class="dynamic-wrap">
                                    <?=$ranfeet[0]["content"];?>
                                </div>

                                <div class="align-center">
                                    <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="...">...</a>
                                </div>
                            </div>
                        </div>
                       <!-- <div class="txtgrad"></div>-->
                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="mainblocksbg" id="main-block7"></div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <a href="http://instagram.com/footyball_russia">
                                <div class="mainblocksbg" id="main-block9"></div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="mainblocksblue" id="main-block10">
                                <div class="blockparslider">
                                    <div class="partnblocklogo">
                                        <a href="http://www.lanta.ru" target="_blank">
                                            <img src="photo/partner/1.png" alt="Инвестиционный партнер">
                                        </a>
                                    </div>
                                    <div class="partnblocklogo">
                                        <a href="http://starttrack.ru" target="_blank">
                                            <img src="photo/partner/2.png" alt="Финансовый партнер">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>

    <script src="assets/js/jQuery.GI.Carousel.min.js"></script>
    <script src="assets/js/jquery.dynamicmaxheight.min.js"></script>
    <script src="assets/js/CounterCircleSlider.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/slick.min.js"></script>

    <script src="assets/js/cab-functions.js"></script>
    <script src="assets/js/allpages-functions.js"></script>

<script>

    /*INDEX FUNCIONS*/
    //resize blocks
    function resizeAllBlocks(){
        blockResize("main-block2");
        blockResize("main-block3");
        blockResize("main-block4");
        blockResize("main-block5");
        blockResize("main-block6");
        blockResize("main-block9");
        blockResize("main-block7");
        blockResize("main-block10");
    }
    function blockResize(idblock) {
        if (idblock=="main-block10"){
            $(window).load(function () {
                $("#main-block10").css("height",Math.ceil($("#main-block7").height()-$("#main-block9").height()-10));
            });
        }else{
            var div = $('#'+idblock);
            var url = div.css('background-image').replace(/url\((['"])?(.*?)\1\)/gi, '$2');
            var img = new Image();
            img.src = url;
            //console.log('img:', img.width + 'x' + img.height); // zero, image not yet loaded
            //console.log('div:', div.width() + 'x' + div.height());
            img.onload = function() {
                div.css("height",Math.ceil(div.width()*(img.height/img.width)));
            }
        }
        return true;
    }
    //counter function
    function timerStartCounter() {
        var counterUp = $("#blockcountchnumb");
        var newpeole = parseInt(<?=$cout?>);
        if(isNaN(newpeole) ){
            var basetime = new Date("23 April 2015 10:00");
            var basepeoleonbasetime = 10000;
            var currenttime = new Date();
            var peoleperhour = 2 ;
            newpeole = Math.round((((currenttime - basetime) / 1000 / 60 / 60) * peoleperhour) + basepeoleonbasetime);
        }
        var cl = new CanvasLoader('countloader');
        cl.setColor('#ff0078'); // default is '#000000'
        cl.setShape('square'); // default is 'oval'
        cl.setDiameter(110); // default is 40
        cl.setDensity(131); // default is 40
        cl.setRange(0.6); // default is 1.3
        cl.setFPS(46); // default is 24
        cl.show(); // Hidden by default

        counterUp.counter({
            autoStart: false,
            duration: 7000,
            countTo: newpeole,
            //countTo: 1000,
            placeholder: "...",
            easing: "easeOutCubic",
            onComplete: function() {
                cl.hide();
                $('#blockcountchnumb').animate({width: 85},100).animate({fontSize: 34},2000);
            }
        });
        counterUp.counter("start");

    }
    //next training
    function freeTraining(vardate){
        var counterUp = $("#freetraindateday");
        counterUp.counter({
            autoStart: false,
            duration: 2000,
            countFrom: 1,
            countTo: vardate,
            //countTo: 1000,
            placeholder: "...",
            easing: "easeOutCubic"
        });
        counterUp.counter("start");
    }


    $(document).ready(function() {

        /*ALL PAGES*/

        /*INDEX PADGE*/
        $('.one-time').slick({
            //dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            lazyLoad: 'progressive',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            //touchMove: false
        });
        //next trainign
        var monthNames = ["", "января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"];
        $(".freetraindateday").html(<?=$datestrdd?>);
        $(".freetraindatemonth").html( monthNames[<?=$datestrmm?>] );
        $('.blockparslider').sss({
            showNav : true,
            transition : 200,
            speed : 5000
        });
        resizeAllBlocks();
        setTimeout(timerStartCounter, 500);
        setTimeout(freeTraining(24), 500);
        $('.dynamic-max-height').dynamicMaxHeight(
            { trigger : '.dynamic-show-more'}
        );
        //carusel
        $('.demo1').GICarousel({
            arrows:true,
            swipeSensibility: 50,
            animationSpeed: 100,
            pagination:true,
            keyboardNavigation:true,
            responsive: false
        });


        /*

child-talks.jpg - 31957ed53f507a816416187dd3a1dc7b
                        4-footy-site-2.jpg - 91b5b5d896ba32744b7d1146cd043811
                        IMG_3346.JPG - bc93f93dc530f193fe37249e7684b3f5
                        1.jpg - b55c7db8312da2bf0427af7ea8422e91
        */

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $("#main-block3-video, #main-block4-video, #main-block5-video, #main-block6-video").css("display","none");
            $(".demo, .demo .helper").css("height","380px");
            $(".mySlideshow li").css("background-size","contain");

            var mobsrc;
            $(".mySlideshow li").each(function(){
                mobsrc =  $( this ).attr("data-mobsrc");
                url =  $( this ).attr("class");
                $(this).css("backgroundImage","url("+mobsrc+")");
            })


            /*$(".slideshow1").css("background-image","url(photo/banner/index/bc93f93dc530f193fe37249e7684b3f5-mobile.jpg)");
            $(".slideshow2").css("background-image","url(photo/banner/index/b55c7db8312da2bf0427af7ea8422e91-mobile.jpg)");
            $(".slideshow3").css("background-image","url(photo/banner/index/91b5b5d896ba32744b7d1146cd043811-mobile.jpg)");
            $(".slideshow4").css("background-image","url(photo/banner/index/31957ed53f507a816416187dd3a1dc7b-mobile.jpg)");*/
        }

    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>
</body>
</html>
