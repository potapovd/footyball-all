<?
include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(5);
$videos = getvideos(0);
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$pageinfo[0]["metatitle"];?></title>
    <meta name="description" content="<?=$pageinfo[0]["metadescr"];?>">
    <meta name="keywords" content="<?=$pageinfo[0]["metakeyw"];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-footytv.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <!--<div class="container-fluid" id="redline"></div>-->
        <div class="container">
            <div class="row" id="videofilters">
                <a href="#" class="filter categ" data-filter="all">все</a>
                <a href="#" class="filter categ" data-filter=".video-daidejst">Видео-дайджест</a>
                <a href="#" class="filter categ" data-filter=".perovo">Перово</a>
                <a href="#" class="filter categ" data-filter=".park-pobedy">Парк Победы</a>
                <a href="#" class="filter categ" data-filter=".savelovskaya">Савеловская</a>
                <a href="#" class="filter categ" data-filter=".futy-cafe">Футикафе</a>
                <a href="#" class="filter categ" data-filter=".leninskaya">Ленинская</a>
                <a href="#" class="filter categ" data-filter=".proekty">Проекты</a>
                <div class="clearfix"></div>
            </div>
            <div class="row" id="allvideos">

                <?foreach($videos as $key => $val){?>


                <div class="col-sm-6 col-sx-12 mix <?=$videos[$key]["unicname"];?>">
                    <div class="footytvblock">
                        <div class="nameblock"><?=$videos[$key]["videoname"];?></div>
                        <div class="catanddate"><span><?=$videos[$key]["date"];?></span> | <span><?=$videos[$key]["name"];?></span></div>
                        <a class="videoblock" href="video.php?cat=footytv&id=<?=$videos[$key]["videoid"];?>">
                           <!-- <img src="photo/footytv/photo1.png" class=" img-responsive" alt="">-->
                            <img src="//footyball.ru/photo/tv/<?=$videos[$key]["videourl"];?>.jpg" alt="<?=$videos[$key]["videoname"];?>" class="img-responsive">
                            <span class="buttonplay"></span>
                        </a>
                        <div class="textblock">
                            <p>

                                <?
                                $b = mb_substr($videos[$key]["desription"], 0, 600);
                                if (mb_strlen($videos[$key]["desription"]) > 600) {
                                    $b .= '...';
                                }
                                echo $videos[$key]["desription"];
                                //echo $b;
                                ?>

                            </p>
                        </div>
                    </div>
                </div>

                <?} ?>


               

            </div>
            
        </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>


    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/jquery.mixitup.min.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>



    $(document).ready(function() {


        $('.toggle-menu').jPushMenu({
            closeOnClickLink: false
        });

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var cat = getUrlParameter('cat');
        $('#allvideos').mixItUp();
        if(cat.length>0){
            if(cat!=="all"){
                $('#allvideos').mixItUp('filter', '.'+cat);
            }
        }


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>
