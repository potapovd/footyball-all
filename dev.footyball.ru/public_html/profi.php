<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт - Профи</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-programms.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">

        <div class="container">

            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">

                    <ul class="mySlideshow">
                        <li>
                            <img src="photo/banner/profi/1.jpg"  alt="" class="sliderimg">
                            <img src="assets/img/profi-logo.png" class="img-responsive center-block sliderlogo" alt="">
                        </li>
                    </ul>

                </div>
            </div>

            <div class="row proline3">
                <div class="col-xs-12">
                    <div class="rsptextline">
                        СТАРТ ПРОФЕССИОНАЛЬНОЙ КАРЬЕРЫ PROFI<br>
                    </div>
                </div>
            </div>
            
        </div>


        <div class="container-fluid nopadding proline4" >
            <div class="container">
                <div class="row">
                    <div class="rsptextline0">
                        <div class="colr pull-right">
                            профи тренировки,<br>
                            ежемесячные турниры
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="rsptextline">
                        УСПЕЙ ВОСПИТАТЬ СЫНА ПОБЕДИТЕЛЕМ!
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="fpline5">
                <div class="rsptextline">
                    <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                        <p>
                            Лучшие ученики, обладающие талантом и необходимыми потенциалом, чтобы сделать профессиональную карьеру в футболе, рекомендованные главным тренером на данную программу занимаются в одной команде. Состав групп определяет главный тренер футбольного клуба «FootyBall», опираясь на рекомендации старших тренеров.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row proline6">
                <div class="col-sm-6 col-xs-12 imgblock imgblockl">
                    <img src="assets/img/pro-block1.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">номерная форма</span>
                </div>
                <div class="col-sm-6 col-xs-12 imgblock imgblockr">
                    <img src="assets/img/pro-block2.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">дополнительное занятие к ФП</span>
                </div>
            </div>

            <div class="row proline7">
                <div class="col-sm-offset-4 col-sm-4 hidden-xs line"></div>
            </div>

            <div class="row proline8">
                <div class="col-sm-offset-3 col-sm-6 col-xs-12 imgblock imgblockr">
                    <img src="assets/img/pro-block3.jpg" class="img-responsive" alt="" >
                    <span class="imgblocktxt">именной дневник достижений</span>
                </div>
            </div>

            <div class="row proline9">
                <div class="col-xs-12">
                    <div class="buttblock">
                        <div class="buttout">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                                <div class="col-sm-4 col-xs-12 butout">
                                    <a href="#" class="butt toggle-menu menu-top">узнать подробности</a>
                                </div>
                                <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            </div>
                        </div>
                        <div class="butline"></div>
                    </div>
                </div>
            </div>

            <div class="row fpline10">

                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/71.jpg" class="big">
                        <img src="photo/gallery/profi/71_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/73.jpg" class="big">
                        <img src="photo/gallery/profi/73_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/74.jpg" class="big">
                        <img src="photo/gallery/profi/74_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/75.jpg" class="big">
                        <img src="photo/gallery/profi/75_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/77.jpg" class="big">
                        <img src="photo/gallery/profi/77_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/78.jpg" class="big">
                        <img src="photo/gallery/profi/78_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/80.jpg" class="big">
                        <img src="photo/gallery/profi/80_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="photo/gallery/profi/85.jpg" class="big">
                        <img src="photo/gallery/profi/85_s.jpg" class="img-responsive" alt="" >
                    </a>
                </div>






            </div>

            <div class="row proline11">
            <div class="col-xs-12">
                <div class="buttblock">
                    <div class="buttout">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                            <div class="col-sm-4 col-xs-12 butout">
                                <a href="#" class="butt toggle-menu menu-top">записаться на тренировку</a>
                            </div>
                            <div class="col-sm-4 col-xs-12 hidden-xs buttline"></div>
                        </div>
                    </div>
                    <div class="butline"></div>
                </div>
            </div>
        </div>
        </div>

        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <div class="subscrform">
                            <?include_once("inc/subscribeform.inc.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/simple-lightbox.min.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/allpages-functions.js"></script>


<script>

    $(document).ready(function() {

        $('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });

        $(".fpline10 a").simpleLightbox();

    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>