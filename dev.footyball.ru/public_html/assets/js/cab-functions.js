/*CABINET FUNCTIONS*/
//view messages
function viewmMsg(msg,idform){
    var txtblock;
    if(idform == "reg"){
        txtblock = $("#errortxtregform span");
    }else if(idform == "res"){
        txtblock = $("#errortxtresform span");
    }else if(idform == "sub"){
        txtblock = $("#errortxtloginform span");
    }
    $(txtblock).stop(true,true).fadeOut().hide().html("");
    $(txtblock).html(msg);
    $(txtblock).fadeIn("slow").delay(9000).queue(function () {
        $(this).fadeOut();
        $(".infoblockin").stop(true,true).html("").removeClass("errortxt").removeClass("noerrortxt");
        $(this).dequeue();
    });
    return true;
}
//send data
function funsubmformsend(str) {
    if($("a.buttonsend").hasClass("buttlocked")){
        //обработка запроса
        return false;
    }else{
        var act,phone;
        if(str=="reg"){
            act ="reg";
            phone = $(".phonereginp").eq(1).val().trim();
            if(phone.length!==17){
                viewmMsg("Ошибка! Неправильный телефон","reg");
                return false
            }else{
                if( !/[\+7][\(]\d{3}[\)][\ ]\d{3}[\ ]\d{2}[\ ]\d{2}/.test(phone) ){
                    viewmMsg("Ошибка! Неправильный телефон");
                    return false
                }else{
                    viewmMsg("Выполняется проверка данных...","reg");
                }
            }
        }else if(str=="res"){
            act="rec";
            phone = $(".phonerecovinp").eq(1).val().trim();
            if(phone.length!==17){
                viewmMsg("Ошибка! Неправильный телефон","res");
                return false
            }else{
                if( !/[\+7][\(]\d{3}[\)][\ ]\d{3}[\ ]\d{2}[\ ]\d{2}/.test(phone) ){
                    viewmMsg("Ошибка! Неправильный телефон","res");
                    return false
                }else{
                    viewmMsg("Выполняется проверка данных...","res");
                }
            }
        }else{
            return false
        }
        $.ajax({
            statusCode : { 404: function(){alert('Not Found');} },
            url: 'cabinet/cabinet.php',
            type: 'post',
            data: {'action':act,'phone':phone},
            dataType:'JSON',
            crossDomain: false,
            timeout: 10000,
            cache:false,
            beforeSend:  function(res){
                $("a.buttonsend").addClass("buttlocked");
            },success: function(res){
                var response;
                //response = res.answertxt.trim();
                response = res.answertxt;
                if(str=="reg"){
                    viewmMsg(response,"reg");
                }else if(str=="res"){
                    viewmMsg(response,"res");
                }

                if(res.errorflag==0){
                    setTimeout(function() {$('.vex-dialog-button-secondary').trigger('click');}, 3000);
                    setTimeout(function() {
                        $('.line12').trigger('click');
                        $('.subscrform #phonecab').val(phone);
                    }, 3500);
                }
                $("a.buttonsend").removeClass("buttlocked");
            },error: function(error){
                if(str=="reg"){
                    viewmMsg("Произошла ошибка сервера! Повторите попытку позже","reg");
                }else if(str=="res"){
                    viewmMsg("Произошла ошибка сервера! Повторите попытку позже","res");
                }
                console.log("Error:");
                console.log(error);
                $("a.buttonsend").removeClass("buttlocked");
            }
        });
        return false;
    }
}
//open windows
function openrecovandregwindow(str){
    vex.defaultOptions.className = 'vex-theme-plain vex-custom';
    vex.defaultOptions.showCloseButton = true;
    vex.defaultOptions.closeClassName = "iconmenuclose";
    //vex.dialog.buttons.YES.text = 'Отправить';
    vex.dialog.buttons.YES.text = '';
    vex.dialog.buttons.NO.text = 'Отмена';
    vex.dialog.open({
        input: [str].join(''),
        message: "",
        buttons: [
            $.extend({}, vex.dialog.buttons.NO, { text: 'Отмена' })
        ],
        callback: function (data) {
            if (!data) {
                //return console.log('Cancelled')
            }else{

            }
        }
    });
    return true
}

$(document).ready(function() {
    /*CABINET*/
    $('#phonerecov, #phonereg, #phonecab').mask('+7(999) 999 99 99');
    $("#rerist").on("click",function(e){
        var str = $("#popregistration").html();
        openrecovandregwindow(str);
        return false
    });

    $("#lostpass").on("click",function(e){
        var str = $("#poprecovery").html();
        openrecovandregwindow(str);
        return false
    });

    $("#cabsubm").on("click",function(e){

        var phone = $("#phonecab").val();
        var pass = $("#passcab").val();
        var response="";

        viewmMsg("Выполняется проверка данных...","sub");
        setTimeout(function() {
            if(phone.length!==17){
                viewmMsg("Ошибка! Неправильный телефон","sub");
                return false;
            }else{
                if( !/[\+7][\(]\d{3}[\)][\ ]\d{3}[\ ]\d{2}[\ ]\d{2}/.test(phone) ){
                    viewmMsg("Ошибка! Неправильный телефон","sub");
                    return false;
                }
            }
            if(pass.length<7){
                viewmMsg("Ошибка! Неправильный пароль","sub");
                return false;
            }else{
                if( !/[A-Za-z0-9]/.test(pass) ){
                    viewmMsg("Ошибка! Неправильный пароль","sub");
                    return false;
                }
            }
            $.ajax({
                statusCode : { 404: function(){alert('Not Found');} },
                url: 'cabinet/cabinet.php',
                type: 'post',
                data: {'action':'sub','phone':phone,'pass':pass},
                dataType: 'JSON',
                crossDomain: false,
                timeout: 10000,
                cache:false,
                beforeSend:  function(res){
                    viewmMsg("Выполняется проверка данных...","sub");
                },
                success: function(res){
                    //alert(res);
                    if (res.errorflag==0) {
                        window.location.href = res.redirect.trim();
                    }else{
                        response = res.answertxt.trim();
                        viewmMsg(response,"sub");
                        return false;
                    }
                },error: function(error){
                    viewmMsg("Произошла ошибка сервера! Повторите попытку позже","sub");
                    console.log("Error:");
                    console.log(error);
                }
            });

        }, 2000);
        return false
    });
});