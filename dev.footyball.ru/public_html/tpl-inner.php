<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Footyball - Официальный сайт -</title>
    <meta name="description" content="Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">


    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row">
                <div class=" col-sm-offset-1 col-sm-10 col-sx-12">




                </div>
            </div>
        </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/allpages-functions.js"></script>

<script>


    function showViewPortSize(display) {
        if(display) {
            var height = jQuery(window).height();
            var width = jQuery(window).width();
            jQuery('body').prepend('<a href="log.html" id="viewportsize" style="z-index:9999;position:fixed;bottom:10px;right:5px;color:#fff;background:#000;padding:3px;text-align:center; font-size: 10px;opacity:0.5;display:block;">'+width+' x '+height+'<br>0.1.4</a>');
            jQuery(window).resize(function() {
                height = jQuery(window).height();
                width = jQuery(window).width();
                $('#viewportsize').html(width+' x '+height+'');
                //$(".irs-addtext").css("left",$(".irs-slider").css("left"))
            });
            //$(".col-menu,.col-widgets .widgetsinner").css("height",$(".col-conten").outerHeight()+32 );
        }
    }


    $(document).ready(function() {

        //setTimeout(showViewPortSize(1), 5000);

    })
</script>

</body>
</html>
