<?
include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(4);

$ranfeet = getrandomfeetback(1);
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$pageinfo[0]["metatitle"];?></title>
    <meta name="description" content="<?=$pageinfo[0]["metadescr"];?>">
    <meta name="keywords" content="<?=$pageinfo[0]["metakeyw"];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-video.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row">

                <?
                foreach($ranfeet as $key => $type)  {
                if($type["type"]==0){
                ?>
                <div class="col-sm-4 col-sx-12">
                    <div class="feedbackblock">
                        <div class="feedbackblockinner">
                            <div class="nameblock">
                                <div class="feedbackphoto">
                                    <img class="feetback img-circle img-responsive" alt="<?=$type["name"];?>" src="/photo/feedback/<?=$type["photo"];?>">
                                </div>
                                <div class="feedbackname">
                                    <?=$type["name"];?> <br><span>о нашем клубе </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="200">

                                    <!--<div class="dynamic-wrap">
                                        <p> <?/*=$type["content"];*/?></p>
                                    </div>
                                    <div class="align-center">
                                        <div class="align-center">
                                            <a class="dynamic-show-more" href="javascript:void(0);" title="Read more" data-replace-text="Show less">Read more</a>
                                        </div>
                                    </div>-->

                                    <div class="dynamic-wrap">
                                        <?=$type["content"];?>
                                    </div>
                                    <div class="align-center">
                                        <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="Закрыть...">...</a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?}else{?>
                <div class="col-sm-4 col-sx-12">
                    <div class="feedbackblock">
                        <div class="nameblock">
                            <div class="feedbackphoto">
                                <img class="feetback img-circle img-responsive" alt="<?=$type["name"];?>" src="/video/feedback/<?=$type["photo"];?>">
                            </div>
                            <div class="feedbackname">
                                <?=$type["name"];?> <br> <span>о нашем клубе</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="feedbacktxt">
                            <a class="feedbackvideo" href="video.php?cat=feedback&id=<?=$type["id"];?>">
                                <img class="feetback  img-responsive" alt="" src="assets/img/feedback-video.png">
                            </a>

                        </div>
                    </div>
                </div>
                <?}}?>

               

            </div>
            
        </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/jquery.dynamicmaxheight.min.js"></script>
    <script src="assets/js/allpages-functions.js"></script>

<script>


    $(document).ready(function() {

        $('.dynamic-max-height').dynamicMaxHeight(
            { trigger : '.dynamic-show-more'}
        );

        $(".feetback").hover(function(){
            $(this).attr("src", "assets/img/feedback-video-2.png");
        }, function(){
            $(this).attr("src", "assets/img/feedback-video.png");
        });


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>