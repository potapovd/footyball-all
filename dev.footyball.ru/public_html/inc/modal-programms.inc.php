<div class="modal-wrapper">
    <div class="modal-window modal-window-programm js-blur" id="modalwindowprogramm" style="width:940px; height:620px">
        <button class="close-modal">X</button>
        
        <div role="tabpanel">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active" id="programm-rabvsinepob">
                    <a href="#rabvsinepob" aria-controls="rabvsinepob" role="tab" data-toggle="tab"></a>
                </li>
                <li role="presentation" id="programm-strtpob">
                    <a href="#strtpob" aria-controls="strtpob" role="tab" data-toggle="tab"></a>
                </li>
                <li role="presentation" id="programm-fundpob">
                    <a href="#fundpob" aria-controls="fundpob" role="tab" data-toggle="tab"></a>
                </li>
                <li role="presentation" id="programm-putpob">
                    <a href="#putpob" aria-controls="putpob" role="tab" data-toggle="tab"></a>
                </li>
                <li role="presentation" id="programm-futprofi">
                    <a href="#futprofi" aria-controls="futprofi" role="tab" data-toggle="tab"></a>
                </li>
            </ul>

            <div class="tab-content">
                
                <div role="tabpanel" class="tab-pane active" id="rabvsinepob">
                    <div class="rabvsinepobtopbg"></div>
                    <div class="rabvsinepobconent">
                        <div class="rabvsinepobbg">
                            <div class="str1">разбуди в сыне победителя</div>
                            <div class="str2-1"><span class="otvoz">от</span><span class="otnumlet">2</span><span class="otletlet">лет</span></div>
                            <div class="str2-2"><span class="otvoz">до</span><span class="otnumlet">6.5</span><span class="otletlet">лет</span></div>
                            <div class="str3 str3-1">уникальная методика</div>
                            <div class="str3 str3-2">знакомство <br> с спортом</div>
                            <div class="str3 str3-3">приветливые тренеры</div>
                            <div class="str3 str3-4">професиональное обрудование</div>
                            <div class="str3 str3-5">современная <br> школа</div>
                            <div class="str3 str3-6">абсолютно бесплатно</div>
                        </div>
                        <div class="str4">
                            Для посещения занятия «Разбуди в сыне победителя» необходимо: 
                        </div>
                        <div class="str5ul">
                            <div class="str5-1">
                                <div class="str5-1-numb pull-left">1</div>
                                <div class="str5-1-txt pull-left">
                                    <a href="#" id="linktofreetrainig" data-modawindowid="modalwindowform">Заполнить заявку</a> на бесплатную тренировку, чтобы наши операторы помогли подобрать удобную для вас дату и время.
                                </div>
                                <div class="clearfix"></div>
                            </div> 
                               <div class="str5-2">
                                <div class="str5-2-numb pull-left">2</div>
                                <div class="str5-2-txt pull-left">
                                    Принести с собой на тренировку удобную спортивную обувь для ребенка. Форму мы выдадим сами.
                                </div>
                                <div class="clearfix"></div>
                                   <div class="str6">Успей воспитать сына победителем!</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="strtpob">
                    <div class="strtpobtopbg"></div>
                    <div class="strtpobconent">
                        <div class="strtpobbg">
                            <div class="str1"> Старт победителя </div>
                            <div class="str2-1"><span class="otvoz">от</span><span class="otnumlet">2</span><span class="otletlet">лет</span></div>
                            <div class="str2-2"><span class="otvoz">до</span><span class="otnumlet">6.5</span><span class="otletlet">лет</span></div>
                            <div class="str3 str3-2">футбольные <br> тренировки</div>
                            <div class="str3 str3-3">мини-сессии <br> английского</div>
                            <div class="str3 str3-4">книга <br> Успеть до 7 лет!</div>
                            <div class="str3 str3-5">уникальная <br> форма</div>
                        </div>
                        <div class="str4">
                            Программа проводится на следующей неделе после первого пробного занятия. <br>
                            Она является подготовительной к программе «Фундамент Победителя». <br>
                            Позволяет ребенку адаптироваться к школе, тренеру, окружающей его атмосфере.
                        </div>
                        <div class="str6">Успей воспитать сына победителем!</div>
                    </div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="fundpob">
                    <div class="fundpobtopbg"></div>
                    <div class="fundpobconent">
                        <div class="fundpobbg">
                            <div class="str1">фундамент победителя</div>
                            <div class="str2 no-svg blockvideo">
                                <video width="640" height="360" poster="assets/video/programm-fundpob-poster.jpg" id="programvideo" controls="controls" preload="none">
                                    <source src="assets/video/programm-fundpob.webm" type="video/webm">
                                    <source src="assets/video/programm-fundpob.mp4" type="video/mp4">
                                </video>
                            </div>
                            <div class="str3 str3-1">два <br> тренера </div>
                            <div class="str3 str3-2">система <br> занятий</div>
                            <div class="str3 str3-2-2">x80</div>
                            <div class="str3 str3-3">игровые сeccuи<br> английского</div>
                            <div class="str3 str3-3-2">x80</div>
                            <div class="str3 str3-4">дневник <br> чемпиона</div>
                            <div class="str3 str3-5">уникальная <br> форма</div>
                            <div class="str3 str3-6">дни рождения <br> вместе</div>
                        </div>
                        <div class="str4">
                            Целая система уникальных комплексных занятий для раннего развития ребенка, разработанная по лучшим европейским методикам. В программу входят восемь тренировок по футболу в месяц на каждое из таких качеств, как: дисциплина, целеустремленность, командное взаимодействие, смелость, внимательность, развитие лидерских качеств, координация, быстрота, гибкость.
                        </div>
                        <div class="str6">Успей воспитать сына победителем!</div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="putpob">
                    <div class="putpobtopbg"></div>
                    <div class="putpobconent">
                        <div class="putpobbg">
                            <div class="str1">путь победителя</div>
                            <!--<div class="str2 no-svg blockvideo">
                                <video width="560" height="315" poster="assets/video/programm-fundpob-poster.jpg" id="programvideo" controls="controls" preload="none">
                                    <source src="assets/video/programm-fundpob.webm" type="video/webm">
                                    <source src="assets/video/programm-fundpob.mp4" type="video/mp4">
                                </video>
                            </div>-->
                            <div class="str3 str3-1">два <br> тренера </div>
                            <div class="str3 str3-2">система <br> занятий</div>
                            <div class="str3 str3-2-2">x72</div>
                            <div class="str3 str3-3">длительность<br> тренировки</div>
                            <div class="str3 str3-3-2">60</div>
                            <div class="str3 str3-4">именной  <br> дневник</div>
                            <div class="str3 str3-5">форма с <br> фамилией</div>
                            <div class="str3 str3-6">командное <br> взаимодействие</div>
                        </div>
                        <div class="str4" style="text-align: left ">
                            <ul>
                                <li> Программа тренировок акцентирована на обучении профессиональным футбольным навыкам!</li>
                                <li> К участию в программе допускаются только дети прошедшие «фундамент победителя»!Поэтому ваш сын тренируется с подготовленными партнерами!</li>
                                <li>Программа включает в себя обучение детей футбольному интеллекту: Выполнение заданий позволяет ребенку научиться быстро думать и принимать решения!</li>
                                <li>Программа включает тактические занятия в игровой форме.Это даст возможность полноценно погрузиться ребенку в захватывающий мир футбола!</li>
                                <li>Тренировки программы способствуют воспитанию и проявлению лидерских качеств у футболиста!</li>
                                <li>Программа включает большое количество соревновательных тренировок,благодаря этому вырабатывается стрессоустойчивость и навык конкуренции!</li>
                                <li>Программа рассчитана на обучение командному взаимодействию,что способствует воспитанию индивидуальной и командной ответственности!</li>



                            </ul>
                        </div>
                        <div class="str6">Успей воспитать сына победителем!</div>
                    </div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="futprofi">
            
                    <div class="futprofitopbg"></div>
                    <div class="futproficonent">
                        <div class="futprofitbg">
                            <div class="str1">СТАРТ ПРОФЕССИОНАЛЬНОЙ КАРЬЕРЫ <span>profi</span></div>
                            <div class="str2-1"><span class="otvoz">от</span><span class="otnumlet">2</span><span class="otletlet">лет</span></div>
                            <div class="str2-2"><span class="otvoz">до</span><span class="otnumlet">6.5</span><span class="otletlet">лет</span></div>
                            <div class="str3 str3-1">профи <br> тренировки</div>
                            <div class="str3 str3-2">сразу <br>  два тренера</div>
                            <div class="str3 str3-2-2">x2</div>
                            <div class="str3 str3-3">дополнительное  занятие к ФП</div>
                            
                            <div class="str3 str3-4">именной дневник достижений</div>
                            <div class="str3 str3-5">номерная  <br>  форма</div>
                            <div class="str3 str3-6">ежемесячные  турниры</div>
                        </div>
                        <div class="str4">
                            Лучшие ученики, обладающие талантом и необходимыми потенциалом, чтобы сделать профессиональную карьеру в футболе, рекомендованные главным тренером на данную программу занимаются в одной команде. Состав групп определяет главный тренер футбольного клуба «FootyBall», опираясь на рекомендации старших тренеров.
                        </div>   
                        <div class="str6">Успей воспитать сына победителем!</div>
                    </div>
                </div>

            </div>
                
        </div>
    </div>
 </div>