<div id="topmenu">

    <div class="topmenudesctop">
        <div class="container">
            <div class="row line1">
                <div class="col-sm-10">
                    <div class="line11">футбольный клуб для дошкольников №1</div>
                </div>
                <div class="col-sm-2">
                    <?if($_SERVER["SCRIPT_NAME"]=="/cabinet/index.php"){?>
                        <a href="#" class="line12" id="cabinetlogout">
                            выход <i class="fa fa-sign-out" aria-hidden="true"></i>
                        </a>
                    <?}else{?>
                        <?if(  isset($_GET["cab"])  ){?>
                        <a href="#" class="line12 toggle-menu menu-right pull-right toggle-menuheader">
                            личный кабинет <i class="headercabinet"></i>
                        </a>
                        <?}?>
                    <?}?>
                </div>
            </div>
            <div class="row line2">
                <div class="col-sm-1">&nbsp;</div>
                <div class="col-sm-3">
                    <a href="#mainblockfooter" class="line21">
                        адреса клубов
                    </a>
                </div>
                <div class="col-sm-4 line22">
                    <a href="index.php">
                        <img alt="FootyBall logo" onerror="this.onerror=null; this.src='/assets/img/logo.png'" class="img-responsive" src="/assets/img/logo.svg" width="150" height="74" id="topmenulogo">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#" class="line23 toggle-menu menu-top">
                        запишись бесплатно</br>
                        <span>8-800-707-88-99</span>
                    </a>
                </div>
                <div class="col-sm-1">&nbsp;</div>
            </div>
        </div>
        <div class="redline"></div>
        <div class="container-fluid" id="mandectopmaenu">
            <div class="row">


                <div class="nav">
                    <ul>
                        <!--<li class="homemainmenu"><a class="active" href="index.php">Главная</a></li>-->
                        <li class="homemainmenu"><a href="index.php">Главная</a></li>
                        <li class="aboutmainmenu"><a href="about.php">О нас</a></li>
                        <li class="programmsmainmenu"><a href="#">Программы <i class="fa fa-angle-down"></i></a>
                            <ul>
                                <li><a href="rsp.php"><span>Разбуди победителя</span></a></li>
                                <li><a href="sp.php"><span>Старт победителя</span></a></li>
                                <li><a href="fp.php"><span>Фундамент победителя</span></a></li>
                                <li><a href="pp.php"><span>Путь победителя</span></a></li>
                                <li><a href="profi.php"><span>Профи</span></a></li>
                                <li><a href="star.php"><span>Звезда</span></a></li>
                                <li><a href="eng-club.php"><span>English club</span></a></li>
                            </ul>
                        </li>
                        <li class="feedbackmainmainmenu"><a href="feedback.php">Отзывы</a></li>
                        <li class="footytvmainmenu"><a href="#">FootyTV <i class="fa fa-angle-down"></i></a>
                            <ul>
                                <li><a href="footytv.php?cat=all"><span>Вce</span></a></li>
                                <li><a href="footytv.php?cat=video-daidejst"><span>Видео-дайджест</span></a></li>
                                <li><a href="footytv.php?cat=perovo"><span>Перово</span></a></li>
                                <li><a href="footytv.php?cat=park-pobedy"><span>Парк Победы</span></a></li>
                                <li><a href="footytv.php?cat=belorusskaya"><span>Белорусская</span></a></li>
                                <li><a href="footytv.php?cat=futy-cafe"><span>Футикафе</span></a></li>
                                <li><a href="footytv.php?cat=leninskaya"><span>Ленинская</span></a></li>
                                <li><a href="footytv.php?cat=proekty"><span>Проекты</span></a></li>
                            </ul>
                        </li>
                        <li class="blogmainmenu"><a href="#">Блог <i class="fa fa-angle-down"></i></a>
                            <ul>
                                <li><a href="blog.php?cat=all"><span>Все</span></a></li>
                                <li><a href="blog.php?cat=statiii"><span>Статьи</span></a></li>
                                <li><a href="blog.php?cat=novosti-footyball"><span>Новости FootyBall</span></a></li>
                                <li><a href="blog.php?cat=aktsii"><span>Акции</span></a></li>
                            </ul>
                        </li>
                        <li><a href="invest.php"><span>Инвестиции</span></a></li>
                        <!--<li class="footycafemainmainmenu"><a href="#">Footycafe</a></li>-->
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <div class="topmenumobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 nopadding" >
                    <button class="toggle-menu menu-left pull-left toggle-menuheader">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="clearfix"></div>
                    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
                        <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
                        <ul class="metismenu blockinnermenu" id="menu">
                            <li><a href="index.php"><span>Главная</span></a></li>
                            <li><a href="about.php"><span>О нас</span></a></li>
                            <li>
                                <a href="#" aria-expanded="false"><span class="upmenu">Программы<i class="fa arrow"></i></span></a>
                                <ul aria-expanded="false">
                                    <li><a href="rsp.php"><span>Разбуди победителя</span></a></li>
                                    <li><a href="sp.php"><span>Старт победителя</span></a></li>
                                    <li><a href="fp.php"><span>Фундамент победителя</span></a></li>
                                    <li><a href="pp.php"><span>Путь победителя</span></a></li>
                                    <li><a href="profi.php"><span>Проффи</span></a></li>
                                    <li><a href="star.php"><span>Звезда</span></a></li>
                                    <li><a href="eng-club.php"><span>English club</span></a></li>
                                </ul>
                            </li>
                            <li><a href="feedback.php"><span>Отзывы</span></a></li>
                            <li>
                                <a href="#" aria-expanded="false"><span class="upmenu">FootyTV<i class="fa arrow"></i></span></a>
                                <ul aria-expanded="false">
                                    <li><a href="footytv.php?cat=all"><span>Вce</span></a></li>
                                    <li><a href="footytv.php?cat=video-daidejst"><span>Видео-дайджест</span></a></li>
                                    <li><a href="footytv.php?cat=perovo"><span>Перово</span></a></li>
                                    <li><a href="footytv.php?cat=park-pobedy"><span>Парк Победы</span></a></li>
                                    <li><a href="footytv.php?cat=belorusskaya"><span>Белорусская</span></a></li>
                                    <li><a href="footytv.php?cat=futy-cafe"><span>Футикафе</span></a></li>
                                    <li><a href="footytv.php?cat=leninskaya"><span>Ленинская</span></a></li>
                                    <li><a href="footytv.php?cat=proekty"><span>Проекты</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" aria-expanded="false"><span class="upmenu">Блог<i class="fa arrow"></i></span></a>
                                <ul aria-expanded="false">
                                    <li><a href="blog.php?cat=all"><span>Все</span></a></li>
                                    <li><a href="blog.php?cat=statiii"><span>Статьи</span></a></li>
                                    <li><a href="blog.php?cat=novosti-footyball"><span>Новости FootyBall</span></a></li>
                                    <li><a href="blog.php?cat=aktsii"><span>Акции</span></a></li>
                                </ul>
                            </li>
                            <li><a href="invest.php"><span>Инвестиции</span></a></li>
                        </ul>
		                    <div class="footsocblock">
			                    <a target="_blank" id="footsoclinkfb" class="footsoclink fa" href="//www.facebook.com/footyballru">
			                    </a>
			                    <a target="_blank" id="footsoclinkin" class="footsoclink fa" href="//instagram.com/footyball_russia"></a>
			                    <a target="_blank" id="footsoclinkyt" class="footsoclink fa" href="//www.youtube.com/channel/UCL8tDsCfvfDWxG8WEiDDrDA"></a>
			                    <a target="_blank" id="footsoclinkvk" class="footsoclink fa" href="//vk.com/footyballru"></a>
		                    </div>
                    </nav>

                    <div class="headerlefttxt">
                        <span>футбольный клуб для дошкольников</span>
                    </div>
                </div>
                <div class="col-xs-6 nopadding">
                    <a href="index.php">
                        <img alt="FootyBall logo" onerror="this.onerror=null; this.src='/assets/img/logo.png'" class="img-responsive" src="/assets/img/logo.svg" width="150" height="74" id="topmenulogomob">
                    </a>
                </div>
                <div class="col-xs-3 nopadding" >
                    <?if(  isset($_GET["cabinetaccess"]) && !empty($_GET["cabinetaccess"])  ){?>
                    <button class="toggle-menu menu-right pull-right toggle-menuheader">
                        <i class="headercabinet"></i>
                    </button>
                    <?}?>
                    <div class="clearfix"></div>
                    <div class="headerrighttxt">
                        <span>call-центр <span>8-800-707-88-99</span> (звонок бесплатный)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
        <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
        <div class="blockinnermenu col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
            <div class="subscrform">
                <h2>Личный кабинет</h2>
                <div id="errortxtloginform"><span></span></div>
                <!--<form action="cabinet/index.php" method="POST">-->

                <form action="#" method="POST">
                    <input class="namechildinp" type="tel" placeholder="Телефон" required="required" id="phonecab" name="phonecab" pattern="^[\+][7][(][0-9]{3}[)][\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}" maxlength="17" size="17" inputmode="numeric">
                    <!--<input class="nameparentinp" type="password" placeholder="Пароль" id="passcab" name="passcab" required="required" maxlength="8" size="8" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">-->
                    <input class="nameparentinp" type="password" placeholder="Пароль" id="passcab" name="passcab" required="required" maxlength="25">
                    <input class="buttonsend" type="submit" id="cabsubm" value="Войти" >
                </form>

                <div class="linkblock">
                    <a href="#" id="rerist" class="rerist">Зарегистрироваться</a>
                    <a href="#" id="lostpass" class="lostpass">Напомнить пароль</a>
                </div>
            </div>
        </div>
    </nav>
    <?//if($_SERVER["SCRIPT_NAME"]!=="version2/cabinet/index.php"){?>
    <?if($_SERVER["SCRIPT_NAME"]!=="/cabinet/index.php"){?>

    <div id="allwarnings">
        <div id="popregistration">
            <div class="row warningfreezing">
                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                    <div class="warhead">Регистрация личного кабинета</div>
                    <div class="warline1">
                        Пароль будет выслан в смс на номер телефона
                    </div>
                    <div id="errortxtregform"><span></span></div>
                    <div class="warline1 formregorrec">
                        <input class="namechildinp phonereginp" name="phonereg" id="phonereg" type="tel" placeholder="Телефон" pattern="^[\+][7][(][0-9]{3}[)][\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}" required="required" maxlength="17" size="17">
                        <a href="#" class="buttonsend" onclick="funsubmformsend('reg')">Отправить</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="poprecovery">
            <div class="row warningfreezing">
                <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                    <div class="warhead">Восстановление пароля</div>
                    <div class="warline1">
                        Пароль будет выслан в смс на номер телефона
                    </div>
                    <div id="errortxtresform"><span></span></div>
                    <div class="warline1 formregorrec">
                        <input class="namechildinp phonerecovinp" name="phoneresov" id="phonerecov" type="tel" placeholder="Телефон" pattern="^[\+][7][(][0-9]{3}[)][\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}" required="required" maxlength="17" size="17">
                        <a href="#" class="buttonsend" onclick="funsubmformsend('res')">Отправить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?}?>

</div>