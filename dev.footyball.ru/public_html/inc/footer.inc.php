<div class="container desctfooter">
     <div class="row">
        <div class="col-sm-12">
            <span class="namemaps">Адреса клубов</span>
        </div>
    </div>
    <div class="row">
        <!--<div class="col-xs-6 col-sm-3 nopadding">
            <div class="mapout">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2249.8552431805792!2d37.43861218320034!3d55.674117349895255!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x63e9cf9fc167b361!2z0J7Rh9Cw0LrQvtCy0L4!5e0!3m2!1sru!2sua!4v1473359554062" frameborder="0"></iframe>
                </div>
                <div class="maptxt">
                    <span>Адрес центрального офиса:</span>
                    <br>
                    Очаково ул. Б. Очаковская, 47 А,
                    <br>
                    г. Москва
                    <br>
                    Пн-Пт 10:00-18:00
                </div>
            </div>
        </div>-->
        <div class="col-xs-6 col-sm-3 nopadding">
            <div class="mapout">
                <div class="map">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d280.68023331684464!2d37.772959!3d55.750826!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414acaa351f2331f%3A0xccc02244985c4906!2zRm9vdHliYWxsINCf0LXRgNC-0LLQvg!5e0!3m2!1suk!2sus!4v1505860488943?hl=ru-RU" frameborder="0"></iframe>
                </div>
                <div class="maptxt">
                    <span>Футбольный клуб "Перово":</span>
                    <br>
                    1-й проезд Перова Поля, д.9, стр. 5,
                    <br>
                    г.Москва
                    <br>
                    Пн-Вс 8:00-22:00
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 nopadding">
            <div class="mapout">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d280.74572789826453!2d37.5231655!3d55.7417228!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x15a94a005cb6bfe8!2z0KTQo9Ci0JjQkdCe0Js!5e0!3m2!1suk!2sus!4v1505860596293?hl=ru-RU" frameborder="0"></iframe>
                </div>
                <div class="maptxt">
                    <span>Футбольный клуб "Парк победы":</span>
                    <br>
                    Кутузовский просп., 36, стр. 51,
                    <br>
                    г.Москва
                    <br>
                    Пн-Вс 8:00-22:00
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 nopadding">
            <div class="mapout">
                <div class="map">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2243.231786769775!2d37.582767!3d55.789213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x610be68e44e5935e!2zRm9vdHliYWxsINCR0LXQu9C-0YDRg9GB0YHQutC40Lk!5e0!3m2!1suk!2sus!4v1505860634589?hl=ru-RU" frameborder="0"></iframe>
                </div>
                <div class="maptxt">
                    <span>Футбольный клуб "Белорусский":</span>
                    <br>
                    Правды, д. 24, стр. 8, Заезд с ул. 5-ого Ямского поля. Через КПП №5". Мы находимся возле "Бодрый день" и кафе "Рецептор"
                    <br>
                    г.Москва
                    <br>
                    Пн-Вс 8:00-22:00
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 nopadding lastblock">
            <div class="mapout">
                <div class="map">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0286946851465!2d37.59668895125075!3d55.705872980445335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54b66fc70758f%3A0x747bd65c42127c40!2zRm9vdHliYWxsINCb0LXQvdC40L3RgdC60LjQuQ!5e0!3m2!1suk!2sus!4v1505860657953?hl=ru-RU" frameborder="0"></iframe>
                </div>
                <div class="maptxt">
                    <span>Футбольный клуб "Ленинский":</span>
                    <br>
                    5-й Донской проезд 15, стр. 5, 11 подъезд
                    <br>
                    г. Москва
                    <br>
                    Пн-Вс 8:00-22:00
                </div>
            </div>
        </div>
    </div>
</div>
<div class="redline"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div class="footercopyright">
                © 2017 Footyball
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="/" class="footerlogo">
                <img src="/assets/img/logo-footer.png" width="50" alt="">
            </a>
        </div>
        <div class="col-sm-3 col-xs-12 socblock">
            <div class="footsocblock">
                <a target="_blank" id="footsoclinkfb" class="footsoclink fa" href="//www.facebook.com/footyballru">
                </a>
                <a target="_blank" id="footsoclinkin" class="footsoclink fa" href="//instagram.com/footyball_russia"></a>
                <a target="_blank" id="footsoclinkyt" class="footsoclink fa" href="//www.youtube.com/channel/UCL8tDsCfvfDWxG8WEiDDrDA"></a>
                <a target="_blank" id="footsoclinkvk" class="footsoclink fa" href="//vk.com/footyballru"></a>
            </div>
        </div>
    </div>

</div>

<div id="mobilefooter">
    <div class="container">
        <div class="row" id="addpanelmob">
            <div class="col-xs-12">
                <ul>
                    <li><a href="https://goo.gl/maps/DYU9Az7jRCk">Футбольный клуб "Перово"</a></li>
                    <li><a href="https://goo.gl/maps/ggtTmCMPEGF2">Футбольный клуб "Парк победы"</a></li>
                    <li><a href="https://goo.gl/maps/M94KpzcXUHM2">Футбольный клуб "Белорусский"</a></li>
                    <li><a href="https://goo.gl/maps/7JF2cF1V6AC2">Футбольный клуб "Ленинский"</a></li>
                    <li><a href="//goo.gl/maps/xy4sSdrcNoH2">Центральный офис</a></li>
                </ul>
            </div>
        </div>
        <div class="row" id="mainpanelmob">
            <div class="col-xs-6 nopadding">
                <a class="mobilnk" href="tel:88007078899">
                    <i class="fa fa-phone buttoncall"></i> Позвонить
                </a>
            </div>
            <div class="col-xs-6 nopadding" id="mapmobout">
                <a class="mobilnk" href="#addpanelmob" id="mapmob">
                    <i class="fa fa-map-marker buttoncall"></i> Доехать
                </a>
            </div>
        </div>
    </div>
</div>