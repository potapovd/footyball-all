<div class="modal-wrapper">
    <div class="modal-window js-blur" id="modalwindowphoto">
        <button class="close-modal">X</button>
        <h2>ФОТО ЧЕМПИОНОВ</h2>
        <div class="photoform">
            <div class="slider fadeslider">
                <?foreach($gallery as $key => $val){?>
                <div>
                    <div class="image">
                        <img src="photo/index/<?=$gallery[$key]["name"];?>.jpg" alt="Фото <?=$gallery[$key]["id"];?>">
                    </div>
                </div>
                <?}?>
            </div>
        </div>
    </div>
</div>