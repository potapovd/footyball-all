<?
include_once("lib/cms_view_inc.php");
$pageinfo = getpageinfo(1);

$ranfeet = getrandomfeetback(0);
$fivevideo = getfivevideo();


/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ВЫВОД СЧЕТЧИКА*/
$contfilename = "RSPVisits.txt";
$filesting = file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
$nowdate = gmdate('j.n.Y G:i',time()); //текуща дата
$nowdate = strtotime($nowdate);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate+(60*60*4); // время файла +4ч

if($nowdate<$olddateplus24hour){
   //echo ("читаем файл");
    $cout = $filearray[1];
}else{
    //echo ("пишем в файл");
    $newtimeforfile = gmdate('j.n.Y G:i',time());

    $urlsf ="https://login.salesforce.com/services/oauth2/token";
    $saleforceresp = callsaleforce($urlsf,"getRSPVisits");
    //echo "<pre>";
    //print_r($saleforceresp);
    //echo "</pre>";
    $resultssaleforce = json_decode($saleforceresp, true);    
    $resultssaleforce = $resultssaleforce['RSPVisits']*1;
    if($resultssaleforce>0){
        $cout = $resultssaleforce;
        $newstr = $newtimeforfile.";".$cout;
    	file_put_contents($contfilename, $newstr);
    }else{
        $cout = 52000;
    }
    
}
//$cout = 51195;


/* ПОЛУЧЕНИЕ КЕШИРОВАНИЕ И ДАТЫ СЛЕД. ТРЕНЕРОВКИ*/
$contfilename = "RSPDate.txt";
$filesting = file_get_contents($contfilename, FILE_USE_INCLUDE_PATH);
$nowdateshort = gmdate('j.n.Y G:i',time()); //текуща дата
$nowdate = strtotime($nowdateshort);
$filearray = explode(";", $filesting);
$olddate = strtotime($filearray[0]); // время файла
$olddateplus24hour = $olddate+(60*60*6); // время файла +6ч


if($nowdate<$olddateplus24hour){
    //echo ("читаем файл");
    $datestr = $filearray[1];
    $datestrfull = explode("/", $datestr);
    $datestrdd =  $datestrfull[0];
    $datestrmm =  $datestrfull[1];
}else{
    //echo ("пишем в файл");
    $newtimeforfile = gmdate('j.n.Y G:i',time());

    $urlsf ="https://login.salesforce.com/services/oauth2/token";
    $saleforceresp = callsaleforce($urlsf,"getRSPDate");
    /*echo "<pre>";
    print_r($saleforceresp);
    print_r($nowdate);
    echo "</pre>";*/

    $resultssaleforce = json_decode($saleforceresp, true);
    $datestr = explode("/", $resultssaleforce['RSPDate']);
    $datestrdd =  $datestr[0];
    $datestrmm =  $datestr[1];
    $datestrdd = $datestrdd*1;

    if($datestrdd>0){
        $newstr = $newtimeforfile.";".$resultssaleforce['RSPDate'];
        file_put_contents($contfilename, $newstr);
    }else{
    	$datestrdd =  gmdate('j',time()+(60*60*24));
    	$datestrmm =  gmdate('n',time());
    }

}

?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$pageinfo[0]["metatitle"];?></title>
    <meta name="description" content="<?=$pageinfo[0]["metadescr"];?>">
    <meta name="keywords" content="<?=$pageinfo[0]["metakeyw"];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">
<!--                    <ul class="mySlideshow">
                        <li>
                            <span class="helper">
                                <img src="photo/banner/1.jpg"  alt="">
                                <div class="textbanner">Мы дарим детям сказку</div>
                            </span>
                        </li>
                    </ul>-->


                    <div class="demo">
                        <div class="GICarousel demo1 GI_C_wrapper">
                            <ul class="GI_IC_items mySlideshow">
                                <li style="background-image:url('photo/banner/index/4-footy-site-2.jpg')" class="slideshow3">
                                    <a href="https://www.instagram.com/p/BNPfAljDRrn/?taken-by=footyball_russia">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"></div>
                                        </span>
                                    </a>
                                </li>
                                <li style="background-image:url('photo/banner/index/IMG_3346.JPG')" class="slideshow1">
                                    <a href="/video.php?cat=footytv&id=81">
                                        <span class="helper" style="width: 100%; height: 540px; display: block;">
                                            <div class="textbanner"></div>
                                        </span>
                                    </a>
                                </li>
                                <li style="background-image:url('photo/banner/index/1.jpg')" class="slideshow2">
                                    <a href="rsp.php">
                                        <span class="helper">
                                            <div class="textbanner">Мы дарим детям сказку</div>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>



                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <a href="rsp.php">
                        <div class="mainblocksbg" id="main-block2">
                            <div>
                                <span>абсолютно</span>
                                бесплатная незабываемая пробная тренировка
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="rsp.php">
                        <div class="mainblockspink">
                            <div class="textblock2"><span>детей</span></div>
                            <div class="textblock22">на бесплатных тренировках</div>
                            <div class="blockcounterall">
                                <div id="countloader"></div>
                                <div id="blockcountchnumb" class="nameblock">0</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <a href="#" class="toggle-menu menu-top">
                        <div class="mainblockspink">
                            <div class="blockfreetranall">
                                <div class="textblock3"><span>выберите дату</span></div>
                                <div class="textblock22">бесплатной тренировки</div>
                                <div class="freetraindate">
                                    <div class="freetraindateday" id="freetraindateday"></div>
                                    <div class="freetraindatemonth"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <a href="sp.php">
                        <div class="mainblocksbg" id="main-block3">
                            <video id="main-block3-video" muted autoplay loop width='100%' height='100%' preload="auto" tabindex="0" poster="assets/img/main-block3-2.jpg">
                                <source src="video/index/sp.m4v" >
                                <source src="video/index/sp.webm" type="video/webm">
                                <source src="video/index/sp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>старт победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="fp.php">
                        <div class="mainblocksbg" id="main-block4">
                           <video id="main-block4-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block4-2.jpg">
                                <source src="video/index/fp.m4v" >
                                <source src="video/index/fp.webm" type="video/webm">
                                <source src="video/index/fp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>фундамент победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="pp.php">
                        <div class="mainblocksbg" id="main-block5">
                            <video id="main-block5-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block5-2.jpg">
                                <source src="video/index/pp.m4v" >
                                <source src="video/index/pp.webm" type="video/webm">
                                <source src="video/index/pp.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>путь победителя</span></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a href="profi.php">
                        <div class="mainblocksbg" id="main-block6">
                            <video id="main-block6-video" muted autoplay loop width='100%' height='100%' preload="none" tabindex="0" poster="assets/img/main-block6-2.jpg">
                                <source src="video/index/profi.m4v" >
                                <source src="video/index/profi.webm" type="video/webm">
                                <source src="video/index/profi.mp4" type="video/mp4">
                            </video>
                            <div class="nametextblockab3biz6"><span>PROFI</span></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="mainblocksblack">
            <div class="container containergallery">
                <div class="slider one-time">

                    <div class="multiple">
                        <a href="footytv.php?cat=all" class="videoblock">
                            <img alt="" class="img-responsive" src="assets/img/footytvindex2.png">
                        </a>
                    </div>
                    <?foreach($fivevideo as $key => $val){?>
                    <div class="multiple">
                        <a href="video.php?cat=footytv&id=<?=$fivevideo[$key]["id"];?>" class="videoblock">
                            <img alt="" class="img-responsive" src="//footyball.ru/photo/tv/<?=$fivevideo[$key]["videourl"];?>.jpg">
                            <span class="buttonplay"></span>
                        </a>
                        <div class="videoname"><?=$fivevideo[$key]["name"];?></div>
                    </div>
                    <?}?>



                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin-bottom: 15px">

                <div class="col-sm-6 col-xs-12">
                    <div class="mainblocksbg" id="main-block8">

                        <div class="feedbackblockinner">
                            <a href="feedback.php">
                                <div class="nameblock">
                                    <div class="feedbackphoto">
                                        <img class="feetback img-circle img-responsive" alt="<?=$ranfeet[0]["name"];?>" src="/photo/feedback/<?=$ranfeet[0]['photo'];?>">
                                    </div>
                                    <div class="feedbackname">
                                        <?=$ranfeet[0]["name"];?> <br><span>о нашем клубе </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <div class="feedbacktxt dynamic-max-height" data-maxheight="170">
                                <div class="dynamic-wrap">
                                    <?=$ranfeet[0]["content"];?>
                                </div>

                                <div class="align-center">
                                    <a class="dynamic-show-more" href="javascript:void(0);" title="..." data-replace-text="...">...</a>
                                </div>
                            </div>
                        </div>
                       <!-- <div class="txtgrad"></div>-->
                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="mainblocksbg" id="main-block7"></div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <a href="http://instagram.com/footyball_russia">
                                <div class="mainblocksbg" id="main-block9"></div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="mainblocksblue" id="main-block10">
                                <div class="blockparslider">
                                    <div class="partnblocklogo">
                                        <a href="http://www.lanta.ru" target="_blank">
                                            <img src="photo/partner/1.png" alt="Инвестиционный партнер">
                                        </a>
                                    </div>
                                    <div class="partnblocklogo">
                                        <a href="http://starttrack.ru" target="_blank">
                                            <img src="photo/partner/2.png" alt="Финансовый партнер">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
            <i class="fa fa-close iconmenuclose cbp-spmenuclose"></i>
            <div class="blockinnermenu">
                <div class="row">
                    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-6 col-xs-10">
                        <?include_once("inc/subscribeform.inc.php");?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.sticky.js"></script>
    <script src="assets/js/jPushMenu.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>


    <script src="assets/js/jQuery.GI.Carousel.min.js"></script>

    <script src="assets/js/jquery.mask.min.js"></script>
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <script src="assets/js/jquery.dynamicmaxheight.min.js"></script>
    <script src="assets/js/CounterCircleSlider.js"></script>
    <script src="assets/js/jquery.edslider.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/mediaelement-and-player.min.js"></script>
    <script src="assets/js/vex.combined.min.js"></script>


<script>
    function resizeAllBlocks(){
        blockResize("main-block2");
        blockResize("main-block3");
        blockResize("main-block4");
        blockResize("main-block5");
        blockResize("main-block6");
        blockResize("main-block9");
        blockResize("main-block7");
        blockResize("main-block10");
    }

    function blockResize(idblock) {

        if (idblock=="main-block10"){
            $(window).load(function () {
                $("#main-block10").css("height",Math.ceil($("#main-block7").height()-$("#main-block9").height()-10));
            });
        }else{
            //idblockvar div = $('#main-block1');
            var div = $('#'+idblock);
            //var url = div.css('background-image').replace(/^url\(\'?(.*)\'?\)$/, '$1');
            var url = div.css('background-image').replace(/url\((['"])?(.*?)\1\)/gi, '$2');
            var img = new Image();
            img.src = url;
            //console.log('img:', img.width + 'x' + img.height); // zero, image not yet loaded
            //console.log('div:', div.width() + 'x' + div.height());
            img.onload = function() {
                //console.log(img.width/img.height);
                //console.log(Math.ceil(div.width()*(img.height/img.width)));
                div.css("height",Math.ceil(div.width()*(img.height/img.width)));
                //console.log('img:', img.width + 'x' + img.height, (img.width/div.width()));
            }
        }
        return true;
    }

    /*counter function*/
    function timerStartCounter() {
        var counterUp = $("#blockcountchnumb");
        /*var basetime = new Date("23 April 2015 10:00");
        var basepeoleonbasetime = 7632;
        var currenttime = new Date();
        var peoleperhour = 0.85 ;
        var newpeole = Math.round((((currenttime - basetime) / 1000 / 60 / 60) * peoleperhour) + basepeoleonbasetime);*/
        var newpeole = <?=$cout?>;

        //alert(newpeole);

        var cl = new CanvasLoader('countloader');
        cl.setColor('#ff0078'); // default is '#000000'
        cl.setShape('square'); // default is 'oval'
        cl.setDiameter(110); // default is 40
        cl.setDensity(131); // default is 40
        cl.setRange(0.6); // default is 1.3
        cl.setFPS(46); // default is 24
        cl.show(); // Hidden by default

        counterUp.counter({
            autoStart: false,
            duration: 7000,
            countTo: newpeole,
            //countTo: 1000,
            placeholder: "...",
            easing: "easeOutCubic",
            onComplete: function() {
                cl.hide();
                $('#blockcountchnumb').animate({width: 85},100).animate({fontSize: 34},2000);
            }
        });
        counterUp.counter("start");

    }

    function freeTraining(vardate){
        var counterUp = $("#freetraindateday");
        counterUp.counter({
            autoStart: false,
            duration: 2000,
            countFrom: 1,
            countTo: vardate,
            //countTo: 1000,
            placeholder: "...",
            easing: "easeOutCubic"
        });
        counterUp.counter("start");
    }



    function showViewPortSize(display) {
        if(display) {
            var height = jQuery(window).height();
            var width = jQuery(window).width();
            jQuery('body').prepend('<a href="log.html" id="viewportsize" style="z-index:9999;position:fixed;bottom:10px;right:5px;color:#fff;background:#000;padding:3px;text-align:center; font-size: 10px;opacity:0.5;display:block;">'+width+' x '+height+'<br>0.1.5</a>');
            jQuery(window).resize(function() {
                height = jQuery(window).height();
                width = jQuery(window).width();
                $('#viewportsize').html(width+' x '+height+'');
                //$(".irs-addtext").css("left",$(".irs-slider").css("left"))
            });
            //$(".col-menu,.col-widgets .widgetsinner").css("height",$(".col-conten").outerHeight()+32 );
        }
    }




    $(document).ready(function() {




        /*TOP MENU*/
        $("#topmenu").sticky({
            topSpacing: 0,
            zIndex:200,
            center:true,
            responsiveWidth:true
        });
        $('#topmenu').on('sticky-start', function() {
            $('#topmenulogo,#topmenulogomob').attr("src","assets/img/logo-small.svg");
            $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='assets/img/logo-small.png'");
        });
        $('#topmenu').on('sticky-end', function() {
            $('#topmenulogo,#topmenulogomob').attr("src","assets/img/logo.svg");
            $('#topmenulogo,#topmenulogomob').attr("onerror","this.onerror=null; this.src='assets/img/logo.png'");
            $("#topmenu").sticky({
                topSpacing: 0,
                zIndex:200,
                center:true,
                responsiveWidth:true
            });
        });
        var lastScrollTop = 0;
        $(window).scroll(function(event){
            var st = $(this).scrollTop();
            if (st > lastScrollTop){
                var st2 = st - lastScrollTop;
                if (st2>10){
                    $("#mandectopmaenu").slideUp();
                }
                //console.log("down");
            } else {
                $("#mandectopmaenu").slideDown();
                //console.log("up");
            }
            lastScrollTop = st;
        });
        $('.toggle-menu').jPushMenu({
            closeOnClickLink: false
        });
        $("#rerist").on("click",function(e){
            var str = $("#popregistration").html();

            vex.defaultOptions.className = 'vex-theme-plain vex-custom';
            vex.defaultOptions.showCloseButton = true;
            vex.defaultOptions.closeClassName = "iconmenuclose";
            vex.dialog.buttons.YES.text = 'Отправить';
            vex.dialog.buttons.NO.text = 'Отмена';
            vex.dialog.open({
                input: str,
                message: "",
                callback: function (data) {
                    if (data === false) {
                       // $('#id-js-candlestickunlim').lcs_cancel(status);
                    }else{
                        console.log("Запрос на регистрацию");
                    }
                }
            });
            return false
        });
        $("#lostpass").on("click",function(e){
            var str = $("#poprecovery").html();

            vex.defaultOptions.className = 'vex-theme-plain vex-custom';
            vex.defaultOptions.showCloseButton = true;
            vex.defaultOptions.closeClassName = "iconmenuclose";
            vex.dialog.buttons.YES.text = 'Отправить';
            vex.dialog.buttons.NO.text = 'Отмена';
            vex.dialog.open({
                input: str,
                message: "",
                callback: function (data) {
                    if (data === false) {
                       // $('#id-js-candlestickunlim').lcs_cancel(status);
                    }else{
                        console.log("Запрос на восстановление");
                    }
                }
            });
            return false
        });


        $('.one-time').slick({
            //dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            lazyLoad: 'progressive',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            //touchMove: false
        });


        //next trainign
        //var today = new Date();
        //var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
        var monthNames = ["", "января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"];
        //$(".freetraindateday").html(tomorrow.getDate());
        //$(".freetraindatemonth").html(monthNames[tomorrow.getMonth()]);
        //
        $(".freetraindateday").html(<?=$datestrdd?>);
        $(".freetraindatemonth").html( monthNames[<?=$datestrmm?>] );



        $('.blockparslider').sss({
            showNav : true,
            transition : 200,
            speed : 5000
        });

        /*$('.mySlideshow').edslider({
            width : '100%',
            height: 500
        });*/
        //$(".demo").css("height","380");
        //$(".slideshow1").css("background-image","url(photo/banner/index/IMG_3346-mobile.jpg)");
        /*$('.demo1').GICarousel({
            arrows:true,
            pagination:true,
            keyboardNavigation:true,
            responsive: false
        });*/







        /*var video = document.getElementById("main-block3-video");
        video.addEventListener( "canplaythrough", function() {
            this.play();
        });
        function startVideoIfNotStarted () {
            $("#main-block3-video").ready(function () {
                window.setTimeout(function(){
                    //videojs("main-block3-video").play()
                    $('#main-block3-video').mediaelementplayer({
                        alwaysShowControls: false ,
                        features: [],
                        startVolume: 0.0
                    });
                }, 1000);
            });
        }
        $(startVideoIfNotStarted);*/

       /*$('#main-block3-video, #main-block4-video, #main-block5-video, #main-block6-video').mediaelementplayer({
            alwaysShowControls: false ,
            features: [],
            startVolume: 0.0
        });*/
        /*
        var video = document.getElementById('videobg');
        video.addEventListener('click', function() {
            video.play();
        });
        video.load();
        video.play();*/

        /*var video = document.getElementById('videobg');
        video.addEventListener('click',function(){
            video.play();
        },false);*/




        resizeAllBlocks();


        setTimeout(timerStartCounter, 500);
        setTimeout(freeTraining(24), 500);

        $('#menu').metisMenu();


        $('input[name="phone"], #phonerecov, #phonereg, #phonecab').mask('+7(999) 999 99 99');
        $("#00N200000098YTQ").ionRangeSlider({
            min: 2.5,
            max: 7,
            from: 3,
            grid: true,
            values: [2.5,3,3.5,4,4.5,5,5.5,6,6.5,7],
            max_postfix: " лет"
        });

        $('.dynamic-max-height').dynamicMaxHeight(
            { trigger : '.dynamic-show-more'}
        );


        //$("#mobilefooter").show("slow");


        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            //$("#mobilefooter").show("slow");
            $(".topmenumobile").css("display","block");
            $("#main-block3-video, #main-block4-video, #main-block5-video, #main-block6-video").css("display","none");
            $(".demo, .demo .helper").css("height","380px");
            $(".desctfooter").css("display","none");
            $("#mobilefooter").show("slow");
            $(".socblock").css("padding-bottom","60px");
            $(".mySlideshow li").css("background-size","contain");
            $(".slideshow1").css("background-image","url(photo/banner/index/IMG_3346-mobile.jpg)");
            $(".slideshow2").css("background-image","url(photo/banner/index/1-mobile.jpg)");
            $(".slideshow3").css("background-image","url(photo/banner/index/4-footy-3.jpg)");
        }else{
            $(".topmenudesctop").css("display","block");
        }
        //if(jQuery(window).width()<600){
            //$(".demo, .demo .helper").css("height","380");
            //$(".slideshow1").css("background-image","url(photo/banner/index/IMG_3346-mobile.jpg)");
           // $(".slideshow2").css("background-image","url(photo/banner/index/1-mobile.jpg)");
        //}
        //$(".demo, .demo .helper").css("height","540");

        $('.demo1').GICarousel({
            arrows:true,
            swipeSensibility: 50,
            animationSpeed: 100,
            pagination:true,
            keyboardNavigation:true,
            responsive: false
        });


        $('#mapmob').click(function(){
            var collapse_content_selector = $(this).attr('href');
            //var toggle_switch = $(this);
            $(collapse_content_selector).slideToggle(function(){
                if($(this).css('display')=='none'){
                    //toggle_switch.html('Show');
                }else{
                    //toggle_switch.html('Hide');
                }
            });
            return false;
        });





        //setTimeout(showViewPortSize(1), 5000);



    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>
</body>
</html>
