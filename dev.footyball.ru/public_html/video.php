<?
include_once("lib/cms_view_inc.php");

if(isset($_GET["cat"])&&isset($_GET["id"])) {
    $cat = cleardata($_GET["cat"]);
    $id = cleardata($_GET["id"],"i");
    if(($cat!=="feedback")&&($cat!=="footytv")||($id<1)){
        header("Location: http://".$_SERVER['HTTP_HOST']);
        exit();
    }else{
        if($cat=="feedback"){
            $videoone = getonevideofeetback($id);
            if(!empty($videoone)){
                if($videoone[0]["type"]==0){
                    header("Location: http://".$_SERVER['HTTP_HOST']);
                    exit();
                }
            }
            else{
                header("Location: http://".$_SERVER['HTTP_HOST']);
                exit();
            }
        }else{
            $videoone = getonevideo($id);
            if(empty($videoone)){
                header("Location: http://".$_SERVER['HTTP_HOST']);
                exit();
            }
        }
    }
}else{
    header("Location: http://".$_SERVER['HTTP_HOST']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$videoone[0]["name"];?></title>
    <meta name="description" content="<?=$videoone[0]["name"];?> о нашем клубе. Официальный сайт компании Footyball">
    <meta name="keywords" content="Footyball">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/style-tmpl.css">
    <link rel="stylesheet" href="assets/css/style-video.css">

    <!--[if IE]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://footyball.ru/36/36p/js/oldies.js" charset="utf-8"></script>
    <![endif]-->

    <link rel="image_src" href="http://footyball.ru/assets/img/logo-4fb.png"/>
    <meta property="og:locale" content="ru_RU" >
    <meta property="og:title" content="Footyball - Официальный сайт" >
    <meta property="og:site_name" content="Footyball - официальный сайт">
    <meta property="og:url" content="http://footyball.ru/" >
    <meta property="og:description" content="Официальный сайт компании Footyball" >
    <meta property="og:image" content="http://footyball.ru/assets/img/logo-4fb.png" >
    <meta property="og:image:type" content="image/png" >
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:type" content="website" >
    <meta property="fb:app_id" content="826699460747158" />

    <meta name="author" content="PotapovD">

</head>

<body>
    <header>
        <?include_once("inc/header.inc.php");?>
    </header>
    <div id="mainbody">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-1 col-sx-12 buttback">
                    <a href="#">  <i class="fa fa-chevron-left"></i> Назад</a>
                </div>
                <div class="col-sm-8 col-sx-12 videonamestr">
                    <h1><?=$videoone[0]["name"];?></h1>
                    <?if($cat=="footytv"){?>
                    <div class="dateandcat">
                        <span class="date"><?=$videoone[0]["date"];?></span>
                        <span class="categ"><?=$videoone[0]["catname"];?></span>
                    </div>
                    <?}?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8 col-sx-12">

                    <div class="no-svg blockvideo">
                        <!-- /assets/video/simple.jpg autoplay="autoplay" -->

                        <video id="footyballtv" controls="controls" autoplay="autoplay" style="width: 100%; height: 100%;">
                            <?if($cat=="footytv"){?>
                                <source src="/video/tv/<?=$videoone[0]["videourl"];?>.mp4" type="video/mp4">
                                <source src="/video/tv/<?=$videoone[0]["videourl"];?>.webm" type="video/webm">
                            <?}else{?>
                                <source src="/video/feedback/<?=$videoone[0]["content"];?>.mp4" type="video/mp4">
                                <source src="/video/feedback/<?=$videoone[0]["content"];?>.webm" type="video/webm">
                            <?}?>
                        </video>
                    </div>


                </div>
            </div>
        </div>
        <div class="container-fluid nopadding" id="mainblockfooter">
            <?include_once("inc/footer.inc.php");?>
        </div>
    </div>

    <?include_once("inc/allmainjs.php");?>
    <script src="assets/js/mediaelement-and-player.min.js"></script>
    <script src="assets/js/allpages-functions.js"></script>




<script>

    $(document).ready(function() {

        $('#footyballtv').mediaelementplayer({
            alwaysShowControls: true ,
            features: ['playpause','progress','volume','fullscreen'],
            videoVolume: 'horizontal',
            startVolume: 0.7,
            enableKeyboard: false
        });

        $('.buttback a').click(function(){
            parent.history.back();
            return false;
        });


    })
</script>
    <?include_once("inc/beforeclose.inc.php")?>

</body>
</html>
