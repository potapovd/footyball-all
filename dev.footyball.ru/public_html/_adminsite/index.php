<?
//session_start();
include_once("lib/cms_view_inc.php");
checkpassandroll("0");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>Главная страница</h2>

        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $("#mainmenuindex").addClass("active");
        });
    </script>
</div>
</body>
</html>