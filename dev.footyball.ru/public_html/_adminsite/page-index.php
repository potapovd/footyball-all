<?
include_once("lib/cms_view_inc.php");
checkpassandroll("index");

$indexmap = getindexpageinfo("blockmap");
$gallery = getgallery();
$allpartn = getallpartn();


$indexsliderphoto = getsliderphoto('index');

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/js/dropzone/dropzone.css">
    <link rel="stylesheet" href="assets/js/jcrop/jquery.Jcrop.min.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>Главная страница</h2>

        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal form-groups-bordered">
                    <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                        <li class="active">
                            <a href="#gallery" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-home"></i></span>
                                <span class="hidden-xs">Галерея</span>
                            </a>
                        </li>
                        <!--<li>
                            <a href="#tabphoto" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-home"></i></span>
                                <span class="hidden-xs">Фото</span>
                            </a>
                        </li>-->
                        <li>
                            <a href="#tabpartn" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-home"></i></span>
                                <span class="hidden-xs">Партнеры</span>
                            </a>
                        </li>
                        <!--<li>
                            <a href="#tabvideo" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-user"></i></span>
                                <span class="hidden-xs">Видео</span>
                            </a>
                        </li>-->
                        <!--<li>
                            <a href="#tabmap" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-mail"></i></span>
                                <span class="hidden-xs">Карта</span>
                            </a>
                        </li>-->

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="gallery">
                            <div class="row">

                                <div class="col-md-12">

                                    <table class="table table-bordered table-hover datatable" id="tableid1">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-3">Превью</th>
                                            <th class="col-md-3">Ссылка</th>
                                            <th class="col-md-3">Текст</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $i = 1;

                                        foreach ($indexsliderphoto as $key => $val) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td>
                                                    <img src="/photo/banner/index/<?=$indexsliderphoto[$key]["name"]; ?>.jpg" alt="" class="img-thumbnail img-responsive">
                                                </td>
                                                <td>
                                                    <a href="<?= $indexsliderphoto[$key]["link"]; ?>" target="_blank">
                                                        <?= $indexsliderphoto[$key]["link"]; ?>
                                                    </a>
                                                </td>
                                                <td><?= $indexsliderphoto[$key]["text"]; ?></td>
                                                <td>
                                                    <div data-toggle="buttons" class="btn-group">
                                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#sample-modal" data-toggle="modal"
                                                               data-target="#editslide" onclick="showAjaxModal(<?=$indexsliderphoto[$key]["id"];?>)" class="bg">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </label>
                                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                                            <a href="#" class="videoline" onclick="removeslide('removeindexslide',<?=$indexsliderphoto[$key]["id"];?>)" data-videoid ="<?=$indexsliderphoto[$key]["id"];?>" data-rel="collapse">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="col-md-1">#</th>
                                            <th class="col-md-6">Назавание</th>
                                            <th class="col-md-1">Просмотры</th>
                                            <th class="col-md-2">Дата</th>
                                            <th class="col-md-2">Действие</th>
                                        </tr>
                                        </tfoot>
                                    </table>


                                    <button class="btn btn-success btn-icon" href="#sample-modal" data-toggle="modal"
                                            data-target="#editslide" onclick="showAjaxModal(0);" type="button">Добавить новый слайд <i class="fa fa-plus"></i></button>


                                </div>

                            </div>
                        </div>


                        <div class="tab-pane" id="tabphoto">
                            <div class="row">

                            </div>
                            <div class="gallery-env">
                                <div class="row rowgall">

                                    <?foreach($gallery as $key => $val){?>
                                    <div class="col-sm-2 col-xs-4" data-tag="1d" data-photoid="<?=$gallery[$key]["id"];?>">
                                        <article class="image-thumb">
                                            <a href="#" class="image">
                                                <img src="/photo/index/<?=$gallery[$key]["name"];?>.jpg"/>
                                            </a>
                                            <div class="image-options">
                                                <a href="#" class="delete"><i class="fa fa-times"></i></a>
                                            </div>
                                        </article>
                                    </div>
                                    <?}?>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Фото
                                            <button class="btn btn-default btn-xs popover-default"
                                                    data-original-title="Требование к фото:"
                                                    data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                                                    2)Разрешение: 960x640 пикселей;&nbsp;&nbsp;
                                                                    3)Оптимальный размер: до 250 КБ
                                                                    4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                                                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </label>



                                        <div class="col-sm-10">
                                            <input type="file" class="form-control file2 inline btn btn-primary"
                                                   id="newphotos"
                                                   name="newphotos"
                                                   data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Добавить фото"/>
                                            <button type="button" class="btn btn-success btn-icon" id="photoinp">Сохранить <i class="fa fa-floppy-o right"></i></button>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="tabpartn">
                            <div class="row">

                            </div>
                            <div class="gallery-env">
                                <div class="row rowgall">

                                    <?foreach($allpartn as $key => $val){?>
                                        <div class="col-sm-2 col-xs-4" data-tag="1d" data-photoid="<?=$allpartn[$key]["id"];?>">
                                            <article class="image-thumb">
                                                <a href="#" class="image">
                                                    <img src="/photo/partner/<?=$allpartn[$key]["name"];?>"/>
                                                </a>
                                                <div class="image-options">
                                                    <a href="#" class="edit"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </article>
                                        </div>
                                    <?}?>


                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Парнер
                                            <button class="btn btn-default btn-xs popover-default"
                                                    data-original-title="Требование к лого:"
                                                    data-content="  1)Формат: png;&nbsp;&nbsp;
                                                                    2)Разрешение: 211x109 пикселей;&nbsp;&nbsp;
                                                                    3)Оптимальный размер: до 25 КБ
                                                                    4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                                                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </label>



                                        <div class="col-sm-10">
                                            <input type="file" class="form-control file2 inline btn btn-primary"
                                                   id="newppartn"
                                                   name="newppartn"
                                                   data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Добавить логотип"/>
                                            <button type="button" class="btn btn-success btn-icon" id="logoinp">Сохранить <i class="fa fa-floppy-o right"></i></button>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>


                        <!--<div class="tab-pane" id="tabvideo">-->
                            <!--<form id="tabvideoform" enctype="multipart/form-data" method="post">-->

                                <!--<div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Видео .mp4 <span class="inpreq">*</span>
                                                <button class="btn btn-default btn-xs popover-default"
                                                        data-original-title="Требование к видео:"
                                                        data-content="  1)Формат: mp4;&nbsp;&nbsp;
                                                                        2)Разрешение: 640x360 пикселей;&nbsp;&nbsp;
                                                                        3)Оптимальный размер: до 30 МБ
                                                                        4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                                                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                                                    <i class="fa fa-info"></i>
                                                </button>
                                            </label>

                                            <div class="col-sm-7">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <span class="btn btn-info btn-file ">
                                                            <span class="fileinput-new">Выбрать видео</span>
                                                            <span class="fileinput-exists">Изменить</span>
                                                            <input type="file" name="tabvideomp4" id="tabvideomp4">
                                                        </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none">&times;</a>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Видео .webm <span class="inpreq">*</span>
                                                <button class="btn btn-default btn-xs popover-default"
                                                        data-original-title="Требование к видео:"
                                                        data-content="  1)Формат: webm;&nbsp;&nbsp;
                                                                        2)Разрешение: 640x360 пикселей;&nbsp;&nbsp;
                                                                        3)Оптимальный размер: до 25 МБ
                                                                        4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                                                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                                                    <i class="fa fa-info"></i>
                                                </button>

                                            </label>

                                            <div class="col-sm-7">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-info btn-file">
                                                                <span class="fileinput-new">Выбрать видео</span>
                                                                <span class="fileinput-exists">Изменить</span>
                                                                <input type="file" name="tabvideowebm" id="tabvideowebm" >
                                                            </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none">&times;</a>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Постер <span class="inpreq">*</span>
                                                <button class="btn btn-default btn-xs popover-default"
                                                        data-original-title="Требование к постеру:"
                                                        data-content="1)Разрешение: 561x315 пикселей;&nbsp;&nbsp;"
                                                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                                                    <i class="fa fa-info"></i>
                                                </button></label>

                                            <div class="col-sm-7">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail"
                                                         style="width: 280px; height: 157px;" data-trigger="fileinput">
                                                        <img src="/assets/video/FootyBall-main-poster.jpg" alt="">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 280px; max-height: 157px"></div>
                                                    <div>
                                                            <span class="btn btn-white btn-file">
                                                                <span class="fileinput-new">Выбрать файл</span>
                                                                <span class="fileinput-exists">Изиенить</span>
                                                                <input type="file" name="tabvideposter" id="tabvideposter" >
                                                            </span>
                                                        <a href="#" class="btn btn-orange fileinput-exists"
                                                           data-dismiss="fileinput">Удалить</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            <button type="button" class="btn btn-success btn-icon" id="tabvideoinp">Сохранить <i class="fa fa-floppy-o right"></i></button>-->


                            <!--</form>-->
                        <!--</div>-->
                        <div class="tab-pane" id="tabmap">
                            <form id="tabmapform">
                                <div class="row">
                                    <div class="col-md-4" id="mapcoderesult">
                                        <?=$indexmap[0]["content"]; ?>
                                    </div>
                                    <div class="col-md-8">
                                           <textarea name="mapcode" id="mapcode" class="form-control" rows="10">
                                               <?=$indexmap[0]["content"]; ?>
                                           </textarea>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success btn-icon" id="tabmapinp">Сохранить <i class="fa fa-floppy-o right"></i></button>
                            </form>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <!-- Sample Modal (Default skin) -->
        <div class="modal fade" id="editpartn">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Информация о партнере</h4>
                    </div>

                    <div class="modal-body"> <h3>Загрузка...</h3> </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="savepartninfo">Сохранить <i class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editslide">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"> Слайдер</h4>
                    </div>

                    <div class="modal-body"> <h3>Загрузка...</h3> </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="saveslide">Сохранить <i class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
            <script src="assets/js/dropzone/dropzone.js"></script>
            <!--<script src="assets/js/jcrop/jquery.Jcrop.min.js"></script>-->

        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script type="text/javascript">

        function showAjaxModal(id){
            //alert (id);
            $('#editslide').modal('show', {backdrop: 'static'});
            $.ajax({
                url: "page-gallery-oneslide.php",
                type: "POST",
                async: false,
                cache: false,
                data:"id="+id,
                success: function(response)
                {
                    $('#editslide .modal-body').html(response);
                }
            });
        }

        function removeslide(object,id){
            var formData = {
                'object' : object,
                'id' : id,
                'formname' : "removeelement"
            };
            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                success: function(results) {
                    //var posts = JSON.parse(results);
                    if(results["type"] == 'error'){
                        toastr.error(results["text"], optstoastr);

                    }else{
                        toastr.success(results["text"], optstoastr);
                        $("* [data-videoid='"+id+"']").parent().parent().slideUp("slow");

                    }
                },
                error: function(results) {
                    toastr.error('Ошибка! '+results["responseText"]);
                    //console.log(results);
                }
            });


            return false;

        }




        jQuery(document).ready(function ($) {

            $("#logout").on("click",function() {
                $.post( "lib/cms_update_inc.php",{formname: "logout"});
                //return false;
            });

            $("#mainmenuindexpage").addClass("active");

            //TAB PHOTO
            $("#photoinp").on("click",function() {
                //var formData = {
                //'tabvideowebm' : $('#tabvideowebm').val(),
                //'tabvideomp4' : $('#tabvideomp4').val(),
                //'tabvideposter' : $('#tabvideposter')[0].files[0],
                //'tabvideposter' : jQuery('input[name=tabvideomp4]')[0].files[0],
                //'formname' : "tabvideoinp"
                //};
                var formDataPhoto = new FormData();
                formDataPhoto.append( 'formname', 'tabphoto');
                formDataPhoto.append( 'newphotos', $('input[name=newphotos]')[0].files[0]);
                //formData.append( 'tabvideomp4', $('input[name=tabvideomp4]')[0].files[0]);
                //formData.append( 'tabvideposter', $('input[name=tabvideposter]')[0].files[0]);


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                //var formData = new FormData($("#tabvideoform")[0]);
                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formDataPhoto,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        //$("#tabvideoinp").attr("disabled","disabled");
                        //$("#tabvideoinp").attr("value","Запись...");
                    },
                    success: function(results) {
                        //var posts = JSON.parse(results);
                        if(results["type"] == 'error'){
                            toastr.error(results["text"], optstoastr);

                        }else{
                            toastr.success(results["text"], optstoastr);
                            $(".file-input-name").html("");
                            var newline = ''+
                                '<div class="col-sm-2 col-xs-4" data-tag="1d">'+
                                '<article class="image-thumb">'+
                                '<a href="#" class="image">'+
                                '<img src="/photo/index/'+results["src"]+'.jpg"/>'+
                                '</a>'+
                                //'<div class="image-options">'+
                                //'<a href="#" class="delete"><i class="fa fa-times"></i></a>'+
                                //'</div>'+
                                '</article>'+
                                '</div>';
                            $(".gallery-env .rowgall").append(newline);
                            //alert (results["src"]);
                            //$("#mapcoderesult").html(formData["mapcode"])
                            //$("#mapcode").val(formData["mapcode"])
                        }
                    },
                    error:function(results) {
                        toastr.error('Ошибка! '+results["responseText"]);
                    },
                    complete: function() {
                        //$("#tabvideoinp").removeAttr("disabled");
                        //$("#tabvideoinp").attr("value","Сохранить");
                    }
                });
                return false;
            });
            $("#tabphoto .gallery-env").on("click", ".image-thumb .image-options a.delete", function (ev) {
                ev.preventDefault();


                var erroflag = 0;
                var id = $(this).parent().parent().parent().attr('data-photoid');
                //console.log(id)
                var formData = {
                    'object': 'removephotohero',
                    'id': id,
                    'formname': "removeelement"
                };


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    success: function (results) {
                        //var posts = JSON.parse(results);
                        if (results["type"] == 'error') {
                            toastr.error(results["text"], optstoastr);
                            erroflag=1;

                        } else {
                            toastr.success(results["text"], optstoastr);

                            //$("* [data-feetbackid='" + id + "']").parent().parent().parent().parent().slideUp("slow");

                        }
                    },
                    error: function (results) {
                        toastr.error('Ошибка! ' + results["responseText"]);
                        //console.log(results);
                        erroflag=1
                    }
                });

                if(erroflag==0){
                    var $image = $(this).closest('[data-tag]');
                    var t = new TimelineLite({
                        onComplete: function () {
                            $image.slideUp(function () {
                                $image.remove();
                            });
                        }
                    });
                    $image.addClass('no-animation');
                    t.append(TweenMax.to($image, .2, {css: {scale: 0.95}}));
                    t.append(TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}));
                }


            });


            //TAB SLIDER
            $("#saveslide").on("click",function() {
                //var namevideo = $("#datevideo").val()
                //alert("namevideo: "+namevideo);


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                var formData = new FormData();
                if($("#sliderid").val()!==undefined) {
                    //alert("edit slider");
                        //$('input[name=sliderbigimg]')[0].files[0]!==undefined ){

                        formData.append('formname', 'updateoneslide');
                        formData.append( 'slidetxt', $("#slidetxt").val());
                        formData.append( 'slidelink', $("#slidelink").val());
                        formData.append( 'slideid', $("#sliderid").val());
                        formData.append( 'slidermobimg', $('input[name=slidermobimg]')[0].files[0]);
                        formData.append( 'sliderbigimg', $('input[name=sliderbigimg]')[0].files[0]);

                        //var formData = new FormData($("#tabvideoform")[0]);
                        $.ajax({
                            url: "lib/cms_update_inc.php",
                            type: "POST",
                            data: formData,
                            dataType: 'json',
                            async: false,
                            cache: false,
                            //enctype: 'multipart/form-data',
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                toastr.success("Идет обновление слайдера...", optstoastr);
                            },
                            success: function (results) {
                                if (results["type"] == 'error') {
                                    toastr.error(results["text"], optstoastr);

                                } else {
                                    toastr.success(results["text"], optstoastr);
                                    $('#addedivdideo').modal('hide');
                                }
                            },
                            error: function (results) {
                                toastr.error('Ошибка! ' + results["responseText"]);
                            },
                            complete: function () {
                            }
                        });

                }else{
                    //alert("new blogpost");

                    if (  $('input[name=slidermobimg]')[0].files[0]!==undefined &&
                        $('input[name=sliderbigimg]')[0].files[0]!==undefined ){
                        formData.append('formname', 'addoneslide');
                        formData.append( 'slidetxt', $("#slidetxt").val());
                        formData.append( 'slidelink', $("#slidelink").val());
                        formData.append( 'slidermobimg', $('input[name=slidermobimg]')[0].files[0]);
                        formData.append( 'sliderbigimg', $('input[name=sliderbigimg]')[0].files[0]);


                        $.ajax({
                            url: "lib/cms_update_inc.php",
                            type: "POST",
                            data: formData,
                            dataType: 'json',
                            //async: false,
                            cache: false,
                            //enctype: 'multipart/form-data',
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                toastr.success("Идет добавление слайда...", optstoastr);
                            },
                            success: function(results) {
                                if(results["type"] == 'error'){
                                    toastr.error(results["text"], optstoastr);

                                }else{
                                    toastr.success(results["text"], optstoastr);
                                    $("#tableid"+$("#categoryvideo").val()+" tr:first").after("<tr><td>0</td><td>"+$("#namevideo").val()+"</td><td></td><td></td></tr>");
                                    $('#addedivdideo').modal('hide');
                                }
                            },
                            error:function(results) {
                                toastr.error('Ошибка! '+results["responseText"]);
                            },
                            complete: function() {
                            }
                        });


                    }else{
                        toastr.error('Ошибка. Постеры - должны быть заполнены');
                        return false;
                    }
                }



                //return false;

                //console.log(formData);


                //return false
            });


//removeslide('removeindexslide




            //TAB VIDEO
            /*$("#tabvideoinp").on("click",function() {

                var formDataVideo = new FormData();
                formDataVideo.append( 'formname', 'tabvideoinp');
                formDataVideo.append( 'tabvideowebm', $('input[name=tabvideowebm]')[0].files[0]);
                formDataVideo.append( 'tabvideposter', $('input[name=tabvideposter]')[0].files[0]);
                formDataVideo.append( 'tabvideomp4', $('input[name=tabvideomp4]')[0].files[0]);
                //formData.append( 'tabvideomp4', $('input[name=tabvideomp4]')[0].files[0]);
                //formData.append( 'tabvideposter', $('input[name=tabvideposter]')[0].files[0]);


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                var optstoastrstart = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "10000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                //var formData = new FormData($("#tabvideoform")[0]);

                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formDataVideo,
                    dataType: 'json',
                    //async: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $("#tabvideoinp").attr("disabled","disabled");
                        //$("#tabvideoinp").attr("value","Запись...");
                        toastr.success("Загрузка видео началась... <br>Это процедура займет несколько минут.", optstoastrstart);

                    },
                    succss: function(results) {
                        //var posts = JSON.parse(results);
                        if(results["type"] == 'error'){
                            toastr.error(results["text"], optstoastr);

                        }else{
                            toastr.success(results["text"], optstoastr);
                            //$("#mapcoderesult").html(formData["mapcode"])
                            //$("#mapcode").val(formData["mapcode"])
                        }
                    },
                    error:function(results) {
                        toastr.error('Ошибка! '+results["responseText"]);
                    },
                    complete: function() {
                        $("#tabvideoinp").removeAttr("disabled");
                        //$("#tabvideoinp").attr("value","Сохранить");
                    }
                });
                return false;
            });*/

            //TAB MAP
            $("#tabmapinp").on("click",function() {
                var formDataMap = {
                    'mapcode' : $('#mapcode').val(),
                    'formname' : "tabmapinp"
                };
                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formDataMap,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    beforeSend: function() {
                        $("#tabmapinp").attr("disabled","disabled");
                        $("#tabmapinp").attr("value","Запись...");
                        $("#mapcoderesult").html("Запись...")
                    },
                    success: function(results) {
                        //var posts = JSON.parse(results);
                        if(results["type"] == 'error'){
                            toastr.error(results["text"], optstoastr);

                        }else{
                            toastr.success(results["text"], optstoastr);
                            //
                            //$("#mapcoderesult").html(formData["mapcode"])
                            //$("#mapcode").val(formData["mapcode"])
                        }
                    },
                    error: function(results) {
                        toastr.error('Ошибка! '+results["responseText"]);
                    },
                    complete: function() {
                        $("#tabmapinp").removeAttr("disabled");
                        $("#tabmapinp").attr("value","Сохранить");
                    }
                });
                return false;
            });

            //TAB PARTNER

            $("#logoinp").on("click",function() {
                //var formData = {
                //'tabvideowebm' : $('#tabvideowebm').val(),
                //'tabvideomp4' : $('#tabvideomp4').val(),
                //'tabvideposter' : $('#tabvideposter')[0].files[0],
                //'tabvideposter' : jQuery('input[name=tabvideomp4]')[0].files[0],
                //'formname' : "tabvideoinp"
                //};
                var formDataPhoto = new FormData();
                formDataPhoto.append( 'formname', 'tabpartn');
                formDataPhoto.append( 'newphotos', $('input[name=newppartn]')[0].files[0]);
                //formData.append( 'tabvideomp4', $('input[name=tabvideomp4]')[0].files[0]);
                //formData.append( 'tabvideposter', $('input[name=tabvideposter]')[0].files[0]);


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                //var formData = new FormData($("#tabvideoform")[0]);
                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formDataPhoto,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        //$("#tabvideoinp").attr("disabled","disabled");
                        //$("#tabvideoinp").attr("value","Запись...");
                    },
                    success: function(results) {
                        //var posts = JSON.parse(results);
                        if(results["type"] == 'error'){
                            toastr.error(results["text"], optstoastr);

                        }else{
                            toastr.success(results["text"], optstoastr);
                            $(".file-input-name").html("");
                            var newline = ''+
                                '<div class="col-sm-2 col-xs-4" data-tag="1d">'+
                                '<article class="image-thumb">'+
                                '<a href="#" class="image">'+
                                '<img src="/photo/partner/'+results["src"]+'"/>'+
                                '</a>'+
                                    //'<div class="image-options">'+
                                    //'<a href="#" class="delete"><i class="fa fa-times"></i></a>'+
                                    //'</div>'+
                                '</article>'+
                                '</div>';
                            $(".gallery-env .rowgall").append(newline);
                            //alert (results["src"]);
                            //$("#mapcoderesult").html(formData["mapcode"])
                            //$("#mapcode").val(formData["mapcode"])
                        }
                    },
                    error:function(results) {
                        toastr.error('Ошибка! '+results["responseText"]);
                    },
                    complete: function() {
                        //$("#tabvideoinp").removeAttr("disabled");
                        //$("#tabvideoinp").attr("value","Сохранить");
                    }
                });
                return false;
            });





            $("#tabpartn .gallery-env").on("click", ".image-thumb .image-options a.delete", function (ev) {
                ev.preventDefault();


                var erroflag = 0;
                var id = $(this).parent().parent().parent().attr('data-photoid');
                //console.log(id)
                var formData = {
                    'object': 'removephotopartn',
                    'id': id,
                    'formname': "removeelement"
                };


                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    success: function (results) {
                        //var posts = JSON.parse(results);
                        if (results["type"] == 'error') {
                            toastr.error(results["text"], optstoastr);
                            erroflag=1;

                        } else {
                            toastr.success(results["text"], optstoastr);

                            //$("* [data-feetbackid='" + id + "']").parent().parent().parent().parent().slideUp("slow");

                        }
                    },
                    error: function (results) {
                        toastr.error('Ошибка! ' + results["responseText"]);
                        //console.log(results);
                        erroflag=1
                    }
                });

                if(erroflag==0){
                    var $image = $(this).closest('[data-tag]');
                    var t = new TimelineLite({
                        onComplete: function () {
                            $image.slideUp(function () {
                                $image.remove();
                            });
                        }
                    });
                    $image.addClass('no-animation');
                    t.append(TweenMax.to($image, .2, {css: {scale: 0.95}}));
                    t.append(TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}));
                }
            });

                $("#tabpartn .gallery-env").on("click", ".image-thumb .image-options a.edit", function (ev) {

                    ev.preventDefault();

                    var erroflag = 0;
                    var id = $(this).parent().parent().parent().attr('data-photoid');
                    //console.log(id)
                    var formData = {
                        'object': 'removephotopartn',
                        'id': id,
                        'formname': "removeelement"
                    };
                    //alert(formData);

                    $('#editpartn').modal('show', {backdrop: 'static'});
                    $.ajax({
                        url: "page-index-partner.php",
                        type: "POST",
                        async: false,
                        cache: false,
                        data:"id="+id,
                        success: function(response)
                        {
                            $('#editpartn .modal-body').html(response);
                        }
                    });

                });

            $("#savepartninfo").on("click",function() {
                var optstoastr = {
                    "closeButton": false,
                    "debug": false,
                    "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                    "toastClass": "black",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                var formData = new FormData();
                formData.append('formname', 'tabprtnpartnupd');
                formData.append('partnerlink', $("#partnerlink").val());
                formData.append('partnertype', $("#partnertype").val());
                formData.append('partnerid', $("#partnerid").val());
                //formData.append('tvposter', $('input[name=tvposter]')[0].files[0]);

                //var formData = new FormData($("#tabvideoform")[0]);
                $.ajax({
                    url: "lib/cms_update_inc.php",
                    type: "POST",
                    data: formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    //enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    success: function (results) {
                        if (results["type"] == 'error') {
                            toastr.error(results["text"], optstoastr);

                        } else {
                            toastr.success(results["text"], optstoastr);
                            $('#addedivdideo').modal('hide');
                        }
                    },
                    error: function (results) {
                        toastr.error('Ошибка! ' + results["responseText"]);
                    },
                    complete: function () {
                    }
                });

            })




        })
    </script>
</div>
</body>
</html>