<?
include_once("lib/cms_view_inc.php");
checkpassandroll("users");

if(!isset($_POST["id"])){ die();}

$getrandpass = generateCode(12);

$userid = $_POST["id"];

if($userid>0){
    $userinfo = getoneuserinfo($userid);
    $perm = getoneperm($userid);
    $nu=0;
}else{
    $nu=1;
}
if(is_ajax2()){
?>
<form class="form-horizontal form-groups-bordered" role="form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Логин <span class="inpreq">*</span></label>
                <div class="col-sm-9">
                    <input type="text" required id="nameuser" name="nameuser" class="form-control" <?if($nu==0){?>disabled<?}?> value="<?if($nu==0){echo $userinfo[0]["username"];}?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Новый пароль <span class="inpreq">*</span> </label>
                <div class="col-sm-9"><input type="text" placeholder="" required id="newpas" name="newpas" class="form-control"  value=""></div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Повтор пароля <span class="inpreq">*</span> </label>
                <div class="col-sm-9"><input type="text" placeholder="" required id="newpas2" name="newpas2" class="form-control" value=""></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="namevideo">Случайный пароль</label>
                <div class="col-sm-9"><input type="text"  id="gennewpas" name="gennewpas" class="form-control"  value="<?=$getrandpass;?>"></div>
            </div>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <label class="col-sm-3 control-label" for="namevideo">Права доступа </label>
            <div class="col-sm-9">
                <div class="form-group">
                    <?if($nu==0){echo $perm;}else{?>

                    <div class="checkbox"> <label> <input type="checkbox" id="cbindex" name="cbindex">Редактирование <b>главной страницы</b></label> </div>
                    <div class="checkbox"> <label> <input type="checkbox" id="cbtv"  name="cbtvcbindex">Редактирование <b>ТВ страницы</b></label> </div>
                    <div class="checkbox"> <label> <input type="checkbox" id="cbfeetback"  name="cbfeetback">Редактирование <b>страницы отзывов</b></label> </div>

                    <?}?>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="currentuserid" id="currentuserid" value="<?if($nu==0){echo $userinfo[0]["id"];}else{echo 0;}?>">
</form>
<?}?>

