<?
include_once("lib/cms_view_inc.php");
checkpassandroll("blog");

$allcategories = getblogcatigoriesname();

if(!isset($_POST["id"])){ die();}

$blogpostid = $_POST["id"];
if($blogpostid>0){
    $nv = 1;
    $oneblogpost = getoneblogpost($blogpostid);
}
if(is_ajax2()){
?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="nameblogpost">Название <span class="inpreq">*</span> </label>

            <div class="col-sm-10"><input type="text" placeholder="" required id="nameblogpost" name="nameblogpost" class="form-control" value="<?if(isset($nv))echo $oneblogpost[0]["blogpostsname"];?>"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="field-1">Дата <span class="inpreq">*</span></label>

            <div class="col-sm-4">
                <div class="input-group">
                    <input type="text" data-format="dd/mm/yyyy" class="form-control datepicker" name="dateblogpost" id="dateblogpost" value="<?if(isset($nv)){echo $oneblogpost[0]["date"];}else{echo date("d/m/Y");}?>">

                    <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                </div>
            </div>

            <label class="col-sm-2 control-label" for="field-1">Категория <span class="inpreq">*</span></label>

            <div class="col-sm-4">
                <select class="form-control" name="categoryblogpost" id="categoryblogpost">
                    <?
                    if($blogpostid==0){
                        foreach ($allcategories as $key => $val) { ?>
                            <option value="<?= $allcategories[$key]["id"]; ?>"><?= $allcategories[$key]["name"]; ?></option>
                    <?  }
                    }else{
                    ?>
                        <?foreach ($allcategories as $key => $val) {?>
                            <option <?if($oneblogpost[0]["categoryid"]==$allcategories[$key]["id"]){echo "selected='selected'";}?> value="<?= $allcategories[$key]["id"]; ?>"><?= $allcategories[$key]["name"]; ?></option>
                        <?}?>

                    <?}?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group no-margin">
            <label for="blogpostdesription"  class="control-label">Описание</label>
            <textarea class="form-control autogrow" rows="5" name="blogpostdesription" id="blogpostdesription"><?if(isset($nv))echo $oneblogpost[0]["text"];?></textarea>
        </div>
    </div>
</div>


<?if($blogpostid==0){?>
<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <label class="col-sm-9 control-label">Постер <span class="inpreq">*</span>
                <button class="btn btn-default btn-xs popover-default"
                        data-original-title="Требование к постеру:"
                        data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                        2)Разрешение: 570x250 пикселей;&nbsp;&nbsp;"
                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                    <i class="fa fa-info"></i>
                </button>
            </label>

            <div class="col-sm-3">

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-info btn-file">
                        <span class="fileinput-new">Выбрать постер</span>
                        <span class="fileinput-exists">Изменить</span>
                        <input type="file" name="blogposter" id="blogposter" >
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>

            </div>

        </div>
    </div>
</div>
<?}else{?>
<br>
<div class="row">

</div>       
<br>
<div class="row">

    <div class="col-md-6">
        <label class="col-sm-9 control-label">Постер <span class="inpreq">*</span>
            <button class="btn btn-default btn-xs popover-default"
                    data-original-title="Требование к постеру:"
                    data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                        2)Разрешение: 570x250 пикселей;&nbsp;&nbsp;"
                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                <i class="fa fa-info"></i>
            </button>
        </label>
        <img src="/photo/blog/<?=$oneblogpost[0]["imgurl"];?>" alt="" width="259">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать фото</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="blogposter" id="blogposter">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
               style="float: none">&times;</a>
        </div>
    </div>

</div>
<input type="hidden" name="blogpostid" id="blogpostid" value="<?=$blogpostid;?>">
<!--<input type="hidden" name="blogposturl" id="blogposturl" value="<?/*=$oneblogpost[0]["blogposturl"];*/?>">-->
<?}?>

<script type="text/javascript">

    //$('blogpost').mediaelementplayer(/* Options */);

    $('#blogpostdesription').summernote({
        lang: 'ru-RU',
        height: 250,
        placeholder: 'текст поста...',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture','link']],
            ['misc', ['fullscreen','undo','redo']]
        ]
    });

    // Datepicker
    if ($.isFunction($.fn.datepicker)) {
        $(".datepicker").each(function (i, el) {
            var $this = $(el),
                opts = {
                    format: attrDefault($this, 'format', 'dd/mm/yyyy'),
                    startDate: attrDefault($this, 'startDate', ''),
                    endDate: attrDefault($this, 'endDate', ''),
                    daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
                    startView: attrDefault($this, 'startView', 0),
                    rtl: rtl()
                },
                $n = $this.next(),
                $p = $this.prev();

            $this.datepicker(opts);

            if ($n.is('.input-group-addon') && $n.has('a')) {
                $n.on('click', function (ev) {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }

            if ($p.is('.input-group-addon') && $p.has('a')) {
                $p.on('click', function (ev) {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }
        });
    }


    // Popovers and tooltips
    $('[data-toggle="popover"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'right'),
            trigger = attrDefault($this, 'trigger', 'click'),
            popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

        $this.popover({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.popover', function(ev)
        {
            var $popover = $this.next();

            $popover.addClass(popover_class);
        });
    });

    $('[data-toggle="tooltip"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'top'),
            trigger = attrDefault($this, 'trigger', 'hover'),
            popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

        $this.tooltip({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.tooltip', function(ev)
        {
            var $tooltip = $this.next();

            $tooltip.addClass(popover_class);
        });
    });






</script>
<?}?>

