<?
include_once("lib/cms_view_inc.php");
checkpassandroll("programm");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <?include_once("inc/head.inc.php");?>
</head>
<body class="page-body  page-fade" data-url="">
    <div class="page-container">
        <div class="sidebar-menu">
            <?include_once("inc/sidebar.inc.php");?>   
            <link rel="stylesheet" href="assets/js/dropzone/dropzone.css">
            <link rel="stylesheet" href="assets/js/jcrop/jquery.Jcrop.min.css">
        </div>
        <div class="main-content">
            <div class="row">     
                <?include_once("inc/topmenu.inc.php");?>   
            </div>
            <hr />
            <h2>Программа</h2>
            
            <div class="row">
                <div class="col-md-12">                    
                    <form role="form" class="form-horizontal form-groups-bordered">
                        <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                            
                            <li class="active">
                                <a href="#tabprogramm" data-toggle="tab">
                                    <span class="visible-xs"> </span>
                                    <span class="hidden-xs">Программы</span>
                                </a>
                            </li>
                            <li>
                                <a href="#tabvideo" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-user"></i></span>
                                    <span class="hidden-xs">Видео</span>
                                </a>
                            </li>
                            <li>
                                <a href="#tabvideobg" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-mail"></i></span>
                                    <span class="hidden-xs">Видео Фон</span>
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabprogramm">

                                <div class="row">
                                    
                                </div>

                            </div>
                            <div class="tab-pane" id="tabvideo">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Видео</label>

                                            <div class="col-sm-8">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Выбрать видео</span>
                                                        <span class="fileinput-exists">Изменить</span>
                                                        <input type="file" name="..."  accept="video/*">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                                   
                                   <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Превью</label>

                                            <div class="col-sm-8">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                                        <img src="http://placehold.it/200x150" alt="...">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                                    <div>
                                                        <span class="btn btn-white btn-file">
                                                            <span class="fileinput-new">Выбрать файл</span>
                                                            <span class="fileinput-exists">Изиенить</span>
                                                            <input type="file" name="..." accept="image/*">
                                                        </span>
                                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Удалить</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="tabvideobg">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Видео</label>

                                            <div class="col-sm-8">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Выбрать видео</span>
                                                        <span class="fileinput-exists">Изменить</span>
                                                        <input type="file" name="..."  accept="video/*">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </div>

                            

                            </div>
                        </div>
                        <input type="submit" class="btn btn-success" value="Сохранить">
                    </form>
                </div>
            </div>
           
            <footer class="main">
                <?include_once("inc/footer.inc.php");?>
                <!-- Imported scripts on this page -->
                <script src="assets/js/fileinput.js"></script>
                <script src="assets/js/dropzone/dropzone.js"></script>
                <script src="assets/js/jcrop/jquery.Jcrop.min.js"></script>
                
                
            </footer>
        </div>
        <?include_once("inc/js.inc.php");?>      
        <script type="text/javascript">
            jQuery(document).ready(function($){
                
                $("#mainmenuprogramm").addClass("active");
                
                
                $(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev) {
                    ev.preventDefault();


                    var $image = $(this).closest('[data-tag]');

                    var t = new TimelineLite({
                        onComplete: function()
                        {
                            $image.slideUp(function()
                                           {
                                $image.remove();
                            });
                        }
                    });

                    $image.addClass('no-animation');

                    t.append( TweenMax.to($image, .2, {css: {scale: 0.95}}) );
                    t.append( TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}) );

                }).on("click", ".image-thumb .image-options a.edit", function(ev)
                      {
                    ev.preventDefault();

                    // This will open sample modal
                    $("#album-image-options").modal('show');

                    // Sample Crop Instance
                    var image_to_crop = $("#album-image-options img"),
                        img_load = new Image();

                    img_load.src = image_to_crop.attr('src');
                    img_load.onload = function()
                    {
                        if(image_to_crop.data('loaded'))
                            return false;

                        image_to_crop.data('loaded', true);

                        image_to_crop.Jcrop({
                            //boxWidth: $("#album-image-options").outerWidth(),
                            boxWidth: 580,
                            boxHeight: 385,
                            onSelect: function(cords)
                            {
                                // you can use these vars to save cropping of the image coordinates
                                var h = cords.h,
                                    w = cords.w,

                                    x1 = cords.x,
                                    x2 = cords.x2,

                                    y1 = cords.w,
                                    y2 = cords.y2;

                            }
                        }, function()
                                            {
                            var jcrop = this;

                            jcrop.animateTo([600, 400, 100, 150]);
                        });
                    }
                });


                // Sample Filtering
                var all_items = $("div[data-tag]"),
                    categories_links = $(".image-categories a");

                categories_links.click(function(ev)
                                       {
                    ev.preventDefault();

                    var $this = $(this),
                        filter = $this.data('filter');

                    categories_links.removeClass('active');
                    $this.addClass('active');

                    all_items.addClass('not-in-filter').filter('[data-tag="' + filter + '"]').removeClass('not-in-filter');

                    if(filter == 'all' || filter == '*')
                    {
                        all_items.removeClass('not-in-filter');
                        return;
                    }
                });

            });
        </script>
    </div>
</body>
</html>