<?
include_once("lib/cms_view_inc.php");
checkpassandroll("users");

$allusers = getallusers();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/css/mediaelementplayer.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>
            Пользователи
            <div class="btn-group">
                <button class="btn btn-success" href="#sample-modal" data-toggle="modal"
                        data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button"><i class="fa fa-plus"></i></button>
            </div>
        </h2>



        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover datatable" id="tableusers">
                    <thead>
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-7">Логин</th>
                        <th class="col-md-2">Роль</th>
                        <th class="col-md-2">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $i = 1;
                    foreach ($allusers as $key => $val) {
                        ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $allusers[$key]["username"]; ?></td>
                            <td><?= $allusers[$key]["role"]; ?></td>

                            <td>
                                <?if($allusers[$key]["id"]>1){?>
                                <div data-toggle="buttons" class="btn-group">
                                    <label class="btn btn-white tooltip-default" data-original-title="Редактировать"
                                           title="" data-placement="bottom" data-toggle="tooltip">
                                        <a href="#sample-modal" data-toggle="modal"
                                           data-target="#addedivdideo"
                                           onclick="showAjaxModal(<?= $allusers[$key]["id"]; ?>)" class="bg">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </label>
                                    <label class="btn btn-white tooltip-default" data-original-title="Удалить" title=""
                                           data-placement="bottom" data-toggle="tooltip">
                                        <a href="#" class="videoline"
                                           onclick="removeelement('removeuser',<?= $allusers[$key]["id"]; ?>)"
                                           data-feetbackid="<?= $allusers[$key]["id"]; ?>" data-rel="collapse">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </label>
                                </div>
                                <?}?>
                            </td>
                        </tr>
                        <?
                        $i++;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-7">Логин</th>
                        <th class="col-md-2">Роль</th>
                        <th class="col-md-2">Действие</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>


        <div class="btn-group">
            <button class="btn btn-success btn-icon" href="#sample-modal" data-toggle="modal"
                    data-target="#addedivdideo" onclick="showAjaxModal(0);" type="button">Добавить пользователя <i
                    class="fa fa-plus"></i></button>
        </div>


        <!-- Sample Modal (Default skin) -->
        <div class="modal fade" id="addedituser">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Пользователь</h4>
                    </div>

                    <div class="modal-body"><h3>Загрузка...</h3></div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="saveoneuser">Сохранить <i
                                class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <script src="assets/js/mediaelement-and-player.min.js"></script>

    <script type="text/javascript">

        function showAjaxModal(id) {
            var modalvars = "id=" + id;
            //alert (videoid);
            $('#addedituser').modal('show', {backdrop: 'static'});
            $.ajax({
                url: "page-user-oneuser.php",
                type: "POST",
                async: false,
                cache: false,
                data: modalvars,
                success: function (response) {
                    $('#addedituser .modal-body').html(response);
                }
            });
            return false;
        }

        $("#saveoneuser").on("click", function () {
            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            //if ($("#newpas").val() == "") {
            //    toastr.error("Пароль не может быть пустым", optstoastr);
            //    return false;
            //}


            if ($("#newpas").val() !== $("#newpas2").val()) {
                toastr.error("Пароли не совпадают", optstoastr);
                return false;
            } else {



                if (($("#cbindex").prop('checked')) || ($("#cbtv").prop('checked')) || ($("#cbfeetback").prop('checked'))) {

                    var formData = new FormData();
                    if ($("#currentuserid").val()!=="0"){
                        formData.append('formname', 'updateoneuser');

                    }else{
                        formData.append('formname', 'addoneuser');
                        formData.append('username', $("#nameuser").val());
                    }
                    formData.append('currentuserid', $("#currentuserid").val());
                    formData.append('newpas', $("#newpas").val());
                    if ($("#cbindex").prop('checked')) {
                        formData.append('cbindex', 1);
                    } else {
                        formData.append('cbindex', 0);
                    }
                    if ($("#cbtv").prop('checked')) {
                        formData.append('cbtv', 1);
                    } else {
                        formData.append('cbtv', 0);
                    }
                    if ($("#cbfeetback").prop('checked')) {
                        formData.append('cbfeetback', 1);
                    } else {
                        formData.append('cbfeetback', 0);
                    }

                    $.ajax({
                        url: "lib/cms_update_inc.php",
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        async: false,
                        cache: false,
                        //enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        success: function (results) {
                            if (results["type"] == 'error') {
                                toastr.error(results["text"], optstoastr);

                            } else {
                                toastr.success(results["text"], optstoastr);
                                $('#addedituser').modal('hide');
                            }
                        },
                        error: function (results) {
                            toastr.error('Ошибка! ' + results["responseText"]);
                        },
                        complete: function () {
                        }

                    });

                } else {
                    toastr.error("Пользователь не может быть без прав", optstoastr);
                    return false;
                }






            }

        });


        function removeelement(object, id) {
            var formData = {
                'object': object,
                'id': id,
                'formname': "removeelement"
            };


            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                success: function (results) {
                    //var posts = JSON.parse(results);
                    if (results["type"] == 'error') {
                        toastr.error(results["text"], optstoastr);

                    } else {
                        toastr.success(results["text"], optstoastr);
                        $("* [data-feetbackid='" + id + "']").parent().parent().parent().parent().slideUp("slow");

                    }
                },
                error: function (results) {
                    toastr.error('Ошибка! ' + results["responseText"]);
                    //console.log(results);
                }
            });


            return false;

        }


        $(document).ready(function ($) {
            $("#mainmenuuserpage").addClass("active");
        });


    </script>

</div>
</body>
</html>