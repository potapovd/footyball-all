<?
include_once("lib/cms_view_inc.php");
checkpassandroll("tv");

$allcategories = getcatigoriesname();

if(!isset($_POST["id"])){ die();}

$videoid = $_POST["id"];
if($videoid>0){
    $nv = 1;
    $onevideo = getonevideo($videoid);
}
if(is_ajax2()){
?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="namevideo">Название <span class="inpreq">*</span> </label>

            <div class="col-sm-10"><input type="text" placeholder="" required id="namevideo" name="namevideo" class="form-control" value="<?if(isset($nv))echo $onevideo[0]["name"];?>"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="field-1">Дата <span class="inpreq">*</span></label>

            <div class="col-sm-4">
                <div class="input-group">
                    <input type="text" data-format="dd/mm/yyyy" class="form-control datepicker" name="datevideo" id="datevideo" value="<?if(isset($nv)){echo $onevideo[0]["date"];}else{echo date("d/m/Y");}?>">

                    <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                </div>
            </div>

            <label class="col-sm-2 control-label" for="field-1">Категория <span class="inpreq">*</span></label>

            <div class="col-sm-4">
                <select class="form-control" name="categoryvideo" id="categoryvideo">
                    <?
                    if($videoid==0){
                        foreach ($allcategories as $key => $val) { ?>
                            <option value="<?= $allcategories[$key]["id"]; ?>"><?= $allcategories[$key]["name"]; ?></option>
                    <?  }
                    }else{
                    ?>
                        <?foreach ($allcategories as $key => $val) {?>
                            <option <?if($onevideo[0]["categoryid"]==$allcategories[$key]["id"]){echo "selected='selected'";}?> value="<?= $allcategories[$key]["id"]; ?>"><?= $allcategories[$key]["name"]; ?></option>
                        <?}?>

                    <?}?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group no-margin">
            <label for="videodesription"  class="control-label">Описание</label>
            <textarea class="form-control autogrow" rows="5" name="videodesription" id="videodesription"><?if(isset($nv))echo $onevideo[0]["desription"];?></textarea>
        </div>
    </div>
</div>


<?if($videoid==0){?>
<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <label class="col-sm-9 control-label">Видео .mp4 <span class="inpreq">*</span>
                <button class="btn btn-default btn-xs popover-default"
                        data-original-title="Требование к видео:"
                        data-content="  1)Формат: mp4;&nbsp;&nbsp;
                                        2)Разрешение: 960x540 пикселей;&nbsp;&nbsp;
                                        3)Оптимальный размер: до 95 МБ
                                        4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                    <i class="fa fa-info"></i>
                </button>
            </label>

            <div class="col-sm-3">

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-info btn-file">
                        <span class="fileinput-new">Выбрать видео</span>
                        <span class="fileinput-exists">Изменить</span>
                        <input type="file" name="tvvideomp4" id="tvvideomp4" >
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>

            </div>


        </div>


    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <label class="col-sm-9 control-label">Видео .webm <span class="inpreq">*</span>
                <button class="btn btn-default btn-xs popover-default"
                        data-original-title="Требование к видео:"
                        data-content="  1)Формат: webm;&nbsp;&nbsp;
                                        2)Разрешение: 960x540 пикселей;&nbsp;&nbsp;
                                        3)Оптимальный размер: до 80 МБ
                                        4)Максимальный размер: <?=ini_get('post_max_size');?>Б"
                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                    <i class="fa fa-info"></i>
                </button>
            </label>

            <div class="col-sm-3">

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-info btn-file">
                        <span class="fileinput-new">Выбрать видео</span>
                        <span class="fileinput-exists">Изменить</span>
                        <input type="file" name="tvvideowebm" id="tvvideowebm">
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <label class="col-sm-9 control-label">Постер <span class="inpreq">*</span>
                <button class="btn btn-default btn-xs popover-default"
                        data-original-title="Требование к постеру:"
                        data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                        2)Разрешение: 640x360 пикселей;&nbsp;&nbsp;"
                        data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                    <i class="fa fa-info"></i>
                </button>
            </label>

            <div class="col-sm-3">

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-info btn-file">
                        <span class="fileinput-new">Выбрать постер</span>
                        <span class="fileinput-exists">Изменить</span>
                        <input type="file" name="tvposter" id="tvposter" >
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                       style="float: none">&times;</a>
                </div>

            </div>

        </div>
    </div>
</div>
<?}else{?>
<br>
<div class="row">

</div>       
<br>
<div class="row">

    <div class="col-md-6">
        <label class="col-sm-9 control-label">Постер <span class="inpreq">*</span>
            <button class="btn btn-default btn-xs popover-default"
                    data-original-title="Требование к постеру:"
                    data-content="  1)Формат: jpg;&nbsp;&nbsp;
                                        2)Разрешение: 259x140 пикселей;&nbsp;&nbsp;"
                    data-placement="top" data-trigger="hover" data-toggle="popover" type="button">
                <i class="fa fa-info"></i>
            </button>
        </label>
        <img src="/photo/tv/<?=$onevideo[0]["videourl"];?>.jpg" alt="" width="259">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-info btn-file">
                <span class="fileinput-new">Выбрать фото</span>
                <span class="fileinput-exists">Изменить</span>
                <input type="file" name="tvposter" id="tvposter">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
               style="float: none">&times;</a>
        </div>
    </div>
    <div class="col-md-6">
        <label class="col-sm-9 control-label">Видео</label><br>
        <div style="margin-top: 10px;">
            <video id="player1" width="260" height="149" controls="controls" preload="none">
                <source src="/video/tv/<?=$onevideo[0]["videourl"];?>.mp4" type="video/mp4" />
                <source src="/video/tv/<?=$onevideo[0]["videourl"];?>.webm" type="video/webm" />
            </video>
        </div>
    </div>
</div>
<input type="hidden" name="videoid" id="videoid" value="<?=$videoid;?>">
<input type="hidden" name="videourl" id="videourl" value="<?=$onevideo[0]["videourl"];?>">
<?}?>

<script type="text/javascript">

    $('video').mediaelementplayer(/* Options */);

    // Datepicker
    if ($.isFunction($.fn.datepicker)) {
        $(".datepicker").each(function (i, el) {
            var $this = $(el),
                opts = {
                    format: attrDefault($this, 'format', 'dd/mm/yyyy'),
                    startDate: attrDefault($this, 'startDate', ''),
                    endDate: attrDefault($this, 'endDate', ''),
                    daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
                    startView: attrDefault($this, 'startView', 0),
                    rtl: rtl()
                },
                $n = $this.next(),
                $p = $this.prev();

            $this.datepicker(opts);

            if ($n.is('.input-group-addon') && $n.has('a')) {
                $n.on('click', function (ev) {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }

            if ($p.is('.input-group-addon') && $p.has('a')) {
                $p.on('click', function (ev) {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }
        });
    }


    // Popovers and tooltips
    $('[data-toggle="popover"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'right'),
            trigger = attrDefault($this, 'trigger', 'click'),
            popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

        $this.popover({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.popover', function(ev)
        {
            var $popover = $this.next();

            $popover.addClass(popover_class);
        });
    });

    $('[data-toggle="tooltip"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'top'),
            trigger = attrDefault($this, 'trigger', 'hover'),
            popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

        $this.tooltip({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.tooltip', function(ev)
        {
            var $tooltip = $this.next();

            $tooltip.addClass(popover_class);
        });
    });






</script>
<?}?>

