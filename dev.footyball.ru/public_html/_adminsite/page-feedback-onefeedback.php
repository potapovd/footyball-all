<?
include_once("lib/cms_view_inc.php");
checkpassandroll("feedback");

if(!isset($_POST["id"])||!isset($_POST["type"])){ die();}


$feedbackid = $_POST["id"];
$feedbacktype = $_POST["type"];

//echo $feedbackid." ".$feedbacktype;
if ($feedbackid > 0) {
    $nv = 1;
    $onefeedback = getonefeedback($feedbackid);
}

if(is_ajax2()){
?>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group no-margin">
                        <label for="videodesription" class="control-label">Фото <span class="inpreq">*</span>
                            <button class="btn btn-default btn-xs popover-default"
                                    data-original-title="Требование к фото:"
                                    data-content="  1)Формат: jpeg;&nbsp;&nbsp;
                                        2)Разрешение: 97x97 пикселей;&nbsp;&nbsp;
                                        4)Максимальный размер: <?= ini_get('post_max_size'); ?>Б"
                                    data-placement="bottom" data-trigger="hover" data-toggle="popover" type="button">
                                <i class="fa fa-info"></i>
                            </button>
                        </label>

                        <div class="fileinput-new thumbnail" data-trigger="fileinput"
                             style="width: 97px; height: 97px;">

                            <?
                            if (isset($nv)) {
                                if ($onefeedback[0]["type"] == 0) {
                                    $url = '/photo/feedback/' . $onefeedback[0]["photo"];
                                } else {
                                    $url = '/video/feedback/' . $onefeedback[0]["photo"];
                                }
                            } else {
                                $url = 'http://placehold.it/97x97';
                            }
                            ?>

                            <img src="<?= $url; ?>" alt="">
                        </div>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-info btn-file">
                                <span class="fileinput-new">Выбрать фото</span>
                                <span class="fileinput-exists">Изменить</span>
                                <input type="file" name="feedbackava" id="feedbackava">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none">&times;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="field-1">Фамилия Имя <span
                                class="inpreq">*</span></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="feedbackfio" id="feedbackfio"
                                   value="<? if (isset($nv)) {
                                       echo $onefeedback[0]["name"];
                                   } ?>">
                        </div>
                    </div>
                </div>
            </div>


            <?
            if (isset($nv)) {
                if ($onefeedback[0]["type"] == 0) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="videodesription" class="control-label">Комментарий</label>
                                <textarea class="form-control autogrow" rows="5" name="feedbacktxt"
                                          id="feedbacktxt"><?= $onefeedback[0]["content"]; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <?
                } else {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="videodesription" class="control-label">Видео</label>
                                <video id="player1" width="360" height="220" controls="controls" preload="none">
                                    <source src="/video/feedback/<?= $onefeedback[0]["content"]; ?>.mp4"
                                            type="video/mp4"/>
                                    <source src="/video/feedback/<?= $onefeedback[0]["content"]; ?>.webm"
                                            type="video/webm"/>
                                </video>
                            </div>
                        </div>
                    </div>
                    <?
                }
            } else {

                if ($feedbacktype == 0) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="videodesription" class="control-label">Комментарий</label>
                                <textarea class="form-control autogrow" rows="5" name="videodesription"
                                          id="videodesription"></textarea>
                            </div>
                        </div>
                    </div>
                    <?
                } else {
                    ?>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-8 control-label">Видео .mp4 <span class="inpreq">*</span>
                                    <button class="btn btn-default btn-xs popover-default"
                                            data-original-title="Требование к видео:"
                                            data-content="  1)Формат: mp4;&nbsp;&nbsp;
                            2)Разрешение: 960x540 пикселей;&nbsp;&nbsp;
                            3)Оптимальный размер: до 95 МБ
                            4)Максимальный размер: <?= ini_get('post_max_size'); ?>Б"
                                            data-placement="top" data-trigger="hover" data-toggle="popover"
                                            type="button">
                                        <i class="fa fa-info"></i>
                                    </button>
                                </label>

                                <div class="col-sm-3">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Выбрать видео</span>
                                            <span class="fileinput-exists">Изменить</span>
                                            <input type="file" name="feedbackvideomp4" id="tvvideowebm">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                           style="float: none">&times;</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="col-sm-8 control-label">Видео .webm <span class="inpreq">*</span>
                                    <button class="btn btn-default btn-xs popover-default"
                                            data-original-title="Требование к видео:"
                                            data-content="  1)Формат: webm;&nbsp;&nbsp;
                                        2)Разрешение: 960x540 пикселей;&nbsp;&nbsp;
                                        3)Оптимальный размер: до 80 МБ
                                        4)Максимальный размер: <?= ini_get('post_max_size'); ?>Б"
                                            data-placement="top" data-trigger="hover" data-toggle="popover"
                                            type="button">
                                        <i class="fa fa-info"></i>
                                    </button>
                                </label>

                                <div class="col-sm-3">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Выбрать видео</span>
                                            <span class="fileinput-exists">Изменить</span>
                                            <input type="file" name="feedbackvideowebm" id="tvvideowebm">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                           style="float: none">&times;</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <?
                }


            }
            ?>


        </div>
    </div>
    <input type="hidden" name="feedbackid" id="feedbackid" value="<?= $feedbackid; ?>">
    <input type="hidden" name="feedbacktype" id="feedbacktype" value="<?= $feedbacktype; ?>">
    <?if(isset($nv)){?>
        <input type="hidden" name="feedbacavaname" id="feedbacavaname" value="<?=$onefeedback[0]["photo"]; ?>">
    <?}?>

</div>
</div>
</div>

<script type="text/javascript">

    $('video').mediaelementplayer(/* Options */);


    // Popovers and tooltips
    $('[data-toggle="popover"]').each(function (i, el) {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'right'),
            trigger = attrDefault($this, 'trigger', 'click'),
            popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

        $this.popover({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.popover', function (ev) {
            var $popover = $this.next();

            $popover.addClass(popover_class);
        });
    });

    $('[data-toggle="tooltip"]').each(function (i, el) {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'top'),
            trigger = attrDefault($this, 'trigger', 'hover'),
            popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

        $this.tooltip({
            placement: placement,
            trigger: trigger
        });

        $this.on('shown.bs.tooltip', function (ev) {
            var $tooltip = $this.next();

            $tooltip.addClass(popover_class);
        });
    });


</script>
<?}?>