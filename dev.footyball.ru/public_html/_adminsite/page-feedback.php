<?
include_once("lib/cms_view_inc.php");
checkpassandroll("feedback");


$feetbacks = getrandomfeetback(1);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/css/mediaelementplayer.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>
            Отзывы
            <div class="btn-group">
                <button class="btn btn-success dropdown-toggle " data-toggle="dropdown" type="button"><i class="fa fa-plus"></i></button>
                <ul role="menu" class="dropdown-menu dropdown-green">
                    <li><a href="#" onclick="showAjaxModal(0,1)">Видео отзыв</a> </li>
                    <li><a href="#" onclick="showAjaxModal(0,0)">Текстовый отзыв</a> </li>
                </ul>
            </div>
        </h2>



            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover datatable" id="tablefeedback">
                        <thead>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-7">Имя</th>
                            <th class="col-md-2">Тип</th>
                            <th class="col-md-2">Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        $i = 1;
                        foreach ($feetbacks as $key => $val) {
                            ?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td><?= $feetbacks[$key]["name"]; ?></td>
                                <td>
                                    <? if( $feetbacks[$key]["type"] == 1){ ?>
                                        <i class="fa fa-file-video-o"></i>
                                    <?}?>
                                </td>
                                <td>
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-white tooltip-default" data-original-title="Редактировать" title="" data-placement="bottom" data-toggle="tooltip">
                                            <a href="#sample-modal" data-toggle="modal"
                                               data-target="#addedivdideo" onclick="showAjaxModal(<?=$feetbacks[$key]["id"];?>,<?=$feetbacks[$key]["type"];?>)" class="bg">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </label>
                                        <label class="btn btn-white tooltip-default" data-original-title="Удалить" title="" data-placement="bottom" data-toggle="tooltip">
                                            <a href="#" class="videoline" onclick="removevideos('removefeetback',<?=$feetbacks[$key]["id"];?>)" data-feetbackid ="<?=$feetbacks[$key]["id"];?>" data-rel="collapse">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <?
                            $i++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-7">Имя</th>
                            <th class="col-md-2">Тип</th>
                            <th class="col-md-2">Действие</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>


            <div class="btn-group">
                <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Добавить отзыв <i class="fa fa-plus"></i>       </button>
                <ul role="menu" class="dropdown-menu dropdown-green">
                    <li><a href="#" onclick="showAjaxModal(0,1)">Видео отзыв</a> </li>
                    <li><a href="#" onclick="showAjaxModal(0,0)">Текстовый отзыв</a> </li>
                </ul>
            </div>




            <!-- Sample Modal (Default skin) -->
            <div class="modal fade" id="addedifeedback">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Отзыв</h4>
                        </div>

                        <div class="modal-body"> <h3>Загрузка...</h3> </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            <button type="button" class="btn btn-success btn-icon" id="savefeedinfo">Сохранить <i class="fa fa-floppy-o right"></i></button>
                        </div>
                    </div>
                </div>
            </div>


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <script src="assets/js/mediaelement-and-player.min.js"></script>

    <script type="text/javascript">

        $("#logout").on("click",function() {
            $.post( "lib/cms_update_inc.php",{formname: "logout"});
            //return false;
        });


        function showAjaxModal(id,type){
            var modalvars = "id="+id+"&type="+type;
            //alert (videoid);
            $('#addedifeedback').modal('show', {backdrop: 'static'});
            $.ajax({
                url: "page-feedback-onefeedback.php",
                type: "POST",
                async: false,
                cache: false,
                data:modalvars,
                success: function(response)
                {
                    $('#addedifeedback .modal-body').html(response);
                }
            });
            return false;
        }
        $("#savefeedinfo").on("click",function() {
            //var namevideo = $("#datevideo").val()
            //alert("namevideo: "+namevideo);

            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };


            //редактирование
            if($('#feedbackid').val()>0){
                //текст
                if($('#feedbacktype').val()==0){
                    //alert("update text");

                    if ( $("#feedbackfio").val()!==""){
                        var formData = new FormData();
                            formData.append('formname', 'updatefeedback');
                            formData.append('feedbackid', $("#feedbackid").val());
                            formData.append('feedbackava', $('input[name=feedbackava]')[0].files[0]);
                            formData.append('feedbackfio', $("#feedbackfio").val());
                            formData.append('feedbacavaname', $("#feedbacavaname").val());
                            formData.append('feedbacktxt', $("#feedbacktxt").val());
                            formData.append('feedbacktype', $("#feedbacktype").val());
                    }else{
                        toastr.error('Ошибка! Фамилия Имя должно быть заполнено');
                        return false;
                    }



                //видео
                }else{
                    //alert("update video");

                    if ( $("#feedbackfio").val()!==""){
                        var formData = new FormData();
                            formData.append('formname', 'updatefeedback');
                            formData.append('feedbackid', $("#feedbackid").val());
                            formData.append('feedbackava', $('input[name=feedbackava]')[0].files[0]);
                            formData.append('feedbackfio', $("#feedbackfio").val());
                            formData.append('feedbacktype', $("#feedbacktype").val());
                            formData.append('feedbacavaname', $("#feedbacavaname").val());
                    }else{
                        toastr.error('Ошибка! Фамилия Имя должно быть заполнено');
                        return false;
                    }

                }


            //новый
            }else{
                //текст
                if($('#feedbacktype').val()==0){
                    //alert("add text");

                    if ( $("#feedbackfio").val()!==""&& $('input[name=feedbackava]')[0].files[0]!==undefined){
                        var formData = new FormData();
                            formData.append('formname', 'addfeedback');
                            formData.append('feedbackava', $('input[name=feedbackava]')[0].files[0]);
                            formData.append('feedbackfio', $("#feedbackfio").val());
                            formData.append('feedbacktxt', $("#feedbacktxt").val());
                            formData.append('feedbacktype', $("#feedbacktype").val());
                    }else{
                        toastr.error('Ошибка! Фамилия Имя и Фото должно быть заполнено');
                        return false;
                    }


                //видео
                }else{
                    //alert("add video");

                    if ( $("#feedbackfio").val()!==""&& $('input[name=feedbackvideomp4]')[0].files[0]!==undefined && $('input[name=feedbackvideowebm]')[0].files[0]!==undefined && $('input[name=feedbackava]')[0].files[0]!==undefined){
                        var formData = new FormData();
                        formData.append('formname', 'addfeedback');
                        formData.append('feedbackava', $('input[name=feedbackava]')[0].files[0]);
                        formData.append('feedbackfio', $("#feedbackfio").val());
                        formData.append('feedbacktype', $("#feedbacktype").val());
                        formData.append('feedbackvideomp4', $('input[name=feedbackvideomp4]')[0].files[0]);
                        formData.append('feedbackvideowebm', $('input[name=feedbackvideowebm]')[0].files[0]);

                    }else{
                        toastr.error('Ошибка! Фамилия Имя, Фото и Видео должно быть заполнено');
                        return false;
                    }

                }
            }


            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                //enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                success: function (results) {
                    if (results["type"] == 'error') {
                        toastr.error(results["text"], optstoastr);

                    } else {
                        toastr.success(results["text"], optstoastr);
                        //$("#tablefeedback tr:first").after("<tr><td>0</td><td>"+$("#feedbackfio").val()+"</td><td></td></tr>");

                        $('#addedifeedback').modal('hide');
                    }
                },
                error: function (results) {
                    toastr.error('Ошибка! ' + results["responseText"]);
                },
                complete: function () {
                }
            });



        });



        function removevideos(object,id){
            var formData = {
                'object' : object,
                'id' : id,
                'formname' : "removeelement"
            };


            var optstoastr = {
                "closeButton": false,
                "debug": false,
                "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
                "toastClass": "black",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajax({
                url: "lib/cms_update_inc.php",
                type: "POST",
                data: formData,
                dataType: 'json',
                async: false,
                cache: false,
                success: function(results) {
                    //var posts = JSON.parse(results);
                    if(results["type"] == 'error'){
                        toastr.error(results["text"], optstoastr);

                    }else{
                        toastr.success(results["text"], optstoastr);
                        $("* [data-feetbackid='"+id+"']").parent().parent().parent().parent().slideUp("slow");

                    }
                },
                error: function(results) {
                    toastr.error('Ошибка! '+results["responseText"]);
                    //console.log(results);
                }
            });


            return false;

        }


        $(document).ready(function ($) {
            $("#mainmenufeedback").addClass("active");
        });




    </script>

</div>
</body>
</html>