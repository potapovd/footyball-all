<?
include_once("lib/cms_view_inc.php");
checkpassandroll("debug");


$debuginfo = getdebuginfo();

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <? include_once("inc/head.inc.php"); ?>
    <link rel="stylesheet" href="assets/css/mediaelementplayer.css">
</head>
<body class="page-body  page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu">
        <? include_once("inc/sidebar.inc.php"); ?>
    </div>
    <div class="main-content">
        <div class="row">
            <? include_once("inc/topmenu.inc.php"); ?>
        </div>
        <hr/>
        <h2>Отладочная информация</h2>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover datatable" id="tabledebug">
                    <thead>
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-2">Имя</th>
                        <th class="col-md-2">Дата</th>
                        <th class="col-md-7">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $i = 1;
                    foreach ($debuginfo as $key => $val) {
                        ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $debuginfo[$key]["user"]; ?></td>
                            <td><?= $debuginfo[$key]["date"]; ?></td>
                            <td><?= $debuginfo[$key]["action"]; ?></td>
                        </tr>
                        <?
                        $i++;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-2">Имя</th>
                            <th class="col-md-2">Дата</th>
                            <th class="col-md-7">Действие</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>


        <button class="btn btn-success btn-icon" id="remobedebug" type="button"> Очистить <i class="fa fa-remove"></i>
        </button>





        <!-- Sample Modal (Default skin) -->
        <!--<div class="modal fade" id="addedifeedback">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Отзыв</h4>
                    </div>

                    <div class="modal-body"> <h3>Загрузка...</h3> </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-icon" id="savefeedinfo">Сохранить <i class="fa fa-floppy-o right"></i></button>
                    </div>
                </div>
            </div>
        </div>-->


        <footer class="main">
            <? include_once("inc/footer.inc.php"); ?>
        </footer>
    </div>
    <? include_once("inc/js.inc.php"); ?>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <script src="assets/js/mediaelement-and-player.min.js"></script>

    <script type="text/javascript">


        $("#remobedebug").on("click", function () {
            $.post("lib/cms_update_inc.php", {formname: "cleardebug"});
        });


        $(document).ready(function ($) {
            $("#mainmenudebug").addClass("active");
        });


    </script>

</div>
</body>
</html>