<div class="col-md-6 col-sm-8 clearfix">
    <ul class="user-info pull-left pull-none-xsm">
        <li class="profile-info dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="showAjaxModalUserinfo('<?=$_SESSION["id"];?>')">
                <img src="assets/images/no-avatar_male.jpg" alt="" class="img-circle" width="44" /> <?=$_SESSION["id"];?>
            </a>
        </li>
    </ul>
</div>

<div class="col-md-6 col-sm-4 clearfix hidden-xs">
    <ul class="list-inline links-list pull-right">
        <li>
            <a href="login.php" id="logout" class="btn btn-red btn-icon">Выход <i class="fa fa-sign-out right"></i></a>
        </li>
    </ul>
</div>