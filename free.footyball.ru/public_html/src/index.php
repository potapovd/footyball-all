<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Сеть футбольных клубов для дошкольников № 1</title>
  <meta name="keywords" content="футбол для детей от 3 лет, футбол для детей, футбольные клубы для детей в москве, детская футбольная школа, детские футбольные школы москвы, футбол для детей от 3, футбольный клуб для детей в москве, детские футбольные клубы москвы, детский футбольный клуб, детский футбол в москве"/>
	<meta name="description" content="Сеть футбольных клубов для детей от 0 до 7 лет в Москве - запись на бесплатное занятие. "/>

  <link rel="stylesheet" href="build/css/style.min.css" />
  <link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<meta name="author" content="PotapovD" />
  <meta name="yandex-verification" content="a2cb9c45aa8dd6e5" />
  <meta name="google-site-verification" content="cN_nSeAYvAkNfSks07bQ85_h5k6bupZVjEX3v1yyq_U" />
	<meta name="theme-color" content="#0065b3">
	<meta name="msapplication-navbutton-color" content="#0065b3">
	<meta name="apple-mobile-web-app-status-bar-style" content="#0065b3">
</head>

<body>

	<!-- Nav -->
  <nav>
    <div class="wrapper">
      <div class="logo">
        <img src="build/img/logo.svg" alt="">
        <p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
      </div>
      <div class="menu">
        <a class="menu__button"><hr><hr><hr></a>
        <ul class="menu__list">
          <!--<li><a class="scrollto" href="#prog">Программы</a></li>-->
          <li><a class="scrollto" href="#about">О нас</a></li>
          <li><a class="scrollto" href="#comments">Отзывы</a></li>
          <li><a class="scrollto" href="#video">Видео</a></li>
          <li><a class="scrollto" href="#adr">Адреса центров</a></li>
        </ul>
      </div>
      <!--<a href="#callme" class="callme fancy">Записаться на<br> бесплатное занятие</a>-->
      <a href="#callme" class="callme fancy">Записаться на <br> бесплатный праздник</a>
    </div>
  </nav>
  <!-- //Nav -->
  <!-- Header -->  
  <header>
    <div class="box">
      <img class="icon" src="build/img/loggo.png" alt="">
	    <!--<p class="num">+7(495)-191-17-01&nbsp;</p>-->
      <p class="text"><!--Узнайте, насколько подходят <br>занятия футболом вашему сыну--> &nbsp;</p>
      <hr class="line">
	    <!--<h1>Футбольный клуб для мальчиков<br>&nbsp; от 2 до 7 лет&nbsp;<br>с профессиональными тренерами!</h1><br>-->
	    <h1>Запишитесь на детский футбольный праздник 27-28 января!</h1><br>
	    <ul class="advantage">
		    <li class="advantagepic1">основы футбола<br> в формате сказки</li>
		    <li class="advantagepic2">проводят праздник<br> 2 тренера - педагога</li>
		    <li class="advantagepic3">Продолжительность <br> 60 минут</li>
		    <li class="advantagepic4">Незабываемые эмоции<br> бесплатно</li>
	    </ul>
	    <!--<p class="text--2">Осталось всего <b>5</b> бесплатных мест на следующую тренировку</p>-->
      <a href="#callme" class="try fancy"><div>Записаться на бесплатный <br> футбольный праздник</div></a>
    </div>
  </header>
  <!-- //Header -->

  <!-- Block7 -->
  <section class="b7 dynamic addback" id="comments">
    <div class="wrapper">
      <h2>Отзывы родителей</h2>
      <p class="subtitle">Успешные и состоятельные люди выбирают для своих детей Footyball</p>
      <div class="slider">
        <div class="item">
          <div class="im"> <iframe  src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/Ig1wrijKpMc" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="cont">
            <p class="text"> Гарик Мартиросян отдал своего сына Даниэля в FootyBall. Успехи маленького Чемпиона и отзыв звездного папы в нашем видеосюжете. </p>
            <p class="title">Гарик Мартиросян</p>
            <p class="desc">Ведущий, продюсер телевизионных проектов</p>
          </div>
        </div>
        <div class="item">
          <div class="im"><iframe  src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/-0OZGSzx1Jk" frameborder="0" allowfullscreen></iframe></div>
          <div class="cont">
            <p class="text"> В Footyball пополнение. Актеры Денис и Ирина Никифоровы привели в школу своего сына Сашу. Юному чемпиону 2,5 года, но он уже стремиться забить как можно больше голов </p>
            <p class="title">Денис и Ирина Никифоровы</p>
            <p class="desc">Актеры</p>
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe  src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/wki-MzGPIZ4" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text">  Футибол принял в свои ряды Мирослава Козлова. Сына известного футболиста, игрока московского "Динамо" и сборной России, Алексея Козлова. </p>
            <p class="title">Алексей Козлов</p>
            <p class="desc">Футболист "Динамо"</p>
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe   src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/sbxJTCOnkK4" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text"> В Футибол приходят чемпионы! Трехкратная олимпийская чемпионка по синхронному плаванию Наталья Ищенко с сыном Семеном на тренировке Footyball  </p>
            <p class="title">Наталья Ищенко</p>
            <p class="desc">Олимпийская чемпионка по синхронному плаванию</p>
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe   src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/DXF09OgKn8U" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text"> Кем мечтает стать сын миссис мира 2015 Марины Алексейчик?! И что он любит больше всего в футболе?! Расскажет и сам покажет Даниил Алексейчик </p>
            <p class="title">Марина Алексейчик</p>
            <p class="desc">Мисс мира</p>
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe   src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/qJzZFwkhNA0" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text">Сын игрока московского "Спартака" Александра Зотова продолжает осваивать футбольное мастерство в FootyBall. Тимофею 3,5 года и он уже стремится в группу PROFI. То ли еще будет! </p>
            <p class="title">Александр Зотов</p>
            <p class="desc">Футболист "Спартака"</p>
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe class="dynamic" src="build/img/1.gif" data-src="https://www.youtube.com/embed/gnTxNO4OU8Y" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text">Прабабушка юного чемпиона Влада Герасимова рассказывает о том, как FootyBall меняет представление о футболе </p>
            <!-- <p class="title">Марина Алексейчик</p>
            <p class="desc">Мисс мира</p> -->
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe class="dynamic" src="build/img/1.gif" data-src="https://www.youtube.com/embed/WNltdgrtrU8" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text">Отзыв дедушки юного чемпиона: "Полгода занимается, стал меньше болеть. Над техникой работает, удары пошли хорошо.Тут все поставлено на высоком уровне. Говорю как бывший футболист."</p>
            <!-- <p class="title">Марина Алексейчик</p>
            <p class="desc">Мисс мира</p> -->
          </div>
        </div>
        <div class="item">
          <div class="im"> <iframe class="dynamic" src="build/img/1.gif" data-src="https://www.youtube.com/embed/Rtm5OM79eow" frameborder="0" allowfullscreen></iframe> </div>
          <div class="cont">
            <p class="text">Два в одном - футбол и английский язык. Позитивное настроение и отношение ребенка. В прошлом году разыгрывали iPhone и наш ребенок выиграл, очень приятно!</p>
            <!-- <p class="title">Марина Алексейчик</p>
            <p class="desc">Мисс мира</p> -->
          </div>
        </div>
      </div>
      <div class="photos">
        <a data-pos="0"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/Ig1wrijKpMc/mqdefault.jpg" alt=""> </a>
        <a data-pos="1"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/-0OZGSzx1Jk/mqdefault.jpg" alt=""> </a>
        <a data-pos="2"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/wki-MzGPIZ4/mqdefault.jpg" alt=""> </a>
        <a data-pos="3"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/sbxJTCOnkK4/mqdefault.jpg" alt=""> </a>
        <a data-pos="4"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/DXF09OgKn8U/mqdefault.jpg" alt=""> </a>
        <a data-pos="5"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/qJzZFwkhNA0/mqdefault.jpg" alt=""> </a>
        <a data-pos="6"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/gnTxNO4OU8Y/mqdefault.jpg" alt=""> </a>
        <a data-pos="7"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/WNltdgrtrU8/mqdefault.jpg" alt=""> </a>
        <a data-pos="8"> <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/Rtm5OM79eow/mqdefault.jpg" alt=""> </a>
      </div>
    </div>
  </section>
  <!-- //Block7 -->

  <!-- Block1 -->
  <section class="b1">
    <div class="wrapper">
      <!--<h2>Что ваша семья получает<br>на бесплатном 60 минутном занятии</h2>-->
      <h2>Что ваша семья получает<br>на бесплатном футбольном празднике</h2>
      <p class="text">Нет причин отказать в этом опыте вашему сыну</p>
      <ul>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im1.jpg" alt=""></div>
          <p class="title">Насыщенная программа тренировки</p>
          <p class="text">Все упражнения полностью безопасны<br> даже для самых маленьких детей.</p>
        </li>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im2.jpg" alt=""></div>
          <p class="title">Удобная парковка</p>
          <p class="text">Мы предусмотрели все мелочи,<br> чтобы вам было комфортно.</p>
        </li>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im3.jpg" alt=""></div>
          <p class="title">Вкусное фут-кафе<br> с детским меню</p>
          <p class="text">Правильное питание и возможность<br> увидеть трансляции с полей.</p>
        </li>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im4.jpg" alt=""></div>
          <p class="title">Бесплатная игровая комната</p>
          <p class="text">С аниматором для младших<br> братьев и сестер.</p>
        </li>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im5.jpg" alt=""></div>
          <p class="title">Тренировку ведет 2<br>и более тренеров </p>
          <p class="text">Максимальное качество и<br> эффективность тренировки.</p>
        </li>
        <li>
          <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/b1_im6.jpg" alt=""></div>
          <p class="title">Ребенок ощущает себя<br>настоящим футболистом</p>
          <p class="text">Футбольная форма выдается<br>на 1й тренировке.</p>
        </li>
      </ul>
      <p class="desc">Благодаря этой тренировке вы отлично проведете время<br> и поймете отношение вашего сына к футболу</p>
    </div>
  </section>
  <!-- //Block1 -->
  <!-- Exclusive -->
  <section class="exclusive">
    <div class="wrapper">
      <div class="im"><img class="dynamic" src="build/img/1.gif" data-src="build/img/price.png" alt=""></div>
      <div class="box">
        <p class="title">Бесплатный эксклюзивный 60-ти минутный праздник "Разбуди в сыне победителя"</p>
        <p class="text">Мы подберем вам удобное время. С собой нужно<br> принести только обувь и хорошее настроение!</p>
      </div>
      <a href="#callme" class="free fancy">Записаться на праздник</a>
<!-- <a href="#try" class="free fancy">Записаться на бесплатную<br> программу</a>---->
    </div>
  </section>
  <!-- //Exclusive -->
  <!-- Block2 -->
  <section class="b2">
    <div class="wrapper">
      <h2>Почему тренер Сборной России по футболу<br> отдал своего внука в Footyball</h2>
      <div class="box">
        <div class="left">
          <iframe src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/qp0Vx3RsycM" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="right">
          <p class="title">Footyball - это будущее<br> Российского футбола </p>
          <p class="text">Валерий Георгиевич уверен в том, что малыш продолжит традиции своей футбольной семьи. Посмотрев тренировки в клубе, он сделал выводы о том, что у наших воспитанников есть все шансы стать настоящими профессиональными футболистами! Главное, верить в себя, тренироваться, идти вперед и не останавливаться на достигнутом.</p>
          <p class="text">В свою очередь, мы благодарим семью Газзаевых за выбор и доверие!</p>
        </div>
      </div>
    </div>
  </section>
  <!-- //Block2 -->
  <!-- Block3 -->
  <!--<section class="b3 addback dynamic" id="prog">
    <div class="wrapper">
      <h2>Развитие лидерских качеств Вашего сына<br> благодаря уникальной программе</h2>
      <p class="text">Успей воспитать сына победителем!</p>
      <ul>
        <li>
          <a href="#callme" class="callme fancy"><img src="build/img/1.gif" class="dynamic" data-src="build/img/p1.svg" alt=""></a>
          <p class="title">Разбуди победителя</p>
          <p class="text--2">Бесплатная 60 минутная программа<br> для ознакомления</p>
        </li>
        <li>
          <a href="#callme" class="callme fancy"><img src="build/img/1.gif" class="dynamic" data-src="build/img/p2.svg" alt=""></a>
          <p class="title">Старт победителя</p>
          <p class="text--2"><b>Программа из 2 занятий</b> для<br> адаптации и более глубокого<br> погружения в процесс </p>
        </li>
        <li>
          <a href="#callme" class="callme fancy"><img src="build/img/1.gif" class="dynamic" data-src="build/img/p3.svg" alt=""></a>
          <p class="title">Фундамент победителя</p>
          <p class="text--2"><b>Основная программа<br> из 80 занятий</b> для тренировки<br> 8 качеств лидера </p>
        </li>
        <li>
          <a href="#callme" class="callme fancy"><img src="build/img/1.gif" class="dynamic" data-src="build/img/p4.svg" alt=""></a>
          <p class="title">Путь победителя</p>
          <p class="text--2"><b>Программа формирует<br> профессиональные футбольные<br> навыки</b> и развивает умение<br> быстро думать и принимать<br> решение</p>
        </li>
        <li>
          <a href="#callme" class="callme fancy"><img src="build/img/1.gif" class="dynamic" data-src="build/img/p5.svg" alt=""></a>
          <p class="title">PROFI - Футбольная<br> карьера</p>
          <p class="text--2">Хотите вырастить из сына<br> <b>профессионального футболиста</b><br> - эта программа для самых<br> способных детей!</p>
        </li>
      </ul>
    </div>
  </section>-->
  <!-- //Block3 -->
  <!-- Exclusive -->
  <section class="exclusive">
    <div class="wrapper">
      <div class="im">
	      <img src="build/img/1.gif" class="dynamic" data-src="build/img/price.png" alt="">
      </div>
      <div class="box">
        <p class="title">Бесплатный эксклюзивный 60-ти минутный праздник "Разбуди в сыне победителя"</p>
        <p class="text">Мы подберем вам удобное время. С собой нужно<br> принести только обувь и хорошее настроение!</p>
      </div>
      <a href="#callme" class="free fancy">Записаться на праздник</a>
    </div>
  </section>
  <!-- //Exclusive -->
  <!-- Block4 -->
  <section class="b4">
    <div class="wrapper">
      <h2>Как проходят занятия</h2>
      <ul class="dynamic addback">
        <li>
          <div class="im"></div>
          <p class="text">Длительность<br> от 40-60 минут </p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Занятия в мини группах<br> по 6-10 человек</p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">2 раза в неделю в удобные<br> для вас дни</p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">15 залов по Москве -<br> места хватит всем!</p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Постоянная медицинская<br> поддержка - на месте<br> дежурит врач </p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Мини сессии английского языка в игровой форме </p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Тренировки открыты для<br> родителей - вы можете<br> наблюдать за ходом<br> тренировки из ФутКафе</p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Летом и зимой - в залах<br> и на открытых площадках</p>
        </li>
        <li>
          <div class="im"></div>
          <p class="text">Упражнения безопасны для<br> самых маленьких детей</p>
        </li>
      </ul>
      <p class="desc">Перед занятием ребенку выдается все необходимое - форма, дневник успеха. <br> С собой необходимо принести хорошее настроение!</p>
    </div>
  </section>
  <!-- //Block4 -->
  <!-- Block5 -->
  <section class="b5 dynamic addback" id="about">
    <div class="wrapper">
      <h2>Узнайте почему дети бегут на тренировку в Footyball?</h2>
      <div class="box">
        <div class="left">
          <iframe src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/_w3Mb0-QrB0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="right">
          <ul class="dynamic addback">
            <li>
              <div class="im"></div>
              <p class="title">Дружелюбная атмосфера</p>
              <p class="text">Приходите и почувствуйте сами - <br> мы всегда рады гостям! </p>
            </li>
            <li>
              <div class="im"></div>
              <p class="title">Приятные тренеры</p>
              <p class="text">Наши тренеры очень любят детей<br> и помогают им раскрыть себя </p>
            </li>
            <li>
              <div class="im"></div>
              <p class="title">Возможность проявить себя</p>
              <p class="text">Каждый гол - это момент счастья для ребенка. Это возможность проявить себя! </p>
            </li>
          </ul>
          <a href="#callme" class="see fancy">Увидеть тренировку<br> своими глазами</a>
        </div>
      </div>
    </div>
  </section>
  <!-- //Block5 -->
  <!-- Block6 -->
  <section class="b6 dynamic addback">
    <div class="wrapper">
      <h2>Footyball - это больше чем футбол!</h2>
      <p class="subtitle">Успей воспитать сына победителем!</p>
      <div class="box">
        <ul class="left">
          <li>
            <p>Footyball - это здоровье, успех и социальное превосходство вашего<br> сына в течении всей жизни</p>
          </li>
          <li>
            <p>Развитие лидерских качеств - <br> фундамент развития победителей</p>
          </li>
          <li>
            <p>Мы гарантируем максимальное внимание и индивидуальный подход</p>
          </li>
          <li>
            <p>Изучение английского языка<br> в игровой форме</p>
          </li>
          <li>
            <p>Индивидуальные консультации<br> о физическом и моральном<br> состоянии вашего сына</p>
          </li>
        </ul>
        <div class="right">
          <div class="video">
            <iframe src="build/img/1.gif" class="dynamic" data-src="https://www.youtube.com/embed/fCr4xRXumic" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="ib">
            <div class="fl">
              <p class="text">Вот что говорят наши тренеры о философии Footyball </p>
            </div>
            <!--<div class="fr">
              <a href="#cons" class="consult fancy">Получить консультацию<br> тренера</a>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- //Block6 -->
  <!-- Block8 -->
  <section class="b8" id="video">
    <div class="wrapper">
      <h2>Видео с наших мероприятий</h2>
      <p class="subtitle">В клубе проводится масса мероприятий - дни рождения, праздники, конкурсы</p>
      <div class="slider">
        <div class="item">
          <p class="title">Максим и Артем</p>
          <a data-id="cUFmpclGVuw" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/cUFmpclGVuw/mqdefault.jpg" alt="">
          </a>
          <p class="text"> Очередная порция красивых голов и хитроумных комбинаций в исполнении PROFI футболистов. В этот раз сошлись команды тренеров Максима и Артема 8:3 </p>
        </div>
        <div class="item">
          <p class="title">Елка</p>
          <a data-id="0we5FxHsn7M" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/0we5FxHsn7M/mqdefault.jpg" alt="">
          </a>
          <p class="text">Был или не был пенальти?! Один из главных вопросов, который повис в воздухе во время матча команд профи. Окончательный ответ на него дает видеоповтор от Footy TV</p>
        </div>
        <div class="item">
          <p class="title"> Матч года. Декабрь. Профи.</p>
          <a data-id="nDM52LogDiQ" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/nDM52LogDiQ/mqdefault.jpg" alt="">
          </a>
          <p class="text">Это была славная футбольная битва. Первый показательный матч между командами профи завершен. Победу праздновали красные 13:11. Все самые интересные моменты смотрите в специальном видеоотчете.</p>
        </div>
        <div class="item">
          <p class="title">Футбольные блюда.</p>
          <a data-id="B_iHpfbdiq8" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/B_iHpfbdiq8/mqdefault.jpg" alt="">
          </a>
          <p class="text"> Бесподобные медальоны победителей, восхитительные фанатские сырники, неповторимые судейские сендвичи. Список меню футикафе можно перечислять вечно. Но лучше все увидеть вживую. А еще лучше съесть! </p>
        </div>
        <div class="item">
          <p class="title">"Футибол" в Перово 1 год.</p>
          <a data-id="7X0bHrJka3g" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/7X0bHrJka3g/mqdefault.jpg" alt="">
          </a>
          <p class="text">Шарики, конфетки и улыбки! Школа "Футибол" в Перово отпраздновала свой первый день рождения. За год у нас появилось много друзей и воспитанников. О тех, кому стал близок футбол в стенах Футибола наш ролик</p>
        </div>
        <div class="item">
          <p class="title">Матч лета</p>
          <a data-id="JyKy1GWLPrc" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/JyKy1GWLPrc/mqdefault.jpg" alt="">
          </a>
          <p class="text"> Для тех кто пропустил матч лета! Папы vs тренеры. Голы, дриблинги, столкновения. Все самое интересное что было в игре в нашем видеоотчете </p>
        </div>
        <div class="item">
          <p class="title">10.000 чемпион</p>
          <a data-id="TLSDbJojPV0" class="im tube">
            <img src="build/img/1.gif" class="dynamic" data-src="http://img.youtube.com/vi/TLSDbJojPV0/mqdefault.jpg" alt="">
          </a>
          <p class="text">Что будет если вовремя придти на тренировку в FootyBall?! Смотрите ролик и узнаете)</p>
        </div>
      </div>
    </div>
  </section>
  <!-- //Block8 -->
  <!-- Block9 -->
  <section class="b9" id="adr">
    <div class="adress">
      <div class="top">
        <h2>Адреса клубов</h2>
        <!--<p class="num">Позвоните нам: <b>+7(495)-191-17-01</b></p>-->
      </div>
      <div class="slider">
        <div class="item">
          <div class="box">
            <div class="oct">1</div>
            <p class="title">Футбольный клуб "Парк Победы"</p>
            <p class="adr"> Кутузовский проспект, 36, стр 51, г. Москва </p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
        <div class="item">
          <div class="box">
            <div class="oct">2</div>
            <p class="title">Футбольный клуб "Перово"</p>
            <p class="adr">1й проезд Перова поля, д. 9 стр. 5, г. Москва</p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
		    <div class="item">
          <div class="box">
            <div class="oct">3</div>
            <p class="title">Футбольный клуб "Белорусский"</p>
            <p class="adr"> 
              ул. Правды, 24, стр.8, г. Москва 
            </p>
            <p>Заезд с ул. 5-ого Ямского поля. Через КПП №5". Мы находимся возле "Бодрый день" и кафе "Рецептор"</p>
            <p class="graph"> Пн-Вс 8-00 - 22-00</p>
          </div>
        </div>
        <div class="item">
          <div class="box">
            <div class="oct">4</div>
            <p class="title">Футбольный клуб "Ленинский"</p>
            <p class="adr">5-й Донской проезд 15, стр. 5, 11 подъезд, г. Москва</p>
            <p class="graph">Пн-Вс 8:00-22:00</p>
          </div>
        </div>
      </div>
    </div>
    <div id="map"></div>
  </section>
  <!-- //Block9 -->
  <!-- Block10 -->
  <section class="b10 dynamic addback">
    <div class="wrapper">
      <div class="im"><img src="build/img/1.gif" class="dynamic" data-src="build/img/cup1.png" alt=""></div>
      <div class="text">
        <p>Мы не для всех - для тех, кто по<br> настоящему любит своих детей</p>
      </div>
      <a href="#callme" class="call fancy">Вы с нами?</a>
    </div>
  </section>
  <!-- //Block10 -->
  <!-- Footer -->
  <footer>
    <div class="wrapper">
      <div class="logo">
        <img class="dynamic" src="build/img/1.gif" data-src="build/img/logo.svg" alt="">
        <p class="logo__text">СЕТЬ ФУТБОЛЬНЫХ КЛУБОВ<br> ДЛЯ ДОШКОЛЬНИКОВ №1 </p>
      </div>
      <div class="polit">
        <a href="#polit" class="fancy">Политика конфиденциальности</a>
		<p>ОГРН: 1147746697119</p>
      </div>
      <div class="cont">
        <!--<p class="cont__text">Позвоните нам</p>
        <p class="cont__num">+7(495)-191-17-01</p>-->
      </div>
    </div>
  </footer>
  <!-- //Footer -->
  <!-- Popups -->
  <div class="hide">

    <!--
    <div id="try">
      <h2> Успей воспитать сына победителем </h2>
      <p class="subtitle">От 2 до 7 лет</p>
      <div class="box">
        <div class="left">
          <p class="title">Разбуди в сыне победителя</p>
          <p class="text">Это бесплатная 60 минутная программа, настоящий футбольный праздник для ребенка и его родитетелей. Занятия ведут 2 тренера, в программу включены спортивные упражнения в игровом формате и ознаколение ребенка с футболом.</p>
          <ul>
            <li>
              <div class="im"></div>
              <p>Уникальная<br> методика</p>
            </li>
            <li>
              <div class="im"></div>
              <p>Знакомство<br> с спортом</p>
            </li>
            <li>
              <div class="im"></div>
              <p>Приветливые<br> тренеры</p>
            </li>
            <li>
              <div class="im"></div>
              <p>Профессиональное<br> оборудование</p>
            </li>
            <li>
              <div class="im"></div>
              <p>Современная<br> школа</p>
            </li>
            <li>
              <div class="im"></div>
              <p>Абсолютно<br>бесплатно</p>
            </li>
          </ul>
        </div>
        <div class="right">
          <p class="title">Запишитесь на бесплатную<br> пробную тренировку</p>
          <div class="text">в удобное время</div>

          <form class="feedback" action="https://salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
			     <input type="hidden" name="lead_source" value="таргет">
            <input type="hidden" name="oid" value="00D20000000onaw">
            <input type="hidden" name="retURL" value="http://free.footyball.ru/index.php#thanks">
            <input  id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="Имя ребенка"/><br>
            <!-- <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" placeholder="Фамилия ребенка"/><br>
            <input  id="email" maxlength="80" name="email" size="20" type="text" placeholder="Email"/><br> -->
            <!--<input  id="00N200000098YTY" maxlength="255" name="00N200000098YTY" size="20" type="text" placeholder=" Имя родителя"/><br>
            <input  id="phone" maxlength="40" name="phone" class="phone" size="20" type="text" placeholder="Телефон"/><br>
            <input type="hidden" id="00N200000098lUX" maxlength="255" name="00N200000098lUX" size="20" value="<?=@$_GET['utm_campaign'];?>"/>
            <input type="hidden" id="00N200000098lUc" maxlength="255" name="00N200000098lUc" size="20" value="<?=@$_GET['utm_content'];?>"/>
            <input type="hidden" id="00N200000098lUh" maxlength="255" name="00N200000098lUh" size="20" value="<?=@$_GET['utm_medium'];?>"/>
            <input type="hidden" id="00N200000098lUm" maxlength="255" name="00N200000098lUm" size="20" value="<?=@$_GET['utm_source'];?>"/>
            <input type="hidden" id="00N200000098lUr" maxlength="255" name="00N200000098lUr" size="20" value="<?=@$_GET['utm_term'];?>"/>
            <input type="hidden" id="URL" maxlength="80" name="URL" size="20" value="http://<?=@$_SERVER["SERVER_NAME"];?>/" />
            <p class="desc" style="max-width:320px; margin:20px auto; ">
              <input type="checkbox" id="00N0J000009xwo5" name="00N0J000009xwo5" value="1"  style="float:left;width: 20px;"/>
              <label class="checkboxrights" for="00N0J000009xwo5" style="font-size:11px; text-align:justify">
                Я уведомлен и выражаю своё согласие на обработку моих персональных данных администрацией 
                Сайта в целях оказания мне услуг Компании.<br>
                Я уведомлен и выражаю своё согласие на получение смс-уведомлений от администрации
                 Сайта в целях оказания мне услуг Компании.
                </label>
            </p>
            <input type="submit" class="feedback__button" id="sendWebToLeadbutton" value="отправить" 
                  style="padding-right:0;padding-left:0; background:#C81688"
            >
          </form>
        </div>
      </div>
    </div>-->


       <div id="callme">
      <h2>Запишитесь на бесплатную пробную тренировку</h2>
      <p class="subtitle">в удобное время</p>

          <form class="feedback" action="https://salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
			     <input type="hidden" name="lead_source" value="таргет">
            <input type="hidden" name="oid" value="00D20000000onaw">
            <input type="hidden" name="retURL" value="http://free.footyball.ru/index.php#thanks">
            <input  id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="Имя ребенка"/><br>
            <!-- <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" placeholder="Фамилия ребенка"/><br>
            <input  id="email" maxlength="80" name="email" size="20" type="text" placeholder="Email"/><br> -->
            <input  id="00N200000098YTY" maxlength="255" name="00N200000098YTY" size="20" type="text" placeholder=" Имя родителя"/><br>
            <input  id="00N200000098YTQ" maxlength="10" name="00N200000098YTQ" size="20" type="text" placeholder="Возраст"/><br>
            <input  id="phone" maxlength="40" name="phone" class="phone" size="20" type="text" placeholder="Телефон"/><br>
            <input type="hidden" id="00N200000098lUX" maxlength="255" name="00N200000098lUX" size="20" value="<?=@$_GET['utm_campaign'];?>"/>
            <input type="hidden" id="00N200000098lUc" maxlength="255" name="00N200000098lUc" size="20" value="<?=@$_GET['utm_content'];?>"/>
            <input type="hidden" id="00N200000098lUh" maxlength="255" name="00N200000098lUh" size="20" value="<?=@$_GET['utm_medium'];?>"/>
            <input type="hidden" id="00N200000098lUm" maxlength="255" name="00N200000098lUm" size="20" value="<?=@$_GET['utm_source'];?>"/>
            <input type="hidden" id="00N200000098lUr" maxlength="255" name="00N200000098lUr" size="20" value="<?=@$_GET['utm_term'];?>"/>
            <input type="hidden" id="URL" maxlength="80" name="URL" size="20" value="http://<?=@$_SERVER["SERVER_NAME"];?>/" />
            <p class="desc" style="max-width:320px; margin:20px auto; ">
              <input type="checkbox" id="00N0J000009xwo5" name="00N0J000009xwo5" value="1"  style="float:left;width: 20px;"/>
              <label class="checkboxrights" for="00N0J000009xwo5" style="font-size:11px; text-align:justify">
                Я уведомлен и выражаю своё согласие на обработку моих персональных данных администрацией 
                Сайта в целях оказания мне услуг Компании.<br>
                Я уведомлен и выражаю своё согласие на получение смс-уведомлений от администрации
                 Сайта в целях оказания мне услуг Компании.
                </label>
            </p>
            <input type="submit" class="feedback__button" id="sendWebToLeadbutton" value="отправить" 
                  style="padding-right:0;padding-left:0; background:#C81688"
            >
          </form>
          
    </div>

    <!--<div id="cons">
      <h2>Получите консультацию тренера</h2>
      <p class="subtitle">по вопросам развития ребенка</p>

          <form class="feedback" action="https://salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
			       <input type="hidden" name="lead_source" value="таргет">
            <input type="hidden" name="oid" value="00D20000000onaw">
            <input type="hidden" name="retURL" value="http://free.footyball.ru/index.php#thanks">
            <input  id="first_name" maxlength="40" name="first_name" size="20" type="text" placeholder="Имя ребенка"/><br>
            <!-- <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" placeholder="Фамилия ребенка"/><br>
            <input  id="email" maxlength="80" name="email" size="20" type="text" placeholder="Email"/><br> -->
            <!--<input  id="00N200000098YTY" maxlength="255" name="00N200000098YTY" size="20" type="text" placeholder=" Имя родителя"/><br>
            <input  id="00N200000098YTQ" maxlength="10" name="00N200000098YTQ" size="20" type="text" placeholder="Возраст"/><br>
            <input  id="phone" maxlength="40" name="phone" class="phone" size="20" type="text" placeholder="Телефон"/><br>
            <input type="hidden" id="00N200000098lUX" maxlength="255" name="00N200000098lUX" size="20" value="<?=@$_GET['utm_campaign'];?>"/>
            <input type="hidden" id="00N200000098lUc" maxlength="255" name="00N200000098lUc" size="20" value="<?=@$_GET['utm_content'];?>"/>
            <input type="hidden" id="00N200000098lUh" maxlength="255" name="00N200000098lUh" size="20" value="<?=@$_GET['utm_medium'];?>"/>
            <input type="hidden" id="00N200000098lUm" maxlength="255" name="00N200000098lUm" size="20" value="<?=@$_GET['utm_source'];?>"/>
            <input type="hidden" id="00N200000098lUr" maxlength="255" name="00N200000098lUr" size="20" value="<?=@$_GET['utm_term'];?>"/>
            <input type="hidden" id="URL" maxlength="80" name="URL" size="20" value="http://<?=@$_SERVER["SERVER_NAME"];?>/" />
            <p class="desc" style="max-width:320px; margin:20px auto; ">
              <input type="checkbox" id="00N0J000009xwo5" name="00N0J000009xwo5" value="1"  style="float:left;width: 20px;"/>
              <label class="checkboxrights" for="00N0J000009xwo5" style="font-size:11px; text-align:justify">
                Я уведомлен и выражаю своё согласие на обработку моих персональных данных администрацией 
                Сайта в целях оказания мне услуг Компании.<br>
                Я уведомлен и выражаю своё согласие на получение смс-уведомлений от администрации
                 Сайта в целях оказания мне услуг Компании.
                </label>
            </p>
            <input type="submit" class="feedback__button" id="sendWebToLeadbutton" value="отправить" 
                  style="padding-right:0;padding-left:0; background:#C81688"
            >
            
          </form>

      <p class="desc">Мы гарантируем конфиденциальность данных</p>
    </div>-->


    <div id="thanks">
      <h2>Ваше сообщение успешно отправлено.<br>Наш менеджер свяжется с вами в ближайшее время.</h2>
    </div>

    <div id="tube">
      <div class="video">
        <iframe src="http://" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

    <div id="polit">
      <h2>Политика конфиденциальности</h2>
      <div class="text">
				<p>Соблюдение Вашей конфиденциальности важно для нас. По этой причине, мы разработали Политику Конфиденциальности, которая описывает, как мы используем и храним Вашу информацию. Пожалуйста, ознакомьтесь с нашими правилами соблюдения конфиденциальности и сообщите нам, если у вас возникнут какие-либо вопросы.</p>
				<h6>Сбор и использование персональной информации</h6>
				<p>Под персональной информацией понимаются данные, которые могут быть использованы для идентификации определенного лица либо связи с ним.</p>
				<p>От вас может быть запрошено предоставление вашей персональной информации в любой момент, когда вы связываетесь с нами.</p>
				<p>Ниже приведены некоторые примеры типов персональной информации, которую мы можем собирать, и как мы можем использовать такую информацию.</p>
				<p>Какую персональную информацию мы собираем:</p>
				<ul>
					<li>Когда вы оставляете заявку на сайте, мы можем собирать различную информацию, включая ваши имя, номер телефона, адрес электронной почты и т.д.</li>
				</ul>
				<p>Как мы используем вашу персональную информацию:</p>
				<ul>
				    <li>Собираемая нами персональная информация позволяет нам связываться с вами и сообщать об уникальных предложениях, акциях и других мероприятиях и ближайших событиях.</li>
				    <li>Время от времени, мы можем использовать вашу персональную информацию для отправки важных уведомлений и сообщений.</li>
				    <li>Мы также можем использовать персональную информацию для внутренних целей, таких как проведения аудита, анализа данных и различных исследований в целях улучшения услуг предоставляемых нами и предоставления Вам рекомендаций относительно наших услуг.</li>
				    <li>Если вы принимаете участие в розыгрыше призов, конкурсе или сходном стимулирующем мероприятии, мы можем использовать предоставляемую вами информацию для управления такими программами.</li>
				</ul>
				<h6>Раскрытие информации третьим лицам</h6>
				<p>Мы не раскрываем полученную от Вас информацию третьим лицам.</p>
				<p>Исключения:</p>
				<ul>
				    <li>В случае если необходимо — в соответствии с законом, судебным порядком, в судебном разбирательстве, и/или на основании публичных запросов или запросов от государственных органов на территории РФ — раскрыть вашу персональную информацию. Мы также можем раскрывать информацию о вас если мы определим, что такое раскрытие необходимо или уместно в целях безопасности, поддержания правопорядка, или иных общественно важных случаях.</li>
				    <li>В случае реорганизации, слияния или продажи мы можем передать собираемую нами персональную информацию соответствующему третьему лицу – правопреемнику.</li>
				</ul>
				<h6>Защита персональной информации</h6>
				<p>Мы предпринимаем меры предосторожности — включая административные, технические и физические — для защиты вашей персональной информации от утраты, кражи, и недобросовестного использования, а также от несанкционированного доступа, раскрытия, изменения и уничтожения.</p>
				<h6>Соблюдение вашей конфиденциальности на уровне компании</h6>
				<p>Для того чтобы убедиться, что ваша персональная информация находится в безопасности, мы доводим нормы соблюдения конфиденциальности и безопасности до наших сотрудников, и строго следим за исполнением мер соблюдения конфиденциальности.</p>
      </div>
    </div>

  </div>
  <!-- //Popups -->

  <script src="build/js/main.min.js"></script>

	
  </script>
  


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47020173 = new Ya.Metrika({
                    id:47020173,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47020173" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>
