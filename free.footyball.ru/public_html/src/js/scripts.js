function addScript(src) {
	var script = document.createElement('script');
	script.src = src;
	script.async = false;
	document.head.appendChild(script);
}
addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");



$(document).ready(function() {

	$('.fancy').fancybox({
		padding: 0,
		margin: 0
	});

	$('.phone').inputmask({
		mask: '+7(999)9999999',
		showMaskOnHover: false,
		showMaskOnFocus: true,
		clearIncomplete: true
	});

	$('.scrollto').click(function (e) {
		var anchor = $(this).attr('href');
		$('html, body').stop().animate({
			scrollTop: $(anchor).offset().top - $('nav').outerHeight()
		}, 1000);
		e.preventDefault();
	});

	$('.menu__button').click(function () {
		$(this).toggleClass('open');
	});

	$('.menu__list a').click(function () {
		$('.menu__button').removeClass('open');
	});

	$('.b7 .slider').slick({
		adaptiveHeight: true
	});

	$('.b7 .photos a').click(function () {
		$('.b7 .slider').slick('slickGoTo', $(this).data('pos'));
	});


	$('.b8 .slider').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1050,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});

});


$('.tube').click(function() {
	$('#tube iframe').attr('src','https://www.youtube.com/embed/'+$(this).data('id'));
	$.fancybox.open({
		wrapCSS: 'tubevideo',
		href:'#tube'
	});
});


window.onload = function() {

  if (window.location.href.indexOf('thanks') != -1) {
    $.fancybox.open('#thanks');
  }

  // $('.b9 .slider').on('init', function(event, slick, currentSlide, nextSlide){
  //   initMap($('.b9 .slick-current .adr').html());
  // });
  //
  // $('.b9 .slider').slick({
  //   slidesToShow: 3,
  //   vertical: true,
  //   verticalSwiping: true
  // });
  //
  // $('.b9 .slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
  //   initMap($('.b9 .slick-current .adr').html());
  // });
  //
  // $('.b9').on('click','.slick-slide',function() {
  //   if (!$(this).hasClass('slick-current')) {
  //     $('.b9 .slider').slick('slickGoTo',$(this).attr('data-slick-index'));
  //   }
  // });

  $('.b9').on('click','.item',function() {
    $('.b9 .item').removeClass('slick-current');
    $(this).addClass('slick-current');
    initMap($(this).find('.adr').html());
  });

  $('.b9 .item:first-of-type').click();

};


// валидация формы
$('.feedback').on('submit',function(e){

	if($(this).find('input#first_name').val().length<3||$(this).find('input#first_name').val().length>100){
		$(this).find('input#first_name').css({"background":"red"})
		return false
	}else{
		$(this).find('input#first_name').css({"background":"#eaeaea"})
	}

	if($(this).find('input#00N200000098YTY').val().length<3||$(this).find('input#00N200000098YTY').val().length>100){
		$(this).find('input#00N200000098YTY').css({"background":"red"})
		return false
	}else{
		$(this).find('input#00N200000098YTY').css({"background":"#eaeaea"})
	}

	var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
	if (!filter.test($(this).find('input#phone').val())) {
		$(this).find('input#phone').css({"background":"red"})
		return false
	}else{
		$(this).find('input#phone').css({"background":"#eaeaea"})
	}

	var termstatus = $(this).find('input#00N0J000009xwo5').is(':checked');
	if(termstatus==false){
		$(this).find('label.checkboxrights').css({"color":"red"})
		return false;
	}else{
		$(this).find('label.checkboxrights').css({"color":"#222"})
	}
})


// Корректировка цвета поля ввода при фокусе
$('input').focus(function() {
    $(this).css('border-color', '');
});

function initMap(adress) {

  $("#map").html('');

  var myGeocoder = ymaps.geocode(adress);
  myGeocoder.then(
    function(res) {
      var pos = res.geoObjects.get(0).geometry.getCoordinates();
      var myMap = new ymaps.Map('map', {
        center: pos,
        zoom: 17
      });
      var bpos = {
        lat: pos[0],
        lng: pos[1]
      };
      var ppos = {
        lat: pos[0],
        lng: pos[1] -= 0.003
      };

      var placemark = new ymaps.Placemark([bpos.lat,bpos.lng], {
        balloonContentHeader: '<p class="adr">' + adress + '</p>'
      },{
        iconLayout: 'default#image',
       iconImageHref: 'build/img/marker.png',
       iconImageSize: [28, 33]
      });
      setPlacemark();


      myMap.geoObjects.add(placemark);

      myMap.controls
        .add('zoomControl')
        .add('typeSelector')
        .add('mapTools');

      myMap.events.add('click', function(e) {
        myMap.balloon.close();
      });

      window.onresize = function() {
        setPlacemark();
      };

      function setPlacemark() {
          if (window.innerWidth < 740) {
            myMap.setCenter([bpos.lat,bpos.lng]);
          } else {
            myMap.setCenter([ppos.lat,ppos.lng]);
          }
      }

    }
  );

}

// -------------------------------------- Dynamic images and backgrounds
showVisible();

window.onscroll = function() {
  showVisible();
};

function showVisible() {
  var imgs = Array.prototype.slice.call(document.querySelectorAll('.dynamic'));
  if (imgs.length) {
    imgs.forEach(function(img) {
      if (isVisible(img)) {
        if (img.classList.contains('addback')) {
          img.classList.remove('addback');
          img.classList.add('backimg');
        } else {
          img.src = img.getAttribute('data-src');
          img.removeAttribute('data-src');
        }
        img.classList.remove('dynamic');
      }
    });
  }
}

function isVisible(elem) {
  var coords = elem.getBoundingClientRect();
  var windowHeight = document.documentElement.clientHeight;

  var topVisible = coords.top >= -windowHeight/2 && coords.top < windowHeight*1.5;
  var bottomVisible = coords.bottom < windowHeight*1.5 && coords.bottom > -windowHeight/2;

  return topVisible || bottomVisible;
}
// ----------------------------------- Dynamic images and backgrounds End
